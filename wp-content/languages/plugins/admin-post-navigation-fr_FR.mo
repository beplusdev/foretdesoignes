��          t      �                   a   /     �     �     �     �     �     �  8   �  �   5     5     C  �   V     �     �                =     J  8   b                             	                        
       %s Navigation &larr; Previous Adds links to navigate to the next and previous posts when editing a post in the WordPress admin. Admin Post Navigation Next %1$s: %2$s Next &rarr; Previous %1$s: %2$s Scott Reilly http://coffee2code.com/ http://coffee2code.com/wp-plugins/admin-post-navigation/ PO-Revision-Date: 2015-10-14 15:18:47+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Stable (latest release)
 %s Navigation &larr; Précédent Ajoute des liens pour naviguer vers l’article suivant ou précédent lorsqu'un article est modifié dans l’administration de WordPress. Admin Post Navigation Suivant %1$s&nbsp;: %2$s Suivant &rarr; Précédent %1$s&nbsp;: %2$s Scott Reilly http://coffee2code.com/ http://coffee2code.com/wp-plugins/admin-post-navigation/ 