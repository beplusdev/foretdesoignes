<?php
/*
Plugin Name: AO Social Share 
Author URI: http://www.alleyoop.be
Description: Define function to insert social share buttons in posts/page
Author: Simon Arickx & Bruno Patris
Version: 1.0
*/

if(!defined('WP_CONTENT_URL')) {
	$ao_social_share_url = get_option('siteurl') . '/wp-content/plugins/' . plugin_basename(dirname(__FILE__)).'/';
}else{
	$ao_social_share_url = WP_CONTENT_URL . '/plugins/' . plugin_basename(dirname(__FILE__)) . '/';
}

define('ao_social_share_VERSION', '1.0');
define('ao_social_share_AUTHOR', 'Simon Arickx & Bruno Patris');
define('ao_social_share_url', $ao_social_share_url);

// set text domain
$domain = 'ao-social-share';
$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $domain . '-' . get_locale() . '.mo';
load_textdomain( $domain, $mofile );

// This function does the actual work
function ao_insert_social_share($url)
{
	echo '<div class="social-share">';
	/* echo '<a class="btn btn-social-icon btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u=' . <?php $url; ?> . '" target="_blank"><i class="fa fa-facebook"></i></a>'; */
	echo '<a class="btn btn-social-icon btn-facebook" href="http://www.facebook.com/sharer.php?u='. $url . '" target="_blank" title="'. __('Share on Facebook', 'ao-social-share') .'" alt="'. __('Share on Facebook', 'ao-social-share') .'"><i class="fa fa-facebook"></i></a>';
	echo '<a class="btn btn-social-icon btn-twitter" href="https://twitter.com/share?url='. $url . '" target="_blank" title="'. __('Share on Twitter', 'ao-social-share') .'" alt="'. __('Share on Twitter', 'ao-social-share') .'"><i class="fa fa-twitter"></i></a>';
	echo '<a class="btn btn-social-icon btn-google-plus" href="https://plus.google.com/share?url='. $url . '" target="_blank" title="'. __('Share on Google+', 'ao-social-share') .'" alt="'. __('Share on Google+', 'ao-social-share') .'"><i class="fa fa-google-plus"></i></a>';
	echo '</div>';
}

?>