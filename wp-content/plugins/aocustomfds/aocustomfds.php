<?php
/** aocustomfds.php
 *
 * Plugin Name: AO Custom Foret de SOignes
 * Plugin URI: 	http://www.foretdesoignessport.be
 * Description: Plugin to customize Foret de Soignes website with specific post types, menus, etc...
 * Version: 	1.0.0
 * Author: 		Alley Oop
 * Author URI: 	http://www.alleyoop.be
 * Text Domain:	aocustomfds
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 *
 */


defined('ABSPATH') or die("No script kiddies please!"); 

add_action( 'plugins_loaded', 'aocustomfds_load_textdomain' );
/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
function aocustomfds_load_textdomain() {
  load_plugin_textdomain( 'aocustomfds', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' ); 
}


/**
 * Generate specific post types and taxonomies.
 *
 * @link http://codex.wordpress.org/Post_Types
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @uses register_post_type()
 * @uses wp_insert_term()
 *
 * @since AO Custom Foret de SOignes 1.0.0
 *
 * @return void
 */ 
 
function aocustomfds_post_types_and_taxonomies() {

	## Post type : Slides
	
	$labels = array(
		'name' 					=> _x('Slides', 'plural','aocustomfds'),
		'singular_name' 		=> _x('Slide', 'singular','aocustomfds'),
		'add_new' 				=> __('Add', 'aocustomfds'),
		'add_new_item' 			=> __('Add a slide','aocustomfds'),
		'edit_item' 			=> __('Modify a slide','aocustomfds'),
		'new_item' 				=> __('New slide','aocustomfds'),
		'view_item' 			=> __('View slide','aocustomfds'),
		'search_items' 			=> __('Find a slide','aocustomfds'),
		'not_found' 			=> __('No slide found','aocustomfds'),
		'not_found_in_trash' 	=> __('No slide found in the trash','aocustomfds'),
		'parent_item_colon' 	=> ''
	); 

	register_post_type( 'slide' ,  array(
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'query_var' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'menu_position' 		=> null,
		'supports' 				=> array('title','editor','thumbnail'),
		'has_archive' 			=> true,
		'rewrite'				=> array('slug' => 'slides'),
	) );

	## Post type : News
	
	$labels = array(
		'name' 					=> _x('News', 'plural','aocustomfds'),
		'singular_name' 		=> _x('News', 'singular','aocustomfds'),
		'add_new' 				=> __('Add', 'aocustomfds'),
		'add_new_item' 			=> __('Add a news','aocustomfds'),
		'edit_item' 			=> __('Modify a news','aocustomfds'),
		'new_item' 				=> __('New news','aocustomfds'),
		'view_item' 			=> __('View news','aocustomfds'),
		'search_items' 			=> __('Find a news','aocustomfds'),
		'not_found' 			=> __('No news found','aocustomfds'),
		'not_found_in_trash' 	=> __('No news found in the bin','aocustomfds'),
		'parent_item_colon' 	=> ''
	); 

	register_post_type( 'news' ,  array(
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'query_var' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'menu_position' 		=> null,
		'supports' 				=> array('title','editor','thumbnail'),
		'has_archive' 			=> true,
		'rewrite'				=> array('slug' => 'news'),
	) );

	// Insert taxonomy "newscat" for "news"
	$newscat_id = register_taxonomy( 'newscat', array('news'), array(
		'hierarchical' 		=> true, 
		'label' 			=> __('News categories','aocustomfds'), 
		'singular_label' 	=> __('News category','aocustomfds'),
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'_builtin' 			=> false,
		'paged'				=> true,
		'rewrite' 			=> true,
	) );
	
	// Insert term "News à la une"	
	if ($term = get_term_by('slug', 'news', 'newscat')) {
		$news = $term;
	}
	else {	
		$news = wp_insert_term( __('Flash news', 'aocustomfds'), 'newscat', $args = array(
			'description'	=> __('Flash news', 'aocustomfds'),
    		'slug' 			=> 'flashnews',
	    ) );
	}
	
	## Post type : Sports
	
	$labels = array(
		'name' 					=> _x('Sports', 'plural','aocustomfds'),
		'singular_name' 		=> _x('Sport', 'singular','aocustomfds'),
		'add_new' 				=> __('Add', 'aocustomfds'),
		'add_new_item' 			=> __('Add a sport','aocustomfds'),
		'edit_item' 			=> __('Modify a sport','aocustomfds'),
		'new_item' 				=> __('New sport','aocustomfds'),
		'view_item' 			=> __('View sport','aocustomfds'),
		'search_items' 			=> __('Find a sport','aocustomfds'),
		'not_found' 			=> __('No sport found','aocustomfds'),
		'not_found_in_trash' 	=> __('No sport found in the trash','aocustomfds'),
		'parent_item_colon' 	=> ''
	); 

	register_post_type( 'sport' ,  array(
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'query_var' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'menu_position' 		=> null,
		'supports' 				=> array('title','editor','thumbnail'),
		'has_archive' 			=> true,
		'rewrite'				=> array('slug' => 'sport'),
	) );

	$newscat_id = register_taxonomy( 'sportcat', array('sport'), array(
		'hierarchical' 		=> true, 
		'label' 			=> __('Sport categories','aocustomfds'), 
		'singular_label' 	=> __('Sport category','aocustomfds'),
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'_builtin' 			=> false,
		'paged'				=> true,
		'rewrite' 			=> true,
	) );
	
	## Post type : Events
	
	$labels = array(
		'name' 					=> _x('Activities', 'plural','aocustomfds'),
		'singular_name' 		=> _x('Activity', 'singular','aocustomfds'),
		'add_new' 				=> __('Add', 'aocustomfds'),
		'add_new_item' 			=> __('Add an activity','aocustomfds'),
		'edit_item' 			=> __('Modify an activity','aocustomfds'),
		'new_item' 				=> __('New activity','aocustomfds'),
		'view_item' 			=> __('View activity','aocustomfds'),
		'search_items' 			=> __('Find an activity','aocustomfds'),
		'not_found' 			=> __('No activity found','aocustomfds'),
		'not_found_in_trash' 	=> __('No activity found in the trash','aocustomfds'),
		'parent_item_colon' 	=> ''
	); 

	register_post_type( 'activity' ,  array(
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'query_var' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'menu_position' 		=> null,
		'supports' 				=> array('title','editor','thumbnail'),
		'has_archive' 			=> true,
		'rewrite'				=> array('slug' => 'activities'),
	) );

	$newscat_id = register_taxonomy( 'activitycat', array('activity'), array(
		'hierarchical' 		=> true, 
		'label' 			=> __('Activity categories','aocustomfds'), 
		'singular_label' 	=> __('Activity category','aocustomfds'),
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'_builtin' 			=> false,
		'paged'				=> true,
		'rewrite' 			=> true,
	) );
	
	## Post type : Courts
	
	$labels = array(
		'name' 					=> _x('Infrastructures', 'plural','aocustomfds'),
		'singular_name' 		=> _x('Infrastructure', 'singular','aocustomfds'),
		'add_new' 				=> __('Add', 'aocustomfds'),
		'add_new_item' 			=> __('Add an infrastructure','aocustomfds'),
		'edit_item' 			=> __('Modify an infrastructure','aocustomfds'),
		'new_item' 				=> __('New infrastructure','aocustomfds'),
		'view_item' 			=> __('View infrastructure','aocustomfds'),
		'search_items' 			=> __('Find an infrastructure','aocustomfds'),
		'not_found' 			=> __('No infrastructure found','aocustomfds'),
		'not_found_in_trash' 	=> __('No infrastructure found in the trash','aocustomfds'),
		'parent_item_colon' 	=> ''
	); 

	register_post_type( 'infrastructure' ,  array(
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'query_var' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'menu_position' 		=> null,
		'supports' 				=> array('title','editor','thumbnail'),
		'has_archive' 			=> true,
		'rewrite'				=> array('slug' => 'infrastructure'),
	) );

	$newscat_id = register_taxonomy( 'infrastructurecat', array('court'), array(
		'hierarchical' 		=> true, 
		'label' 			=> __('Infrastructure categories','aocustomfds'), 
		'singular_label' 	=> __('Infrastructure category','aocustomfds'),
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'_builtin' 			=> false,
		'paged'				=> true,
		'rewrite' 			=> true,
	) );

}
add_action('init', 'aocustomfds_post_types_and_taxonomies');



/**
 * Custom admin menu 
 *
 * @link http://code.tutsplus.com/articles/customizing-the-wordpress-admin-custom-admin-menus--wp-33200
 *
 * @uses wp_get_current_user()
 # @uses WP_Users::has_cap()
 * @uses remove_menu_page()
 * @uses remove_submenu_page()
 * @uses get_page_by_path()
 * @uses add_menu_page()
 * @uses add_submenu_page()
 *
 * @since AO Custom Foret de SOignes 1.0.0
 *
 * @return void
 */
function aocustomfds_custom_menu_items() {

	// Remove not authorized entries
    $user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_network' ) ) {
        remove_menu_page( 'edit.php' );
        //remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'edit.php?post_type=acf' );
        remove_menu_page( 'nav-menus.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'options-general.php' );
        //remove_menu_page( 'edit-comments.php' );
        remove_submenu_page( 'themes.php', 'nav-menus.php' );
		remove_submenu_page( 'index.php', 'my-sites.php' );
    }
}
add_action( 'admin_menu', 'aocustomfds_custom_menu_items' );


/**
 * Change admin menu labels 
 *
 * @link http://wordpress.stackexchange.com/questions/9211/changing-admin-menu-labels
 *
 * @since AO Custom Foret de SOignes 1.0.0
 *
 * @return void
 */
function aocustomfds_change_post_menu_label() {

	global $menu;
    global $submenu;
    
    //error_log('$menu:'.json_encode($menu));
    
    // Posts
    //$menu[5][0] = __('News', 'aocustomfds');
    //$submenu['edit.php'][5][0] = __('All news', 'aocustomfds');
    //$submenu['edit.php'][10][0] = __('Add news', 'aocustomfds');
    //$submenu['edit.php'][15][0] = 'Status'; // Change name for categories
    //$submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
    
    // Pages
    $menu[20][0] = __('Other pages', 'aocustomfds');
	echo '';
}
add_action( 'admin_menu', 'aocustomfds_change_post_menu_label' );

/**
 * Custom admin menu order
 *
 * @link http://code.tutsplus.com/articles/customizing-the-wordpress-admin-custom-admin-menus--wp-33200
 *
 * @uses get_page_by_path()
 *
 * @since AO Custom Foret de SOignes 1.0.0
 *
 * @return array -> Menu order
 */
function aocustomfds_change_menu_order( $menu_order ) {

    return array(
        'index.php',
        'edit.php?post_type=slide',
        'edit.php?post_type=news',
        'edit.php?post_type=sport',
        'edit.php?post_type=activity',
        'edit.php?post_type=infrastructure',
        'edit.php?post_type=page',
    );

}
add_filter( 'custom_menu_order', '__return_true' );
add_filter( 'menu_order', 'aocustomfds_change_menu_order' );



/**
 * Custom admin menu icons (with FontAwesome) by inserting some styles in 
 * the <head> section of the administration panel
 *
 * @link http://clarknikdelpowell.com/blog/3-ways-to-use-icon-fonts-in-your-wordpress-theme-admin/
 *
 * @uses get_page_by_path()
 *
 * @since AO Custom Foret de SOignes 1.0.0
 *
 * @return void
 */
function aocustomfds_custom_menu_icons() {

	$leclub 	= get_page_by_path('history');    
	$infos 		= get_page_by_path('join');    
	$sponsors 	= get_page_by_path('sponsors');    
	$contact 	= get_page_by_path('contact');    
	?>
	<style type="text/css">
		#adminmenu #menu-posts-slide .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f03e';
		}
		#adminmenu #menu-posts-news .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1ea';
		}
		#adminmenu #menu-posts-sport .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1e3'; /*'\f091';*/
		}
		#adminmenu #menu-posts-activity .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f073';
		}
		#adminmenu #menu-posts-infrastructure .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f19c';
		}
		#adminmenu li.wp-menu-separator {
    		height: 8px;
    	}
     </style>
	<?php
}
add_action('admin_head', 'aocustomfds_custom_menu_icons');


/**
 * Add FontAwesome stylesheet 
 *
 * @link http://fortawesome.github.io/Font-Awesome/
 *
 * @uses wp_enqueue_style()
 *
 * @since AO Custom Foret de SOignes 1.0.0
 *
 * @return void
 */
function fontawesome_dashboard() {
   wp_enqueue_style('fontawesome', 'http:////netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css', '', '4.2.0', 'all'); 
}
add_action('admin_init', 'fontawesome_dashboard');



/* End of file aocustomfds.php */
/* Location: ./wp-content/plugins/aocustomfds/aocustomfds.php */