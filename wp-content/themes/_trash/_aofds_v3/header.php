<?php
/** header.php
 *
 * Displays all of the <head> section and everything up till </header>
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php load_theme_textdomain( 'aofds', get_template_directory() . '/lang' ); ?>


<script> 
var $buoop = {vs:{i:8,f:15,o:12.1,s:5.1},c:2}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script> 


<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
        
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
        
        <?php aofds_social_meta_tags(); ?>
        
		<?php wp_head(); ?>

	</head>
	
	<body <?php body_class(); ?>>

        
        <div id="site-header" class="clearfix">

            <div class="inner">

                <div class="container">

                    <div class="row">

                        <nav class="navbar" role="navigation">
                            <div class="navbar-inner">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only"><?php _e('Toggle navigation', 'aolawoluwe'); ?></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                                        <img class="logo" src="<?php echo get_option( 'logo_url' ); ?>" alt="<?php echo get_bloginfo( 'name', 'display' ); ?>">
                                        <span class="title"><?php echo get_bloginfo( 'name', 'display' ); ?></span>
                                    </a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <?php
                                    wp_nav_menu( array(
                                        'container'			=>	'nav',
                                        'container_class'	=>	'subnav clearfix',
                                        'theme_location'	=>	'primary',
                                        'menu_class'		=>	'nav navbar-nav pull-left',
                                        'depth'				=>	3,
                                        'fallback_cb'		=>	false,
                                        'walker'			=>	new AOForetDeSoignes_Nav_Walker,
                                    ) ); 
                                    ?>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                
            </div>

        </div>

        <div id="site" class="container">
        
            <div class="row">
                            
                <div id="site-center" class="clearfix">

                    <div id="site-left-column">
                
                        <?php get_sidebar( 'left-content' ); ?>

                    </div>

                    <div id="site-main-column">

                        <div id="site-main-header">

                            <?php
                            $post = $wp_query->post;
                            if (is_home()) :
                                get_sidebar( 'header' );
                            elseif (is_singular() and ($image = get_field('images_header', $post->ID))) :
                                echo '<div class="header-image" style="background-image: url('.$image['sizes']['large'].');"></div>';
                            else :
                                echo '<div class="header-image" style="background-image: url('.get_option('header_image').');"></div>';
                            endif; 
                            ?>

                            <?php 
                            if (is_singular()) :
                                $sport_id = get_field('club_infos_sport', $post->ID);
                                if ($sport_id) $parent_id = get_field('sport_parent', $sport_id);
                                else $parent_id = get_field('sport_parent', $post->ID);
                                if ($parent_id or $sport_id) : ?>
                                    <div class="sport">
                                        <?php if ($parent_id) : ?>
                                            <a href="<?php echo get_post_permalink($parent_id); ?>">
                                                <i class="flaticon flaticon-<?php echo get_field('images_icon', $parent_id); ?>"></i><?php echo get_the_title($parent_id); ?>
                                            </a>
                                            <?php if ($sport_id) : ?>
                                            <i class="fa fa-caret-right"></i>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php if ($sport_id) : ?>
                                            <a href="<?php echo get_post_permalink($sport_id); ?>">
                                                <i class="flaticon flaticon-<?php echo get_field('images_icon', $sport_id); ?>"></i><?php echo get_the_title($sport_id); ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <?php 
                                endif; 
                            endif; 
                            ?> 

                            <div class="booking">
                                <div class="phone">
                                    <h3><?php echo (isset($post) and ($label = get_field('direct_contact_label', $post->ID))) ? $label : get_option('direct_contact_label'); ?></h3>
                                    <div>
                                        <i class="flaticon flaticon-phone21"></i><?php echo (isset($post) and ($number = get_field('direct_contact_number', $post->ID))) ? $number : get_option('direct_contact_number'); ?>
                                    </div>
                                </div>
                                <div class="address">
                                    <h3 style="margin-top: 10px;">Adresse</h3>
                                    <address><em>Chaussée de Wavre, 2057<br>1160 Bruxelles</em></address>
                                </div>
                            </div>

                            <div class="booking-mobile">
                                <div class="phone">
                                    <i class="flaticon flaticon-phone21"></i><?php echo (isset($post) and ($number = get_field('direct_contact_number', $post->ID))) ? $number : get_option('direct_contact_number'); ?>
                                </div>
                                <div class="address nowrap">
                                    <i class="fa fa-map-marker"></i><address><em>Chaussée de Wavre 2057, 1160 Bruxelles</em></address>
                                </div>
                            </div>

                        </div>

                        <div id="site-main">


<?
/* End of file header.php */
/* Location: ./wp-content/themes/aofds/header.php */