<?php
/** singular.php
 *
 * The template for displaying all singular posts.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <article>

                    <h1 class="entry-title">
                        <?php aofds_event_calendar(); ?>
                        <?php the_title(); ?>
                    </h1>   

                    <?php if ($post_type == 'news') : ?>
                        <div class="entry-meta">
                            <?php aofds_entry_meta_news(); ?>
                        </div>
                    <?php endif; ?>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>   

                    <?php if ( has_term( 'event', 'newscat', get_the_ID() ) ) : ?>
                    <div class="entry-event">
                        <?php aofds_display_layout_event(); ?>
                    </div>
                    <?php endif; ?>

                    <?php
                    $flexible_field = get_post_type() . '_flexible';

                    // check if the flexible content field has rows of data
                    if ( have_rows( $flexible_field ) ):

                        // loop through the rows of data
                        while ( have_rows( $flexible_field ) ) : the_row();

                            aofds_display_layout( get_row_layout() );

                        endwhile;

                    else :

                        // no layouts found

                    endif;

                ?>
                    
                </article>

                <?php

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <?php
        //$post = $wp_query->post;
        //echo $post->ID;
        ?>
        
    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file singular.php */
/* Location: ./wp-content/themes/aofds/singular.php */