<?php
/** content.php
 *
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
 
 load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if (is_single() or is_page()) : ?>
        <?php get_template_part('article', 'default-header'); ?>
    <?php endif; ?>

    <?php if (!is_single() && !is_home() && !is_page()) : ?>
    
        <!-- Entry title -->
        <h1 class="entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark">
            <?php aofds_event_calendar(); ?>
            <?php the_title(); ?>
            </a>
        </h1>        
        <!--/Entry title -->

        <!-- Entry meta -->
        <div class="entry-meta">
            <?php aofds_entry_meta(); ?>
        </div>
        <!--/Entry meta -->

    <?php endif; ?>
    
    <?php if ( is_search() ) : // Only display Excerpts for Search ?>
        
        <!-- Entry summary -->
        <div class="entry-summary">
            <?php the_excerpt(); ?>
        </div>
        <!--/Entry summary -->
	
    <?php else : // !is_search() ?>
	
        <!-- Entry content -->
        <div class="entry-content">
                 
            <?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'aofds' ) ); ?>
            			
        </div>
        <!--/Entry content -->
            
    <?php endif; // is_search() ?>

    <?php get_template_part('article', 'footer'); ?>
     
</article><!-- #post -->

<?php
/* End of file content.php */
/* Location: ./wp-content/themes/aothemefds/content.php */