<?php
/** footer.php
 *
 * Displays the footer
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

                        </div>
                        <!--/#site-main -->

                    </div>
                    <!--/#site-main-column -->

                </div>
                <!--/#site-center -->

                <div id="site-footer">

                    <div class="inner">
                        
                        <div class="footer-contacts">

                            <!-- Contacts -->
                            <?php 
                            $contacts = explode(',', get_option('footer_contacts'));
                            foreach ($contacts as $c) :
                                echo '<div class="contact">';
                                echo '<h2>'.get_field('contact_name', $c).'</h2>';
                                echo '<div>';

                                if ( have_rows( 'contact_flexible', $c ) ):

                                    echo '<div class="details '.(($image) ? 'with-image' : '').'">';

                                    while ( have_rows( 'contact_flexible', $c ) ) : the_row();

                                        switch ( get_row_layout() ) :

                                            case 'address' :
                                                aofds_display_address();
                                                break;

                                            case 'phone' :
                                                aofds_display_phone();
                                                break;

                                            case 'mobile' :
                                                aofds_display_mobile();
                                                break;

                                            case 'email' :
                                                aofds_display_email();
                                                break;

                                            case 'web' :
                                                aofds_display_web();
                                                break;

                                            case 'motto' :
                                                aofds_display_motto();
                                                break;

                                            case 'images' :
                                                aofds_display_images();
                                                break;

                                        endswitch;

                                    endwhile;

                                    echo '</div>';

                                endif;
/*
                                if ($address = get_field('contact_address', $c))
                                    echo $address.'<br/>';
                                while (has_sub_field('contact_infos', $c)) :
                                    if ($value = get_sub_field('contact_infos_value', $c)) :
                                        switch (get_sub_field('contact_infos_type', $c)) :
                                            case 'phone' :
                                                echo '<i class="fa fa-phone"></i>';
                                                echo $value;
                                                break;
                                            case 'fax' :
                                                echo '<i class="fa fa-fax"></i>';
                                                echo $value;
                                                break;
                                            case 'mobile' :
                                                echo '<i class="fa fa-mobile"></i>';
                                                echo $value;
                                                break;
                                            case 'email' :
                                                echo '<i class="fa fa-envelope"></i>';
                                                echo '<a href="mailto:'.antispambot($value).'">'.antispambot($value).'</a>';
                                                break;
                                            case 'webpage' :
                                                echo '<i class="fa fa-link"></i>';
                                                echo '<a href="'.$value.'">'.$value.'</a>';
                                                break;
                                        endswitch;
                                        echo '<br/>';
                                    endif;
                                endwhile;
                                */
                                echo '</div>';
                                echo '</div>';
                            endforeach;
                            ?>
                            <!--/Contacts -->

                        </div>

                    </div>

                    <?php get_sidebar( 'footer' ); ?>

                    <div id="map"></div>
                    <!--/#map -->

                    <div id="footer-menu">
                        <?php wp_nav_menu( array('theme_location' => 'footer-menu') ); ?>
                    </div>
                    <!--/#footer menu -->

                    <div id="copyright">
                        <p>Copyright &copy; <?php echo date('Y'); ?> Foret de Soignes  - <?php _e('All rights reserved', 'aofds'); ?>.</p>
                        <?php wp_nav_menu( array('container_class' => 'legal-menu', 'theme_location' => 'legal-menu') ); ?>
                        <div class="designed-by">
                            <a href="http://www.alleyoop.be" target="_blank">	
                                <img src="<?php echo get_template_directory_uri(); ?>/images/alleyoop.png" title="<?php _e('Designed by Alley Oop', 'aofds'); ?>" alt="<?php _e('Designed by Alley Oop', 'aofds'); ?>" />
                            </a>
                        </div>
                    </div>
                    <!--/#copyright -->	

                </div>
                <!--/#site-footer -->

            </div>
            <!--/.row -->
                         
        </div>
        <!--/#site -->

        <?php wp_footer(); ?>                

    </body>
    
</html>
           
<?php


/* End of file footer.php */
/* Location: ./wp-content/themes/aothemefds/footer.php */