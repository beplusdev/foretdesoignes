<?php
/** single.php
 *
 * The template for displaying all single posts.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <h1 class="entry-title">
                    <?php aofds_event_calendar(); ?>
                    <?php the_title(); ?>
                </h1>   
        
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>   
        
                <?php
                $flexible_field = get_post_type() . '_flexible';

                echo $flexible_field;
                // check if the flexible content field has rows of data
                if ( have_rows( $flexible_field ) ):

                    echo 'ici';

                    // loop through the rows of data
                    while ( have_rows( $flexible_field ) ) : the_row();

                    echo 'là';
                    echo get_row_layout();
                        switch ( get_row_layout() ) :

                            case 'section' :
                                echo '<h2>'.get_sub_field('section_title').'</h2>';
                                break;

                            case 'subsection' :
                                echo '<h3>'.get_sub_field('subsection_title').'</h3>';
                                break;

                            case 'content' :
                                echo '<div class="entry-content">'.get_sub_field('content_text').'</div>';
                                break;

                            case 'gallery' :
                                aofds_display_gallery();
                                break;

                            case 'prices' :
                                aofds_display_prices();
                                break;

                            case 'equipment' :
                                aofds_display_equipment();
                                break;

                            case 'features' :
                                aofds_display_features();
                                break;

                            case 'schedule' :
                                aofds_display_schedule();
                                break;

                            case 'contact' :
                                aofds_display_contact();
                                break;

                            case 'address' :
                                aofds_display_address();
                                break;

                            case 'phone' :
                                aofds_display_phone();
                                break;

                            case 'mobile' :
                                aofds_display_mobile();
                                break;

                            case 'email' :
                                aofds_display_email();
                                break;

                            case 'web' :
                                aofds_display_web();
                                break;

                            case 'facebook' :
                                aofds_display_facebook();
                                break;

                            case 'description' :
                                aofds_display_description();
                                break;

                        endswitch;

                    endwhile;

                else :

                    // no layouts found

                endif;

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
    </div>
    
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file single.php */
/* Location: ./wp-content/themes/aofds/sinle.php */