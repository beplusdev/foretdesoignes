<?php
/** content-none.php
 *
 * The template for displaying a "No posts found" message.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
 
 load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );
?>

<article>

    <!-- Entry header -->
    <header class="entry-header"> 
				
			<!-- Entry title -->
			<h1 class="entry-title">
				<?php _e( 'Nothing Found', 'aofds' ); ?>
			</h1>        
			<!--/Entry title -->
		
    </header>
    <!--/Entry header -->
    
    <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'aofds' ); ?></p>
	<?php get_search_form(); ?>


</article><!-- #post -->

<?php
/* End of file content-none.php */
/* Location: ./wp-content/themes/aothemefds/content-none.php */