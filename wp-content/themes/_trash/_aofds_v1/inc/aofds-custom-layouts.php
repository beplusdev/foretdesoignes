<?php
/** aofds-custom-layouts.php
 *
 * AO Custom layouts added with ACF
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );



function aofds_display_gallery() { 
    
    // check if the nested repeater field has rows of data
    if ( have_rows( 'gallery_repeater' ) ) :
    
        $images = array();
        $caption = array();

        // loop through the rows of data
        while ( have_rows('gallery_repeater') ) : the_row();

            $images[] = get_sub_field('gallery_repeater_image');
            $captions[] = get_sub_field('gallery_repeater_caption');

        endwhile;
    
        switch ( get_sub_field('gallery_options') ) :

            case 'gallery' :
                aofds_gallery($images, $captions);
                break;

            case 'diaporama' :
                aofds_diaporama($images, $captions);
                break;

        endswitch;
    
    endif;
}



function aofds_display_prices() { 
}



function aofds_display_equipment() { 
}



function aofds_display_features() { 
}



function aofds_display_schedule() { 
}



function aofds_display_contact() { 
}



function aofds_display_address() { 
}



function aofds_display_phone() { 
}



function aofds_display_mobile() { 
}



function aofds_display_email() { 
}



function aofds_display_web() { 
}



function aofds_display_facebook() { 
}



function aofds_display_description() { 
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function aoclubs_diaporama($medias, $descriptions) {

    if ( $n = count( $medias ) ) :
    
        ?>

        <div class="carousel slide" data-ride="carousel" data-interval="false">
            
            <!-- Indicators -->
            <?php if ($n > 1) : ?>
                <ol class="carousel-indicators">
                    <?php $first = true; ?>
                    
                    <?php foreach ($medias as $i => $media) : ?>
                        <li data-target="#carousel" data-slide-to="<?php echo $i; ?>" <?php if ($first) { echo 'class="active"'; $first = false; } ?>></li>
                    <?php endforeach; ?>
                    
                </ol>
            <?php endif; ?>
            
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php $first = true; ?>
                
                <?php foreach ($medias as $i => $media) : ?>
                    <div class="item <?php if ($first) { echo 'active'; $first = false; } ?>" id="carousel-item-<?php echo $i; ?>">
                        <a href="<?php echo $media['url']; ?>" rel="lightbox[gallery-]" title="<?php echo $descriptions[$i]; ?>">
                            <?php $image_width = (is_mobile()) ? 'thumbnail' : 'large'; ?>
                            <div class="image" style="background-image: url(<?php echo $media['sizes'][$image_width]; ?>);"></div>
                            <?php if ($descriptions[$i]) : ?>
                                <div class="carousel-caption">
                                    <?php echo $descriptions[$i]; ?>
                                </div>
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
                
            </div>
            
            <!-- Controls -->
            <?php if ($n > 1) : ?>
                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?php _e('Previous', 'aoclubs'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?php _e('Next', 'aoclubs'); ?></span>
                </a>
            <?php endif; ?>
        </div>

        <?php

    endif;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function aofds_gallery($medias, $descriptions) {

    ?>
    <div class="row gallery">
        <?php
        $n = count($medias);
        foreach ($medias as $i => $media) : 
            if ($n >= 4) :
                echo '<div class="col col-lg-3 col-md-3 col-sm-4 col-xs-4">';
                $image_width = 'thumbnail';
            elseif ($n >= 3) :
                echo '<div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">';
                $image_width = 'thumbnail';
            elseif ($n >= 2) :
                echo '<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">';
                $image_width = 'one-third';
            else :
                echo '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
                $image_width = (is_mobile()) ? 'one-third' : 'two-third';
            endif;
            ?>
                <div class="item">
                    <a href="<?php echo $media['sizes']['large']; ?>" rel="lightbox[gallery-medias]" title="<?php echo $descriptions[$i]; ?>">
                        <img src="<?php echo $media['sizes'][$image_width]; ?>" alt="<?php echo $descriptions[$i]; ?>">
                    </a>
                    <?php if ($descriptions[$i]) : ?>
                        <div class="description">
                            <?php echo $descriptions[$i]; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php
}


/* End of file aofds-custom-layouts.php */
/* Location: ./wp-content/themes/aofds/inc/aofds-custom-layouts.php */