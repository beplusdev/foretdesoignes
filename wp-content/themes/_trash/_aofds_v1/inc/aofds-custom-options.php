<?php
/** aofds-custom-options.php
 *
 * AO Custom options for the theme
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );

function aofds_custom_options_create_menu() {

	// Create new sublevel menu
	add_submenu_page( 'options-general.php', __('AO Options', 'aofds'), __('AO Options', 'aofds'), 'manage_options', 'aofds-custom-options', 'aofds_custom_options_page');

	// Call register options function
	add_action( 'admin_init', 'aofds_custom_options_register' );
}
add_action( 'admin_menu', 'aofds_custom_options_create_menu' );


function aofds_custom_options_register() {
	
	//register our settings
	register_setting( 'aofds-custom-options-group', 'logo_url' );
	register_setting( 'aofds-custom-options-group', 'small_logo_url' );
	register_setting( 'aofds-custom-options-group', 'social_share_logo_url' );
	register_setting( 'aofds-custom-options-group', 'main_bgcolor' ); //#B8D532
	register_setting( 'aofds-custom-options-group', 'second_bgcolor' ); //#58595B
	register_setting( 'aofds-custom-options-group', 'main_textcolor' );
	register_setting( 'aofds-custom-options-group', 'second_textcolor' );
	register_setting( 'aofds-custom-options-group', 'header_image' );
	register_setting( 'aofds-custom-options-group', 'news_image' );
	register_setting( 'aofds-custom-options-group', 'post_types' );
	register_setting( 'aofds-custom-options-group', 'contact_address' );
	register_setting( 'aofds-custom-options-group', 'contact_booking' );
	register_setting( 'aofds-custom-options-group', 'contact_commercial' );
	register_setting( 'aofds-custom-options-group', 'address_lat' );
	register_setting( 'aofds-custom-options-group', 'address_lng' );
	register_setting( 'aofds-custom-options-group', 'default_sport_image' );
	register_setting( 'aofds-custom-options-group', 'default_infrastructure_image' );
	register_setting( 'aofds-custom-options-group', 'mailchimp_form_action' );
	register_setting( 'aofds-custom-options-group', 'mailchimp_form_code' );
	register_setting( 'aofds-custom-options-group', 'free_css' );
	register_setting( 'aofds-custom-options-group', 'price1_title' );
	register_setting( 'aofds-custom-options-group', 'price2_title' );
}


function aofds_custom_options_page() {
?>
<div class="wrap">
<h2><?php _e('AO Options', 'aofds'); ?></h2>

<form method="post" action="options.php">
    <?php settings_fields( 'aofds-custom-options-group' ); ?>
    <?php do_settings_sections( 'aofds-custom-options-group' ); ?>
    <table class="form-table">

        <tr valign="top">
        <tr valign="top">
        <th scope="row"><?php _e('Logo (url)', 'aofds'); ?></th>
        <td><input type="text" name="logo_url" value="<?php echo esc_attr( get_option('logo_url') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Small Logo (url)', 'aofds'); ?></th>
        <td><input type="text" name="small_logo_url" value="<?php echo esc_attr( get_option('small_logo_url') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Social share default Logo (url)', 'aofds'); ?></th>
        <td><input type="text" name="social_share_logo_url" value="<?php echo esc_attr( get_option('social_share_logo_url') ); ?>" /></td>
        </tr>
		
        <tr valign="top">
        <th scope="row"><?php _e('Main BG Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" name="main_bgcolor" value="<?php echo esc_attr( get_option('main_bgcolor') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Second BG Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" name="second_bgcolor" value="<?php echo esc_attr( get_option('second_bgcolor') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Main Text Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" name="main_textcolor" value="<?php echo esc_attr( get_option('main_textcolor') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Second Text Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" name="second_textcolor" value="<?php echo esc_attr( get_option('second_textcolor') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default header image', 'aofds'); ?></th>
        <td><input type="text" name="header_image" value="<?php echo esc_attr( get_option('header_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default news image', 'aofds'); ?></th>
        <td><input type="text" name="news_image" value="<?php echo esc_attr( get_option('news_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Post types (coma separated)', 'aofds'); ?></th>
        <td><input type="text" name="post_types" value="<?php echo esc_attr( get_option('post_types') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Address contact id', 'aofds'); ?></th>
        <td><input type="text" name="contact_address" value="<?php echo esc_attr( get_option('contact_address') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Booking contact id', 'aofds'); ?></th>
        <td><input type="text" name="contact_booking" value="<?php echo esc_attr( get_option('contact_booking') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Commercial contact id', 'aofds'); ?></th>
        <td><input type="text" name="contact_commercial" value="<?php echo esc_attr( get_option('contact_commercial') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Latitude de l\'adresse', 'aofds'); ?></th>
        <td><input type="text" name="address_lat" value="<?php echo esc_attr( get_option('address_lat') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Longitude de l\'adresse', 'aofds'); ?></th>
        <td><input type="text" name="address_lng" value="<?php echo esc_attr( get_option('address_lng') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default sport image (squared)', 'aofds'); ?></th>
        <td><input type="text" name="default_sport_image" value="<?php echo esc_attr( get_option('default_sport_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default infrastructure image (squared)', 'aofds'); ?></th>
        <td><input type="text" name="default_infrastructure_image" value="<?php echo esc_attr( get_option('default_infrastructure_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Mailchimp form action', 'aofds'); ?></th>
        <td><input type="text" name="mailchimp_form_action" value="<?php echo esc_attr( get_option('mailchimp_form_action') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Mailchimp form code', 'aofds'); ?></th>
        <td><input type="text" name="mailchimp_form_code" value="<?php echo esc_attr( get_option('mailchimp_form_code') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Free CSS', 'aofds'); ?></th>
            <td><textarea type="text" name="free_css" rows="20" cols="60"><?php echo esc_attr( get_option('free_css') ); ?></textarea></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Title of price 1', 'aofds'); ?></th>
        <td><input type="text" name="price1_title" value="<?php echo esc_attr( get_option('price1_title') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Title of price 2', 'aofds'); ?></th>
        <td><input type="text" name="price2_title" value="<?php echo esc_attr( get_option('price2_title') ); ?>" /></td>
        </tr>

    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php 
}


function aofds_custom_options_css() {
	?>
	<!-- AO Theme Options CSS --> 
	<style type="text/css">
	.aologo {
		background: url("<?php echo get_option( 'logo_url' ); ?>") no-repeat;
	}
	#map .overlay{
		background-image: url("<?php echo get_option( 'small_logo_url' ); ?>");
	}
	.aomainbgcolor, #navbar .nav > li:hover, #nabar .dropdown-menu > li:hover > a {
		background: <?php echo get_option( 'main_bgcolor' ); ?> !important;
	}
	.aomaintextcolor, #navbar .nav > li:hover > a, #navbar .dropdown-menu > li:hover > a {
		color: <?php echo get_option( 'main_textcolor' ); ?> !important;
	}
	.aosecondbgcolor {
		background: <?php echo get_option( 'second_bgcolor' ); ?>;
	}
	.aosecondtextcolor {
		color: <?php echo get_option( 'second_textcolor' ); ?>;
	}
        
    a, a:hover {
        color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    #navbar .nav .dropdown-menu > li:hover > a {
    background: <?php echo get_option( 'main_bgcolor' ); ?> !important; 
    }
    .homepage .news .entry-meta .edit-link i.fa {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .not-homepage .entry-meta .edit-link i.fa {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    #content .event-pills > li.active > a, 
    #content .event-pills > li.active > a:hover, 
    #content .event-pills > li.active > a:focus {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .panel-heading {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?> !important;
    }
    #footer h3, #footer h4 {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .event-date {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') -45px -85px no-repeat; 
    }
    .sport .bookings .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .sport #sport_subscriptions .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .sport .club .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .infrastructure .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .sidebar-content .widget-ao-upcoming-events li a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget-ao-upcoming-events .event-date {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') -45px -85px no-repeat;
    }
    .sidebar-content .widget-ao-upcoming-events .btn-success {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > a:before {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > ul li a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > ul li a:before {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .panel-heading {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .wp-polls-ans .btn-success {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .wp-polls .pollbar {
    background: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .infrastructure .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .gallery img {
    /*border-color: <?php echo get_option( 'second_bgcolor' ); ?> !important;*/
    }
        
    @media (max-width: 992px) {

        .aomainbgcolor, #navbar .nav > li:hover, #nabar .dropdown-menu > li:hover > a,
        .aomaintextcolor, #navbar .nav > li:hover > a, #navbar .dropdown-menu > li:hover > a,
        #navbar .nav .dropdown-menu > li:hover > a {
        background-color: transparent !important;
        color: #333 !important;
        }
    }

    <?php echo get_option( 'free_css' ); ?>


	</style> 
	<!-- /AO Theme Options CSS -->
	<?php
}
add_action( 'wp_head' , 'aofds_custom_options_css' );



function aofds_custom_options_scripts() {
	?>
		<script type="text/javascript">
		var map;
		jQuery(document).ready(function(){"use strict";
		  map = new GMaps({
			scrollwheel: false,
			el: '#map',
			lat: <?php echo get_option( 'address_lat' ); ?>,
			lng: <?php echo get_option( 'address_lng' ); ?>,
			'zoom':13,
			//icon: "wp-content/themes/aofds/images/Logo2-LaWoluwe.png"
			});
		  map.drawOverlay({
			lat: map.getCenter().lat(),
			lng: map.getCenter().lng(),
			//icon: "wp-content/themes/aofds/images/Logo2-LaWoluwe.png",
			layer: 'overlayLayer',
			content: '<div class="overlay"></div>',
			verticalAlign: 'bottom',
			horizontalAlign: 'center'
		  });
		});
    </script>
    <?php
}
add_action( 'wp_head' , 'aofds_custom_options_scripts' );


/* End of file aofds-custom-options.php */
/* Location: ./wp-content/themes/aofds/inc/aofds-custom-options.php */