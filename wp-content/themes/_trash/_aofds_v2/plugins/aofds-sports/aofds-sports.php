<?php
/*
 * Plugin Name: AO FDS - Sports 
 * Author URI: http://www.alleyoop.be
 * Description: Sports widget for AO FDS.
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AOFDS_SPORTS_VERSION', '1.0');
define('AOFDS_SPORTS_AUTHOR', 'Alley Oop');
define('AOFDS_SPORTS_URL', get_template_directory_uri() . '/plugins/aofds-sports/');


## Include the required scripts
function aofds_sports_public_scripts(){
    wp_enqueue_script('jquery');
	wp_register_script('aofds-sports-js', AOFDS_SPORTS_URL . 'js/aofds-sports.js', array(), '1');
	wp_enqueue_script(array('jquery', 'aofds-sports-js'));
}
add_action('wp_enqueue_scripts', 'aofds_sports_public_scripts');


## Include the required styles
function aofds_sports_public_styles(){
	wp_register_style('aofds-sports-css', AOFDS_SPORTS_URL . 'css/aofds-sports.css', array(), '2');
	wp_enqueue_style('aofds-sports-css');
}
add_action('wp_enqueue_scripts', 'aofds_sports_public_styles');



class aofds_sports_widget extends WP_Widget{

	## Initialize
	
	function aofds_sports_widget(){
	
		// set text domain
		$dom = 'aofds-sports';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-aofds-sports',
			'description' => __('A display of sports', 'aofds-sports'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('aofds-sports', __('Sports', 'aofds-sports'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('aofds_sports', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 1;

        $order = isset( $instance['order'] ) ? $instance['order'] : 'rand';;
        //$sportcat = $instance['sportcat'];
        $options = json_decode($instance['options']);
		//$show_all_title = $instance['show_all_title'];

		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'sport',
                /*
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'sportcat',
						'field' 	=> 'id',
						'terms' 	=> $sportcat,
					),
				),
                */
				'orderby' 	=> $order, 
			)
		));
		//echo $r->request; 
        
		echo $before_widget; 
		if ( $title ) echo $before_title . $title . $after_title; 

		if ($r->have_posts()) :
        ?>
			<ul class="sports">
                
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li class="sport">
                    <div class="row">
                        <?php $image = get_field('sport_id_photo'); ?>
                        <div id="sport-id-photo-<?php the_ID(); ?>" class="col-lg-5 col-md-5 col-sm-5 col-xs-5 sport-id-photo">
                            <a href="<?php the_permalink(); ?>" class="image">
                                <?php if ($image) : ?>
                                    <?php /*<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />*/ ?>	
                                    <div class="id-photo" style="background-image:url(<?php echo $image['sizes']['medium']; ?>);"></div> 
                                <?php else : ?>
                                    <div class="id-photo default"></div> 
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 sport-details">
                            <a href="<?php the_permalink(); ?>" class="caption">
                                <div class="title"><?php the_title(); ?></div>
                            </a>
                            <div class="details">
                                <span class="number"><?php if ($num = get_field('sport_number')) echo '#'.$num; ?></span>
                                <span class="post"><?php the_field('sport_post'); ?></span>
                            </div>
                            <div class="details">
                                <span class="size"><?php the_field('sport_size'); ?></span>
                            </div>
                            <div class="details">
                                <span class="birthdate"><?php echo date_i18n('d/m/Y', strtotime(get_field('sport_birthdate'))); ?></span>
                            </div>
                            <div class="details">
                                <span class="nationality"><?php the_field('sport_nationality'); ?></span>
                            </div>
                            <div class="details">
                                <span class="team"><?php the_field('sport_team'); ?></span>
                            </div>
                        </div>
                    </div>
                    </li>
                <?php endwhile; ?>

                <?php /* if ($show_all_title) : ?>
                <div class="buttons">
                    <a class="btn btn-success" href="<?php echo get_post_type_archive_link( 'sport' ); ?>"><?php echo $show_all_title; ?></a>
                </div>
                <?php endif; */?>

            </ul>

        <?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no sport', 'aofds-sports').'</p>';
		endif;

		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('aofds_sports', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		//error_log('sarickx >>> old instance sports: ' . json_encode($old_instance));;
		error_log('sarickx >>> new instance sports: ' . json_encode($new_instance));;
		error_log('sarickx >>> title : ' . $new_instance['title1'] );
		
		
		$instance = $old_instance;
		$instance['title']          = stripslashes($new_instance['title']);
		$instance['number']         = isset( $new_instance['number'] ) ? (int) $new_instance['number'] : 1;
		$instance['order']          = isset( $new_instance['order'] ) ? $new_instance['order'] : 'rand';
        //$instance['sportcat']      = $new_instance['sportcat'];
		$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['aofds_sports']) )
			delete_option('aofds_sports');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('ao_upcoming_events', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 1;
		$order     = isset( $instance['order'] ) ? $instance['order'] : 'rand';
		//$sportcat = isset( $instance['sportcat'] ) ? $instance['sportcat'] : '';
		//$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
		$options = json_decode($instance['options']);
		
		?>

		<p class="aofds-sports-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'aofds-sports' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'sportcat' ); ?>"><?php _e( 'Roles:', 'aofds-sports' ); ?></label>
			<select multiple class="widefat sportcat-selector" id="<?php echo 'sportcat-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'sportcat' ); ?>[]" >
                <?php
                $terms = get_terms( 'sportcat', array('hide_empty' => 0) );
                foreach ($terms as $term) {
                    ?>
                    <option value="<?php echo $term->term_id; ?>" <?php if (in_array($term->term_id, $sportcat)) echo 'selected="selected"'; ?> ><?php echo $term->name; ?></option>      
                    <?php 
                } ?>
            </select>
        </p>
        */ ?>

		<p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'aofds-sports' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order', 'aofds-sports' ); ?></label>
			<select class="order-selector" id="<?php echo 'order-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" >
                <option value="rand" <?php if ($order == 'rand') echo 'selected="selected"'; ?>><?php _e('Random','aofds-sports'); ?></option>
                <option value="title" <?php if ($order == 'title') echo 'selected="selected"'; ?>><?php _e('Title','aofds-sports'); ?></option>
                <option value="date" <?php if ($order == 'date') echo 'selected="selected"'; ?>><?php _e('Date','aofds-sports'); ?></option>
            </select>
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'aofds-sports' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" />
        </p>
        */ ?>

		<?php
	}
}

function aofds_sports_init(){
	register_widget('aofds_sports_widget');
}
add_action('widgets_init', 'aofds_sports_init');


function aofds_sports_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'aofds_sports_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'aofds_sports_widget_scripts' );

function aofds_sports_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'aofds_sports_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'aofds_sports_widget_css' );

?>
