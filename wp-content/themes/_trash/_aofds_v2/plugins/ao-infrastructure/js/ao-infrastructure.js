jQuery(window).load(function() {

	jQuery( ".infrastructure-thumbnail" ).mouseenter(function() {
        var width = jQuery(window).width();
        if (width > 992) {
            var id = jQuery(this).attr('id').replace('infrastructure-thumbnail', 'infrastructure-main');
            jQuery( ".infrastructure-main" ).removeClass("show");
            jQuery( "#"+id ).addClass("show");
        }
    });

	jQuery( ".infrastructure-thumbnail" ).click(function() {
        var width = jQuery(window).width();
        if (width <= 992) {
            var id = jQuery(this).attr('id').replace('infrastructure-thumbnail', 'infrastructure-main');
            jQuery( ".infrastructure-main" ).removeClass("show");
            jQuery( "#"+id ).addClass("show");
        }
    });

})
