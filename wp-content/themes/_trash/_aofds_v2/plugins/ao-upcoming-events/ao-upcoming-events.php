<?php
/*
 * Plugin Name: AO - Upcoming events 
 * Author URI: http://www.alleyoop.be
 * Description: List of upcoming events.
 * Author: Alley Oop
 * Version: 1.0
 */

## Constants definition

define('AO_UPCOMING_EVENTS_VERSION', '1.0');
define('AO_UPCOMING_EVENTS_AUTHOR', 'Simon Arickx');
define('AO_UPCOMING_EVENTS_URL', get_template_directory_uri() . '/plugins/ao-upcoming-events/');

## Include the required styles

function ao_upcoming_events_css(){
	wp_register_style('ao-upcoming-events-css', AO_UPCOMING_EVENTS_URL . 'css/ao-upcoming-events.css', array(), '0.5');
	wp_enqueue_style('ao-upcoming-events-css');
}
add_action('wp_enqueue_scripts', 'ao_upcoming_events_css');



class ao_upcoming_events_widget extends WP_Widget {

	function ao_upcoming_events_widget(){
	
		// set text domain
		$domain = 'ao-upcoming-events';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $domain . '-' . get_locale() . '.mo';
		load_textdomain( $domain, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-ao-upcoming-events', 
			'description' => __( 'List of your upcoming events.', 'ao-upcoming-events') 
		);
		parent::WP_Widget('ao-upcoming-events', __('Upcoming events', 'ao-upcoming-events'), $widget_ops);
		//$this->alt_option_name = 'ao_upcoming_events';

		//add_action( 'save_post', array($this, 'flush_widget_cache') );
		//add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		//add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('ao_upcoming_events', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Upcoming events', 'ao-upcoming-events' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
 			
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : true;
		$show_all_title = ( ! empty( $instance['show_all_title'] ) ) ? $instance['show_all_title'] : __( 'Show complete agenda', 'ao-upcoming-events' );

/*
        $today = new DateTime('today');
		//echo $today->getTimestamp();
		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'event', 
				'meta_query'=> array(
					array (
						'key' 		=> 'event_infos_start',
    	    		    'value' 	=> $today->getTimestamp(),
						'compare' 	=> '>=',
						'type' => 'NUMERIC',
					),
				),
				'sortby' 	=> 'get_most_viewed', 
				'orderby' 	=> 'meta_value', 
				'meta_key'	=> 'event_infos_start',
				'order' 	=> 'ASC', 
			)
		));
*/
        $today = new DateTime('today');
		//echo $today->getTimestamp();
		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'news', 
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'newscat',
						'field' 	=> 'slug',
						'terms' 	=> array( 'event' ),
					),
				),
				'meta_query'=> array(
					array (
						'key' 		=> 'event_start',
    	    		    'value' 	=> $today->getTimestamp(),
						'compare' 	=> '>='
					),
				),
				'sortby' 	=> 'get_most_viewed', 
				'orderby' 	=> 'meta_value', 
				'meta_key'	=> 'event_start',
				'order' 	=> 'ASC', 
			)
		));

        
        
		// echo $r->request; 
		echo $before_widget; 
		if ( $title ) echo $before_title . $title . $after_title; 
		
		if ($r->have_posts()) :
?>
			<div class="ao-upcoming-events">
            <ul>
			<?php while ( $r->have_posts() ) : $r->the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>">
					<?php if ( $show_date and ( $start = get_field('event_start')) ) : ?>
						<span class="event-date">
							<span class="event-day"><?php echo date_i18n( 'j' , $start ); ?></span>
							<span class="event-month"><?php echo ucfirst(__(date_i18n( 'M' , $start ))); ?></span>
						</span>
						<span class="event-title">
							<?php get_the_title() ? the_title() : the_ID(); ?>
						</span>
					<?php else : ?>
						<span class="event-title">
							<?php get_the_title() ? the_title() : the_ID(); ?> 
						</span>
					<?php endif; ?>
					<p class="event-intro">
						<?php 
						$text = strip_tags(get_the_content());
						echo (strlen($text) > 100) ? substr($text,0,100).'...' : $text; 
						?>
					</p>
					</a>

				</li>
			<?php endwhile; ?>
			</ul>

            <div class="buttons">
                <a class="btn btn-success" href="<?php echo get_term_link( 'event', 'newscat' ); ?>"><?php echo $show_all_title; ?></a>
            </div>
            </div>

<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no upcoming events', 'ao-upcoming-events').'</p>';
		endif;

		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('ao_upcoming_events', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : true;
		$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['ao_upcoming_events']) )
			delete_option('ao_upcoming_events');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('ao_upcoming_events', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
		$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'ao-upcoming-events' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'ao-upcoming-events' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?', 'ao-upcoming-events' ); ?></label></p>

		<p><label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'ao-upcoming-events' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" /></p>
<?php
	}
}

function ao_upcoming_events_init(){
	register_widget('ao_upcoming_events_widget');
}
add_action('widgets_init', 'ao_upcoming_events_init');
