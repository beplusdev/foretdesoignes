<?php
/** header.php
 *
 * Displays all of the <head> section and everything up till </header>
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php load_theme_textdomain( 'aofds', get_template_directory() . '/lang' ); ?>


<script> 
var $buoop = {vs:{i:8,f:15,o:12.1,s:5.1},c:2}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script> 


<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
        
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
        
        <?php aofds_social_meta_tags(); ?>
        
		<?php wp_head(); ?>

	</head>
	
	<body <?php body_class(); ?>>

        
        <div id="site" class="container">
        
            <div class="row">
            
                <div id="site-left-column" class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                
                    <div id="logo" class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only"><?php _e('Toggle navigation', 'aofds'); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                            <img class="logo" src="<?php echo get_option( 'logo_url' ); ?>" alt="<?php echo get_bloginfo( 'name', 'display' ); ?>">
                            <span class="title"><?php echo get_bloginfo( 'name', 'display' ); ?></span>
                        </a>
                    </div>

                    <?php get_sidebar( 'left-content' ); ?>

                </div>
            
                <div id="site-main-column" class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                
                    <div id="site-header">
                        
                        <div id="navigation" class="navbar-collapse collapse">
                            <?php
                            wp_nav_menu( array(
                                'container'			=>	'nav',
                                'container_class'	=>	'subnav clearfix',
                                'theme_location'	=>	'primary',
                                'menu_class'		=>	'nav navbar-nav pull-right',
                                'depth'				=>	3,
                                'fallback_cb'		=>	false,
                                'walker'			=>	new AOForetDeSoignes_Nav_Walker,
                            ) ); 
                            ?>

                        </div>

                    <?php get_sidebar( 'header' ); ?>
                    
                    </div>

                    <div id="site-main">
                    

<?
/* End of file header.php */
/* Location: ./wp-content/themes/aofds/header.php */