<?php
/** archive-news.php
 *
 * The template for displaying news archive page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
*/

get_header(); ?>

<div class="container">

    <div class="row">

        <div class="col-md-8 col-sm-12 col-xs-12">

            <!-- Content -->
            <div id="content" class="site-content" role="main">

                    <?php 
                    $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
                    $title = $term->name;
                    ?>

                    <header class="archive-header">
                        <h1 class="archive-title"><?php echo _x('News', 'plural','aofds'); ?></h1>
                    </header><!-- .archive-header -->

                    <?php if ( have_posts() ) : ?>

                        <?php /* The loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                        
                            <?php get_template_part( 'content' ); ?>
                
                        <?php endwhile; ?>

                        <?php //twentythirteen_paging_nav(); ?>

                    <?php else : ?>
                        <?php get_template_part( 'content', 'empty' ); ?>
                    <?php endif; ?>

            </div>
            <!--/Content -->

            <?php get_sidebar( 'bottom-content' ); ?>

        </div>

        <div class="col-md-4 col-sm-12 col-xs-12">

            <?php get_sidebar( 'right-content' ); ?>				

        </div>

    </div><!--/.row -->

    <?php get_sidebar( 'bottom-page' ); ?>
    
</div><!--/.container -->
    
<?php get_footer(); ?>

<?
/* End of file archive-news.php */
/* Location: ./wp-content/themes/aofds/archive-news.php */