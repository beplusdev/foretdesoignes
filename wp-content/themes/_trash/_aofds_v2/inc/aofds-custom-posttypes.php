<?php
/** aofds-custom-posttypes.php
 *
 * AO Custom post types for the theme
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );

/**
 * Generate specific post types and taxonomies.
 *
 * @link http://codex.wordpress.org/Post_Types
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @uses register_post_type()
 * @uses wp_insert_term()
 *
 * @since 1.0.0
 *
 * @return void
 */  
function aofds_custom_posttypes_and_taxonomies() {

	$post_types = array_map('trim', explode(",", get_option( 'post_types' )));
	
	
	## Post type : News
	
	if (in_array('news', $post_types)) :

		$labels = array(
			'name' 					=> _x('News', 'plural','aofds'),
			'singular_name' 		=> _x('News', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a news','aofds'),
			'edit_item' 			=> __('Modify a news','aofds'),
			'new_item' 				=> __('New news','aofds'),
			'view_item' 			=> __('View news','aofds'),
			'search_items' 			=> __('Find a news','aofds'),
			'not_found' 			=> __('No news found','aofds'),
			'not_found_in_trash' 	=> __('No news found in the bin','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'news' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'news'),
		) );

		// Insert taxonomy "newscat" for "news"
		$newscat_id = register_taxonomy( 'newscat', array('news'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('News categories','aofds'), 
			'singular_label' 	=> __('News category','aofds'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
	
        // Insert term "homepage"
        if ($term = get_term_by('slug', 'homepage', 'newscat')) {
            $homepage = $term;
        }
        else {	
            $homepage = wp_insert_term( __('Homepage','aofds'), 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'homepage',
            ) );
        }

        // Insert term "lastneaws"
        if ($term = get_term_by('slug', 'lastnews', 'newscat')) {
            $lastnews = $term;
        }
        else {	
            $lastnews = wp_insert_term( 'News', 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'lastnews',
            ) );
        }

        // Insert term "event"
        if ($term = get_term_by('slug', 'event', 'newscat')) {
            $event = $term;
        }
        else {	
            $event = wp_insert_term( __('Events','aofds'), 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'event',
            ) );
        }

        // Insert term "press"
        if ($term = get_term_by('slug', 'press', 'newscat')) {
            $press = $term;
        }
        else {	
            $press = wp_insert_term( __('Press','aofds'), 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'press',
            ) );
        }

    
	endif;
	
	
	## Post type : Sport
	
	if (in_array('sport', $post_types)) :

		$labels = array(
			'name' 					=> _x('Sports', 'plural','aofds'),
			'singular_name' 		=> _x('Sport', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a sport','aofds'),
			'edit_item' 			=> __('Modify a sport','aofds'),
			'new_item' 				=> __('New sport','aofds'),
			'view_item' 			=> __('View sport','aofds'),
			'search_items' 			=> __('Find a sport','aofds'),
			'not_found' 			=> __('No sport found','aofds'),
			'not_found_in_trash' 	=> __('No sport found in the trash','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'sport' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'sport'),
		) );
        
        /*
		$newscat_id = register_taxonomy( 'sportcat', array('sport'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Sport categories','aofds'), 
			'singular_label' 	=> __('Sport category','aofds'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
        */
	
	endif;
	
	
	## Post type : Club
	
	if (in_array('club', $post_types)) :

		$labels = array(
			'name' 					=> _x('Clubs', 'plural','aofds'),
			'singular_name' 		=> _x('Club', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a club','aofds'),
			'edit_item' 			=> __('Modify a club','aofds'),
			'new_item' 				=> __('New club','aofds'),
			'view_item' 			=> __('View club','aofds'),
			'search_items' 			=> __('Find a club','aofds'),
			'not_found' 			=> __('No club found','aofds'),
			'not_found_in_trash' 	=> __('No club found in the trash','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'club' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title', 'editor'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'club'),
		) );
        
		// Insert taxonomy "newscat" for "news"
		$clubcat_id = register_taxonomy( 'clubcat', array('club'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Club categories','aofds'), 
			'singular_label' 	=> __('Club category','aofds'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
		
	endif;
	
	
	## Post type : Infrastructure

	if (in_array('infrastructure', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Infrastructures', 'plural','aofds'),
			'singular_name' 		=> _x('Infrastructure', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add an infrastructure','aofds'),
			'edit_item' 			=> __('Modify an infrastructure','aofds'),
			'new_item' 				=> __('New infrastructure','aofds'),
			'view_item' 			=> __('View infrastructure','aofds'),
			'search_items' 			=> __('Find an infrastructure','aofds'),
			'not_found' 			=> __('No infrastructure found','aofds'),
			'not_found_in_trash' 	=> __('No infrastructure found in the trash','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'infrastructure' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'infrastructure'),
		) );

        /*
		$newscat_id = register_taxonomy( 'infrastructurecat', array('court'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Infrastructure categories','aofds'), 
			'singular_label' 	=> __('Infrastructure category','aofds'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
        */

	endif;


	## Post type : meeting

	if (in_array('meeting', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Meeting rooms', 'plural','aofds'),
			'singular_name' 		=> _x('Meeting room', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a meeting room','aofds'),
			'edit_item' 			=> __('Modify a meeting room','aofds'),
			'new_item' 				=> __('New meeting room','aofds'),
			'view_item' 			=> __('View meeting room','aofds'),
			'search_items' 			=> __('Find a meeting room','aofds'),
			'not_found' 			=> __('No meeting room found','aofds'),
			'not_found_in_trash' 	=> __('No meeting room found in the trash','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'meeting' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'meeting'),
		) );

        /*
		$newscat_id = register_taxonomy( 'infrastructurecat', array('court'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Infrastructure categories','aofds'), 
			'singular_label' 	=> __('Infrastructure category','aofds'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
        */

	endif;


	## Post type : Price
	
	if (in_array('price', $post_types)) :

		$labels = array(
			'name' 					=> _x('Prices', 'plural','aofds'),
			'singular_name' 		=> _x('Price', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a price','aofds'),
			'edit_item' 			=> __('Modify a price','aofds'),
			'new_item' 				=> __('New price','aofds'),
			'view_item' 			=> __('View price','aofds'),
			'search_items' 			=> __('Find a price','aofds'),
			'not_found' 			=> __('No price found','aofds'),
			'not_found_in_trash' 	=> __('No price found in the trash','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'price' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'price'),
		) );
        
	endif;
	
	
	## Post type : Team
	
	if (in_array('team', $post_types)) :

		$labels = array(
			'name' 					=> _x('Teams', 'plural','aofds'),
			'singular_name' 		=> _x('Team', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a team','aofds'),
			'edit_item' 			=> __('Modify a team','aofds'),
			'new_item' 				=> __('New team','aofds'),
			'view_item' 			=> __('View team','aofds'),
			'search_items' 			=> __('Find a team','aofds'),
			'not_found' 			=> __('No team found','aofds'),
			'not_found_in_trash' 	=> __('No team found in the bin','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'team' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor'), //, 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'team'),
		) );
    
    endif;


    ## Post type : Contact

	if (in_array('contact', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Contacts', 'plural','aofds'),
			'singular_name' 		=> _x('Contact', 'singular','aofds'),
			'add_new' 				=> __('Add', 'aofds'),
			'add_new_item' 			=> __('Add a contact','aofds'),
			'edit_item' 			=> __('Modify a contact','aofds'),
			'new_item' 				=> __('New contact','aofds'),
			'view_item' 			=> __('View contact','aofds'),
			'search_items' 			=> __('Find a contact','aofds'),
			'not_found' 			=> __('No contact found','aofds'),
			'not_found_in_trash' 	=> __('No contact found in the trash','aofds'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'contact' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor','thumbnail'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'contact'),
		) );

	endif;
	
}
add_action('init', 'aofds_custom_posttypes_and_taxonomies');


function aofds_post_types_admin_order( $wp_query ) {
    
    if (is_admin()) {

        // Get the post type from the query
        $post_type = $wp_query->query['post_type'];

        if ( in_array($post_type, array('news', 'sport', 'infrastructure', 'meeting')) ) {

            $wp_query->set('orderby', 'title');

            $wp_query->set('order', 'ASC');
        }
    }
}
add_filter('pre_get_posts', 'aofds_post_types_admin_order');

/* Function to display the menu */
/*
add_action('admin_init', 'debug_admin_menu');
function debug_admin_menu() {
    echo '<pre>'.print_r($GLOBALS['menu'], true).'</pre>';
}
*/


/**
 * Custom admin menu 
 *
 * @link http://code.tutsplus.com/articles/customizing-the-wordpress-admin-custom-admin-menus--wp-33200
 *
 * @uses wp_get_current_user()
 # @uses WP_Users::has_cap()
 * @uses remove_menu_page()
 * @uses remove_submenu_page()
 * @uses get_page_by_path()
 * @uses add_menu_page()
 * @uses add_submenu_page()
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_custom_menu_items() {

	global $menu;
    global $submenu;

    // Remove not authorized entries
    $user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_network' ) ) {
        remove_menu_page( 'edit.php' );
        //remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'edit.php?post_type=acf' );
        //remove_menu_page( 'nav-menus.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'options-general.php' );
        //remove_menu_page( 'edit-comments.php' );
		//remove_menu_page( 'edit.php?post_type=cycloneslider' );
		remove_menu_page( 'themes.php' );
        //remove_submenu_page( 'themes.php', 'nav-menus.php' );
		//remove_submenu_page( 'index.php', 'my-sites.php' );
		remove_submenu_page( 'edit.php?post_type=cycloneslider', 'cycloneslider-settings' );
        remove_submenu_page( 'wp-polls/polls-manager.php', 'wp-polls/polls-templates.php' );
        remove_submenu_page( 'wp-polls/polls-manager.php', 'wp-polls/polls-options.php' );
		remove_submenu_page( 'edit.php?post_type=lshowcase', 'lshowcase_settings' );
		remove_submenu_page( 'edit.php?post_type=lshowcase', 'lshowcase_shortcode' );
		
        // Add widget and menus separatley 
		add_menu_page( __('Menus'), __('Menus'), 'edit_pages', 'nav-menus.php', '', '' ); 
		add_menu_page( __('Widgets'), __('Widgets'), 'edit_pages', 'widgets.php', '', '' ); 
    }
}
add_action( 'admin_menu', 'aofds_custom_menu_items', 999 );


/**
 * Change admin menu labels 
 *
 * @link http://wordpress.stackexchange.com/questions/9211/changing-admin-menu-labels
 *
 * @since AO Custom Foret de Soignes 1.0.0
 *
 * @return void
 */
function aofds_custom_post_menu_label() {

	global $menu;
    global $submenu;
    
    //error_log('$menu:'.json_encode($menu));
    //error_log('$submenu:'.json_encode($submenu));
    
    // Posts
    //$menu[5][0] = __('News', 'aofds');
    //$submenu['edit.php'][5][0] = __('All news', 'aofds');
    //$submenu['edit.php'][10][0] = __('Add news', 'aofds');
    //$submenu['edit.php'][15][0] = 'Status'; // Change name for categories
    //$submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
    
    // Pages
    $menu[20][0] = __('Other pages', 'aofds');
	//$menu[60][0] = __('Menus', 'aofds');
	//$menu[61][0] = __('Widgets', 'aofds');
	echo '';
}
add_action( 'admin_menu', 'aofds_custom_post_menu_label' );


/**
 * Custom admin menu order
 *
 * @link http://code.tutsplus.com/articles/customizing-the-wordpress-admin-custom-admin-menus--wp-33200
 *
 * @uses get_page_by_path()
 *
 * @since AO Custom Foret de Soignes 1.0.0
 *
 * @return array -> Menu order
 */
function aofds_custom_menu_order( $menu_order ) {
	
	$order[] = 'index.php';
    $order[] = "separator1";
	$order[] = 'edit.php?post_type=cycloneslider';
	
	$post_types = array_map('trim', explode(",", get_option( 'post_types' )));
	foreach ($post_types as $post_type) :
		$order[] = 'edit.php?post_type=' . $post_type;
	endforeach;
	
	$order[] = "edit.php?post_type=page";
    $order[] = "separator2";
	$order[] = "upload.php";
	//$order[] = "edit-comments.php";
    $order[] = "wp-polls/polls-manager.php";
	$order[] = "widgets.php";
	$order[] = "nav-menus.php";
	$order[] = "edit.php?post_type=lshowcase";
    $order[] = "separator-last";
    $order[] = "users.php";
	
	return $order;
}
add_filter( 'custom_menu_order', '__return_true' );
add_filter( 'menu_order', 'aofds_custom_menu_order' );


/**
 * Custom admin menu icons (with FontAwesome) by inserting some styles in 
 * the <head> section of the administration panel
 *
 * @link http://clarknikdelpowell.com/blog/3-ways-to-use-icon-fonts-in-your-wordpress-theme-admin/
 *
 * @uses get_page_by_path()
 *
 * @since AO Custom Foret de Soignes 1.0.0
 *
 * @return void
 */
function aofds_custom_menu_icons() {

	?>
	<style type="text/css">
		#adminmenu #menu-posts-cycloneslider .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f03e';
		}
		#adminmenu #menu-posts-news .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1ea';
		}
		#adminmenu #menu-posts-sport .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1e3'; /*'\f091';*/
		}
		#adminmenu #menu-posts-club .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f015'; /*'\f091';*/
		}
		#adminmenu #menu-posts-infrastructure .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f19c';
		}
		#adminmenu #menu-posts-meeting .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f086';
		}
		#adminmenu #menu-posts-price .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f153'; /*'\f091';*/
		}
		#adminmenu #menu-posts-team .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f0c0';
		}
		#adminmenu #menu-posts-contact .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f095';
		}
		#adminmenu li.wp-menu-separator {
    		height: 8px;
    	}
		#adminmenu #toplevel_page_widgets div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1b3'; /*'\f12e';*/
    		margin-top: 1px;
		}
		#adminmenu #toplevel_page_nav-menus div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f0c9';
    		margin-top: 1px;
		}
     </style>
	<?php
}
add_action('admin_head', 'aofds_custom_menu_icons');


/**
 * Add FontAwesome stylesheet 
 *
 * @link http://fortawesome.github.io/Font-Awesome/
 *
 * @uses wp_enqueue_style()
 *
 * @since AO Custom Foret de Soignes 1.0.0
 *
 * @return void
 */
function aofds_fontawesome_dashboard() {
   wp_enqueue_style('fontawesome', 'http:////netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css', '', '4.2.0', 'all'); 
}
add_action('admin_init', 'aofds_fontawesome_dashboard');

/**
 * Management of users and profiles/roles 
 */

/**
 * Remove all unnecessary roles, to only keep administrator and editor.
 *
 * @uses WP_Roles::remove_role()
 *
 * @since AO Custom Foret de Soignes 1.0.0
 */
 function aofds_remove_roles() {
	$wp_roles = new WP_Roles();
	$wp_roles->remove_role( 'contributor' );
	$wp_roles->remove_role( 'author' );
	$wp_roles->remove_role( 'subscriber' );
	$wp_roles->remove_role( 'mstw_tr_admin' );
}
add_action( 'init', 'aofds_remove_roles' );



/*
function ao_comments_open( $open, $post_id ) {

	$post = get_post( $post_id );

	if ( in_array($post->post_type, array('news', 'sport', 'infrastructure')) ) {
		$open = true;
    }
    else {
		$open = false;
    }
    
	return $open;
}
add_filter( 'comments_open', 'ao_comments_open', 10, 2 );
*/




/**
 * Custom columns displayed for 'Price' posts
 *
 * @uses add_filter()
 *
 * @since AO Custom Foret de Soignes 1.0.0
 *
 * @param array actualites_columns -> ???? to check
 * @return array -> The updated columns
 */
function aofds_add_new_price_columns($price_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';     
    $new_columns['title'] = __('Price', 'aofds');
    $new_columns['sport'] = __('Sport','aofds');
    $new_columns['publish_start_date'] = __('Publish start date','aofds');     
    $new_columns['publish_end_date'] = __('Publish end date','aofds');     
    $new_columns['author'] = __('Author','aofds');     
    $new_columns['date'] = _x('Date', 'column name','aofds');
	if (is_super_admin()) 
        $new_columns['id'] = __('ID');
 
    return $new_columns;
}

function aofds_custom_price() 
{
	add_filter('manage_edit-price_columns', 'aofds_add_new_price_columns');
}
add_action( 'admin_init', 'aofds_custom_price' );

function aofds_manage_price_columns($column_name, $id) {
    global $wpdb, $post;
    switch ($column_name) {
    case 'id':
        echo $id;
		break; 
    case 'sport':
        echo get_the_title(get_field('price_sport'));
		break;
    case 'publish_start_date':
		echo get_field('price_publish_start_date');  
		break;
    case 'publish_end_date':
		echo get_field('price_publish_end_date');  
		break;
    default:
        break;
    } // end switch
}
add_action('manage_price_posts_custom_column', 'aofds_manage_price_columns', 10, 2);




/**
 * Add 'Sport' filter for prices list
 *
 * @uses add_filter()
 *
 * @since AO Custom Foret de Soignes 1.0.0
 */
/* sarickx--- DOESN'T WORK !!

function aofds_restrict_price_by_sport() {
    global $typenow;
    if ($typenow == 'price') {
        $selected = isset($_GET['sport']) ? $_GET['sport'] : '';
        $arg = array(
            'show_option_none' => __("Show for all sports"),
            'orderby' => 'title',
            'hide_empty' => false,
            'suppress_filters' => true,
            'post_type' => 'sport',
            'selected' => $selected
        );
        wp_dropdown_pages($arg);
    };
}
add_action('restrict_manage_posts', 'aofds_restrict_price_by_sport');

function aofds_filter_price_on_sport($query) {
    global $pagenow;
    $post_type = 'price'; 
    $q_vars = &$query->query_vars;
    if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type) {
    $selected = isset($_GET['sport']) ? $_GET['sport'] : '';
        $q_vars['meta_query'] = array( array(
            'key'       => 'price_sport',
            'value'     => $selected,
            'compare'   => '='
        ));
    }
}
add_filter('parse_query', 'aofds_filter_price_on_sport');

---sarickx */


/* End of file aofds-custom-posttypes.php */
/* Location: ./wp-content/themes/aofds/inc/aofds-custom-posttypes.php */