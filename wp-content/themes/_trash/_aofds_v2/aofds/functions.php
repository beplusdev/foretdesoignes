<?php
/** functions.php
 *
 * Custom functions for Foret de Soignes
 *
 * @author		Alley Oop
 * @package		AO Foret de Soignes
 * @since		1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 *
 */


/*
 * Mobble provides mobile related conditional functions for your site. 
 * e.g. is_iphone(), is_mobile() and is_tablet()
 *
 * is_handheld(); // any handheld device (phone, tablet, Nintendo)
 * is_mobile(); // any type of mobile phone (iPhone, Android, etc)
 * is_tablet(); // any tablet device
 * is_ios(); // any Apple device (iPhone, iPad, iPod)
 * is_iphone();
 * is_ipad();
 * is_ipod();
 * is_android();
 * is_blackberry();
 * is_opera_mobile();
 * is_symbian();
 * is_kindle();
 * is_windows_mobile();
 * is_motorola();
 * is_samsung();
 * is_samsung_tablet();
 * is_sony_ericsson();
 * is_nintendo();
*/
include_once('plugins/mobble/mobble.php');



/***************************************************************************************
 	TRANSLATIONS
 ***************************************************************************************/


/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_load_textdomain() {
	load_plugin_textdomain( 'aofds', false, get_stylesheet_directory() . '/lang' ); 
}
add_action( 'plugins_loaded', 'aofds_load_textdomain' );




/***************************************************************************************
 	NAVIGATION
 ***************************************************************************************/


/**
 * Registers multiple custom navigation menus in the menu editor. This allows for the 
 * creation of custom menus in the dashboard for use in Foret de Soignes.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_setup_navs() {
	register_nav_menus( array(
		'primary'     =>	__( 'Navigation', 'aofds' ),
		'footer-menu' =>	__( 'Footer Menu', 'aofds' ),
		'legal-menu'  =>	__( 'Legal Menu', 'aofds' ),
	) );
}
add_action( 'after_setup_theme', 'aofds_setup_navs' );

/**
 * Custom Nav Menu handler for the Navbar.
 */
require_once('inc/nav-menu-walker.php');




/***************************************************************************************
    CUSTOM FRONT
 ***************************************************************************************/


/**
 * Three new dimensions for images.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_register_custom_image_sizes() {
    add_image_size( 'one-third', 300, 168, true ); 
    add_image_size( 'two-third', 630, 354, true ); 
    add_image_size( 'half-third', 150, 84, true ); 
}
add_action( 'after_setup_theme', 'aofds_register_custom_image_sizes' );



/**
 * Populate meta tags for social share (OpenGraph).
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_social_meta_tags() {

    $site_name = get_bloginfo( 'name', 'display' );
    $title = $site_name;
    $description = get_bloginfo( 'description', 'display' );
    $twitter_description = get_bloginfo( 'description', 'display' );
    $image = get_option('social_share_logo_url');
    $url = get_bloginfo( 'url', 'display' );
    
    if (is_single()) :
        $title = get_the_title();
        $content = strip_tags(get_post_field('post_content', get_the_ID()));
        $description = (strlen($content) > 500) ? substr($content,0,500).'...' : $content; 
        $twitter_description = (strlen($content) > 200) ? substr($content,0,200).'...' : $content; 
        $url = get_permalink();
        if ($image_header = get_field('images_header')) : 
            $image = $image_header['url'];
        endif;
    elseif (is_archive())  :
        $title = post_type_archive_title('',false) . ' | ' . $site_name;
        $url = get_permalink();
    endif;
    ?>

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="<?php echo $title; ?>" />
    <meta name="twitter:description" content="<?php echo $twitter_description; ?>" />
    <meta name="twitter:url" content="<?php echo $url; ?>" />
    <meta name="twitter:image" content="<?php echo $image; ?>" />
    <meta property="og:type" content="article"/>
    <meta property="og:site-name" content="<?php echo $site_name; ?>" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:description" content="<?php echo $description; ?>" />
    <meta property="og:image" content="<?php echo $image; ?>" />
    <meta property="og:url" content="<?php echo $url; ?>" />
    
    <?php
}
    
    

/**
 * Customize WP queries for front size.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aolawolue_custom_queries( $query ) {

	global $gloss_category;  

    if ( $query->is_search ) 
        return $query;
    
    // Special query for homepage
    if( is_home() ) {
    
    	// Only 5 posts with term "homepage"
        /*
		set_query_var('post_type','news');
		set_query_var('posts_per_page', 3 );
	
		$taxquery = array(
			array(
				'taxonomy' 	=> 'newscat',
				'field' 	=> 'slug',
				'terms' 	=> array( 'homepage' ),
        	)
	    );
		set_query_var('tax_query', $taxquery );
        */
        
		set_query_var('name','accueil');
		set_query_var('post_status', 'publish' );
		set_query_var('post_type','page');
		set_query_var('posts_per_page', 1 );

	}

    
	// Special query for archive of events
	if ( !is_admin() && is_tax( 'newscat', 'event' ) ) {
	
		// Only future or past events (based on end date)
		if (isset($_GET['w']) and ($_GET['w'] == 'past')) {
			$compare = '<';
			$order = 'DESC';
		}
		else {
			$compare = '>=';
			$order = 'ASC';
		}
		$today = new DateTime('today');
		$meta_query = array(
			array(
				'key' => 'event_start',
        	    'value' => $today->getTimestamp(),
				'compare' => $compare,
				'type' => 'NUMERIC',
			),
		);
		set_query_var('meta_query', $meta_query );
		
		// Ordering by start date
		set_query_var('orderby','meta_value');
		set_query_var('meta_key', 'event_start' );
		set_query_var('order', $order );

	}

	// Special query for archive of sports
	if ( !is_admin() && is_post_type_archive( 'sport' ) ) {
	
		set_query_var('posts_per_page', -1 );
		set_query_var('orderby','title');
		set_query_var('order', 'ASC' );

	}

	// Special query for archive of infrastructures
	if ( !is_admin() && is_post_type_archive( 'infrastructure' ) ) {
	
		set_query_var('posts_per_page', -1 );
		set_query_var('orderby','title');
		set_query_var('order', 'ASC' );

	}

}
add_action('pre_get_posts', 'aolawolue_custom_queries' );



/**
 * Displays meta information for a post.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_entry_meta() {

	// Published date
	if (get_post_type() == "news") 
		echo '<span class="post-date"><i class="fa fa-calendar"></i>' . ((!is_mobile()) ? __('Published on','aofds') . ' ' : '') . get_the_date() . '</span>';
	
	// Terms
	$categories = array();
    $terms = get_the_terms( get_the_ID(), 'newscat' );
    if ( !empty( $terms ) ) {
        foreach ( $terms as $term ) {
            if (!in_array($term->slug, array('fr', 'en', 'homepage'))) {
                $categories[] = '<a href="' .get_term_link($term->slug, $term->taxonomy) .'">'.$term->name.'</a> ';
            }
        }
    }
    $categories_list = implode(', ', $categories); 
    if ( $categories_list ) {
        echo '<span class="post-categories"><i class="fa fa-folder-open"></i> ' . $categories_list . '</span>';
    }

    // Event
    /*
    if ($start = get_field('event_start')) :
        echo '<span class="event-dates"><i class="fa fa-calendar"></i>';
            if ($end = get_field('event_end')) :
                echo __( 'From', 'aofds' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aofds' ) . date_i18n( ' G:i ' , $start ) . __( 'to', 'aofds' ) . date_i18n( ' l d/m ' , $end ) . __( 'at', 'aofds' ) . date_i18n( ' G:i' , $end );
            else :
                echo __( 'On', 'aofds' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aofds' ) . date_i18n( ' G:i' , $start );
            endif;	
        echo '</span>';
    endif;
    */
    // Locations
    $locations = array();
    while (has_sub_field('event_infrastructures_list')) :
        $location = get_sub_field('event_infrastructures_list_infrastructure');
        $locations[] = '<a href="'.$location->guid.'">'.$location->post_title.'</a>';
    endwhile;
    $locations_list = implode(', ', $locations); 
    if ( $locations_list ) {
        echo '<span class="post-locations"><i class="fa fa-map-marker"></i> ' . $locations_list . '</span>';
    }
                
    // Edit
    edit_post_link( __( 'Edit', 'aofds' ), '<span class="edit-link"><i class="fa fa-pencil"></i>', '</span>' ); 
}



/**
 * Displays event information.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_event_info() {

    if (get_field('event_start') || has_sub_field('event_infrastructures_list') || get_field('event_location')) :
    
        echo '<div class="event-details">';

        // Dates
        if ($start = get_field('event_start')) :
            //echo '<h3>'.__( 'When ?', 'aofds' ).'</h3>';
            echo '<p><i class="fa fa-calendar"></i> ';
                if ($end = get_field('event_end')) :
                    echo __( 'From', 'aofds' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aofds' ) . date_i18n( ' G:i ' , $start ) . __( 'to', 'aofds' ) . date_i18n( ' l d/m ' , $end ) . __( 'at', 'aofds' ) . date_i18n( ' G:i' , $end );
                else :
                    echo __( 'On', 'aofds' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aofds' ) . date_i18n( ' G:i' , $start );
                endif;	
            echo '</p>';
        endif; 
    
        // Infra
        $locations = array();
        while (has_sub_field('event_infrastructures_list')) :
            $location = get_sub_field('event_infrastructures_list_infrastructure');
            $locations[] = '<a href="'.$location->guid.'">'.$location->post_title.'</a>';
        endwhile;
        $locations_list = implode(', ', $locations); 
        if ( $locations_list ) {
            //echo '<h3>'.__( 'Where ?', 'aofds' ).'</h3>';
            echo '<p><i class="fa fa-map-marker"></i> ' . $locations_list . '</p>';
        }    

        // Address
        if ($location = get_field('event_location')) :
            //echo '<h3>'.__( 'Where ?', 'aofds' ).'</h3>';
            echo '<p><i class="fa fa-map-marker"></i> <a href="http://maps.google.com?q='.$location['address'].' target="_blank"">'.$location['address'].'</a></p>';
            //echo '<div class="map"><div id="gmap" class="gmap"></div></div>';
        endif;

        echo '</div>';
    
    endif;

}



/**
 * Displays event calendar.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_event_calendar() {

    if ( has_term( 'event', 'newscat', get_the_ID() ) and ( $start = get_field('event_start') ) ) :
        echo '<span class="event-date">';
        echo '<span class="event-day">'.date_i18n( 'j' , $start ).'</span>';
        echo '<span class="event-month">'.ucfirst(__(date_i18n( 'M' , $start ))).'</span>';
        echo '</span>';
    endif;

}



/**
 * Displays navigation to next/previous post when applicable.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_post_nav() {
    
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php __( 'Post navigation', 'aofds' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'aofds' ) ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'aofds' ) ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}



/**
 * Take into account the horizontal admin bar
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_admin_bar() {
    
    if (is_admin_bar_showing()) :
	?>
        <!-- AO Theme Admin Bar CSS --> 
        <style type="text/css">
        .navbar-fixed-top {
        top: 32px !important;
        }
        /*
        #page.other .header-image .image {
        top: 112px !important;
        }
        #sidebar-header {
        top: 112px;
        }
        */
        </style> 
        <!-- /AO Theme Options CSS -->
	<?php
    endif;
}
add_action( 'wp_head' , 'aofds_admin_bar' );



/**
 * Replace the "#more-..." anchor added in the read more link
 *
 * @since 1.0.0
 *
 * @return void
 */

function aofds_replace_the_more($content) {
    return preg_replace('/\#more-(\d)/' ,'' ,$content);
}
add_filter('the_content','aofds_replace_the_more');




/***************************************************************************************
 	WIDGETS
 ***************************************************************************************/


/**
 * Registers widget areas.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Header Sidebar', 'aofds' ),
		'id'            => 'sidebar-header',
		'description'   => __( 'Appears in the header of the page.', 'aofds' ),
		'before_widget' => '<aside id="%1$s" class="widget sidebar-header %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Left Content Sidebar', 'aofds' ),
		'id'            => 'sidebar-left-content',
		'description'   => __( 'Appears on the left of the content.', 'aofds' ),
		'before_widget' => '<aside id="%1$s" class="widget sidebar-left-content %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Bottom Content Sidebar', 'aofds' ),
		'id'            => 'sidebar-bottom-content',
		'description'   => __( 'Appears underneath the content.', 'aofds' ),
		'before_widget' => '<aside id="%1$s" class="widget sidebar-bottom-content %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Right Content Sidebar', 'aofds' ),
		'id'            => 'sidebar-right-content',
		'description'   => __( 'Appears on the right of the content.', 'aofds' ),
		'before_widget' => '<aside id="%1$s" class="widget sidebar-right-content %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Bottom Footer', 'aofds' ),
		'id'            => 'sidebar-footer',
		'description'   => __( 'Appears the footer of the page.', 'aofds' ),
		'before_widget' => '<aside id="%1$s" class="widget sidebar-footer %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'aofds_widgets_init' );



/**
 * Hide unused WP widgets.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_hide_widgets() {
    unregister_widget('WP_Widget_Pages'); //Widget Pages
    //unregister_widget('WP_Widget_Calendar'); //Widget Calendrier
    unregister_widget('WP_Widget_Archives'); //Widget Archives
    //unregister_widget('WP_Widget_Links'); //Widget Liens
    unregister_widget('WP_Widget_Meta'); //Widget Meta
    unregister_widget('WP_Widget_Search'); //Widget Rechercher
    unregister_widget('WP_Widget_Categories'); //Widget Catégories
    unregister_widget('WP_Widget_Recent_Posts'); //Widget Articles recents
    unregister_widget('WP_Widget_Recent_Comments'); //Widget Commentaires recents
    unregister_widget('WP_Widget_RSS'); //Widget Flux
    unregister_widget('WP_Widget_Tag_Cloud'); //Widget Nuag de mots clef
	unregister_widget('Akismet_Widget');
	unregister_widget('Akismet_Widget');
	unregister_widget('PLL_Widget_Languages');
	unregister_widget('PLL_Widget_Calendar');
	unregister_widget('Multisite_Global_Search');
	unregister_widget('Cyclone_Slider_Widget');

/*
	if ( empty ( $GLOBALS['wp_widget_factory'] ) )
        return;

    $widgets = array_keys( $GLOBALS['wp_widget_factory']->widgets );
    print '<pre>$widgets = ' . esc_html( var_export( $widgets, TRUE ) ) . '</pre>'; */
 }
 add_action('widgets_init', 'aofds_hide_widgets', 11);



/***************************************************************************************
 	CUSTOM POST TYPES
 ***************************************************************************************/

require_once('inc/aofds-custom-posttypes.php');




/***************************************************************************************
 	CUSTOM FIELDS (THANKS TO ACF)
 ***************************************************************************************/


require_once('plugins/advanced-custom-fields-pro/acf.php');
//require_once('plugins/advanced-custom-fields/acf.php');
//require_once('plugins/acf-repeater/repeater.php');
//require_once('plugins/acf-location-field-master/acf-location.php');
//require_once('plugins/acf-gallery/acf-gallery.php');
require_once('plugins/acf-field-date-time-picker/acf-date_time_picker.php');

require_once('inc/aofds-custom-fields.php');
require_once('inc/aofds-custom-layouts.php');




/***************************************************************************************
 	CUSTOM OPTIONS
 ***************************************************************************************/


require_once('inc/aofds-custom-options.php');




/***************************************************************************************
 	CUSTOM ADMIN
 ***************************************************************************************/


//require_once('plugins/simple-page-ordering/simple-page-ordering.php');

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Foret de Soignes supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_custom_setup() {

	remove_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'aofds_custom_setup' );



function aofds_remove_dashboard_meta() {

    $user = wp_get_current_user();
	if ( ! $user->has_cap( 'manage_network' ) ) {
		remove_action('welcome_panel', 'wp_welcome_panel');
		//remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		//remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
		//remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		//remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		//remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
		remove_meta_box( 'synved_connect_dashboard_widget', 'dashboard', 'normal' );
		remove_meta_box( 'cyclone-slider-templates-metabox', 'cycloneslider', 'normal' );
	}
}
add_action( 'admin_init', 'aofds_remove_dashboard_meta' );



/**
 * Add a specific stylesheet to the admin
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_admin_theme_style() {

    /*
    $user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_network' ) ) :
    ?>
		<style type="text/css">
			#cyclone-slider-templates-metabox,
			#cyclone-slider-id,
			#cyclone-slider-codes,
			#cyclone-slider-properties-metabox,
			#cyclone-slider-advanced-settings-metabox {
				display: none;
			}
			#wp-admin-bar-new-content {
				display: none !important;
		}
		</style>
	<?php 
	endif;
    */
    
}
add_action('admin_enqueue_scripts', 'aofds_admin_theme_style');


/**
 * Add a specific stylesheet to the login page
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_login_theme_style() {
    ?>
	<style type="text/css">
		body.login {
			background: none repeat scroll 0% 0% #EEEEEE;
		}
		#login h1 a {
			background-image: none, url("<?php echo get_bloginfo( 'stylesheet_directory' ) . '/images/logo-sport-transp.png'; ?>");
            background-size: auto 100px;
            width: 300px;
            height: 100px;
		}
	</style>
	<?php 
}
add_action('login_enqueue_scripts', 'aofds_login_theme_style');




/***************************************************************************************
 	SCRIPTS AND STYLESHEETS
 ***************************************************************************************/


/**
 * Enqueues scripts and styles for front end.
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_scripts_styles() { 

    // Fonts
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic|Roboto:400,500,400italic,500italicc', '', '0.1' );
    
	// Loads fontawesome
	wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', '', '4.4.0', 'all' ); 

	// Loads flaticons
	wp_enqueue_style( 'flaticon', get_template_directory_uri() . '/vendor/flaticon/flaticon.css', '', '0.1', 'all' ); 

	// Loads bootstrap javaScript file.
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '3.3.5', true );

	// Loads bootstrap stylesheet.
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css', array(), '3.3.5' );
	
	// Loads bootstrap social stylesheet.
	//wp_enqueue_style( 'bootstrap-social', get_template_directory_uri() . '/css/bootstrap-social.css', array(), '3.3.5' );

	// Loads specific scripts.
	wp_enqueue_script( 'aofds-gmaps-api', 'http://maps.google.com/maps/api/js?sensor=true', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'aofds-gmaps', get_template_directory_uri() . '/vendor/gmaps/gmaps.js', array( 'jquery' ), '0.4.19', true );

}
add_action( 'wp_enqueue_scripts', 'aofds_scripts_styles' );



/**
 * Include specific plugins (after scripts like jQuery are loaded)
 *
 * @since 1.0.0
 *
 * @return void
 */
include_once('plugins/ao-facebook/ao-facebook.php');
include_once('plugins/ao-upcoming-events/ao-upcoming-events.php');
include_once('plugins/ao-posts-thumbnails/ao-posts-thumbnails.php');
include_once('plugins/ao-infrastructure/ao-infrastructure.php');
include_once('plugins/aofds-sports/aofds-sports.php');

// Only for super admin
if (is_super_admin()) {
    include_once('plugins/what-the-file/what-the-file.php');
    $wtf = new WhatTheFile;
}


/**
 * Enqueues custom scripts and styles for front end (after all other stylesheets and scripts).
 *
 * @since 1.0.0
 *
 * @return void
 */
function aofds_custom_scripts_styles() { 

	// Loads our main stylesheet.
	wp_enqueue_style( 'aofds-style', get_stylesheet_uri(), array(), filemtime( get_stylesheet_directory() ) );

	// Loads AO Custom javaScript file with functionality specific to Foret de Soignes.
	wp_enqueue_script( 'aofds-aoscript', get_template_directory_uri() . '/js/aoscript.js', array( 'jquery' ), '0.03', true );

}
add_action( 'wp_enqueue_scripts', 'aofds_custom_scripts_styles' );




/***************************************************************************************
	GOOGLE ANALYTICS
 ***************************************************************************************/


/**
 * Add google analytics code to each page
 *
 * @since 1.0.0
 *
 * @return void
 */
/* sarickx---
function aofds_googlea_nalytics() { 
	if (!$google_analytics = get_option( 'google_analytics' )) :
	?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga("create", "<?php echo get_option( 'google_analytics' ); ?>", "auto");
			ga("send", "pageview");
		</script>
	<?php
	endif;
}
add_action('wp_head', 'aofds_google_analytics');
*/




/***************************************************************************************
 	TRANSVERSAL FUNCTIONS
 ***************************************************************************************/


/**
 * Determine if current environment is DEV
 *
 * @since 1.0.0
 *
 * @return boolean
 */
function aofds_is_blog_dev() {
	global $blog_id;
	return (strpos(get_blog_option($blog_id, 'siteurl'), 'dev.foretdesoignessport.be') !== false);
}
if (aofds_is_blog_dev()) error_log('===== DEV =====');



/**
 * Determine if current environment is TST
 *
 * @since 1.0.0
 *
 * @return boolean
 */
function aofds_is_blog_tst() {
	global $blog_id;
	return (strpos(get_blog_option($blog_id, 'siteurl'), 'tst.foretdesoignessport.be') !== false);
}
if (aofds_is_blog_tst()) error_log('===== TST =====');



/**
 * Determine if current environment is PROD
 *
 * @since 1.0.0
 *
 * @return boolean
 */
function aofds_is_blog_www() {
	return (!aofds_is_blog_dev() && !aofds_is_blog_tst());
}
if (aofds_is_blog_www()) error_log('===== WWW =====');




/* End of file functions.php */
/* Location: ./wp-content/themes/aofds/functions.php */