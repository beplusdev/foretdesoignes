<?php
/** aofds-custom-layouts.php
 *
 * AO Custom layouts added with ACF
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );



function aofds_display_layout($layout) {
    
    switch ( $layout ) :

        case 'section' :
            aofds_display_section();
            break;

        case 'subsection' :
            aofds_display_subsection();
            break;

        case 'content' :
            aofds_display_content();
            break;

        case 'images' :
            aofds_display_images();
            break;

        case 'prices' :
            aofds_display_prices();
            break;

        case 'equipment_table' :
            aofds_display_equipment_table();
            break;

        case 'features_table' :
            aofds_display_features_table();
            break;

        case 'schedule_table' :
            aofds_display_schedule_table();
            break;

        case 'club_trainer' :
            aofds_display_club_trainer();
            break;

        case 'contact' :
            aofds_display_contact();
            break;

        case 'address' :
            aofds_display_address();
            break;

        case 'phone' :
            aofds_display_phone();
            break;

        case 'mobile' :
            aofds_display_mobile();
            break;

        case 'email' :
            aofds_display_email();
            break;

        case 'web' :
            aofds_display_web();
            break;

        case 'facebook' :
            aofds_display_facebook();
            break;

        case 'description' :
            aofds_display_description();
            break;

    endswitch;
}



/***************************************************************************************
 	SECTION
 ***************************************************************************************/


function aofds_display_section() { 

    echo '<h2 class="section">'.get_sub_field('section_title').'</h2>';
}



/***************************************************************************************
 	SUBSECTION
 ***************************************************************************************/


function aofds_display_subsection() { 

    echo '<h3 class="subsection">'.get_sub_field('subsection_title').'</h3>';
}



/***************************************************************************************
 	CONTENT
 ***************************************************************************************/


function aofds_display_content() { 

    echo '<div class="content">'.get_sub_field('content_text').'</div>';
}



/***************************************************************************************
 	IMAGES
 ***************************************************************************************/


function aofds_display_images() { 
    
    // check if the nested repeater field has rows of data
    if ( have_rows( 'images_repeater' ) ) :
    
        $images = array();
        $caption = array();

        // loop through the rows of data
        while ( have_rows('images_repeater') ) : the_row();

            $images[] = get_sub_field('images_repeater_image');
            $captions[] = get_sub_field('images_repeater_caption');

        endwhile;
    
        //var_dump($images);
        //var_dump($captions);
    
        switch ( get_sub_field('images_options') ) :

            case 'gallery' :
                aofds_display_gallery($images, $captions);
                break;

            case 'carousel' :
                aofds_display_carousel($images, $captions);
                break;

        endswitch;
    
    endif;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function aofds_display_carousel($medias, $descriptions) {

    if ( $n = count( $medias ) ) :
    
        // We generate a random id to be able to have more than one carousel on the same page
        $id = 'carousel-'.rand(0, 999999);
        ?>

        <div id="<?php echo $id; ?>" class="carousel slide" data-ride="carousel" data-interval="false">
            
            <!-- Indicators -->
            <?php if ($n > 1) : ?>
                <ol class="carousel-indicators">
                    <?php $first = true; ?>
                    
                    <?php foreach ($medias as $i => $media) : ?>
                        <li data-target="#<?php echo $id; ?>" data-slide-to="<?php echo $i; ?>" <?php if ($first) { echo 'class="active"'; $first = false; } ?>></li>
                    <?php endforeach; ?>
                    
                </ol>
            <?php endif; ?>
            
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php $first = true; ?>
                
                <?php foreach ($medias as $i => $media) : ?>
                    <div class="item <?php if ($first) { echo 'active'; $first = false; } ?>" id="carousel-item-<?php echo $i; ?>">
                        <a href="<?php echo $media['url']; ?>" rel="lightbox[gallery-]" title="<?php echo $descriptions[$i]; ?>">
                            <?php $image_width = (is_mobile()) ? 'one-third' : 'two-third'; ?>
                            <div class="image" style="background-image: url(<?php echo $media['sizes'][$image_width]; ?>);"></div>
                            <?php if ($descriptions[$i]) : ?>
                                <div class="carousel-caption">
                                    <?php echo $descriptions[$i]; ?>
                                </div>
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
                
            </div>
            
            <!-- Controls -->
            <?php if ($n > 1) : ?>
                <a class="left carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?php _e('Previous', 'aofds'); ?></span>
                </a>
                <a class="right carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?php _e('Next', 'aofds'); ?></span>
                </a>
            <?php endif; ?>
        </div>

        <?php

    endif;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function aofds_display_gallery($medias, $descriptions) {

    ?>
    <div class="row gallery">
        <?php
        $n = count($medias);
        foreach ($medias as $i => $media) : 
            if ($n >= 4) :
                echo '<div class="col col-lg-3 col-md-3 col-sm-4 col-xs-4">';
                $image_width = 'thumbnail';
            elseif ($n >= 3) :
                echo '<div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">';
                $image_width = 'thumbnail';
            elseif ($n >= 2) :
                echo '<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">';
                $image_width = 'one-third'; 
            else :
                echo '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
                $image_width = (is_mobile()) ? 'one-third' : 'two-third';
            endif;
            ?>
                <div class="item">
                    <a href="<?php echo $media['sizes']['large']; ?>" rel="lightbox[gallery-medias]" title="<?php echo $descriptions[$i]; ?>">
                        <img src="<?php echo $media['sizes'][$image_width]; ?>" alt="<?php echo $descriptions[$i]; ?>">
                    </a>
                    <?php if ($descriptions[$i]) : ?>
                        <div class="description">
                            <?php echo $descriptions[$i]; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php
}



/***************************************************************************************
 	EVENT
 ***************************************************************************************/


function aofds_display_layout_event() {
    
	if (get_field('event_start') || get_field('event_location')) : 

        ?>
		
        <div class="event-details">

            <?php 
            if ($start = get_field('event_start')) : 
    
                /* <h3><?php _e( 'When ?', 'aofds' ); ?></h3> */ 
                ?>
                <p>
                    <?php 
                    if ($end = get_field('event_end')) :
                        echo '<i class="fa fa-calendar"></i> ' . __( 'From', 'aofds' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aofds' ) . date_i18n( ' G:i ' , $start ) . __( 'to', 'aofds' ) . date_i18n( ' l d/m ' , $end ) . __( 'at', 'aofds' ) . date_i18n( ' G:i' , $end );
                    else :
                        echo '<i class="fa fa-calendar"></i> ' . __( 'On', 'aofds' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aofds' ) . date_i18n( ' G:i' , $start );
                    endif;	
                    ?>
                </p>
                <?php 
    
            endif; 
            
            if ( have_rows( 'event_location_flexible' ) ):
    
                /* <h3><?php _e( 'Where ?', 'aofds' ); ?></h3> */ 

                while ( have_rows( 'event_location_flexible' ) ) : the_row();

                    switch ( get_row_layout() ) :
    
                        case 'event_location_infrastructure' :
                            aofds_display_event_location_infrastructure();
                            break;
    
                        case 'event_location_address' :
                            aofds_display_event_location_address();
                            break;
    
                        case 'event_location_map' :
                            aofds_display_event_location_map();
                            break;
    
                    endswitch;
    
                endwhile;
    
            endif;
            ?>
            
        </div>

        <?php
    
    endif;
    
}

  

function aofds_display_event_location_infrastructure() {
    
    $locations = array();
    
    while (has_sub_field('event_location_infrastructure_repeater')) :
    
        $location = get_sub_field('event_location_infrastructure_repeater_object');
        $locations[] = '<a href="'.$location->guid.'">'.$location->post_title.'</a>';
   
    endwhile;
    
    $locations_list = implode(', ', $locations); 
    
    if ( $locations_list ) {
        
        //echo '<h3>'.__( 'Where ?', 'aofds' ).'</h3>';
        echo '<p><i class="fa fa-map-marker"></i> ' . $locations_list . '</p>';
        
    }    

}



function aofds_display_event_location_address() {
    
    if ($location = get_sub_field('event_location_address_text')) :
    
        echo '<address><i class="fa fa-map-marker"></i> <a href="http://maps.google.com?q='.strip_tags($location).'" target="_blank">'.$location.'</a></address>';

    endif;
}



function aofds_display_event_location_map() {
    
    if ( $location = get_sub_field('event_location_map_google') ) :
    
        ?>

        <p><?php echo '<i class="fa fa-map-marker"></i> ' . $location['address']; ?></p>
        <div class="map">
            <?php /*
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
            */ ?>
            <div id="gmap" class="gmap"></div> 
            <script type="text/javascript"> 
                jQuery(document).ready(function() {
                    var myLatlng = new google.maps.LatLng(<?php echo $location['coordinates']; ?>);
                    var image = "<?php echo get_template_directory_uri(); ?>/images/basket-marker.png";
                    var myOptions = {
                        zoom: 14,
                        center: myLatlng,
                    };
                    var map = new google.maps.Map(document.getElementById("gmap"), myOptions);
                    var contentString = '<div id="content">'+
                          '<div id="siteNotice">'+
                          '</div>'+
                          '<h4 id="firstHeading" class="firstHeading"><?php the_title(); ?></h4>'+
                          '<div id="bodyContent">'+
                          '<p><?php echo $location['address']; ?></p>'+
                          '</div>'+
                          '</div>';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        icon: image,
                        map: map,
                        title: "<?php the_title(); ?>"
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });
                });
            </script> 
        </div>

        <?php 
    
    endif; 
    
}



/***************************************************************************************
 	PRICES
 ***************************************************************************************/


function aofds_display_prices() { 

    $id = get_sub_field('prices_object');
    
    //setup_postdata(get_post($id));

    $price1_label = get_field( 'price1_label', $id ) ? get_field( 'price1_label', $id ) : get_option( 'price1_label' );
    $price1_caption = get_field( 'price1_caption', $id ) ? get_field( 'price1_caption', $id ) : get_option( 'price2_caption' );
    $price2_label = get_field( 'price2_label', $id ) ? get_field( 'price2_label', $id ) : get_option( 'price2_label' );
    $price2_caption = get_field( 'price2_caption', $id ) ? get_field( 'price2_caption', $id ) : get_option( 'price2_caption' );
    
    if( have_rows('price_repeater', $id) ):

        while ( have_rows('price_repeater', $id) ) : the_row();

            $start = get_sub_field('price_publish_start_date');
            $end = get_sub_field('price_publish_end_date');
            $today = date('Ymd');
            
            if ( ( !$start or ( $today >= $start ) ) and ( !$end or ( $today <= $end ) ) ) :

                echo '<h2>'.get_sub_field( 'price_title' ).'</h2>';
    
                if ( have_rows( 'price_flexible', $id ) ):
    
                    while ( have_rows( 'price_flexible', $id ) ) : the_row();

                        switch ( get_row_layout() ) :
    
                            case 'section' :
                                aofds_display_section();
                                break;

                            case 'subsection' :
                                aofds_display_subsection();
                                break;

                            case 'content' :
                                aofds_display_content();
                                break;

                            case 'images' :
                                aofds_display_images();
                                break;

                            case 'prices_table' :
                                aofds_display_prices_table($price1_label, $price1_caption, $price2_label, $price2_caption);
                                break;

                            case 'equipment_table' :
                                aofds_display_equipment_table();
                                break;

                        endswitch;

                    endwhile;

                endif;
    
             endif;

        endwhile;

    endif;
    
    //wp_reset_postdata();
}



function aofds_display_prices_table($label1, $caption1, $label2, $caption2) { 

    
    if ( $nbrows = count( get_sub_field( 'prices_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        ?>

        <table class="table table-striped prices-table">
            <thead>
                <tr>
                    <th class="description"></th>
                    <th class="price price1" data-toggle="tooltip" data-placement="top" title="<?php echo $caption1; ?>"><?php echo $label1; ?></th>
                    <th class="price price2" data-toggle="tooltip" data-placement="top" title="<?php echo $caption2; ?>"><?php echo $label2; ?></th>
                </tr>
            </thead>
            <tbody>
                
                <?php while ( has_sub_field( 'prices_table_repeater' ) ) : ?>

                    <tr>
                        <td class="description"><?php the_sub_field('prices_table_repeater_description'); ?></td>
                        <td class="price price1"><?php the_sub_field('prices_table_repeater_price1'); ?></td>
                        <td class="price price2"><?php the_sub_field('prices_table_repeater_price2'); ?></td>
                    </tr>

                <?php endwhile; ?>

            </tbody>
        </table>

        <?php
    
    endif;
}



/***************************************************************************************
 	EQUIPMENT
 ***************************************************************************************/


function aofds_display_equipment() { 
}


function aofds_display_equipment_table($label1, $label2) { 

    
    if ( $nbrows = count( get_sub_field( 'equipment_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        ?>

        <table class="table table-striped equipment-table">
            <thead>
                <tr>
                    <th class="equipment"><?php _e('Equipment', 'aofds'); ?></th>
                    <th class="price"><?php _e('Price', 'aofds'); ?></th>
                </tr>
            </thead>
            <tbody>
                
                <?php while ( has_sub_field( 'equipment_table_repeater' ) ) : ?>

                    <tr>    
                        <td class="equipment"><?php the_sub_field('equipment_table_repeater_element'); ?></td>
                        <td class="price"><?php the_sub_field('equipment_table_repeater_price'); ?></td>
                    </tr>

                <?php endwhile; ?>

            </tbody>
        </table>

        <?php
    
    endif;
}



/***************************************************************************************
 	FEATURES
 ***************************************************************************************/


function aofds_display_features_table() { 

    
    if ( $nbrows = count( get_sub_field( 'features_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        ?>

        <table class="table table-striped features-table">
            
            <?php /*
            <thead>
                <tr>
                    <th class="feature"><?php _e('Feature', 'aofds'); ?></th>
                    <th class="price"><?php _e('Value', 'aofds'); ?></th>
                </tr>
            </thead>
            */ ?>
            
            <tbody>
                
                <?php while ( has_sub_field( 'features_table_repeater' ) ) : ?>

                    <tr>    
                        <td class="feature"><?php the_sub_field('features_table_repeater_element'); ?></td>
                        <td class="price"><?php the_sub_field('features_table_repeater_value'); ?></td>
                    </tr>

                <?php endwhile; ?>

            </tbody>
        </table>

        <?php
    
    endif;
}



/***************************************************************************************
 	SCHEDULE
 ***************************************************************************************/


function aofds_display_schedule_table() { 
    
    if ( $nbrows = count( get_sub_field( 'schedule_table_repeater' ) ) ) : 

        ?>

        <div class="table-responsive effect schedule-table">
            <table class="table table--bordered table-striped">
                <thead>
                    <tr>
                        <th><?php _e('Day', 'aofds'); ?></th>
                        <th><?php _e('Time', 'aofds'); ?></th>
                        <th><?php _e('Location', 'aofds'); ?></th>
                        <th><?php _e('Type', 'aofds'); ?></th>
                        <th><?php _e('Level', 'aofds'); ?></th>
                        <th><?php _e('Age', 'aofds'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php while (has_sub_field('schedule_table_repeater')) : ?>
                    <tr>
                        <td class="day"><?php the_sub_field('schedule_table_repeater_day'); ?></td>
                        <td class="time"><?php the_sub_field('schedule_table_repeater_time'); ?></td>
                        <td class="court">
                            <?php if ($court = get_sub_field('schedule_table_repeater_infrastructure')) : ?>
                                <a href="<?php echo $court->guid; ?>" title="<?php _e('See infrastructure', 'aofds'); ?>">
                                    <?php echo $court->post_title; ?>
                                </a>
                            <?php endif; ?>
                        </td>
                        <td class="type"><?php the_sub_field('schedule_table_repeater_type'); ?></td>
                        <td class="level"><?php the_sub_field('schedule_table_repeater_level'); ?></td>
                        <td class="age"><?php the_sub_field('schedule_table_repeater_age'); ?></td>
                    </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
        </div>

        <?php
    
    endif;
}



/***************************************************************************************
 	TRAINER
 ***************************************************************************************/


function aofds_display_club_trainer() { 

    //$id = get_sub_field('contact_object');
    
    //setup_postdata(get_post($id));
    ?>

    <div class="panel panel-default">
        
        <?php if ( $name = get_sub_field( 'club_trainer_name' ) ) : ?>
        
            <div class="panel-heading">
                <?php echo $name; ?>
            </div>
        
        <?php endif; ?>
        
        
        <div class="panel-body">
            
            <?php

            if ( $image = get_sub_field( 'club_trainer_image' ) ) 
                echo '<div class="image"><img src="'.$image['sizes']['thumbnail'].'" /></div>';
    
    
            if ( have_rows( 'club_trainer_flexible' ) ):

                while ( have_rows( 'club_trainer_flexible' ) ) : the_row();

                    switch ( get_row_layout() ) :

                        case 'address' :
                            aofds_display_address();
                            break;

                        case 'phone' :
                            aofds_display_phone();
                            break;

                        case 'mobile' :
                            aofds_display_mobile();
                            break;

                        case 'email' :
                            aofds_display_email();
                            break;

                        case 'web' :
                            aofds_display_web();
                            break;

                        case 'motto' :
                            aofds_display_motto();
                            break;

                        case 'images' :
                            aofds_display_images();
                            break;

                    endswitch;

                endwhile;

            endif;
    
            ?>
                        
        </div>
    
    </div>
 
    <?php
    
    //wp_reset_postdata();
}



/***************************************************************************************
 	CONTACT
 ***************************************************************************************/


function aofds_display_contact() { 

    $id = get_sub_field('contact_object');
    
    //setup_postdata(get_post($id));

    ?>

    <div class="panel panel-default">
        
        <?php if ( $name = get_field( 'contact_name', $id ) ) : ?>
        
            <div class="panel-heading">
                <?php echo $name; ?>
            </div>
        
        <?php endif; ?>
        
        
        <div class="panel-body">
            
            <?php

            if ( $image = get_field( 'contact_image', $id ) ) 
                echo '<div class="image"><img src="'.$image['sizes']['thumbnail'].'" /></div>';
    
    
            if ( have_rows( 'contact_flexible', $id ) ):

                while ( have_rows( 'contact_flexible', $id ) ) : the_row();

                    switch ( get_row_layout() ) :

                        case 'address' :
                            aofds_display_address();
                            break;

                        case 'phone' :
                            aofds_display_phone();
                            break;

                        case 'mobile' :
                            aofds_display_mobile();
                            break;

                        case 'email' :
                            aofds_display_email();
                            break;

                        case 'web' :
                            aofds_display_web();
                            break;

                        case 'motto' :
                            aofds_display_motto();
                            break;

                        case 'images' :
                            aofds_display_images();
                            break;

                    endswitch;

                endwhile;

            endif;
    
            ?>
                        
        </div>
    
    </div>
 
    <?php
    
    //wp_reset_postdata();
}



/***************************************************************************************
 	ADDRESS
 ***************************************************************************************/


function aofds_display_address() { 

    echo '<address class="address">'.get_sub_field('address_text').'</address>';
}



/***************************************************************************************
 	PHONE
 ***************************************************************************************/


function aofds_display_phone() { 

    echo '<div class="phone"><span class="fa fa-phone"></span>'.get_sub_field('phone_text').'</div>';
}



/***************************************************************************************
 	MOBILE
 ***************************************************************************************/


function aofds_display_mobile() { 

    echo '<div class="mobile"><span class="fa fa-mobile"></span>'.get_sub_field('mobile_text').'</div>';
}



/***************************************************************************************
 	EMAIL
 ***************************************************************************************/


function aofds_display_email() { 

    echo '<div class="email"><span class="fa fa-enveloppe"></span>'.antispambot(get_sub_field('email_text')).'</div>';
}



/***************************************************************************************
 	WEB
 ***************************************************************************************/


function aofds_display_web() { 

    echo '<div class="web"><span class="fa fa-globe"></span><a href="'.get_sub_field('web_text').'" target="_blank">'.get_sub_field('web_text').'</a></div>';
}



/***************************************************************************************
 	MOTTO
 ***************************************************************************************/


function aofds_display_motto() { 

    echo '<div class="motto">'.get_sub_field('motto_text').'</div>';
}



/***************************************************************************************
 	FACEBOOK
 ***************************************************************************************/


function aofds_display_facebook() { 
}



/***************************************************************************************
 	DESCRIPTION
 ***************************************************************************************/


function aofds_display_description() { 
}




/* End of file aofds-custom-layouts.php */
/* Location: ./wp-content/themes/aofds/inc/aofds-custom-layouts.php */