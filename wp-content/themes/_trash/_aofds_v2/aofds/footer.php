<?php
/** footer.php
 *
 * Displays the footer
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

                        </div>
                        <!--/#site-main -->

                    </div>
                    <!--/#site-main-column -->

                </div>
                <!--/#site-center -->

                <div id="site-footer">

                    <?php get_sidebar( 'footer' ); ?>

                    <div id="map"></div>
                    <!--/#map -->

                    <div id="footer-menu">
                        <div class="container">
                            <?php wp_nav_menu( array('theme_location' => 'footer-menu') ); ?>
                        </div>
                    </div>
                    <!--/#footer menu -->

                    <div id="copyright">
                        <div class="container">	
                            <p>Copyright &copy; <?php echo date('Y'); ?> Foret de Soignes  - <?php _e('All rights reserved', 'aofds'); ?>.</p>
                            <?php wp_nav_menu( array('container_class' => 'legal-menu', 'theme_location' => 'legal-menu') ); ?>
                            <div class="designed-by">
                                <a href="http://www.alleyoop.be" target="_blank">	
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/alleyoop.png" title="<?php _e('Designed by Alley Oop', 'aofds'); ?>" alt="<?php _e('Designed by Alley Oop', 'aofds'); ?>" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--/#copyright -->	

                    <?php wp_footer(); ?>

                </div>
        
            </div>
            <!--/.row -->
                         
        </div>
        <!--/#site -->
        
    </body>
    
</html>
           
<?php


/* End of file footer.php */
/* Location: ./wp-content/themes/aothemefds/footer.php */