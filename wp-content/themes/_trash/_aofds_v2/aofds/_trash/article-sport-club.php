<?php
/** article-club.php
 *
 * The template for displaying the club section of a sport article.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<!-- Club -->
<div class="club">
    
    <?php if ($image = get_field('sport_club_logo')) : ?>
        <div class="logo wp-caption alignleft">    
            <?php $caption = get_field('sport_club_logo_caption'); ?>
            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />
            <?php if ($caption) : ?>
                <p class="wp-caption-text"><?php echo $caption; ?></p>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    
    <?php if ($text = get_field('sport_club_intro_text')) echo '<div class="text">'.$text.'</div>'; ?>

    <?php if ($s = count(get_field('sport_club_schedule_list'))) : ?>

        <?php if ($title = get_field('sport_club_schedule_title')) echo '<h3>'.$title.'</h3>'; ?>

        <?php if ($text = get_field('sport_club_schedule_text')) echo '<div class="text">'.$text.'</div>'; ?>
    
        <div class="table-responsive effect schedule">
            <table class="table table--bordered table-striped">
                <thead>
                    <tr>
                        <th><?php _e('Day', 'aofds'); ?></th>
                        <th><?php _e('Time', 'aofds'); ?></th>
                        <th><?php _e('Location', 'aofds'); ?></th>
                        <th><?php _e('Type', 'aofds'); ?></th>
                        <th><?php _e('Level', 'aofds'); ?></th>
                        <th><?php _e('Age', 'aofds'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php while (has_sub_field('sport_club_schedule_list')) : ?>
                    <tr>
                        <td class="day"><?php the_sub_field('sport_club_schedule_list_day'); ?></td>
                        <td class="time"><?php the_sub_field('sport_club_schedule_list_time'); ?></td>
                        <td class="court">
                            <?php if ($court = get_sub_field('sport_club_schedule_list_infrastructure')) : ?>
                                <a href="<?php echo $court->guid; ?>" title="<?php _e('See infrastructure', 'aofds'); ?>">
                                    <?php echo $court->post_title; ?>
                                </a>
                            <?php endif; ?>
                        </td>
                        <td class="type"><?php the_sub_field('sport_club_schedule_list_type'); ?></td>
                        <td class="level"><?php the_sub_field('sport_club_schedule_list_level'); ?></td>
                        <td class="age"><?php the_sub_field('sport_club_schedule_list_age'); ?></td>
                    </tr>
                <?php endwhile; ?>
                </tbody>
            </table>
        </div>

    <?php endif; ?>


    <?php 
    $has_contact = count(get_field('sport_club_trainers_list'));
    $has_facebook = get_field('sport_club_facebook');
    $has_album = get_field('sport_club_album_text');
    ?>
    
    <?php if ($has_contact) : ?>

        <?php if ($has_facebook and !$has_album) : ?>
            <div class="row"> 
                <div class="col-md-6 col-sm-6 col-xs-12">
        <?php endif; ?>
   
        <?php if ($title = get_field('sport_club_trainers_title')) echo '<h3>'.$title.'</h3>'; ?>

        <?php if ($text = get_field('sport_club_trainers_text')) echo '<div class="text">'.$text.'</div>'; ?>
    
        <?php
        $c = count(get_field('sport_club_trainers_list'));
        if ($c <= 3) $grid = 12/$c;
        else $grid = 4;
        ?>
        <div class="contacts"> 
            <div class="btn btn-success btn-lg">
                <div class="row"> 
                    <?php while (has_sub_field('sport_club_trainers_list')) : ?>
                        <div class="col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-12 effect">
                            <?php 
                            $name    	= get_sub_field('sport_club_trainers_list_name');
                            $phone 		= get_sub_field('sport_club_trainers_list_phone');
                            $email 		= get_sub_field('sport_club_trainers_list_email');	
                            $webpage 	= get_sub_field('sport_club_trainers_list_webpage');	
                            if ($name)	    echo $name.'<br/>';
                            if ($phone) 	echo '<i class="fa fa-phone"></i>'.$phone.'<br/>';
                            if ($email)		echo '<i class="fa fa-envelope"></i><a href="mailto:'.antispambot($email).'">'.antispambot($email).'</a><br/>';
                            if ($webpage) 	echo '<i class="fa fa-link"></i><a href="'.$webpage.'" target="_blank">'.$webpage.'</a><br/>';		
                            ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if ($has_album and $has_facebook) : ?>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
    <?php endif; ?>

    <?php if ($has_album) : ?>
        <?php if ($title = get_field('sport_club_album_title')) echo '<h3>'.$title.'</h3>'; ?>
        <?php if ($text = get_field('sport_club_album_text')) echo '<div class="text">'.$text.'</div>'; ?>
    <?php endif; ?>

    <?php if ($has_facebook) : ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <h3><?php _e('Page Facebook', 'aolawolue'); ?></h3>
            <iframe src="//www.facebook.com/plugins/likebox.php?href=<?php echo urlencode(get_field('sport_club_facebook')); ?>&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=196698483722882" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px; width:100%" allowTransparency="true"></iframe>
        </div>
    </div>
    <?php endif; ?>

</div>
<!--/Club -->

<?php
/* End of file article-club.php */
/* Location: ./wp-content/themes/aothemefds/article-club.php */