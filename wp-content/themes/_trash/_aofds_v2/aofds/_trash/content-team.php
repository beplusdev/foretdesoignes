<?php
/** content-team.php
 *
 * The template for displaying all team posts.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php get_template_part('article', 'default-header'); ?>
    	
    <!-- Entry content -->
    <div class="entry-content">
            
        <?php the_content(); ?>
        
        <!-- Entry specific -->
        <div class="entry-specific">
                                
            <?php if ($c = count(get_field('team_contacts_list'))) : ?>

                <div class="contacts">    
                   <?php
                    $nbcolumns = get_field('team_contacts_nbcolumns');
                    $grid = 12/$nbcolumns;
                    ?>
                    <div class="row">
                        <?php while (has_sub_field('team_contacts_list')) : ?>
                            <div class="contact <?php if ($nbcolumns>1) echo 'cols'; ?> nbcols-<?php echo $nbcolumns; ?> col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-6 effect">
                                
                                <?php 
                                if (get_sub_field('team_contacts_list_existing_contact')) :
                                    $post = get_sub_field('team_contacts_list_contact');
                                    setup_postdata($post);
                                    $image    	= get_field('contact_image');
                                    $name    	= get_field('contact_name');
                                    $title 		= get_field('contact_title');
                                    $address 	= get_field('contact_address');
                                    $infos 		= get_field('contact_infos');
                                    $motto	 	= get_field('contact_motto');
                                    $text 		= get_field('contact_text');	
                                    $prefix     = '';
                                else :
                                    $image    	= get_sub_field('team_contacts_list_contact_image');
                                    $name    	= get_sub_field('team_contacts_list_contact_name');
                                    $title 		= get_sub_field('team_contacts_list_contact_title');	
                                    $address 	= get_sub_field('team_contacts_list_contact_address');
                                    $infos 		= get_sub_field('team_contacts_list_contact_infos');
                                    $motto 		= get_sub_field('team_contacts_list_contact_motto');
                                    $text	 	= get_sub_field('team_contacts_list_contact_text');
                                    $prefix     = 'team_contacts_list_';
                                endif;

                                $contact_infos = '';
                                while (has_sub_field($prefix.'contact_infos')) :
                                    if ($value = get_sub_field('contact_infos_value')) :
                                        switch (get_sub_field('contact_infos_type')) :
                                            case 'phone' :
                                                $contact_infos .= '<i class="fa fa-phone"></i>'.$value;
                                                break;
                                            case 'fax' :
                                                $contact_infos .= '<i class="fa fa-fax"></i>'.$value;
                                                break;
                                            case 'mobile' :
                                                $contact_infos .= '<i class="fa fa-mobile"></i>'.$value;
                                                break;
                                            case 'email' :
                                                $contact_infos .= '<i class="fa fa-envelope"></i><a href="mailto:'.antispambot($value).'">'.antispambot($value).'</a>';
                                                break;
                                            case 'webpage' :
                                                $contact_infos .= '<i class="fa fa-link"></i><a href="'.$value.'">'.$value.'</a>';
                                                break;
                                        endswitch;
                                        $contact_infos .= '<br/>';
                                    endif;
                                endwhile;

                                switch ($nbcolumns) :

                                    case '6' :
                                    case '4' :
                                    case '3' :
                                        if ($image) :
                                            echo '<div class="contact-image"><div class="inner">';
                                            echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$name.'" title="'.$name.'" />';
                                            if ($address or $contact_infos or $motto or $text) :
                                                echo '<div class="contact-more"><div class="table"><div class="table-cell">';
                                                if ($address)	echo '<address>'.$address.'</address>';
                                                echo $contact_infos;
                                                if ($motto) echo '<p class="motto"><q>'.$motto.'</q></p>';
                                                if ($text) 	echo '<div class="text">'.$text.'</div>';
                                                echo '</div></div></div>';
                                            endif;
                                            echo '</div></div>';
                                        endif;
                                        echo '<div class="contact-infos">';
                                        if ($name) 	    echo '<h4>'.$name.'</h4>';
                                        if ($title) 	echo '<h5>'.$title.'</h5>';
                                        echo '</div>';
                                        if (!$image) :
                                            if ($address or $contact_infos or $motto or $text) :
                                                if ($address)	echo '<address>'.$address.'</address>';
                                                echo $contact_infos;
                                                if ($motto) echo '<p class="motto"><q>'.$motto.'</q></p>';
                                                if ($text) 	echo '<div class="text">'.$text.'</div>';
                                            endif;
                                        endif;
                                        break;

                                    case '2' :
                                        if ($image) :
                                            echo '<div class="row">';
                                            echo '<div class="col-md-5 col-sm-6 col-xs-12">';
                                            echo '<div class="contact-image">';
                                            echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$name.'" title="'.$name.'" />';
                                            echo '</div>';
                                            echo '</div>';
                                            echo '<div class="col-md-7 col-sm-6 col-xs-12">';
                                        endif;
                                        echo '<div class="contact-infos">';
                                        if ($name) 	    echo '<h3>'.$name.'</h3>';
                                        if ($title) 	echo '<h4>'.$title.'</h4>';
                                        if ($address)	echo '<address>'.$address.'</address>';
                                        echo $contact_infos;
                                        echo '</div>';
                                        if ($image) :
                                            echo '</div></div>';
                                        endif;
                                        if ($motto or $text) :
                                            echo '<div class="contact-more"><div class="inner">';
                                            if ($motto) echo '<p class="motto"><q>'.$motto.'</q></p>';
                                            if ($text) 	echo '<div class="text">'.$text.'</div>';
                                            echo '</div></div>';
                                        endif;
                                        break;

                                    default :
                                        if ($image) :
                                            echo '<div class="row">';
                                            echo '<div class="col-md-3 col-sm-4 col-xs-12">';
                                            echo '<div class="contact-image">';
                                            echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$name.'" title="'.$name.'" />';
                                            echo '</div>';
                                            echo '</div>';
                                            echo '<div class="col-md-9 col-sm-8 col-xs-12">';
                                        endif;
                                        echo '<div class="row">';
                                        echo '<div class="col-md-6 col-sm-6 col-xs-12">';
                                        if ($name) 	    echo '<h2>'.$name.'</h2>';
                                        if ($title) 	echo '<h3>'.$title.'</h3>';
                                        if ($address)	echo '<address>'.$address.'</address>';
                                        echo '</div>';
                                        echo '<div class="col-md-6 col-sm-6 col-xs-12">';
                                        echo $contact_infos;
                                        echo '</div>';
                                        echo '<div class="col-md-12 col-sm-12 col-xs-12">';
                                        if ($motto) echo '<p><q>'.$motto.'</q></p>';
                                        if ($text) 	echo $text;
                                        echo '</div>';
                                        echo '</div>';
                                        if ($image) :
                                            echo '</div></div>';
                                        endif;
                                        if ($c > 1) echo '<hr/>';
                                        break;

                                endswitch;
                                if (get_sub_field('team_contacts_list_existing_contact')) :
                                    wp_reset_postdata();
                                endif;
                                ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            <?php endif; ?>
        
        </div>
        <!--/Entry specific -->

    </div>
    <!--/Entry content -->

    <?php get_template_part('article', 'footer'); ?>


</article><!-- #post -->

<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'aofds' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

			
<?php
/* End of file content-sport.php */
/* Location: ./wp-content/themes/aothemefds/content-sport.php */