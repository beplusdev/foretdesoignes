��          �      �       H  	   I     S     a  1   {     �  
   �     �     �  .   �     �           6     U  �   Y  	   S     ]     k  5   �     �     �     �     �  '   �        #   >      b     �                             	         
                           Address:  Coordinates:  Find the complete address Latitude and longitude to center the initial map. Map address Map center Map zoom No Return the address along with the coordinates. Search for a location This address couldn't be found:  This place couldn't be found:  Yes Project-Id-Version: acf-location-field
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
Language: fr_FR
X-Generator: Poedit 1.6.4
 Adresse : Coordonn�es : Trouver l'adresse complete Latitude et longitude du centre de la carte initiale. Adresse de la carte Centre de la carte Zoom de la carte Non Renvoyer l'adresse avec les coordonn�es Chercher un lieu, une adresse Cette adresse n'a pu �tre trouv�e : Cet endroit n'a pu �tre trouv� : Oui 