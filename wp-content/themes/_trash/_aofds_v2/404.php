<?php
/** 404.php
 *
 * The template for displaying 404 pages (Not Found).
 *
 * @author      Alley Oop
 * @package     AO Foret de Soignes
 * @since       1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

    <div class="container">

        <div class="row">

            <div class="col-md-8 col-sm-12 col-xs-12">

                <!-- Content -->
                <div id="content" class="site-content" role="main">

                    <?php get_template_part( 'content', 'none' ); ?>

                </div>
                <!--/Content -->

                <?php get_sidebar( 'bottom-content' ); ?>

            </div>

            <div class="col-md-4 col-sm-12 col-xs-12">

                <?php get_sidebar( 'right-content' ); ?>

            </div>

        </div><!--/.row -->

        <?php get_sidebar( 'bottom-page' ); ?>

    </div><!--/.container -->

<?php get_footer(); ?>

<?
/* End of file 404.php */
/* Location: ./wp-content/themes/aofds/404.php */
