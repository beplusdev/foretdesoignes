<?php
/** sidebar-header.php
 *
 * Displays the sidebar on the header
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */
?>

<!-- Sidebar Header -->
<?php if (is_active_sidebar( 'sidebar-header' )) : ?>
	<div id="sidebar-header">
		<?php dynamic_sidebar( 'sidebar-header' ); ?>
	</div>
<?php endif; ?>
<!--/Sidebar Header -->

<?
/* End of file sidebar-header.php */
/* Location: ./wp-content/themes/bpsmolsport/sidebar-header.php */