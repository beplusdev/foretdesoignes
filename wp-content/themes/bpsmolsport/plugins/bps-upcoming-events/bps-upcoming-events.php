<?php
/*
 * Plugin Name: BPS - Upcoming events 
 * Author URI: https://www.beplusports.com
 * Description: List of upcoming events.
 * Author: be+SPORTS
 * Version: 1.0
 */

## Constants definition

define('BPS_UPCOMING_EVENTS_VERSION', '1.0');
define('BPS_UPCOMING_EVENTS_AUTHOR', 'Simon Arickx');
define('BPS_UPCOMING_EVENTS_URL', get_template_directory_uri() . '/plugins/bps-upcoming-events/');

## Include the required styles

function bps_upcoming_events_css(){
	wp_register_style('bps-upcoming-events-css', BPS_UPCOMING_EVENTS_URL . 'css/bps-upcoming-events.css', array(), '0.5');
	wp_enqueue_style('bps-upcoming-events-css');
}
add_action('wp_enqueue_scripts', 'bps_upcoming_events_css');



class bps_upcoming_events_widget extends WP_Widget {

	function __construct(){
	
		// set text domain
		$domain = 'bps-upcoming-events';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $domain . '-' . get_locale() . '.mo';
		load_textdomain( $domain, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-bps-upcoming-events', 
			'description' => __( 'List of your upcoming events.', 'bps-upcoming-events') 
		);
		parent::__construct('bps-upcoming-events', __('Upcoming events', 'bps-upcoming-events'), $widget_ops);
		//$this->alt_option_name = 'bps_upcoming_events';

		//add_action( 'save_post', array($this, 'flush_widget_cache') );
		//add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		//add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('bps_upcoming_events', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Upcoming events', 'bps-upcoming-events' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
 			
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : true;
        $charlength = ( ! empty( $instance['charlength'] ) ) ? $instance['charlength'] : '100';
		$show_all_title = ( ! empty( $instance['show_all_title'] ) ) ? $instance['show_all_title'] : ''; //__( 'Show complete agenda', 'bps-upcoming-events' );

/*
        $today = new DateTime('today');
		//echo $today->getTimestamp();
		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'event', 
				'meta_query'=> array(
					array (
						'key' 		=> 'event_infos_start',
    	    		    'value' 	=> $today->getTimestamp(),
						'compare' 	=> '>=',
						'type' => 'NUMERIC',
					),
				),
				'sortby' 	=> 'get_most_viewed', 
				'orderby' 	=> 'meta_value', 
				'meta_key'	=> 'event_infos_start',
				'order' 	=> 'ASC', 
			)
		));
*/
        $today = new DateTime('today');
		//echo $today->getTimestamp();
		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'news', 
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'newscat',
						'field' 	=> 'slug',
						'terms' 	=> array( 'event' ),
					),
				),
				'meta_query'=> array(
					array (
						'key' 		=> 'event_start',
    	    		    'value' 	=> $today->getTimestamp(),
						'compare' 	=> '>='
					),
				),
				'sortby' 	=> 'get_most_viewed', 
				'orderby' 	=> 'meta_value', 
				'meta_key'	=> 'event_start',
				'order' 	=> 'ASC', 
			)
		));

        
        
		// echo $r->request; 
		echo $before_widget; 
		if ( $title ) echo $before_title . $title . $after_title; 
		
		if ($r->have_posts()) :
?>
			<div class="bps-upcoming-events">
                
                <ul>
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            
                            <?php if ( $show_date and ( $start = get_field('event_start')) ) : ?>
                                <span class="event-date">
                                    <span class="event-day"><?php echo date_i18n( 'j' , $start ); ?></span>
                                    <span class="event-month"><?php echo ucfirst(__(date_i18n( 'M' , $start ))); ?></span>
                                </span>
                                <span class="event-title">
                                    <?php get_the_title() ? the_title() : the_ID(); ?>
                                </span>
                            <?php else : ?>
                                <span class="event-title">
                                    <?php get_the_title() ? the_title() : the_ID(); ?> 
                                </span>
                            <?php endif; ?>
                            <p class="event-intro">
                                <?php 
                                //$text = strip_tags(get_the_content());
                                //echo (strlen($text) > $charlength) ? substr($text,0,$charlength).'...' : $text; 
                                ?>
                                <?php 
                                if (!$excerpt = get_the_excerpt()) :
                                    $excerpt = get_the_content();
                                endif;
                                $excerpt = wp_strip_all_tags($excerpt);
                                if ( mb_strlen( $excerpt ) > $charlength ) :
                                    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                                    $exwords = explode( ' ', $subex );
                                    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                                    if ( $excut < 0 ) :
                                        echo mb_substr( $subex, 0, $excut );
                                    else :
                                        echo $subex;
                                    endif;
                                    echo '...';
                                else :
                                    echo $excerpt;
                                endif;
                                ?>
                            </p>
                            
                        </a>
                    </li>
                <?php endwhile; ?>
                </ul>

                <?php if ($show_all_title) : ?>
                <div class="buttons">
                    <a class="btn btn-success" href="<?php echo get_term_link( 'event', 'newscat' ); ?>"><?php echo $show_all_title; ?></a>
                </div>
                <?php endif; ?>
                
            </div>

<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no upcoming events', 'bps-upcoming-events').'</p>';
		endif;

		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('bps_upcoming_events', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : true;
        $instance['charlength']     = isset( $instance['charlength'] ) ? $new_instance['charlength'] : '100';
		$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['bps_upcoming_events']) )
			delete_option('bps_upcoming_events');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('bps_upcoming_events', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : true;
        $charlength = isset( $instance['charlength'] ) ? $instance['charlength'] : '100';
		$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'bps-upcoming-events' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'bps-upcoming-events' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?', 'bps-upcoming-events' ); ?></label></p>

		<p>
            <label for="<?php echo $this->get_field_id( 'charlength' ); ?>"><?php _e( 'Number of chars for the text:', 'bps-last-news' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'charlength' ); ?>" name="<?php echo $this->get_field_name( 'charlength' ); ?>" type="text" value="<?php echo $charlength; ?>" size="3" />
        </p>

		<p><label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'bps-upcoming-events' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" /></p>
<?php
	}
}

function bps_upcoming_events_init(){
	register_widget('bps_upcoming_events_widget');
}
add_action('widgets_init', 'bps_upcoming_events_init');
