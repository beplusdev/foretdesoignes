/*
Plugin Name: BPS Last News
Author URI: https://www.beplusports.com
Description: A last news widget.
Author: be+SPORTS
Version: 1.0
*/

jQuery(function(){
    jQuery('.last-news .carousel').carousel({
      interval: 6000
    });
});
