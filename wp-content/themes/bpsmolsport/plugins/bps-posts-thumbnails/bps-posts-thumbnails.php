<?php
/*
 * Plugin Name: BPS - Posts Thumbnails 
 * Author URI: https://www.beplusports.com
 * Description: List of upcoming events.
 * Author: be+SPORTS
 * Version: 1.0
 */

## Constants definition

define('BPS_POSTS_THUMBNAILS_VERSION', '1.0');
define('BPS_POSTS_THUMBNAILS_AUTHOR', 'be+SPORTS');
define('BPS_POSTS_THUMBNAILS_URL', get_template_directory_uri() . '/plugins/bps-posts-thumbnails/');

## Include the required styles

function bps_posts_thumbnails_css(){
	wp_register_style('bps-posts-thumbnails-css', BPS_POSTS_THUMBNAILS_URL . 'css/bps-posts-thumbnails.css', array(), '0.4');
	wp_enqueue_style('bps-posts-thumbnails-css');
}
add_action('wp_enqueue_scripts', 'bps_posts_thumbnails_css');

## Include the required scripts

function bps_posts_thumbnails_scripts(){
	wp_enqueue_script('bps-posts-thumbnails-js', BPS_POSTS_THUMBNAILS_URL . 'js/bps-posts-thumbnails.js', array(), '0.1');
}
add_action('wp_enqueue_scripts', 'bps_posts_thumbnails_scripts');


class bps_posts_thumbnails_widget extends WP_Widget {

	function __construct(){
	
		// set text domain
		$domain = 'bps-posts-thumbnails';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $domain . '-' . get_locale() . '.mo';
		load_textdomain( $domain, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-bps-posts-thumbnails', 
			'description' => __( 'Displays potss thumbnails.', 'bps-posts-thumbnails') 
		);
		parent::__construct('bps-posts-thumbnails', __('Posts thumbnails', 'bps-posts-thumbnails'), $widget_ops);
		//$this->alt_option_name = 'bps_posts_thumbnails';

		//add_action( 'save_post', array($this, 'flush_widget_cache') );
		//add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		//add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('bps_posts_thumbnails', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Posts thumbnails', 'bps-posts-thumbnails' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$post_type = ( ! empty( $instance['post_type'] ) ) ? $instance['post_type'] : 'post';
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
 			
		$show_title = isset( $instance['show_title'] ) ? $instance['show_title'] : true;

		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' 	=> 'publish', 
				'posts_per_page' => ($number-1), 
				'post_type' 	=> $post_type, 
				'orderby'		=> 'rand', //'menu_order',
				'order'			=> 'ASC',
			)
		));
		
		// echo $r->request; 
		echo $before_widget; 
		if ( $title ) echo $before_title . $title . $after_title; 
		
		if ($r->have_posts()) :
?>
			<div class="row">
			<?php $first = true; ?>
			<?php $i = 0; ?>
			<?php while ( $r->have_posts() ) : $r->the_post(); ?>
                <?php if ($i < $number) : ?>
                    <?php $image = get_field('images_thumbnail'); ?>
                    <?php if ($i % 2 == 0) : ?>
                        <?php if (!$first) : ?>
                            </div></div>
                        <?php else : ?>
                            <?php $first = false; ?>
                        <?php endif; ?>
                        <div class="col-md-3 col-sm-3 col-xs-6 effect">
                        <div class="row">
                    <?php endif; ?>
                    <div class="col-md-6 col-sm-6 col-xs-6 effect">
                        <div class="post-thumbnail" id="post-thumbnail-<?php the_ID(); ?>">
                            <a href="<?php the_permalink(); ?>" class="image">
                                <?php if ($image) : ?>
                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
                                <?php elseif ($default = get_option('default_sport_image')) : ?>
                                    <img src="<?php echo $default; ?>" class="default" />
                                <?php endif; ?>
                            </a>
                            <a href="<?php the_permalink(); ?>" class="caption">
                                <div class="title">
                                    <?php 
                                    if ($title = get_field('images_thumbnail_name')) :
                                        echo $title;
                                    else :
                                        echo (strlen(get_the_title()) > 15) ? substr(get_the_title(), 0, 15).'...' : get_the_title(); 
                                    endif;
                                    ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php $i = $i + 1; ?>
                <?php endif; ?>
			<?php endwhile; ?>
            
            <?php if ($i % 2 == 0) : ?>
                <?php if (!$first) : ?>
                    </div></div>
                <?php else : ?>
                    <?php $first = false; ?>
                <?php endif; ?>
                <div class="col-md-3 col-sm-3 col-xs-6 effect">
                <div class="row">
            <?php endif; ?>
            <div class="col-md-6 col-sm-6 col-xs-6 effect">
                <div class="post-thumbnail" id="post-thumbnail-see-more">
                    <a href="<?php echo get_post_type_archive_link( $post_type ); ?>" class="image">
                        <i class="fa fa-plus-circle"></i>
                    </a>
                    <a href="<?php echo get_post_type_archive_link( $post_type ); ?>" class="caption">
                        <div class="title">
                            <?php _e('See more', 'bps-posts-thumbnails'); ?>
                        </div>
                    </a>
                </div>
            </div>

            </div>

<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no thumbnails', 'bps-posts-thumbnails').'</p>';
		endif;

		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('bps_posts_thumbnails', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['post_type'] = strip_tags($new_instance['post_type']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_title'] = isset( $new_instance['show_title'] ) ? (bool) $new_instance['show_title'] : true;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['bps_posts_thumbnails']) )
			delete_option('bps_posts_thumbnails');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('bps_posts_thumbnails', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$post_type     = isset( $instance['post_type'] ) ? esc_attr( $instance['post_type'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_title = isset( $instance['show_title'] ) ? (bool) $instance['show_title'] : true;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'bps-posts-thumbnails' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:', 'bps-posts-thumbnails' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" type="text" value="<?php echo $post_type; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'bps-posts-thumbnails' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_title ); ?> id="<?php echo $this->get_field_id( 'show_title' ); ?>" name="<?php echo $this->get_field_name( 'show_title' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_title' ); ?>"><?php _e( 'Display post title?', 'bps-posts-thumbnails' ); ?></label></p>
<?php
	}
}

function bps_posts_thumbnails_init(){
	register_widget('bps_posts_thumbnails_widget');
}
add_action('widgets_init', 'bps_posts_thumbnails_init');
