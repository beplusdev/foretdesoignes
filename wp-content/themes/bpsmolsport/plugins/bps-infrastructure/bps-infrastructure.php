<?php
/*
 * Plugin Name: BPS - Posts Thumbnails 
 * Author URI: https://www.beplusports.com
 * Description: List of upcoming events.
 * Author: be+SPORTS
 * Version: 1.0
 */

## Constants definition

define('BPS_INFRASTRUCTURE_VERSION', '1.0');
define('BPS_INFRASTRUCTURE_AUTHOR', 'be+SPORTS');
define('BPS_INFRASTRUCTURE_URL', get_template_directory_uri() . '/plugins/bps-infrastructure/');

## Include the required styles

function bps_infrastructure_css(){
	wp_register_style('bps-infrastructure-css', BPS_INFRASTRUCTURE_URL . 'css/bps-infrastructure.css', array(), '0.4');
	wp_enqueue_style('bps-infrastructure-css');
}
add_action('wp_enqueue_scripts', 'bps_infrastructure_css');

## Include the required scripts

function bps_infrastructure_scripts(){
	wp_enqueue_script('bps-infrastructure-js', BPS_INFRASTRUCTURE_URL . 'js/bps-infrastructure.js', array(), '0.2');
}
add_action('wp_enqueue_scripts', 'bps_infrastructure_scripts');



class bps_infrastructure_widget extends WP_Widget {

	function __construct(){
	
		// set text domain
		$domain = 'bps-infrastructure';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $domain . '-' . get_locale() . '.mo';
		load_textdomain( $domain, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-bps-infrastructure', 
			'description' => __( 'Displays infrastructure.', 'bps-infrastructure') 
		);
		parent::__construct('bps-infrastructure', __('Infrastructure', 'bps-infrastructure'), $widget_ops);
		//$this->alt_option_name = 'bps_infrastructure';

		//add_action( 'save_post', array($this, 'flush_widget_cache') );
		//add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		//add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('bps_infrastructure', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Infrastructure', 'bps-infrastructure' );
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$post_type = ( ! empty( $instance['post_type'] ) ) ? $instance['post_type'] : 'post';
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
 			
		$show_title = isset( $instance['show_title'] ) ? $instance['show_title'] : true;

		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => $post_type, 
				'orderby'		=> 'menu_order',
				'order'			=> 'ASC',
			)
		));
		
		// echo $r->request; 
		echo $before_widget; 
		if ( $title ) echo $before_title . $title . $after_title; 
		
		if ($r->have_posts()) :
?>
			<div class="row">
				<div class="col-md-9 col-sm-9 col-xs-12 effect">
	
					<?php $first = true; ?>
					<?php while ( $r->have_posts() ) : $r->the_post(); ?>
						<?php $image = get_field('images_thumbnail'); ?>
						<?php if ($image) : ?>
				
							<div class="infrastructure-main <?php echo ($first) ? 'show' : ''; ?>" id="infrastructure-main-<?php the_ID(); ?>">
								<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
								<div class="filter">
                                    <div class="caption">
                                        <div class="inner">
                                            <div class="title">
                                                <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                            </div>
                                            <?php the_content( false ) ?>
                                        </div>
                                    </div>
								</div>
							</div>
		
							<?php $first = false; ?>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
					
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="row">

						<?php while ( $r->have_posts() ) : $r->the_post(); ?>
							<?php $image = get_field('images_thumbnail'); ?>
							<?php if ($image) : ?>
					
								<div class="col-md-4 col-sm-4 col-xs-2 effect">
									<?php if (is_mobile()) : ?>
                                        <img class="infrastructure-thumbnail" id="infrastructure-thumbnail-<?php the_ID(); ?>" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
									<?php else : ?>
                                        <a href="<?php the_permalink(); ?>">
                                        <img class="infrastructure-thumbnail" id="infrastructure-thumbnail-<?php the_ID(); ?>" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
                                        </a>
									<?php endif; ?>
								</div>
					
							<?php endif; ?> 
						<?php endwhile; ?>
					</div>
				</div>
			</div>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no thumbnails', 'bps-infrastructure').'</p>';
		endif;

		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('bps_infrastructure', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['post_type'] = strip_tags($new_instance['post_type']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_title'] = isset( $new_instance['show_title'] ) ? (bool) $new_instance['show_title'] : true;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['bps_infrastructure']) )
			delete_option('bps_infrastructure');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('bps_infrastructure', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$post_type     = isset( $instance['post_type'] ) ? esc_attr( $instance['post_type'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_title = isset( $instance['show_title'] ) ? (bool) $instance['show_title'] : true;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'bps-infrastructure' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'post_type' ); ?>"><?php _e( 'Post Type:', 'bps-infrastructure' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'post_type' ); ?>" name="<?php echo $this->get_field_name( 'post_type' ); ?>" type="text" value="<?php echo $post_type; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'bps-infrastructure' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_title ); ?> id="<?php echo $this->get_field_id( 'show_title' ); ?>" name="<?php echo $this->get_field_name( 'show_title' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_title' ); ?>"><?php _e( 'Display post title?', 'bps-infrastructure' ); ?></label></p>
<?php
	}
}

function bps_infrastructure_init(){
	register_widget('bps_infrastructure_widget');
}
add_action('widgets_init', 'bps_infrastructure_init');
