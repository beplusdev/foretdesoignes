<?php
/*
 * Template for the output of the Widget
 * Override by placing a file called bps-last-news_template.php in your active theme
 */
	//// !!! THIS FIRST PART IS TO BUILD UP THE QUERY. DO NOT EDIT IT IF YOU'RE NOT SURE WHAT YOU'RE DOING
	
	// store the vars
	$intro = $instance['intro'];
	$max_entries = $instance['max_entries'];
	if ($max_entries=="0"){
		$max_entries= -1;
	}
	$filter_term_id = $instance['filter_term'];
	if ( in_array('any', $filter_term_id) ){
		//$filter_term_id = 'any';
		$homepage = get_term_by('slug', 'homepage', 'newscat');
		$event = get_term_by('slug', 'event', 'newscat');
		$args = array(
			'exclude'           => array($homepage->term_id,$event->term_id),
			'fields'            => 'ids'
		);
		$filter_term_id = get_terms( 'newscat', $args);
	}
	$post_type = $instance['filter_post_type'];
	$taxonomy = $instance['filter_taxonomy'];
	$order_by = $instance['order_by'];
	$order_style = $instance['order_style'];
	$include_children = $instance['include_children'];
	$display_thumb = $instance['display_thumb'];
	$thumb_max_width = $instance['thumb_max_width'];
	$display_excerpt = $instance['display_excerpt'];
	$excerpt_length = $instance['excerpt_length'];
	$display_date = $instance['display_date'];
	$display_in_dropdown = $instance['display_in_dropdown'];
	$exclude_posts = $instance['exclude_posts'];
	if ($exclude_posts!=""){
		$exclude_posts = explode("-", $exclude_posts);	
	}
	$meta_key_name = $instance['meta_key_name'];
	$meta_key_value = $instance['meta_key_value'];
	$meta_is_number = $instance['meta_is_number'];
	// if meta field is numeric and orderby is set to meta key, tweak it
	$meta_compare = $instance['meta_compare'];
	// if orderby is set to meta value, but no meta key name and value are passed, default to order by date
	// if meta field is numeric and orderby is set to meta value, tweak it to numeric order
	if ($order_by=="meta_value"){
		if ($meta_key_name=="none" || $meta_key_value==""){
			$order_by = "date";
		}
		if ($meta_is_number=="yes"){
			$order_by = "meta_value_num";
		}
	}
	// build up the args for the query
	$bps_last_news_args =  array(
		'numberposts' => $max_entries ,
		'post_type' => $post_type,
		'orderby' => $order_by,
		'order' => $order_style,
		'post_status' => 'publish',
		); 
	// if there are posts to be excluded, add the parameter
	if ( !empty($exclude_posts) ){
		$bps_last_news_args['post__not_in'] = $exclude_posts;
	}
	// if a meta key has been passed, add it to the query
	if ($meta_key_name!="none") {
		$bps_last_news_args['meta_key'] = $meta_key_name;
		if ($meta_key_value!="") {
			//  for order purposes
			if ($meta_is_number=="yes" && $order_by=="meta_value_num"){
				$bps_last_news_args['meta_value_num'] = $meta_key_value;	
			}
			elseif ($order_by=="meta_value") {
				$bps_last_news_args['meta_value'] = $meta_key_value;
			}
			// for fetching purposes
			if ($meta_is_number=="yes"){
				$meta_type= "NUMERIC";
			}
			else {
				$meta_type= "CHAR";
			}
			$bps_last_news_args['meta_query'][0] = array('key'=>$meta_key_name, 'value'=>$meta_key_value, 'compare'=>"$meta_compare", 'type'=>$meta_type);

		}
	}
	// if "any" term is selected, we have to retrieve a list of terms ids for the selected taxonomy
	if ($filter_term_id=="any"){
		$filter_term_id = get_terms( $taxonomy, array('fields' => 'ids'	) );
	}
	// add the taxonomy query to the args
	$bps_last_news_args['tax_query'] = array( 
		 array(
			'taxonomy' => $taxonomy, 
			'field' => 'id', 
			'terms' => $filter_term_id, 				
			'operator' => 'IN' 
			)
		);
	// add children inclusion to the tax query
	if ($include_children=="true"){
		$bps_last_news_args['tax_query'][0]['include_children'] = true;
	}
	else {
		$bps_last_news_args['tax_query'][0]['include_children'] = false;
	}

	
	//// THE LOOP STARTS HERE,
	// HERE YOU COULD EDIT THE HTML STRUCTURE OF THE OUTPUT

	// query the posts and print the list
	$posts = get_posts( $bps_last_news_args );
	if (!empty($posts)) :
        ?>
		
        <ul class="bps-last-news-list <?php echo ($display_thumb=="yes") ? 'with-thumb' : 'without-thumb'; ?> <?php echo ($display_date=="yes") ? 'with-date' : 'without-date'; ?> <?php echo ($display_excerpt=="yes") ? 'with-excerpt' : 'without-excerpt'; ?>"> 
                
            <?php foreach( $posts as $post ) :	?>

                <li class="bps-last-news-item">
                    
                    <?php if ($display_thumb=="yes") :?>
                    
                        <?php // display the image if any is set
                        if ($images_list = get_field('article_medias_images_list', $post->ID)) :
                            $images = array();
                            while (has_sub_field('article_medias_images_list', $post->ID)) :
                                if ($image = get_sub_field('article_medias_images_list_image', $post->ID)) :
                                    $images[] = $image;
                                endif;
                            endwhile;    
                            shuffle($images);
                            $url = $images[0]['sizes']['one-third'];
                        else : // display the no-thumb image instead
                            $url = get_template_directory_uri() . '/plugins/bps-last-news/images/no-thumb.jpg'; 
                        endif; 
                        ?>
                        <div class="bps-last-news-thumb" style="background-image:url('<?php echo $url; ?>');">
                        </div>	
                        <?php if ($display_date=="yes") : ?>
                            <span class="bps-last-news-date"><?php echo get_the_date( 'd/m', $post->ID ); ?></span> -
                        <?php endif; ?>
                        <a class="bps-last-news-post-title" href="<?php echo get_permalink( $post->ID ); ?>" title="<?php echo $post->post_title ?>"><?php echo wp_trim_words($post->post_title, 10, '...'); ?></a>              
                    
                    <?php else : ?>

                        <?php if ($display_date=="yes") : ?>
                            <span class="bps-last-news-date"><?php echo get_the_date( 'd/m', $post->ID ); ?></span> - 
                        <?php endif; ?>
                        <a class="bps-last-news-post-title" href="<?php echo get_permalink( $post->ID ); ?>" title="<?php echo $post->post_title ?>"><?php echo wp_trim_words($post->post_title, 30, '...'); ?></a>              

                    <?php endif; ?>

                    <?php /* sarickx--- 
                    // display the excerpt if required
                    if ($display_excerpt=="yes") :
                        ?>
                        <div class="bps-last-news-excerpt">
                            <?php // get the excerpt but display only the number of characters set in the options
                            $excerpt = wp_trim_excerpt( strip_tags( strip_shortcodes($post->post_content) ) ); 
                            $excerpt = substr ( $excerpt, 0, $excerpt_length ); echo $excerpt.' [...]'; ?>                          
                        </div>
                    <?php endif; 
                    ---sarickx */ ?>

                </li>                	

            <?php endforeach; ?>   
            
        </ul>
        
    <?php else : ?>
		<?php _e( 'no matches','bps-last-news' ); ?>
	<?php endif; ?>
	

	<?php
	// reset the postdata
	wp_reset_postdata();
		
?>     

