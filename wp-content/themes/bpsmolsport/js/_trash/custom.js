
/*----------------------------------------------------------*/
/*                  JQUERY CAROUSEL                         */
/*----------------------------------------------------------*/
    if ($('.slidewrap').length && jQuery()) {
        $('.slidewrap').carousel({
                slider: '.slider',
                slide: '.slide',
                slideHed: '.slidehed',
                nextSlide : '.next',
                prevSlide : '.prev',
                addPagination: false,
                addNav : false
            });
    }
