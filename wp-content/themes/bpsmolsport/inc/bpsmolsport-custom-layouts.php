<?php
/** bpsmolsport-custom-layouts.php
 *
 * BPS Custom layouts added with ACF
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'bpsmolsport', get_template_directory() . '/lang' );



function bpsmolsport_display_layout($layout) {
    
    $html = '';
    
    switch ( $layout ) :

        case 'mainsection' :
            $html .= bpsmolsport_display_mainsection();
            break;

        case 'section' :
            $html .= bpsmolsport_display_section();
            break;

        case 'subsection' :
            $html .= bpsmolsport_display_subsection();
            break;

        case 'content' :
            $html .= bpsmolsport_display_content();
            break;

        case 'images' :
            $html .= bpsmolsport_display_images();
            break;

        case 'prices' :
            $html .= bpsmolsport_display_prices();
            break;

        case 'equipment_table' :
            $html .= bpsmolsport_display_equipment_table();
            break;

        case 'features_table' :
            $html .= bpsmolsport_display_features_table();
            break;

        case 'schedule_table' :
            $html .= bpsmolsport_display_schedule_table();
            break;

        case 'club_trainer' :
            $html .= bpsmolsport_display_club_trainer();
            break;

        case 'contact' :
            $html .= bpsmolsport_display_contact();
            break;

        case 'address' :
            $html .= bpsmolsport_display_address();
            break;

        case 'phone' :
            $html .= bpsmolsport_display_phone();
            break;

        case 'mobile' :
            $html .= bpsmolsport_display_mobile();
            break;

        case 'email' :
            $html .= bpsmolsport_display_email();
            break;

        case 'web' :
            $html .= bpsmolsport_display_web();
            break;

        case 'facebook' :
            $html .= bpsmolsport_display_facebook();
            break;

        case 'description' :
            $html .= bpsmolsport_display_description();
            break;

    endswitch;
    
    return $html;
}



/***************************************************************************************
 	MAINSECTION
 ***************************************************************************************/


function bpsmolsport_display_mainsection() { 

    return '<h1 class="mainsection">'.get_sub_field('mainsection_title').'</h1>';
}



/***************************************************************************************
 	SECTION
 ***************************************************************************************/


function bpsmolsport_display_section() { 

    return '<h2 class="section">'.get_sub_field('section_title').'</h2>';
}



/***************************************************************************************
 	SUBSECTION
 ***************************************************************************************/


function bpsmolsport_display_subsection() { 

    return '<h3 class="subsection">'.get_sub_field('subsection_title').'</h3>';
}



/***************************************************************************************
 	CONTENT
 ***************************************************************************************/


function bpsmolsport_display_content() { 

    return '<div class="content">'.get_sub_field('content_text').'</div>';
}



/***************************************************************************************
 	IMAGES
 ***************************************************************************************/


function bpsmolsport_display_images() { 
    
    $html = '';
    
    // check if the nested repeater field has rows of data
    if ( have_rows( 'images_repeater' ) ) :
    
        $images = array();
        $caption = array();

        // loop through the rows of data
        while ( have_rows('images_repeater') ) : the_row();

            if (get_sub_field('images_repeater_image')) :
                $images[] = get_sub_field('images_repeater_image');
                $captions[] = get_sub_field('images_repeater_caption');
            endif;
    
        endwhile;
    
        //var_dump($images);
        //var_dump($captions);
    
        switch ( get_sub_field('images_options') ) :

            case 'gallery' :
                $html .= bpsmolsport_display_gallery($images, $captions);
                break;

            case 'carousel' :
                $html .= bpsmolsport_display_carousel($images, $captions);
                break;

        endswitch;
    
    endif;
    
    return $html;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function bpsmolsport_display_carousel($medias, $descriptions) {

    $html = '';
    
    if ( $n = count( $medias ) ) :
    
        // We generate a random id to be able to have more than one carousel on the same page
        $id = 'carousel-'.rand(0, 999999);

        $html .= '<div id="'.$id.'" class="carousel slide" data-ride="carousel" data-interval="false">';
            
            if ($n > 1) : 
                $html .= '<ol class="carousel-indicators">';
                $first = true;
                foreach ($medias as $i => $media) :
                    $active = '';
                    if ($first) { 
                        $active = 'class="active"'; 
                        $first = false; 
                    }
                    $html .= '<li data-target="#'.$id.'" data-slide-to="'.$i.'" '.$active.'></li>';
                endforeach;              
                $html .= '</ol>';
            endif; 
            
            $html .= '<div class="carousel-inner" role="listbox">';
                $first = true; 
                foreach ($medias as $i => $media) :
                    $active = '';
                    if ($first) { 
                        $active = 'active'; 
                        $first = false; 
                    }
                    $html .= '<div class="item '.$active.'" id="carousel-item-'.$i.'">';
                        $html .= '<a href="'.$media['sizes']['large'].'" rel="lightbox[gallery-]" title="'.$descriptions[$i].'">';
                            $image_width = (is_mobile()) ? 'one-third' : 'two-third';
                            $html .= '<div class="image" style="background-image: url('.$media['sizes'][$image_width].');"></div>';
                            if ($descriptions[$i]) : 
                                $html .= '<div class="carousel-caption">'.$descriptions[$i].'</div>';
                            endif;
                        $html .= '</a>';
                    $html .= '</div>';
                endforeach;
                
            $html .= '</div>';
            
            if ($n > 1) :
                $html .= '
                <a class="left carousel-control" href="#'.$id.'" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">'.__('Previous', 'bpsmolsport').'</span>
                </a>
                <a class="right carousel-control" href="#'.$id.'" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">'.__('Next', 'bpsmolsport').'</span>
                </a>';
            endif;
    
        $html .= '</div>';

    endif;
    
    return $html;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function bpsmolsport_display_gallery($medias, $descriptions) {

    $html .= '<div class="row gallery">';

        $n = count($medias);
        foreach ($medias as $i => $media) : 
            if ($n >= 4) :
                $html .= '<div class="col col-lg-3 col-md-3 col-sm-3 col-xs-4">';
                $image_width = 'thumbnail';
            elseif ($n >= 3) :
                $html .= '<div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">';
                $image_width = 'thumbnail';
            elseif ($n >= 2) :
                $html .= '<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">';
                $image_width = 'one-third'; 
            else :
                echo '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
                $image_width = (is_mobile()) ? 'one-third' : 'two-third';
            endif;

            $html .= '
                <div class="item">
                    <a href="'.$media['sizes']['large'].'" rel="lightbox[gallery-medias]" title="'.$descriptions[$i].'">
                        <img src="'.$media['sizes'][$image_width].'" alt="'.$descriptions[$i].'">
                    </a>';
                    if ($descriptions[$i]) :
                        $html .= '<div class="description">'.$descriptions[$i].'</div>';
                    endif;
                $html .= '</div>';
            $html .= '</div>';
        endforeach;
    $html .= '</div>';

    return $html;
}



/***************************************************************************************
 	EVENT
 ***************************************************************************************/


function bpsmolsport_display_layout_event() {
    
    $html = '';
    
	if (get_field('event_start') || get_field('event_location')) : 

        $html .= '<div class="event-details">';

            if ($start = get_field('event_start')) : 
    
                $html .= '<p>';
    
                    if ($end = get_field('event_end')) :
                        $html .= '<i class="fa fa-calendar"></i> ' . __( 'From', 'bpsmolsport' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'bpsmolsport' ) . date_i18n( ' G:i ' , $start ) . __( 'to', 'bpsmolsport' ) . date_i18n( ' l d/m ' , $end ) . __( 'at', 'bpsmolsport' ) . date_i18n( ' G:i' , $end );
                    else :
                        $html .= '<i class="fa fa-calendar"></i> ' . __( 'On', 'bpsmolsport' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'bpsmolsport' ) . date_i18n( ' G:i' , $start );
                    endif;	
                
                $html .= '</p>';
    
            endif; 
            
            if ( have_rows( 'event_location_flexible' ) ):
    
                while ( have_rows( 'event_location_flexible' ) ) : the_row();

                    switch ( get_row_layout() ) :
    
                        case 'event_location_infrastructure' :
                            $html .= bpsmolsport_display_event_location_infrastructure();
                            break;
    
                        case 'event_location_address' :
                            $html .= bpsmolsport_display_event_location_address();
                            break;
    
                        case 'event_location_map' :
                            $html .= bpsmolsport_display_event_location_map();
                            break;
    
                    endswitch;
    
                endwhile;
    
            endif;
            
        $html .= '</div>';

    endif;
    
    return $html;
}

  

function bpsmolsport_display_event_location_infrastructure() {
    
    $html = '';
    
    $locations = array();
    
    while (has_sub_field('event_location_infrastructure_repeater')) :
    
        $location = get_sub_field('event_location_infrastructure_repeater_object');
        $locations[] = '<a href="'.$location->guid.'">'.$location->post_title.'</a>';
   
    endwhile;
    
    $locations_list = implode(', ', $locations); 
    
    if ( $locations_list ) {
        
        //echo '<h3>'.__( 'Where ?', 'bpsmolsport' ).'</h3>';
        $html .= '<p><i class="fa fa-map-marker"></i> ' . $locations_list . '</p>';
        
    }    

    return $html;
}



function bpsmolsport_display_event_location_address() {
    
    $html = '';
    
    if ($location = get_sub_field('event_location_address_text')) :
    
        $html .= '<address><i class="fa fa-map-marker"></i> <a href="http://maps.google.com?q='.strip_tags($location).'" target="_blank">'.$location.'</a></address>';

    endif;
    
    return $html;
}



function bpsmolsport_display_event_location_map() {
    
    $html = '';
    
    if ( $location = get_sub_field('event_location_map_google') ) :
    
        $html .= '
        <p><i class="fa fa-map-marker"></i>'.$location['address'].'</p>
        <div class="map">
            <div id="gmap" class="gmap"></div> 
            <script type="text/javascript"> 
                jQuery(document).ready(function() {
                    var myLatlng = new google.maps.LatLng('.$location['coordinates'].');
                    var image = "'.get_template_directory_uri().'/images/basket-marker.png";
                    var myOptions = {
                        zoom: 14,
                        center: myLatlng,
                    };
                    var map = new google.maps.Map(document.getElementById("gmap"), myOptions);
                    var contentString = \'<div id="content">\'+
                          \'<div id="siteNotice">\'+
                          \'</div>\'+
                          \'<h4 id="firstHeading" class="firstHeading">'.get_the_title().'</h4>\'+
                          \'<div id="bodyContent">\'+
                          \'<p>'.$location['address'].'</p>\'+
                          \'</div>\'+
                          \'</div>\';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        icon: image,
                        map: map,
                        title: "'.get_the_title().'"
                    });
                    google.maps.event.addListener(marker, \'click\', function() {
                        infowindow.open(map,marker);
                    });
                });
            </script> 
        </div>';
    
    endif; 
    
    return $html;
}



/***************************************************************************************
 	PRICES
 ***************************************************************************************/


function bpsmolsport_display_prices() { 

    $html = '';
    
    $id = get_sub_field('prices_object');
    
    //setup_postdata(get_post($id));

    $price1_label = get_field( 'price1_label', $id ) ? get_field( 'price1_label', $id ) : get_option( 'price1_label' );
    $price1_caption = get_field( 'price1_caption', $id ) ? get_field( 'price1_caption', $id ) : get_option( 'price1_caption' );
    $price2_label = get_field( 'price2_label', $id ) ? get_field( 'price2_label', $id ) : get_option( 'price2_label' );
    $price2_caption = get_field( 'price2_caption', $id ) ? get_field( 'price2_caption', $id ) : get_option( 'price2_caption' );
    
    if( have_rows('price_repeater', $id) ):

        while ( have_rows('price_repeater', $id) ) : the_row();

            $start = get_sub_field('price_publish_start_date');
            $end = get_sub_field('price_publish_end_date');
            $today = date('Ymd');
            $firstTab = true;
            
            if ( ( !$start or ( $today >= $start ) ) and ( !$end or ( $today <= $end ) ) ) :

                $html .= '<h2>'.get_sub_field( 'price_title' ).'</h2>';
    
                if ( have_rows( 'price_flexible', $id ) ):
    
                    $temp = '';
                    $i = 0;
    
                    while ( have_rows( 'price_flexible', $id ) ) : the_row();

                        switch ( get_row_layout() ) :
    
                            case 'mainsection' :
                                $temp .= bpsmolsport_display_mainsection();
                                break;

                            case 'section' :
                                $temp .= bpsmolsport_display_section();
                                break;

                            case 'subsection' :
                                $temp .= bpsmolsport_display_subsection();
                                break;

                            case 'content' :
                                $temp .= bpsmolsport_display_content();
                                break;

                            case 'images' :
                                $temp .= bpsmolsport_display_images();
                                break;

                            case 'prices_table' :
                                $temp .= bpsmolsport_display_prices_table($price1_label, $price1_caption, $price2_label, $price2_caption);
                                break;

                            case 'equipment_table' :
                                $temp .= bpsmolsport_display_equipment_table();
                                break;
    
                            case 'tab' :
                                if ($firstTab) :
                                    $t = 'tabs-'.rand(0, 999999);
                                    $html .= $temp;
                                    $tabContent = array();
                                    $i = 0;
                                else :
                                    $tabContent[$i] = $temp;
                                    $i += 1;
                                endif;
                                $html .= bpsmolsport_display_tab($firstTab, $t.'-'.$i);
                                $firstTab = false;
                                $temp = '';
                                break;

                            case 'tabs_end' :
                                $tabContent[$i] = $temp;
                                $html .= bpsmolsport_display_tabs_end($tabContent, $t);
                                $firstTab = true;
                                $temp = '';
                                break;

                        endswitch;

                    endwhile;

                endif;
    
            endif;
    
        endwhile;

        if (!$firstTab) :
            $tabContent[$i] = $temp;
            $html .= bpsmolsport_display_tabs_end($tabContent, $t);
        endif;

        $html .= $temp;
    
    endif;
    
    return $html;
    //wp_reset_postdata();
}



function bpsmolsport_display_prices_table($label1, $caption1, $label2, $caption2) { 

    $html = '';
    
    if ( $nbrows = count( get_sub_field( 'prices_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        $html .= '
        <table class="table table-striped prices-table">
            <thead>
                <tr>
                    <th class="description">'.get_sub_field('prices_teable_description_label').'</th>
                    <th class="price price1" data-toggle="tooltip" data-placement="top" title="'.$caption1.'">'.$label1.'</th>
                    <th class="price price2" data-toggle="tooltip" data-placement="top" title="'.$caption2.'">'.$label2.'</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"><strong>'.$label1.'</strong>: '.$caption1.'<br><strong>'.$label2.'</strong>: '.$caption2.'</td>
                </tr>
            </tfoot>
            <tbody>';
                
                while ( has_sub_field( 'prices_table_repeater' ) ) :

                    $html .= '
                    <tr>
                        <td class="description">'.get_sub_field('prices_table_repeater_description').'</td>
                        <td class="price price1">'.get_sub_field('prices_table_repeater_price1').'</td>
                        <td class="price price2">'.get_sub_field('prices_table_repeater_price2').'</td>
                    </tr>';

                endwhile;

            $html .= '
            </tbody>
        </table>';
    

    endif;
    
    return $html;
}



/***************************************************************************************
 	EQUIPMENT
 ***************************************************************************************/


function bpsmolsport_display_equipment() { 
}


function bpsmolsport_display_equipment_table() { 
    
    $html = '';
    
    if ( $nbrows = count( get_sub_field( 'equipment_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        $html .= '
        <table class="table table-striped equipment-table">
            <thead>
                <tr>
                    <th class="equipment">'.((get_sub_field('equipment_table_element_label')) ? get_sub_field('equipment_table_element_label') : __('Equipment', 'bpsmolsport')).'</th>
                    <th class="price">'.__('Price', 'bpsmolsport').'</th>
                </tr>
            </thead>
            <tbody>';
                
                while ( has_sub_field( 'equipment_table_repeater' ) ) :

                    $html .= '
                    <tr>    
                        <td class="equipment">'.get_sub_field('equipment_table_repeater_element').'</td>
                        <td class="price">'.get_sub_field('equipment_table_repeater_price').'</td>
                    </tr>';

                endwhile;

            $html .= '
            </tbody>
        </table>';

    endif;
    
    return $html;
}



/***************************************************************************************
 	FEATURES
 ***************************************************************************************/


function bpsmolsport_display_features_table() { 
   
    $html = '';
    
    if ( $nbrows = count( get_sub_field( 'features_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        $html .= '<table class="table table-striped features-table"><tbody>';
                
        while ( has_sub_field( 'features_table_repeater' ) ) :

            $html .= '
                    <tr>    
                        <td class="feature">'.get_sub_field('features_table_repeater_element').'</td>
                        <td class="value">'.get_sub_field('features_table_repeater_value').'</td>
                    </tr>';

        endwhile;
    
        $html .= '</tbody></table>';

    endif;
    
    return $html;
}



/***************************************************************************************
 	SCHEDULE
 ***************************************************************************************/


function bpsmolsport_display_schedule_table() { 
    
    if ( $nbrows = count( get_sub_field( 'schedule_table_repeater' ) ) ) : 

    $html = '
        <div class="table-responsive effect schedule-table">
            <table class="table table--bordered table-striped">
                <thead>
                    <tr>
                        <th>'.__('Day', 'bpsmolsport').'</th>
                        <th>'.__('Time', 'bpsmolsport').'</th>
                        <th>'.__('Location', 'bpsmolsport').'</th>
                        <th>'.__('Type', 'bpsmolsport').'</th>
                        <th>'.__('Level', 'bpsmolsport').'</th>
                        <th>'.__('Age', 'bpsmolsport').'</th>
                    </tr>
                </thead>
                <tbody>';
    
                while (has_sub_field('schedule_table_repeater')) :
    
                    $html .= '
                    <tr>
                        <td class="day nowrap">'.get_sub_field('schedule_table_repeater_day').'</td>
                        <td class="time nowrap">'.get_sub_field('schedule_table_repeater_time').'</td>
                        <td class="court">';
                            if ($court = get_sub_field('schedule_table_repeater_infrastructure')) :
                                $html .= '<a href="'.$court->guid.'" title="'.__('See infrastructure', 'bpsmolsport').'">'.$court->post_title.'</a>';
                            endif;
                        $html .= '
                        </td>
                        <td class="type">'.get_sub_field('schedule_table_repeater_type').'</td>
                        <td class="level">'.get_sub_field('schedule_table_repeater_level').'</td>
                        <td class="age">'.get_sub_field('schedule_table_repeater_age').'</td>
                    </tr>';
                    
                endwhile;
                
                $html .= '
                </tbody>
            </table>
        </div>';

    endif;
    
    return $html;
}



/***************************************************************************************
 	TRAINER
 ***************************************************************************************/


function bpsmolsport_display_club_trainer() { 

    //$id = get_sub_field('contact_object');
    
    //setup_postdata(get_post($id));
    
    $html = '';

    $html .= '<div class="panel panel-default">';
        
        if ( $name = get_sub_field( 'club_trainer_name' ) ) :
        
            $html .= '<div class="panel-heading">'.$name.'</div>';
        
        endif;
        
        $html .= '<div class="panel-body">';
            
            if ( $image = get_sub_field( 'club_trainer_image' ) ) 
                $html .= '<div class="image"><img src="'.$image['sizes']['thumbnail'].'" /></div>';
    
    
            if ( have_rows( 'club_trainer_flexible' ) ):

                $html .= '<div class="details '.(($image) ? 'with-image' : '').'">';
                
                while ( have_rows( 'club_trainer_flexible' ) ) : the_row();

                    switch ( get_row_layout() ) :

                        case 'address' :
                            $html .= bpsmolsport_display_address();
                            break;

                        case 'phone' :
                            $html .= bpsmolsport_display_phone();
                            break;

                        case 'mobile' :
                            $html .= bpsmolsport_display_mobile();
                            break;

                        case 'email' :
                            $html .= bpsmolsport_display_email();
                            break;

                        case 'web' :
                            $html .= bpsmolsport_display_web();
                            break;

                        case 'motto' :
                            $html .= bpsmolsport_display_motto();
                            break;

                        case 'images' :
                            $html .= bpsmolsport_display_images();
                            break;

                        case 'facebook' :
                            $html .= bpsmolsport_display_facebook();
                            break;

                        case 'description' :
                            $html .= bpsmolsport_display_description();
                            break;

                    endswitch;

                endwhile;
    
                $html .= '</div>'; 

            endif;
    
        $html .= '</div>';
    
    $html .= '</div>';
 
    return $html;
    
    //wp_reset_postdata();
}



/***************************************************************************************
 	CONTACT
 ***************************************************************************************/


function bpsmolsport_display_contact() { 

    $id = get_sub_field('contact_object');
    
    //setup_postdata(get_post($id));

    $html = '';

    $html .= '<div class="panel panel-default">';
        
        if ( $name = get_field( 'contact_name', $id ) ) :
        
            $html .= '<div class="panel-heading">'.$name.'</div>';
        
        endif;
                
        $html .= '<div class="panel-body">';
            
            if ( $image = get_field( 'contact_image', $id ) ) 
                $html .= '<div class="image"><img src="'.$image['sizes']['thumbnail'].'" /></div>';
    
    
            if ( have_rows( 'contact_flexible', $id ) ):

                $html .= '<div class="details '.(($image) ? 'with-image' : '').'">';

                while ( have_rows( 'contact_flexible', $id ) ) : the_row();

                    switch ( get_row_layout() ) :

                        case 'address' :
                            $html .= bpsmolsport_display_address();
                            break;

                        case 'phone' :
                            $html .= bpsmolsport_display_phone();
                            break;

                        case 'mobile' :
                            $html .= bpsmolsport_display_mobile();
                            break;

                        case 'email' :
                            $html .= bpsmolsport_display_email();
                            break;

                        case 'web' :
                            $html .= bpsmolsport_display_web();
                            break;

                        case 'motto' :
                            $html .= bpsmolsport_display_motto();
                            break;

                        case 'images' :
                            $html .= bpsmolsport_display_images();
                            break;

                    endswitch;

                endwhile;
    
                $html .= '</div>';

            endif;
                          
        $html .= '</div>';
    
    $html .= '</div>';
 
    return $html;
    
    //wp_reset_postdata();
}



/***************************************************************************************
 	ADDRESS
 ***************************************************************************************/


function bpsmolsport_display_address() { 

    return '<address class="address">'.get_sub_field('address_text').'</address>';
}



/***************************************************************************************
 	PHONE
 ***************************************************************************************/


function bpsmolsport_display_phone() { 

    return '<div class="phone"><span class="fa fa-phone"></span><a href="tel:'.get_sub_field('phone_text').'">'.get_sub_field('phone_text').'</a></div>';
}



/***************************************************************************************
 	MOBILE
 ***************************************************************************************/


function bpsmolsport_display_mobile() { 

    return '<div class="mobile"><span class="fa fa-mobile"></span><a href="tel:'.get_sub_field('mobile_text').'">'.get_sub_field('mobile_text').'</a></div>';
}



/***************************************************************************************
 	EMAIL
 ***************************************************************************************/


function bpsmolsport_display_email() { 

    return '<div class="email"><span class="fa fa-envelope"></span><a href="mailto:' . antispambot( get_sub_field('email_text') ) . '">' . antispambot( get_sub_field('email_text') ) . '</a></div>';
}



/***************************************************************************************
 	WEB
 ***************************************************************************************/


function bpsmolsport_display_web() { 

    return '<div class="web"><span class="fa fa-globe"></span><a href="'.get_sub_field('web_text').'" target="_blank">'.get_sub_field('web_text').'</a></div>';
}



/***************************************************************************************
 	MOTTO
 ***************************************************************************************/


function bpsmolsport_display_motto() { 

    return '<div class="motto"><p>'.get_sub_field('motto_text').'</p></div>';
}



/***************************************************************************************
 	FACEBOOK
 ***************************************************************************************/


function bpsmolsport_display_facebook() { 

    return '<div class="facebook"><span class="fa fa-facebook"></span><a href="'.get_sub_field('facebook_text').'" target="_blank">'.get_sub_field('facebook_text').'</a></div>';
}



/***************************************************************************************
 	DESCRIPTION
 ***************************************************************************************/


function bpsmolsport_display_description() { 

    return '<div class="description"><p>'.get_sub_field('description_text').'</p></div>';
}



/***************************************************************************************
 	TAB
 ***************************************************************************************/


function bpsmolsport_display_tab($firstTab, $t) { 
    
    $html = '';
    if ($firstTab) :
        $html .= '<ul class="nav nav-tabs" role="tablist">';
        $html .= '<li role="presentation" class="active"><a href="#'.$t.'" aria-controls="'.$t.'" role="tab" data-toggle="tab">'.get_sub_field('tab_title').'</a></li>';
    else :
        $html .= '<li role="presentation" class=""><a href="#'.$t.'" aria-controls="'.$t.'" role="tab" data-toggle="tab">'.get_sub_field('tab_title').'</a></li>';
    endif;

    return $html;
}



/***************************************************************************************
 	TABS END
 ***************************************************************************************/


function bpsmolsport_display_tabs_end($tabContent, $t) { 
    
    $html = '</ul>';
    $html .= '<div class="tab-content">';
    $active = 'active';
    foreach ($tabContent as $i => $content) :
        $html .= '<div role="tabpanel" class="tab-pane '.$active.'" id="'.$t.'-'.$i.'">'.$content.'</div>';
        $active = '';
    endforeach;
    $html .= '</div>';

    return $html;
}





/* End of file bpsmolsport-custom-layouts.php */
/* Location: ./wp-content/themes/bpsmolsport/inc/bpsmolsport-custom-layouts.php */