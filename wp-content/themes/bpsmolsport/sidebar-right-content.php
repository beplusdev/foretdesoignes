<?php
/** sidebar-right-content.php
 *
 * Displays the sidebar on the right side of the content
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */
?>

<!-- Sidebar Right Content -->
<?php if (is_active_sidebar( 'sidebar-right-content' )) : ?>
    <div id="sidebar-right-content" class="sidebar-content">
        <div class="row">
        <?php dynamic_sidebar( 'sidebar-right-content' ); ?>
        </div>
    </div>
<?php endif; ?>
<!--/Sidebar Right Content -->

<?
/* End of file sidebar-right-content.php */
/* Location: ./wp-content/themes/bpsmolsport/sidebar-right-content.php */