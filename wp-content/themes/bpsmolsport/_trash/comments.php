<?php
/** header.php
 *
 * The default template for comments.
 *
 * @author		be+SPORTS
 * @package		BPS Molenbeek
 * @since		1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 *
 */ 
 
 load_theme_textdomain( 'bpsmolsport', get_template_directory() . '/lang' );
?>

<?php
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) { 
    echo '<p class="nocomments">' . __('This post is password protected. Enter the password to view comments.', 'bpsmolsport') . '</p>';
    return;
}
?>

<?php if ( comments_open() ) : ?>

<div id="fill-in-comment" class="">

    <h4><a class="title" href="javascript:void(0)"><i class="fa fa-pencil"></i> <?php comment_form_title( _e('Please leave a comment','bpsmolsport')); ?></a></h4>
    <div id="comment-form" style="display: none;">

        <p><?php _e('We\'ll be glad to receive your feedback.','bpsmolsport'); ?></p>

        <div class="cancel-comment-reply">
            <small><?php cancel_comment_reply_link(); ?></small>
        </div>

        <?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
            <p><?php _e('You must be', 'bpsmolsport'); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', 'bpsmolsport'); ?></a> <?php _e('to post a comment.', 'bpsmolsport'); ?></p>
        <?php else : ?>

            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" role="form">

                <?php if ( is_user_logged_in() ) : ?>

                    <p><?php _e('Logged in as', 'bpsmolsport'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.</p>

                <?php else : ?>

                    <div class="form-group">
                        <label>Votre nom *</label>
                        <input type="text" name="author" id="author" class="required form-control" value="" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
                    </div>

                    <div class="form-group">
                        <label>Votre email *</label>
                        <input type="text" name="email" id="email" class="required form-control" value="" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
                    </div>

                    <input type="hidden" name="url" id="url" value="" size="22" tabindex="3" />

                <?php endif; ?>

                <div class="form-group">
                    <label>Votre message *</label>
                    <textarea name="comment" id="comment" class="required form-control" cols="58" rows="10" tabindex="4"></textarea>
                </div>

                <p id="msg"></p>

                <button name="submit" type="submit" id="submit" tabindex="5" class="btn btn-default" /><?php _e('Send', 'bpsmolsport'); ?></button>
                <?php comment_id_fields(); ?>

                <?php do_action('comment_form', $post->ID); ?>

            </form>

        <?php endif; ?>
    </div>

</div>
<?php endif; ?>