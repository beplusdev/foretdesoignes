<?php
/** content-none.php
 *
 * The template for displaying a "No posts found" message.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
 
 load_theme_textdomain( 'bpsmolsport', get_template_directory() . '/lang' );
?>

<article>

    <!-- Entry header -->
    <header class="entry-header"> 
				
			<!-- Entry title -->
			<h1 class="entry-title">
				<?php _e( 'Nothing Found', 'bpsmolsport' ); ?>
			</h1>        
			<!--/Entry title -->
		
    </header>
    <!--/Entry header -->
    
    <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'bpsmolsport' ); ?></p>
	<?php get_search_form(); ?>


</article><!-- #post -->

<?php
/* End of file content-none.php */
/* Location: ./wp-content/themes/aothemefds/content-none.php */