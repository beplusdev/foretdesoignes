<?php
/** content-sport.php
 *
 * The template for displaying all sport posts.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php get_template_part('article', 'default-header'); ?>
    	
    <!-- Entry content -->
    <div class="entry-content">
            
        <?php the_content(); ?>
        
        <!-- Entry specific -->
        <div class="entry-specific">
                                
            <!-- Sections -->
            <?php
            $sections = array();
            $sections[] = 'bookings';
            $sections[] = 'sport_subscriptions';
            $sections[] = 'sport_club';
            $sections[] = 'sport_free1';
            $sections[] = 'sport_free2';
            $sections[] = 'sport_free3';

            $section_names = array();
            foreach($sections as $key => $section) :
                $section_names[$section] = get_field($section.'_section_name');
            endforeach;

            $show_tabs = (count(array_filter($section_names)) > 1);
            ?>
            
            <?php if (count(array_filter($section_names))) : ?>

                <?php if (!is_mobile()) : ?>
            
                    <div class="sections" role="tabpanel">

                        <?php if ($show_tabs) : ?>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach($sections as $key => $section) : ?>
                                    <?php if (!empty($section_names[$section])) : ?>
                                        <li role="presentation" class="<?php if (!$key) echo 'active'; ?>">
                                            <a href="#<?php echo $section; ?>" aria-controls="<?php echo $section; ?>" role="tab" data-toggle="tab">
                                                <?php echo $section_names[$section]; ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                            <!--/Nav tabs -->

                        <?php endif; ?>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <?php foreach($sections as $key => $section) : ?>
                                <?php if (!empty($section_names[$section])) : ?>
                                    <?php if ($show_tabs) : ?>
                                        <div role="tabpanel" class="tab-pane <?php if (!$key) echo 'in active'; ?>" id="<?php echo $section; ?>">
                                    <?php else : ?>
                                        <div id="<?php echo $section; ?>">
                                    <?php endif; ?>
                                    <?php if ($text = get_field($section.'_text')) : ?>
                                        <div class="text"><?php echo $text; ?></div> 
                                    <?php else : ?>
                                        <?php get_template_part('article', str_replace('_', '-', $section)); ?>
                                    <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </div>
                        <!--/Tab panes -->

                    </div>
                        
                <?php else : ?>
                        
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                         <?php foreach($sections as $key => $section) : ?>
                            <?php if (!empty($section_names[$section])) : ?>
                                <?php if ($show_tabs) : ?>
                                <div class="accordion" id="accordion2">
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#<?php echo $section; ?>">
                                                <h2><?php echo $section_names[$section]; ?></h2>
                                            </a>
                                        </div>
                                        <div id="<?php echo $section; ?>" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <?php if ($text = get_field($section.'_text')) : ?>
                                                    <div class="text"><?php echo $text; ?></div> 
                                                <?php else : ?>
                                                    <?php get_template_part('article', str_replace('_', '-', $section)); ?>
                                                <?php endif; ?>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php else: ?>
                                <?php if ($text = get_field($section.'_text')) : ?>
                                    <div class="text"><?php echo $text; ?></div> 
                                <?php else : ?>
                                    <?php get_template_part('article', str_replace('_', '-', $section)); ?>
                                <?php endif; ?> 
                            <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>

                    </div>

                <?php endif; ?>

            <?php endif; ?>
            <!--/Sections -->

            
            <!-- Additional text -->
            <?php if ($text = get_field('sport_additional_text')) : ?>
                <div class="text"><?php echo $text; ?></div> 
            <?php endif; ?>
            <!--/Additional text-->
                                          
            
            <!-- Linked sports and infrastructure -->
            <?php if (get_field('linked_sports_list') and get_field('linked_infrastructures_list')) : ?>
            
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php get_template_part('article', 'linked-sports'); ?>

                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php get_template_part('article', 'linked-infrastructures'); ?>

                    </div>
                </div><!-- .row -->
            
            <?php else : ?>
                <?php get_template_part('article', 'linked-sports'); ?>
                <?php get_template_part('article', 'linked-infrastructures'); ?>
            <?php endif; ?>
            <!--/Linked sports and infrastructure -->

        
        </div>
        <!--/Entry specific -->

    </div>
    <!--/Entry content -->

    <?php get_template_part('article', 'footer'); ?>


</article><!-- #post -->

<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bpsmolsport' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

			
<?php
/* End of file content-sport.php */
/* Location: ./wp-content/themes/aothemefds/content-sport.php */