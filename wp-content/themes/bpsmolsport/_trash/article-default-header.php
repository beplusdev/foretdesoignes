<?php
/** article-header.php
 *
 * The template for displaying the header of an article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<?php $image = get_field('images_header'); ?>

<?php if (!$image) : ?>

    <!-- Entry header -->
    <header class="entry-header"> 

        <!-- Entry title -->
        <h1 class="entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark">
                <?php if ( has_term( 'event', 'newscat', get_the_ID() ) and ( $start = get_field('event_start') ) ) : ?>
                    <span class="event-date">
                        <span class="event-day"><?php echo date_i18n( 'j' , $start ); ?></span>
                        <span class="event-month"><?php echo ucfirst(__(date_i18n( 'M' , $start ))); ?></span>
                    </span>
                <?php endif; ?>
                <?php the_title(); ?>
            </a>
        </h1>        
        <!--/Entry title -->

        <!-- Entry meta -->
        <div class="entry-meta">
            <?php bpsmolsport_entry_meta(); ?>
        </div>
        <!--/Entry meta -->
            
    </header>
    <!--/Entry header -->

<?php endif; // $image ?>

<?php
/* End of file article-header.php */
/* Location: ./wp-content/themes/aothemefds/article-header.php */