<?php
/** taxonomy-newscat-event.php
 *
 * The template for displaying events page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
*/

get_header(); ?>

<div class="container">
    
    <div class="row">

        <div class="col-md-8 col-sm-12 col-xs-12">

            <!-- Content -->
            <div id="content" class="site-content" role="main">

                    <?php 
                    $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
                    $title = $term->name;
                    ?>

                    <header class="archive-header">
                        <h1 class="archive-title"><?php echo $title; ?></h1>
                    </header><!-- .archive-header -->

                    <?php 
                    $when = (isset($_GET['w']) and ($_GET['w'] == 'past')) ? 'past' : 'future'; 
                    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
                    $url = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
                    ?>
                    <ul class="event-pills nav nav-pills">
                        <li class="<?php echo ($when == 'future') ? 'active' : ''; ?>"><a href="<?php echo $url; ?>"><?php _e('Future activities', 'bpsmolsport'); ?></a></li>
                        <li class="<?php echo ($when == 'past') ? 'active' : ''; ?>"><a href="<?php echo $url.'?w=past'; ?>"><?php _e('Past activities', 'bpsmolsport'); ?></a></li>				
                    </ul>

                    <?php if ( have_posts() ) : ?>

                        <?php /* The loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php get_template_part( 'content', get_post_format() ); ?>
                        <?php endwhile; ?>

                        <?php //twentythirteen_paging_nav(); ?>

                    <?php else : ?>
                        <?php get_template_part( 'content', 'empty' ); ?>
                    <?php endif; ?>

            </div>
            <!--/Content -->

            <?php get_sidebar( 'bottom-content' ); ?>

        </div>

        <div class="col-md-4 col-sm-12 col-xs-12">

            <?php get_sidebar( 'right-content' ); ?>				

        </div>

    </div><!--/.row -->

    <?php get_sidebar( 'bottom-page' ); ?>
    
</div><!--/.container -->

<?php get_footer(); ?>

<?
/* End of file taxonomy-newscat-event.php */
/* Location: ./wp-content/themes/bpsmolsport/taxonomy-newscat-event.php */