<?php
/** content-news.php
 *
 * The template for displaying all sport posts.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php get_template_part('article', 'default-header'); ?>
    	
    <!-- Entry content -->
    <div class="entry-content">
            
        <?php if (is_single()) bpsmolsport_event_info(); ?>

        <?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'bpsmolsport' ) ); ?>

        <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bpsmolsport' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
			
    </div>
    <!--/Entry content -->

    <?php get_template_part('article', 'footer'); ?>


</article><!-- #post -->

<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bpsmolsport' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

			
<?php
/* End of file content-news.php */
/* Location: ./wp-content/themes/aothemefds/content-news.php */