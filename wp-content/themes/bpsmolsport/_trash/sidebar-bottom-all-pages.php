<?php
/** sidebar-bottom-all-pages.php
 *
 * Displays the sidebar at the bottom of all pages
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */
?>

<!-- Sidebar Bottom All Pages -->
<?php if (is_active_sidebar( 'bottom-all-pages' )) : ?>
	<div id="bottom-all-pages">
		<?php dynamic_sidebar( 'bottom-all-pages' ); ?>
	</div>
<?php endif; ?>
<!--/Sidebar Bottom All Pages -->

<?
/* End of file sidebar-bottom-all-pages.php */
/* Location: ./wp-content/themes/bpsmolsport/sidebar-bottom-all-pages.php */