<?php
/** sidebar-bottom-content.php
 *
 * Displays the sidebar at the bottom of the content (content width)
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */
?>

<!-- Sidebar Bottom Content -->
<?php if (is_active_sidebar( 'sidebar-bottom-content' )) : ?>
    <div id="sidebar-bottom-content" class="sidebar-content">
        <div class="row">
            <?php dynamic_sidebar( 'sidebar-bottom-content' ); ?>
        </div>
    </div>
<?php endif; ?>
<!--/Sidebar Bottom Content -->

<?
/* End of file sidebar-bottom-content.php */
/* Location: ./wp-content/themes/bpsmolsport/sidebar-bottom-content.php */