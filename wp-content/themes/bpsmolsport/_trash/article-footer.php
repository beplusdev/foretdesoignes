<?php
/** article-footer.php
 *
 * The template for displaying the footer of an article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<!-- Entry meta -->
<footer class="entry-footer">

    <!-- Comment -->
    <?php if ( $post->comment_status == 'open' ) : ?>
        <?php comments_template( '', true ); ?>
    <?php endif; // comments_open() ?>
    <!--/Comment -->

    <!-- Social share -->
    <?php if (function_exists('bps_insert_social_share')) : ?>
        <?php $social_share = get_field('social_share'); ?>
        <?php if ($social_share) : ?>
            <?php $url= get_permalink(); ?>
            <?php bps_insert_social_share($url); ?>
        <?php endif; ?>	
    <?php endif; ?>
    <!--/Social share -->

    <?php if (is_single() && get_post_type() == 'news') : ?>
        <?php bpsmolsport_post_nav(); ?>
    <?php endif; ?>

</footer>
<!--/Entry meta -->

<?php
/* End of file article-footer.php */
/* Location: ./wp-content/themes/aothemefds/article-footer.php */