<?php
/** content-homepage.php
 *
 * Displays a post on the homepage
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */
?>

<div class="col-md-4 col-sm-4 col-xs-12 news-post">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<div class="entry-thumbnail">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
				<?php if ( $image = get_field('images_thumbnail') ) : ?>
					<div class="intro-image" style="background: url(<?php echo $image['sizes']['medium']; ?>) center top no-repeat;"></div>			
				<?php else : ?>
					<div class="default intro-image" style="background: url(<?php echo get_option( 'news_image' ); ?>) center top no-repeat;"></div>			
				<?php endif; ?>
				</a>
			</div>
            <div class="entry-body">
                <h1 class="entry-title effect">
                    <a href="<?php the_permalink(); ?>" rel="bookmark">
                        <?php the_title(); ?>
                    </a>
                </h1>
                <div class="entry-meta effect">
                    <?php edit_post_link( __( 'Edit', 'aolawolue' ), '<span class="edit-link"><i class="fa fa-pencil"></i>', '</span>' ); ?>
                </div><!-- .entry-meta -->
            </div>
		</header><!-- .entry-header -->
		<div class="entry-content effect">
			<?php the_content( __('Read more...', 'bpsmolsport') ) ?>
		</div><!-- .entry-content -->
		<footer class="entry-meta">
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
</div>

<?php
/* End of file content-homepage.php */
/* Location: ./wp-content/themes/aothemefds/content-homepage.php */