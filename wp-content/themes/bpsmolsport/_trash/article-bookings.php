<?php
/** article-bookings.php
 *
 * The template for displaying the bookings section of an article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<!-- Bookings -->
<div class="bookings">
    
    <?php if ($text = get_field('bookings_intro_text')) echo '<div class="text">'.$text.'</div>'; ?>

    <?php if ($b = count(get_field('bookings_prices_list'))) : ?> 

        <?php if ($title = get_field('bookings_prices_title')) echo '<h3>'.$title.'</h3>'; ?>

        <?php if ($text = get_field('bookings_prices_text')) echo '<div class="text">'.$text.'</div>'; ?>

        <?php 
        if ($b == 1) $grid = 12;
        else $grid = 6;
        /*
        if ($b <= ) $grid = 12/$b;
        else $grid = 4;
        /*
        elseif ($n % 4 == 0) $grid = 3;
        elseif ($n % 3 == 0) $grid = 4;
        elseif ($n % 4 > 1) $grid = 3;
        elseif ($n % 3 > 1) $grid = 4;
        else $grid = 3;
        */
        ?>
        <div class="row prices">
            <?php while (has_sub_field('bookings_prices_list')) : ?>
                <div class="col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-12 effect"> 
                    <div class="panel panel-default">
                        <div class="panel-heading" <?php if ($bgcolor = get_sub_field('bookings_prices_list_bgcolor')) echo 'style="background-color:'.$bgcolor.'"'; ?>><?php the_sub_field('bookings_prices_list_time_period'); ?></div>
                        <div class="panel-body">
                            <?php the_sub_field('bookings_prices_list_price'); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    
    
    <?php if ($m = count(get_field('bookings_materials_list'))) : ?>

        <?php if ($title = get_field('bookings_materials_title')) echo '<h3>'.$title.'</h3>'; ?>

        <?php if ($text = get_field('bookings_materials_text')) echo '<div class="text">'.$text.'</div>'; ?>

        <div class="materials">
            <?php /*<h3><?php _e('Materials to rent', 'bpsmolsport'); ?></h3> */ ?>
            <div class="table-responsive effect">
                <table class="table">
                    <?php while (has_sub_field('bookings_materials_list')) : ?>
                        <tr>
                            <td class="material"><?php the_sub_field('bookings_materials_list_material'); ?></td>
                            <td class="price"><?php the_sub_field('bookings_materials_list_price'); ?></td>
                        </tr>
                    <?php endwhile; ?>
                </table>
            </div>
        </div>
    <?php endif; ?>
    

    <?php if ($c = count(get_field('bookings_contacts'))) : ?>
    
        <?php if ($title = get_field('bookings_contacts_title')) echo '<h3>'.$title.'</h3>'; ?>
    
        <?php if ($text = get_field('bookings_contacts_text')) echo '<div class="text">'.$text.'</div>'; ?>

        <div class="contacts">
            <?php while (has_sub_field('bookings_contacts_list')) : ?>
                <div class="row">
                    <?php 
                    $post = get_sub_field('bookings_contacts_list_contact');
                    setup_postdata($post);
                    $name    	= get_field('contact_name');
                    $address 	= get_field('contact_address');
                    /*
                    $phone 		= get_field('contact_phone');
                    $fax 		= get_field('contact_fax');
                    $mobile	 	= get_field('contact_mobile');
                    $email 		= get_field('contact_email');	
                    $webpage 	= get_field('contact_webpage');	
                    $t = (!empty($address)) + (!empty($phone)) + (!empty($fax)) + (!empty($mobile)) + (!empty($email)) + (!empty($webpage)); 
                    */
                    $t = count(get_field('contact_infos'));
                    if ($t <= 3) $grid = 12/$t;
                    else $grid = 4;
                    if ($address)	echo '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg">'.$address.'</button></div>';
                    
                    /*
                    if ($phone) 	echo '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg"><i class="fa fa-phone"></i>'.$phone.'</button></div>';
                    if ($fax) 		echo '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg"><i class="fa fa-fax"></i>'.$fax.'</button></div>';
                    if ($mobile)	echo '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg"><i class="fa fa-mobile"></i>'.$mobile.'</button></div>';
                    if ($email)		echo '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg"><i class="fa fa-envelope"></i><a href="mailto:'.antispambot($email).'">'.antispambot($email).'</a></button></div>';
                    if ($webpage) 	echo '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg"><i class="fa fa-link"></i><a href="'.$webpage.'">'.__('Book online', 'bpsmolsport').'</a></button></div>';		
                    */
                        
                    $contact_infos = '';
                    while (has_sub_field('contact_infos')) :
                        if ($value = get_sub_field('contact_infos_value')) :
                            $contact_infos .= '<div class="col-md-'.$grid.' col-sm-'.$grid.' col-xs-12 effect"><button type="button" class="btn btn-success btn-lg">';
                            switch (get_sub_field('contact_infos_type')) :
                                case 'phone' :
                                    $contact_infos .= '<i class="fa fa-phone"></i>'.$value;
                                    break;
                                case 'fax' :
                                    $contact_infos .= '<i class="fa fa-fax"></i>'.$value;
                                    break;
                                case 'mobile' :
                                    $contact_infos .= '<i class="fa fa-mobile"></i>'.$value;
                                    break;
                                case 'email' :
                                    $contact_infos .= '<i class="fa fa-envelope"></i><a href="mailto:'.antispambot($value).'">'.antispambot($value).'</a>';
                                    break;
                                case 'webpage' :
                                    $contact_infos .= '<i class="fa fa-link"></i><a href="'.$value.'">'.__('Book online', 'bpsmolsport').'</a>';
                                    break;
                            endswitch;
                            $contact_infos .= '</button></div>';
                        endif;
                    endwhile;
                    echo $contact_infos;
                    ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

</div>
<!--/Club -->

<?php
/* End of file article-bookings.php */
/* Location: ./wp-content/themes/aothemefds/article-bookings.php */