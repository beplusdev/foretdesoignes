<?php
/** archive-sport.php
 *
 * The template for displaying sport archive page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
*/

get_header(); ?>

<div class="container">

    <div class="row">

        <div class="col-md-8 col-sm-12 col-xs-12">

            <!-- Content -->
            <div id="content" class="site-content linked-infrastructures" role="main">

                    <?php 
                    $term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
                    $title = $term->name;
                    ?>

                    <header class="archive-header">
                        <h1 class="archive-title"><?php echo _x('Infrastructure', 'plural','bpsmolsport'); ?></h1>
                    </header><!-- .archive-header -->

                    <?php if ( have_posts() ) : ?>

                        <div class="row">
                        <?php $first = true; ?>
                        <?php $i = 0; ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                                <?php $image = get_field('images_thumbnail'); ?>
                            <?php $i = $i + 1; ?>
                            <div class="col-md-2 col-sm-2 col-xs-4 effect">
                                <div class="post-thumbnail" id="post-thumbnail-<?php the_ID(); ?>">
                                    <a href="<?php the_permalink(); ?>" class="image">
                                        <?php if ($image) : ?>
                                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
                                        <?php elseif ($default = get_option('default_sport_image')) : ?>
                                            <img src="<?php echo $default; ?>" class="default" />
                                        <?php endif; ?>
                                    </a>
                                    <a href="<?php the_permalink(); ?>" class="caption">
                                        <div class="title">
                                            <?php 
                                            if ($title = get_field('images_thumbnail_name')) :
                                                echo $title;
                                            else :
                                                echo (strlen(get_the_title()) > 15) ? substr(get_the_title(), 0, 15).'...' : get_the_title(); 
                                            endif;
                                            ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        </div>

                    <?php else : ?>
                        <?php get_template_part( 'content', 'empty' ); ?>
                    <?php endif; ?>

            </div>
            <!--/Content -->

            <?php get_sidebar( 'bottom-content' ); ?>

        </div>

        <div class="col-md-4 col-sm-12 col-xs-12">

            <?php get_sidebar( 'right-content' ); ?>				

        </div>

    </div><!--/.row -->

    <?php get_sidebar( 'bottom-page' ); ?>
    
</div><!--/.container -->

<?php get_footer(); ?>

<?
/* End of file archive-sport.php */
/* Location: ./wp-content/themes/bpsmolsport/archive-sport.php */