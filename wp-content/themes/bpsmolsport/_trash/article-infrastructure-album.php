<?php
/** article-infrastructure-album.php
 *
 * The template for displaying an album to the article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<!-- Album -->
<div class="album">
    
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12 effect">

            <?php $first = true; ?>
            <?php $i = 0; ?>
            <?php while ( has_sub_field('infrastructure_album_list') ) : ?>
                <?php $image = get_sub_field('infrastructure_album_list_image'); ?>
                <?php if ($image) : ?>

                    <div class="infrastructure-main <?php echo ($first) ? 'show' : ''; ?>" id="infrastructure-main-<?php echo $i; ?>">
                        <a href="<?php echo $image['sizes']['large']; ?>" rel="lightbox[gallery-1]">
                        <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo get_sub_field('infrastructure_album_list_title'); ?>" title="<?php echo get_sub_field('infrastructure_album_list_title'); ?>" />			
                        <?php /*
                        <div class="filter">
                            <div class="caption">
                                <div class="inner">
                                    <div class="title">
                                        <?php echo get_sub_field('infrastructure_album_list_title'); ?>
                                    </div>
                                    <p><?php echo get_sub_field('infrastructure_album_list_text') ?></p>
                                </div>
                            </div>
                        </div>
                        */ ?>
                        </a>
                    </div>

                    <?php $first = false; ?>
                <?php endif; ?>
                <?php $i += 1; ?>
            <?php endwhile; ?>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row">

                <?php $i = 0; ?>
                <?php while ( has_sub_field('infrastructure_album_list') ) : ?>
                    <?php $image = get_sub_field('infrastructure_album_list_image'); ?>
                    <?php if ($image) : ?>

                        <div class="col-md-4 col-sm-4 col-xs-2 effect">
			<a href="<?php echo $image['sizes']['large']; ?>" rel="lightbox[gallery-2]">
                            <img class="infrastructure-thumbnail" id="infrastructure-thumbnail-<?php echo $i; ?>" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo get_sub_field('infrastructure_album_list_title'); ?>" title="<?php echo get_sub_field('infrastructure_album_list_title'); ?>" />			
			</a>
                        </div>

                    <?php endif; ?> 
                    <?php $i += 1; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>

</div>
<!--/Album -->

<?php
/* End of file article-infrastructure-album.php */
/* Location: ./wp-content/themes/aothemefds/article-infrastructure-album.php */
