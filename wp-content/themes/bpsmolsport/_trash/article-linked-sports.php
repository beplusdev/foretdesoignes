<?php
/** article-linked-sports.php
 *
 * The template for displaying sports linked to the article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<?php 
if (get_field('linked_infrastructures_list')) : 
    $grid = '4';
else :
    $grid = '2';
endif;
?>

<!-- Linked sports -->
<?php if ($linked_sports = get_field('linked_sports_list')) : ?>

    <div class="linked-sports">
    
        <h3><?php echo ($title = get_field('linked_sports_title')) ? $title : __('Other sports', 'bpsmolsport'); ?></h3>
        
        <div class="row">
            <?php 
            $i = 0;
            $n = (get_field('linked_sports_number')) ? min(count($linked_sports), get_field('linked_sports_number')) : count($linked_sports); 
            ?>
            <?php while (($i < $n) and count($linked_sports)) : ?>
                <?php
                $random = array_rand( $linked_sports );
                $random_sport = $linked_sports[ $random ];
                $post = $random_sport['linked_sports_list_sport'];
                unset($linked_sports[$random]);
                setup_postdata($post);
                $image = get_field('images_thumbnail'); 
                ?>
                <?php $i = $i + 1; ?>
                <div class="col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-4 effect">
                    <div class="post-thumbnail" id="post-thumbnail-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>" class="image">
                            <?php if ($image) : ?>
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
                            <?php elseif ($default = get_option('default_sport_image')) : ?>
                                <img src="<?php echo $default; ?>" class="default" />
                            <?php endif; ?>
                        </a>
                        <a href="<?php the_permalink(); ?>" class="caption">
                            <div class="title">
                                <?php 
                                if ($title = get_field('images_thumbnail_name')) :
                                    echo $title;
                                else :
                                    echo (strlen(get_the_title()) > 15) ? substr(get_the_title(), 0, 15).'...' : get_the_title(); 
                                endif;
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endwhile; ?>
        </div>
 
    </div>

<?php endif; ?>
<!--/Linked sports -->

<?php
/* End of file article-linked-sports.php */
/* Location: ./wp-content/themes/aothemefds/article-linked-sports.php */