<?php
/** article-quote.php
 *
 * The template for displaying a quote in the header of an article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<!-- Quote -->
<?php $quotes = get_field('quotes_list'); ?>
<?php if ($c = count($quotes)) : ?>
    <?php $random_quote = $quotes[ array_rand( $quotes ) ]; ?>
    <div class="quote">
        <div class="inner">
            <div class="container">
                <blockquote>
                    <?php echo $random_quote['quote_list_text']; ?>
                    <cite>
                        <?php if ($random_quote['quote_list_autor']) : ?>
                            <span class="autor"><?php echo $random_quote['quote_list_autor']; ?></span>
                        <?php endif; ?>
                        <?php if ($random_quote['quote_list_avatar']) : ?>
                            <img class="avatar" src="<?php echo $random_quote['quote_list_avatar']['sizes']['thumbnail']; ?>" />
                        <?php endif; ?>
                    </cite>
                </blockquote>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--/Quote -->


<?php
/* End of file article-quote.php */
/* Location: ./wp-content/themes/aothemefds/article-quote.php */