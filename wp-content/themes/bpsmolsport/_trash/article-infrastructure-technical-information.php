<?php
/** article-infrastructure-technical-information.php
 *
 * The template for displaying the technical information to the article.
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */ 
?>

<!-- Technical information -->
<div class="technical-information">

<?php
$dimensions = get_field('infrastructure_technical_information_dimensions');
$surface    = get_field('infrastructure_technical_information_surface');
$covering   = get_field('infrastructure_technical_information_covering');
$capacity   = get_field('infrastructure_technical_information_capacity');
?>

<?php if ($text = get_field('field_infrastructure_technical_information_intro_text')) echo '<div class="text">'.$text.'</div>'; ?>   

<?php if ( $dimensions or $surface or $covering or $capacity ) : ?>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <?php if ($dimensions) : ?>
                        <th class="dimensions"><?php _e('Dimensions', 'bpsmolsport'); ?></th>
                    <?php endif; ?>
                    <?php if ($surface) : ?>
                        <th class="surface"><?php _e('Surface', 'bpsmolsport'); ?></th>
                    <?php endif; ?>
                    <?php if ($covering) : ?>
                        <th class="covering"><?php _e('Covering', 'bpsmolsport'); ?></th>
                    <?php endif; ?>
                    <?php if ($capacity) : ?>
                        <th class="capacity"><?php _e('Capacity', 'bpsmolsport'); ?></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php if ($dimensions) : ?>
                        <td class="dimensions"><?php echo $dimensions; ?></td>
                    <?php endif; ?>
                    <?php if ($surface) : ?>
                        <td class="surface"><?php echo $surface; ?></td>
                    <?php endif; ?>
                    <?php if ($covering) : ?>
                        <td class="covering"><?php echo $covering; ?></td>
                    <?php endif; ?>
                    <?php if ($capacity) : ?>
                        <td class="capacity"><?php echo $capacity; ?></td>
                    <?php endif; ?>
                </tr>
            </tbody>
        </table>
    </div>
<?php endif; ?>

</div>
<!--/Technical information -->

<?php
/* End of file article-infrastructure-technical-information.php */
/* Location: ./wp-content/themes/aothemefds/article-infrastructure-technical-information.php */