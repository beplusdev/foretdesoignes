<?php
/** sidebar-left-content.php
 *
 * Displays the sidebar on the left side of the content
 *
 * @author	be+SPORTS
 * @package	BPS Molenbeek
 * @since	1.0.0
 *
 * Copyright (C) 2018 be+SPORTS <info@beplusports.com>
 */
?>

<!-- Sidebar Right Content -->
<?php if (is_active_sidebar( 'sidebar-left-content' )) : ?>
    <div id="sidebar-left-content" class="sidebar-content">
        <?php dynamic_sidebar( 'sidebar-left-content' ); ?>
    </div>
<?php endif; ?>
<!--/Sidebar Right Content -->

<?
/* End of file sidebar-left-content.php */
/* Location: ./wp-content/themes/bpsmolsport/sidebar-left-content.php */