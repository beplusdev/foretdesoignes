jQuery(window).load(function() {

    // Allow Page URL to activate a tab's ID
	var taburl = document.location.toString();
	if( taburl.match('#') ) {
		jQuery('.nav-tabs a[href=#'+taburl.split('#')[1]+']').tab('show');
	}
 
	// Allow internal links to activate a tab.
	jQuery('a[data-toggle="tab"]').click(function (e) {
		e.preventDefault();
		jQuery('a[href="' + jQuery(this).attr('href') + '"]').tab('show');
	})
    
    //jQuery( "div.cycloneslider-caption" ).wrapInner('<div class="container"></div>');
	//jQuery( "div.cycloneslider-thumbnails" ).wrapInner('<div class="container"></div>');
	//jQuery( "#bottom-home-content .widget" ).wrap('<div class="col-sm-4"></div>');
    var width = jQuery(window).width();
    if (width > 992) {

/*
        jQuery('.cycle-slideshow > div').each(function(){

            var img = jQuery(this).find('img');

            //get img dimensions
            //jQuery(this).parent().parent().css('visibility', 'none');
            var h = img.height();
            if (h == 0) {
                jQuery(this).css({ visibility: "hidden", display: "block" });
                h = img.height();
                jQuery(this).css({ visibility: "visible", display: "none" });
            }
            img.css({ visibility: "visible" });

            //var w = jQuery(this).width();

            //get div dimensions
            var div_h =jQuery(this).height();
            //var div_w =jQuery('.cycle-slideshow').width();

            //set img position
            //alert(div_h+' '+h+' => '+(Math.round((div_h - h) / 2)) + 'px');
            img.css('margin-top', Math.round((div_h - h) / 2) + 'px');
            //this.style.left = '50%';
            //this.style.marginLeft = Math.round(w/2) + 'px';
        });
*/
/*
        // Adaptation to the biggest height for the news in the homepage
        jQuery("#mainpage .widget_nav_menu .menu > li > a").click(function() {
            jQuery(this).parent().find(".sub-menu").toggle();
        });

        jQuery("#mainpage.homepage .news").height(function() {
            var max = 0;
            jQuery("#mainpage.homepage .news").each(function() {
                if (jQuery(this).height() > max) {
                    max = jQuery(this).height();
                }
            });
            return max;
        }); 
*/	
    }
    
	// Display effects on search bar
	jQuery(".search-form .show-search").click(function() {
		jQuery(this).parent().find(".form-control").show("slow");
		jQuery(this).parent().find(".show-search").hide();
		jQuery(this).parent().find(".button-submit").show();
		jQuery(this).parent().find(".hide-search").show();
	})
	jQuery(".search-form .hide-search").click(function() {
		jQuery(this).parent().find(".form-control").hide();
        jQuery(this).parent().find(".button-submit").hide();
        jQuery(this).parent().find(".hide-search").hide();
        jQuery(this).parent().find(".show-search").show();
	})

    // Remove empty columns
    jQuery('table').each(function(a, tbl) {
        jQuery(tbl).find('th').each(function(i) {
            var remove = true;
            var currentTable = jQuery(this).parents('table');
            var tds = currentTable.find('tr td:nth-child(' + (i + 1) + ')');
            tds.each(function(j) { if (this.innerHTML != '') remove = false; });
            if (remove) {
                jQuery(this).hide();
                tds.hide();
            }
        });
    });
    
    jQuery('#fill-in-comment a.title').click(function() {
        jQuery('#comment-form').toggle('slow');
    });
    
    // Scroll effects on navbar and logo
    var width = jQuery(window).width();
    if (width > 992) {
        var scroll = jQuery(window).scrollTop();
        if (scroll < 60) {
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-top', parseInt(15-(scroll/4))+'px');
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-bottom', parseInt(15-(scroll/4))+'px');
            jQuery(".navbar-brand .logo").css('height', (parseInt(110-(scroll*5/6)))+'px');
            jQuery("body").removeClass("fixed");
            jQuery("#site-header .inner").removeClass("color");
        }
        else {
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-top', '0px');
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-bottom', '0px');
            jQuery(".navbar-brand .logo").css('height', '60px');
            jQuery("body").addClass("fixed");
            jQuery("#site-header .inner").addClass("color");
        }
    }
    
    // Menu widget for Adeps
    jQuery('#sidebar-right-content .widget_nav_menu .menu-item-has-children').click(function() {
        if (jQuery(this).hasClass('open')) {
            jQuery(this).removeClass('open');
        }
        else {
            jQuery(this).addClass('open');
        }
    });

});


// Scroll effects on navbar and logo
jQuery(window).scroll(function(event) {
    var width = jQuery(window).width();
    if (width > 992) {
        var scroll = jQuery(window).scrollTop();
        if (scroll < 60) {
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-top', parseInt(15-(scroll/4))+'px');
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-bottom', parseInt(15-(scroll/4))+'px');
            jQuery(".navbar-brand .logo").css('height', (parseInt(110-(scroll*5/6)))+'px');
            jQuery("body").removeClass("fixed");
            jQuery("#site-header .inner").removeClass("color");
        }
        else {
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-top', '0px');
            //jQuery(".navbar-nav>li>a, .navbar-nav>li>span, #navbar .search-form, #navbar ul.polylang").css('padding-bottom', '0px');
            jQuery(".navbar-brand .logo").css('height', '60px');
            jQuery("body").addClass("fixed");
            jQuery("#site-header .inner").addClass("color");
        }
    }
});


// Appear effects on scrolling
(function (jQuery) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */

  jQuery.fn.visible = function(partial) {
    
      var $t            = jQuery(this),
          $w            = jQuery(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

jQuery(".effect").each(function(i, el) {
    var width = jQuery(window).width();
    if (width > 992) {
        var el = jQuery(el);
        if (el.visible(true)) {
            el.addClass("already-visible"); 
        } 
    }
});

jQuery(window).scroll(function(event) {
    var width = jQuery(window).width();
    if (width > 992) {
        jQuery(".effect").each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                el.delay(2000).addClass("come-in"); 
                //el.delay(4000).fadeIn();
            } 
        });
    }
});

// If window width less than 768px, opens submenu on mouse hover 
jQuery('ul.nav li.dropdown, ul.nav li.dropdown-submenu').hover(function() {
    var width = jQuery(window).width();
    if (width >= 768) {
        jQuery(this).find('.dropdown-menu').first().fadeIn(0);
    }
}, function() {
    var width = jQuery(window).width();
    if (width >= 768) {
        jQuery(this).find('.dropdown-menu').first().delay(300).fadeOut(0);
    }
});

// If not, opens it on click
jQuery('ul.nav li.dropdown span, ul.nav li.dropdown-submenu span').click(function() {
    var width = jQuery(window).width();
    if (width < 768) {
        jQuery(this).parent().find('> .dropdown-menu').slideToggle();
        if (jQuery(this).hasClass('open')) {
            jQuery(this).removeClass('open');
        }
        else {
           jQuery(this).addClass('open');
        }

    }
});

// Adds a class to the navbar if open and for mobile
jQuery('.navbar-toggle').click(function() {
    var body = jQuery('body');
    if (body.hasClass('open-mobile')) {
        body.removeClass('open-mobile');
    }
    else {
       body.addClass('open-mobile');
    }
});

