<?php
/** aofds-custom-options.php
 *
 * AO Custom options for the theme
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );

function aofds_custom_options_create_menu() {

	// Create new sublevel menu
	add_submenu_page( 'options-general.php', __('AO Options', 'aofds'), __('AO Options', 'aofds'), 'manage_options', 'aofds-custom-options', 'aofds_custom_options_page');

	// Call register options function
	add_action( 'admin_init', 'aofds_custom_options_register' );
}
add_action( 'admin_menu', 'aofds_custom_options_create_menu' );


function aofds_custom_options_register() {
	
	//register our settings
	register_setting( 'aofds-custom-options-group', 'bg_image_url' );
	register_setting( 'aofds-custom-options-group', 'logo_url' );
	register_setting( 'aofds-custom-options-group', 'small_logo_url' );
	register_setting( 'aofds-custom-options-group', 'social_share_logo_url' );
	register_setting( 'aofds-custom-options-group', 'main_bgcolor' ); //#B8D532
	register_setting( 'aofds-custom-options-group', 'second_bgcolor' ); //#58595B
	register_setting( 'aofds-custom-options-group', 'main_textcolor' );
	register_setting( 'aofds-custom-options-group', 'second_textcolor' );
	register_setting( 'aofds-custom-options-group', 'header_image' );
	register_setting( 'aofds-custom-options-group', 'news_image' );
	register_setting( 'aofds-custom-options-group', 'post_types' );
	register_setting( 'aofds-custom-options-group', 'direct_contact_label' );
	register_setting( 'aofds-custom-options-group', 'direct_contact_number' );
	register_setting( 'aofds-custom-options-group', 'footer_contacts' );
	register_setting( 'aofds-custom-options-group', 'address_lat' );
	register_setting( 'aofds-custom-options-group', 'address_lng' );
	register_setting( 'aofds-custom-options-group', 'map_icon' );
	register_setting( 'aofds-custom-options-group', 'default_news_image' );
	register_setting( 'aofds-custom-options-group', 'default_sport_image' );
	register_setting( 'aofds-custom-options-group', 'default_club_image' );
	register_setting( 'aofds-custom-options-group', 'default_infrastructure_image' );
	register_setting( 'aofds-custom-options-group', 'default_meeting_image' );
	register_setting( 'aofds-custom-options-group', 'mailchimp_form_action' );
	register_setting( 'aofds-custom-options-group', 'mailchimp_form_code' );
	register_setting( 'aofds-custom-options-group', 'free_css' );
	register_setting( 'aofds-custom-options-group', 'price1_label' );
	register_setting( 'aofds-custom-options-group', 'price2_label' );
	register_setting( 'aofds-custom-options-group', 'price1_caption' );
	register_setting( 'aofds-custom-options-group', 'price2_caption' );
}


function aofds_custom_options_page() {
?>
<div class="wrap">
<h2><?php _e('AO Options', 'aofds'); ?></h2>

<form method="post" action="options.php" class="aofds-custom-options">
    <?php settings_fields( 'aofds-custom-options-group' ); ?>
    <?php do_settings_sections( 'aofds-custom-options-group' ); ?>
    <table class="form-table">

        <tr valign="top">
        <th scope="row"><?php _e('Background image (url)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="bg_image_url" value="<?php echo esc_attr( get_option('bg_image_url') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Logo (url)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="logo_url" value="<?php echo esc_attr( get_option('logo_url') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Small Logo (url)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="small_logo_url" value="<?php echo esc_attr( get_option('small_logo_url') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Social share default Logo (url)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="social_share_logo_url" value="<?php echo esc_attr( get_option('social_share_logo_url') ); ?>" /></td>
        </tr>
		
        <tr valign="top">
        <th scope="row"><?php _e('Main BG Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="main_bgcolor" value="<?php echo esc_attr( get_option('main_bgcolor') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Second BG Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="second_bgcolor" value="<?php echo esc_attr( get_option('second_bgcolor') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Main Text Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="main_textcolor" value="<?php echo esc_attr( get_option('main_textcolor') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Second Text Color (hexa)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="second_textcolor" value="<?php echo esc_attr( get_option('second_textcolor') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default header image', 'aofds'); ?></th>
        <td><input type="text" size="70" name="header_image" value="<?php echo esc_attr( get_option('header_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default news image', 'aofds'); ?></th>
        <td><input type="text" size="70" name="news_image" value="<?php echo esc_attr( get_option('news_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Post types (coma separated)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="post_types" value="<?php echo esc_attr( get_option('post_types') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Direct contact label', 'aofds'); ?></th>
        <td><input type="text" size="70" name="direct_contact_label" value="<?php echo esc_attr( get_option('direct_contact_label') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Direct contact number', 'aofds'); ?></th>
        <td><input type="text" size="70" name="direct_contact_number" value="<?php echo esc_attr( get_option('direct_contact_number') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Contacts id for the footer (coma separated)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="footer_contacts" value="<?php echo esc_attr( get_option('footer_contacts') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Latitude de l\'adresse', 'aofds'); ?></th>
        <td><input type="text" size="70" name="address_lat" value="<?php echo esc_attr( get_option('address_lat') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Longitude de l\'adresse', 'aofds'); ?></th>
        <td><input type="text" size="70" name="address_lng" value="<?php echo esc_attr( get_option('address_lng') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Map icon', 'aofds'); ?></th>
        <td><input type="text" size="70" name="map_icon" value="<?php echo esc_attr( get_option('map_icon') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default news image (squared)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="default_news_image" value="<?php echo esc_attr( get_option('default_news_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default sport image (squared)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="default_sport_image" value="<?php echo esc_attr( get_option('default_sport_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default club image (squared)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="default_club_image" value="<?php echo esc_attr( get_option('default_club_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default infrastructure image (squared)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="default_infrastructure_image" value="<?php echo esc_attr( get_option('default_infrastructure_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default meeting room image (squared)', 'aofds'); ?></th>
        <td><input type="text" size="70" name="default_meeting_image" value="<?php echo esc_attr( get_option('default_meeting_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Mailchimp form action', 'aofds'); ?></th>
        <td><input type="text" size="70" name="mailchimp_form_action" value="<?php echo esc_attr( get_option('mailchimp_form_action') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Mailchimp form code', 'aofds'); ?></th>
        <td><input type="text" size="70" name="mailchimp_form_code" value="<?php echo esc_attr( get_option('mailchimp_form_code') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Free CSS', 'aofds'); ?></th>
            <td><textarea type="text" size="70" name="free_css" rows="20" cols="60"><?php echo esc_attr( get_option('free_css') ); ?></textarea></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Label of price 1', 'aofds'); ?></th>
        <td><input type="text" size="70" name="price1_label" value="<?php echo esc_attr( get_option('price1_label') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Label of price 2', 'aofds'); ?></th>
        <td><input type="text" size="70" name="price2_label" value="<?php echo esc_attr( get_option('price2_label') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Caption of price 1', 'aofds'); ?></th>
        <td><input type="text" size="70" name="price1_caption" value="<?php echo esc_attr( get_option('price1_caption') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Caption of price 2', 'aofds'); ?></th>
        <td><input type="text" size="70" name="price2_caption" value="<?php echo esc_attr( get_option('price2_caption') ); ?>" /></td>
        </tr>

    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php 
}


function aofds_custom_options_css() {

    ?>
	<!-- AO Theme Options CSS --> 
	<style type="text/css">
        body {
            <?php if ($bg = get_option( 'bg_image_url' )) : ?>
                background-image: url(<?php echo $bg; ?>);
                background-repeat: no-repeat;
                background-size: cover;
            background-attachment: fixed;
            <?php endif; ?>
        }
	#map .overlay{
		background-image: url(<?php echo get_option( 'map_icon' ); ?>);
	}
    <?php
    /*
    .aologo {
		background: url("<?php echo get_option( 'logo_url' ); ?>") no-repeat;
	}
	.aomainbgcolor, #navbar .nav > li:hover, #nabar .dropdown-menu > li:hover > a {
		background: <?php echo get_option( 'main_bgcolor' ); ?> !important;
	}
	.aomaintextcolor, #navbar .nav > li:hover > a, #navbar .dropdown-menu > li:hover > a {
		color: <?php echo get_option( 'main_textcolor' ); ?> !important;
	}
	.aosecondbgcolor {
		background: <?php echo get_option( 'second_bgcolor' ); ?>;
	}
	.aosecondtextcolor {
		color: <?php echo get_option( 'second_textcolor' ); ?>;
	}
        
    a, a:hover {
        color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    #navbar .nav .dropdown-menu > li:hover > a {
    background: <?php echo get_option( 'main_bgcolor' ); ?> !important; 
    }
    .homepage .news .entry-meta .edit-link i.fa {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .not-homepage .entry-meta .edit-link i.fa {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    #content .event-pills > li.active > a, 
    #content .event-pills > li.active > a:hover, 
    #content .event-pills > li.active > a:focus {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .panel-heading {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?> !important;
    }
    #footer h3, #footer h4 {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .event-date {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') -45px -85px no-repeat; 
    }
    .sport .bookings .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .sport #sport_subscriptions .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .sport .club .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .infrastructure .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .sidebar-content .widget-ao-upcoming-events li a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget-ao-upcoming-events .event-date {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') -45px -85px no-repeat;
    }
    .sidebar-content .widget-ao-upcoming-events .btn-success {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > a:before {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > ul li a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > ul li a:before {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .panel-heading {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .wp-polls-ans .btn-success {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .wp-polls .pollbar {
    background: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .infrastructure .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/lawoluwe-bg5.png') bottom right no-repeat;  
    }
    .gallery img {
    }
        
    @media (max-width: 992px) {

        .aomainbgcolor, #navbar .nav > li:hover, #nabar .dropdown-menu > li:hover > a,
        .aomaintextcolor, #navbar .nav > li:hover > a, #navbar .dropdown-menu > li:hover > a,
        #navbar .nav .dropdown-menu > li:hover > a {
        background-color: transparent !important;
        color: #333 !important;
        }
    }

    <?php echo get_option( 'free_css' ); ?>

    */
    ?>
        
	</style> 
	<!-- /AO Theme Options CSS -->
	<?php
}
add_action( 'wp_head' , 'aofds_custom_options_css' );



function aofds_custom_options_scripts() {
	?>
		<script type="text/javascript">
		var map;
		jQuery(document).ready(function(){"use strict";
		  map = new GMaps({
			scrollwheel: false,
			el: '#map',
			lat: <?php echo get_option( 'address_lat' ); ?>,
			lng: <?php echo get_option( 'address_lng' ); ?>,
			'zoom':13,
			//icon: '<?php echo get_option( 'map_icon' ); ?>'
			});
		  map.drawOverlay({
			lat: map.getCenter().lat(),
			lng: map.getCenter().lng(),
			layer: 'overlayLayer',
			content: '<div class="overlay"></div>',
			verticalAlign: 'bottom',
			horizontalAlign: 'center',
			//icon: '<?php echo get_option( 'map_icon' ); ?>'
		  });
		});
    </script>
    <?php
}
add_action( 'wp_head' , 'aofds_custom_options_scripts' );


/* End of file aofds-custom-options.php */
/* Location: ./wp-content/themes/aofds/inc/aofds-custom-options.php */