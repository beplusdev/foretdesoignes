<?php
/** sidebar-bottom-content.php
 *
 * Displays the sidebar on the bottom side of the content
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Right Content -->
<?php if (is_active_sidebar( 'sidebar-bottom-content' )) : ?>
    <div id="sidebar-bottom-content" class="sidebar-content">
        <?php dynamic_sidebar( 'sidebar-bottom-content' ); ?>
    </div>
<?php endif; ?>
<!--/Sidebar Right Content -->

<?
/* End of file sidebar-bottom-content.php */
/* Location: ./wp-content/themes/aofds/sidebar-bottom-content.php */