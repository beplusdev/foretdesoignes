msgid ""
msgstr ""
"Project-Id-Version: AOBasketFDS widget de liens personnalisés\n"
"POT-Creation-Date: 2015-09-16 18:48+0200\n"
"PO-Revision-Date: 2015-09-16 18:54+0200\n"
"Last-Translator: \n"
"Language-Team: Alley Oop\n"
"Language: fr_FR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.4\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../ao-last-news.php:48
msgid "A display of linked"
msgstr "Affiche les dernières news (d'une ou plusieurs catégories)"

#: ../ao-last-news.php:52
msgid "Linked"
msgstr "Dernières news"

#: ../ao-last-news.php:204
msgid "Previous"
msgstr "Précédent"

#: ../ao-last-news.php:208
msgid "Next"
msgstr "Suivant"

#: ../ao-last-news.php:395
msgid "Title:"
msgstr "Titre :"

#: ../ao-last-news.php:400
msgid "Roles:"
msgstr "Catégories :"

#: ../ao-last-news.php:413
msgid "Number of posts to show:"
msgstr "Nombre maximum de news à afficher :"

#: ../ao-last-news.php:418
msgid "Order"
msgstr "Ordre"

#: ../ao-last-news.php:420
msgid "Random"
msgstr "Aléatoire"

#: ../ao-last-news.php:421
msgid "Title"
msgstr "Titre"

#: ../ao-last-news.php:422
msgid "Date"
msgstr "Date"

#: ../ao-last-news.php:427
msgid "Direction"
msgstr "Sens"

#: ../ao-last-news.php:429
msgid "Asc"
msgstr "Ascendant"

#: ../ao-last-news.php:430
msgid "Desc"
msgstr "Descendant"

#: ../ao-last-news.php:448
msgid "Layout"
msgstr "Mise en page"

#: ../ao-last-news.php:450
msgid "Titles"
msgstr "Titres"

#: ../ao-last-news.php:451
msgid "Titles - Texts"
msgstr "Titres - Textes"

#: ../ao-last-news.php:452
msgid "Thumbnails - Titles"
msgstr "Miniatures - Titres"

#: ../ao-last-news.php:453 ../ao-last-news.php:455
msgid "Dates - Titles"
msgstr "Dates - Titres"

#: ../ao-last-news.php:454
msgid "Thumbnails - Titles - Texts"
msgstr "Miniatures - Titres - Textes"

#: ../ao-last-news.php:456
msgid "Thumbnails - Dates - Titles"
msgstr "Miniatures - Dates - Titres"

#: ../ao-last-news.php:457
msgid "Thumbnails - Dates - Titles - Texts"
msgstr "Miniatures - Dates - Titres - Textes"

#: ../ao-last-news.php:463
msgid "Nombre de colonnes"
msgstr "Nombre de colonnes"

#: ../ao-last-news.php:465
msgid "1"
msgstr "1"

#: ../ao-last-news.php:466
msgid "2"
msgstr "2"

#: ../ao-last-news.php:467
msgid "3"
msgstr "3"

#: ../ao-last-news.php:468
msgid "4"
msgstr "4"

#: ../ao-last-news.php:469
msgid "6"
msgstr "6"

#: ../ao-last-news.php:475
msgid "View"
msgstr "Disposition"

#: ../ao-last-news.php:477
msgid "Grid"
msgstr "Grille"

#: ../ao-last-news.php:478
msgid "Diaporama"
msgstr "Diaporama"

#: ../ao-last-news.php:484
msgid "Number of chars for the text:"
msgstr "Nombre de caractères pour le texte :"

#~ msgid "There is no sport"
#~ msgstr "Il n'y a pas de membre à afficher"

#~ msgid "Status"
#~ msgstr "Statut :"

#~ msgid "Active"
#~ msgstr "Actif"

#~ msgid "Inactive"
#~ msgstr "Inactif"

#~ msgid "Show all title:"
#~ msgstr "Libellé du bouton \"Afficher tous\" :"

#~ msgid "A list of linked of your choice"
#~ msgstr "Une liste des liens de votre choix"

#~ msgid "Personalized linked"
#~ msgstr "Liens personnalisés"

#~ msgid "Sport"
#~ msgstr "Lien"

#~ msgid "http://"
#~ msgstr "http://"

#~ msgid "Stay in Window"
#~ msgstr "Ouvrir dans la même fenêtre"

#~ msgid "Open New Window"
#~ msgstr "Ouvrir dans une nouvelle fenêtre"

#~ msgid "Add"
#~ msgstr "Ajouter"
