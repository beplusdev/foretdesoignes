<?php
/** content-infrastructure.php
 *
 * The template for displaying all infrastructure posts.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php //get_template_part('article', 'header'); ?>
    	
    <!-- Entry content -->
    <div class="entry-content">
            
        <?php the_content(); ?>
        
        <!-- Entry specific -->
        <div class="entry-specific">
                                
            <?php get_template_part('article', 'infrastructure-album'); ?>
            
            <!-- Sections -->
            <?php
            $sections = array();
            $sections[] = 'infrastructure_technical_information';
            $sections[] = 'bookings';
            $sections[] = 'infrastructure_free1';
            $sections[] = 'infrastructure_free2';
            $sections[] = 'infrastructure_free3';

            $section_names = array();
            foreach($sections as $key => $section) :
                $section_names[$section] = get_field($section.'_section_name');
            endforeach;
            ?>
            
            <?php if (count(array_filter($section_names))) : ?>

                <?php foreach($sections as $key => $section) : ?>
                    <?php if (!empty($section_names[$section])) : ?>
                        <h2><?php echo $section_names[$section]; ?></h2>
                        <?php if ($text = get_field($section.'_text')) : ?>
                            <div class="text"><?php echo $text; ?></div> 
                        <?php else : ?>
                            <?php get_template_part('article', str_replace('_', '-', $section)); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>

            <?php endif; ?>
            <!--/Sections -->

            
            <!-- Linked sports and infrastructure -->
            <?php if (get_field('linked_sports_list') and get_field('linked_infrastructures_list')) : ?>
            
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php get_template_part('article', 'linked-sports'); ?>

                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                        <?php get_template_part('article', 'linked-infrastructures'); ?>

                    </div>
                </div><!-- .row -->
            
            <?php else : ?>
                <?php get_template_part('article', 'linked-sports'); ?>
                <?php get_template_part('article', 'linked-infrastructures'); ?>
            <?php endif; ?>
            <!--/Linked sports and infrastructure -->

        
        </div>
        <!--/Entry specific -->

    </div>
    <!--/Entry content -->

    <?php get_template_part('article', 'footer'); ?>


</article><!-- #post -->

<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'aofds' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

			
<?php
/* End of file content-infrastructure.php */
/* Location: ./wp-content/themes/aothemefds/content-infrastructure.php */