<?php
/** article-linked-infrastructures.php
 *
 * The template for displaying infrastructures linked to the article.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<?php 
if (get_field('linked_sports_list')) : 
    $grid = '4';
else :
    $grid = '2';
endif;
?>

<!-- Linked infrastructures -->
<?php if ($linked_infrastructures = get_field('linked_infrastructures_list')) : ?>
    
    <div class="linked-infrastructures">

        <h3><?php echo ($title = get_field('linked_infrastructures_title')) ? $title : __('Other infrastructures', 'aofds'); ?></h3>
        
        <div class="row">
            <?php 
            $i = 0;
            $n = (get_field('linked_infrastructures_number')) ? min(count($linked_infrastructures), get_field('linked_infrastructures_number')) : count($linked_infrastructures); 
            ?>
            <?php while (($i < $n) and count($linked_infrastructures)) : ?>
                <?php
                $random = array_rand( $linked_infrastructures );
                $random_sport = $linked_infrastructures[ $random ];
                $post = $random_sport['linked_infrastructures_list_infrastructure'];
                unset($linked_infrastructures[$random]);
                setup_postdata($post);
                $image = get_field('images_thumbnail'); 
                ?>
                <?php $i = $i + 1; ?>
                <div class="col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-4 effect">
                    <div class="post-thumbnail" id="post-thumbnail-<?php the_ID(); ?>">
                        <a href="<?php the_permalink(); ?>" class="image">
                            <?php if ($image) : ?>
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title() ?>" title="<?php the_title() ?>" />			
                            <?php elseif ($default = get_option('default_infrastructure_image')) : ?>
                                <img src="<?php echo $default; ?>" class="default" />
                            <?php endif; ?> 
                        </a>
                        <a href="<?php the_permalink(); ?>" class="caption">
                            <div class="title">
                                <?php 
                                if ($title = get_field('images_thumbnail_name')) :
                                    echo $title;
                                else :
                                    echo (strlen(get_the_title()) > 15) ? substr(get_the_title(), 0, 15).'...' : get_the_title(); 
                                endif;
                                ?>
                            </div>
                        </a>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
            <?php endwhile; ?>
        </div>

    </div>

<?php endif; ?>
<!--/Linked infrastructures -->

<?php
/* End of file article-linked-infrastructures.php */
/* Location: ./wp-content/themes/aothemefds/article-linked-infrastructures.php */