<?php
/** article-subscriptions.php
 *
 * The template for displaying the subscriptions section of an article.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<!-- Subscriptions -->
<div class="subscriptions">
    
    <?php if ($text = get_field('sport_subscriptions_intro_text')) echo '<div class="text">'.$text.'</div>'; ?>

    <?php if ($s = count(get_field('sport_subscriptions_formulas_list'))) : ?> 

        <?php if ($title = get_field('sport_subscriptions_formulas_title')) echo '<h3>'.$title.'</h3>'; ?>
    
        <?php if ($text = get_field('sport_subscriptions_formulas_text')) echo '<div class="text">'.$text.'</div>'; ?>

        <?php 
        if ($s <= 3) $grid = 12/$s;
        else $grid = 4;
        /*
        elseif ($n % 4 == 0) $grid = 3;
        elseif ($n % 3 == 0) $grid = 4;
        elseif ($n % 4 > 1) $grid = 3;
        elseif ($n % 3 > 1) $grid = 4;
        else $grid = 3;
        */
        $price1_for = get_field('sport_subscriptions_formulas_price1_for');
        $price2_for = get_field('sport_subscriptions_formulas_price2_for');
        ?>
        <div class="row formulas">
            <?php while (has_sub_field('sport_subscriptions_formulas_list')) : ?>
                <div class="col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-12 effect"> 
                    <div class="panel panel-default">
                        <div class="panel-heading" <?php if ($bgcolor = get_sub_field('sport_subscriptions_formulas_list_bgcolor')) echo 'style="background-color:'.$bgcolor.'"'; ?>><?php the_sub_field('sport_subscriptions_formulas_list_name'); ?></div>
                        <?php $period = get_sub_field('sport_subscriptions_formulas_list_period'); ?>
                        <?php $block = get_sub_field('sport_subscriptions_formulas_list_block'); ?>
                        <?php if ($period or $block) : ?>
                            <ul class="list-group">       
                                <?php if ($period) : ?>
                                    <li class="list-group-item"><?php echo $period; ?></li>
                                <?php endif; ?>
                                <?php if ($block) : ?>
                                    <li class="list-group-item"><?php echo $block; ?></li>
                                <?php endif; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="panel-body">
                            <?php if (get_sub_field('sport_subscriptions_formulas_list_price_12_sessions')) : ?>
                                <div class="price">
                                    <?php the_sub_field('sport_subscriptions_formulas_list_price_12_sessions'); ?>
                                    <span class="sessions">/ <?php echo $price1_for; ?></span>
                                </div>
                            <?php endif; ?>
                            <?php if (get_sub_field('sport_subscriptions_formulas_list_price_24_sessions')) : ?>
                                <div class="price">
                                    <?php the_sub_field('sport_subscriptions_formulas_list_price_24_sessions'); ?>
                                    <span class="sessions">/ <?php echo $price2_for; ?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>

    
    <?php if ($c = count(get_field('sport_subscriptions_contacts_list'))) : ?>

        <?php if ($title = get_field('sport_subscriptions_contacts_title')) echo '<h3>'.$title.'</h3>'; ?>

        <?php if ($text = get_field('sport_subscriptions_contacts_text')) echo '<div class="text">'.$text.'</div>'; ?>

        <div class="contacts">    
            <button type="button" class="btn btn-success btn-lg">
               <?php
                if ($c <= 3) $grid = 12/$c;
                else $grid = 4;
                ?>
                <div class="row">
                    <?php while (has_sub_field('sport_subscriptions_contacts_list')) : ?>
                        <div class="col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-12 effect">
                            <?php 
                            $post = get_sub_field('sport_subscriptions_contacts_list_contact');
                            setup_postdata($post);
                            $name    	= get_field('contact_name');
                            $address 	= get_field('contact_address');
                            /*
                            $phone 		= get_field('contact_phone');
                            $fax 		= get_field('contact_fax');
                            $mobile	 	= get_field('contact_mobile');
                            $email 		= get_field('contact_email');	
                            $webpage 	= get_field('contact_webpage');	
                            */
                            if ($name) 	    echo $name.'<br/>';
                            if ($address)	echo $address.'<br/>';
                            /*
                            if ($phone) 	echo '<i class="fa fa-phone"></i>'.$phone.'<br/>';
                            if ($fax) 		echo '<i class="fa fa-fax"></i>'.$fax.'<br/>';
                            if ($mobile)	echo '<i class="fa fa-mobile"></i>'.$mobile.'<br/>';
                            if ($email)		echo '<i class="fa fa-envelope"></i><a href="mailto:'.antispambot($email).'">'.antispambot($email).'</a><br/>';
                            if ($webpage) 	echo '<i class="fa fa-link"></i><a href="'.$webpage.'">'.$webpage.'</a><br/>';		
                            */                            
                            $contact_infos = '';
                            while (has_sub_field('contact_infos')) :
                                if ($value = get_sub_field('contact_infos_value')) :
                                    $contact_infos .= '';
                                    switch (get_sub_field('contact_infos_type')) :
                                        case 'phone' :
                                            $contact_infos .= '<i class="fa fa-phone"></i>'.$value;
                                            break;
                                        case 'fax' :
                                            $contact_infos .= '<i class="fa fa-fax"></i>'.$value;
                                            break;
                                        case 'mobile' :
                                            $contact_infos .= '<i class="fa fa-mobile"></i>'.$value;
                                            break;
                                        case 'email' :
                                            $contact_infos .= '<i class="fa fa-envelope"></i><a href="mailto:'.antispambot($value).'">'.antispambot($value).'</a>';
                                            break;
                                        case 'webpage' :
                                            $contact_infos .= '<i class="fa fa-link"></i><a href="'.$value.'">'.$value.'</a>';
                                            break;
                                    endswitch;
                                    $contact_infos .= '<br/>';
                                endif;
                            endwhile;
                            echo $contact_infos;
                            ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </button>
        </div>
    <?php endif; ?>

</div>
<!--/Club -->

<?php
/* End of file article-subscriptions.php */
/* Location: ./wp-content/themes/aothemefds/article-subscriptions.php */