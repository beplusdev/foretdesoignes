<?php
/** article-header.php
 *
 * The template for displaying the header of an article.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<?php $image = get_field('images_header'); ?>

<?php if ($image) : ?>

    <!-- Entry header -->
    <header class="entry-header"> 

        <!-- Header image -->
        <div class="header-image">
            <div class="image" style="background: url(<?php echo (!is_mobile()) ? $image['url'] : $image['sizes']['medium']; ?>) center center no-repeat;">
                <div class="filter">

                    <?php if (!is_mobile()) get_template_part('article', 'quote'); ?>
                    
                    <!-- Caption -->
                    <div class="caption">
                        <div class="inner">
                            <div class="container">

                                <!-- Entry title -->
                                <h1 class="entry-title">
                                    <?php aofds_event_calendar(); ?>
                                    <?php the_title(); ?>
                                </h1>        
                                <!--/Entry title -->

                                <!-- Entry meta -->
                                <div class="entry-meta">
                                    <?php aofds_entry_meta(); ?>
                                </div>
                                <!--/Entry meta -->

                            </div>
                        </div>
                    </div>
                    <!--/Caption -->

                </div>
            </div>			
        </div>
        <!--/Header image -->

    </header>
    <!--/Entry header -->

<?php else : ?>
        
    <!-- Default header image -->
    <div class="header-image default">
        <div class="image" style="background: url(<?php echo get_option( 'header_image' ); ?>) center center no-repeat;">
        </div>			
    </div>
    <!--/Default header image -->

<?php endif; // $image ?>

<?php
/* End of file article-header.php */
/* Location: ./wp-content/themes/aothemefds/article-header.php */