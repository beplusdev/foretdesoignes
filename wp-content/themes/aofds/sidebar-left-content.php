<?php
/** sidebar-left-content.php
 *
 * Displays the sidebar on the left side of the content
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Right Content -->
<?php if (is_active_sidebar( 'sidebar-left-content' )) : ?>
    <div id="sidebar-left-content" class="sidebar-content">
        <?php dynamic_sidebar( 'sidebar-left-content' ); ?>
    </div>
<?php endif; ?>
<!--/Sidebar Right Content -->

<?
/* End of file sidebar-left-content.php */
/* Location: ./wp-content/themes/aofds/sidebar-left-content.php */