<?php
/** sidebar-bottom-all-pages.php
 *
 * Displays the sidebar at the bottom of all pages
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Bottom All Pages -->
<?php if (is_active_sidebar( 'bottom-all-pages' )) : ?>
	<div id="bottom-all-pages">
		<?php dynamic_sidebar( 'bottom-all-pages' ); ?>
	</div>
<?php endif; ?>
<!--/Sidebar Bottom All Pages -->

<?
/* End of file sidebar-bottom-all-pages.php */
/* Location: ./wp-content/themes/aofds/sidebar-bottom-all-pages.php */