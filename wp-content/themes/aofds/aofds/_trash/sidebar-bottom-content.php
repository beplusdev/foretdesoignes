<?php
/** sidebar-bottom-content.php
 *
 * Displays the sidebar at the bottom of the content (content width)
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Bottom Content -->
<?php if (is_active_sidebar( 'sidebar-bottom-content' )) : ?>
    <div id="sidebar-bottom-content" class="sidebar-content">
        <div class="row">
            <?php dynamic_sidebar( 'sidebar-bottom-content' ); ?>
        </div>
    </div>
<?php endif; ?>
<!--/Sidebar Bottom Content -->

<?
/* End of file sidebar-bottom-content.php */
/* Location: ./wp-content/themes/aofds/sidebar-bottom-content.php */