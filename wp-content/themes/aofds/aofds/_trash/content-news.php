<?php
/** content-news.php
 *
 * The template for displaying all sport posts.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */ 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php get_template_part('article', 'default-header'); ?>
    	
    <!-- Entry content -->
    <div class="entry-content">
            
        <?php if (is_single()) aofds_event_info(); ?>

        <?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'aofds' ) ); ?>

        <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'aofds' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
			
    </div>
    <!--/Entry content -->

    <?php get_template_part('article', 'footer'); ?>


</article><!-- #post -->

<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'aofds' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

			
<?php
/* End of file content-news.php */
/* Location: ./wp-content/themes/aothemefds/content-news.php */