<?php
/** aofds-custom-fields.php
 *
 * AO Custom fields added with ACF
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aofds', get_template_directory() . '/lang' );

/**
 * Generate and manage specific fields (with ACF).
 *
 * @since 1.0.0
 *
 * @return void
 */ 
function aofds_custom_fields_load_acf()
{
	if(function_exists("register_field_group"))
	{

        
        /*********************************************************************/
        /*   FOR NEWS                                                        */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_news',
            'title' => __('Additional information', 'aofds'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_news_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'news_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aofds_layout_section(),
                        aofds_layout_subsection(),
                        aofds_layout_content(),
                        aofds_layout_images(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'news',
                    ),
                ),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR PAGE                                                        */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_page',
            'title' => __('Additional information', 'aofds'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_page_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'page_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aofds_layout_section(),
                        aofds_layout_subsection(),
                        aofds_layout_content(),
                        aofds_layout_images(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ),
                ),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR EVENT                                                       */
		/*********************************************************************/
        
        $term_event = get_term_by('slug', 'event', 'newscat');
		register_field_group(array (
			'id' => 'acf_event',
			'title' => __('Activity details','aolawoluwe'), // Détails sur l'activité
			'fields' => array (
				array (
					'key' => 'field_event_start',
					'label' => __('Start date and time','aolawoluwe'), // Date et heure de début
					'name' => 'event_start',
					'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'show_date' => 'true',
                    'date_format' => 'dd/mm/yy',
                    'time_format' => 'hh:mm',
                    'show_week_number' => 'false',
                    'picker' => 'select',
                    'save_as_timestamp' => 'true',
                    'get_as_timestamp' => 'true',
				),
				array (
					'key' => 'field_event_end',
					'label' => __('End date and time','aolawoluwe'), // Date et heure de fin
					'name' => 'event_end',
					'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'show_date' => 'true',
                    'date_format' => 'dd/mm/yy',
                    'time_format' => 'hh:mm',
                    'show_week_number' => 'false',
                    'picker' => 'select',
                    'save_as_timestamp' => 'true',
                    'get_as_timestamp' => 'true',
				),
                array (
                    'key' => 'field_event_location_flexible',
                    'label' => __('Location', 'aofds'), // 'Lieu',
                    'name' => 'event_location_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), //'Ajouter une élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        array (
                            'key' => 'field_event_location_infrastructure',
                            'name' => 'event_location_infrastructure',
                            'label' => __('Infrastructure', 'aofds'), // 'Infrastructure',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_event_location_infrastructure_repeater',
                                    'label' => '', 
                                    'name' => 'event_location_infrastructure_repeater',
                                    'type' => 'repeater',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'min' => '1',
                                    'max' => '',
                                    'layout' => 'block',
                                    'button_label' => __('Add an infrastructure', 'aofds'), // 'Ajouter une infrastructure',
                                    'sub_fields' => array (
                                        array (
                                            'key' => 'event_location_infrastructure_repeater_object',
                                            'label' => '', // 'Infrastructure',
                                            'name' => 'event_location_infrastructure_repeater_object',
                                            'type' => 'post_object',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => '',
                                                'id' => '',
                                            ),
                                            'post_type' => array (
                                                0 => 'infrastructure',
                                            ),
                                            'taxonomy' => array (
                                            ),
                                            'allow_null' => 0,
                                            'multiple' => 0,
                                            'return_format' => 'object',
                                            'ui' => 1,
                                        ),
                                    ),
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                        array (
                            'key' => 'field_event_location_address',
                            'name' => 'event_location_address',
                            'label' => __('Address', 'aofds'), // 'Adresse',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_event_location_address_text',
                                    'label' => '', //__('Address', 'aofds'), // 'Adresse', 
                                    'name' => 'event_location_address_text',
                                    'type' => 'textarea',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'maxlength' => '',
                                    'rows' => 3,
                                    'new_lines' => 'wpautop',
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                        array (
                            'key' => 'field_event_location_map',
                            'name' => 'event_location_map',
                            'label' => __('Map', 'aofds'), // 'Infrastructure',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_event_location_map_google',
                                    'label' => '', // 'Google Map',
                                    'name' => 'event_location_map_google',
                                    'type' => 'google_map',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'center_lat' => '50.809391',
                                    'center_lng' => '4.443800',
                                    'zoom' => '',
                                    'height' => '',
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                    ),
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'taxonomy',
						'operator' => '==',
						'value' => $term_event->term_id,
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'left',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));       

        
        /*********************************************************************/
        /*   FOR SPORT                                                       */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_sport',
            'title' => __('Additional information', 'aofds'), // 'Information supplémentaire',
            'fields' => array (
                /*
                array (
                    'key' => 'acf_sport_prices',
                    'label' => __('Prices', 'aofds'), // 'Tarifs',
                    'name' => '',
                    'type' => 'message',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => __('Prices will be displayed here', 'aofds'), // 'Les tarifs apparaitront ici..',
                    'esc_html' => 0,
                ),
                */
                array (
                    'key' => 'field_sport_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'sport_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aofds_layout_section(),
                        aofds_layout_subsection(),
                        aofds_layout_content(),
                        aofds_layout_images(),
                        aofds_layout_prices(),
                        aofds_layout_contact(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'sport',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR CLUBS                                              */
		/*********************************************************************/
        
        acf_add_local_field_group(array (
            'key' => 'acf_club',
            'title' => __('Additional information', 'aofds'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_club_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'club_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aofds_layout_section(),
                        aofds_layout_subsection(),
                        aofds_layout_content(),
                        aofds_layout_images(),
                        aofds_layout_schedule_table(),
                        array (
                            'key' => 'field_club_trainer',
                            'name' => 'club_trainer',
                            'label' => __('Trainer', 'aofds'), // 'Entraineur',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_club_trainer_image',
                                    'label' => __('Picture', 'aofds'), // 'Photo',
                                    'name' => 'club_trainer_image',
                                    'type' => 'image',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'array',
                                    'preview_size' => 'thumbnail',
                                    'library' => 'all',
                                    'min_width' => '',
                                    'min_height' => '',
                                    'min_size' => '',
                                    'max_width' => '',
                                    'max_height' => '',
                                    'max_size' => '',
                                    'mime_types' => '',
                                ),
                                array (
                                    'key' => 'field_club_trainer_name',
                                    'label' => __('Name to display', 'aofds'), // Nom à afficher
                                    'name' => 'club_trainer_name',
                                    'type' => 'text',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'html',
                                    'maxlength' => '',
                                ),
                                array (
                                    'key' => 'field_club_trainer_flexible',
                                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                                    'name' => 'club_trainer_flexible',
                                    'type' => 'flexible_content',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                                    'min' => '',
                                    'max' => '',
                                    'layouts' => array (
                                        //aofds_layout_section(),
                                        aofds_layout_address(),
                                        aofds_layout_phone(),
                                        aofds_layout_mobile(),
                                        aofds_layout_email(),
                                        aofds_layout_web(),
                                        aofds_layout_facebook(),
                                        aofds_layout_description(),
                                    ),
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'club',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
        
        
        acf_add_local_field_group(array (
            'key' => 'acf_club_infos',
            'title' => __('Club infos', 'aofds'), // 'Infos club', 
            'fields' => array (
                array (
                    'key' => 'field_club_infos_sport',
                    'label' => __('Sport', 'aofds'), // 'Discipline',
                    'name' => 'club_infos_sport',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array (
                        0 => 'sport',
                    ),
                    'taxonomy' => array (
                    ),
                    'allow_null' => 0,
                    'multiple' => 0,
                    'return_format' => 'id',
                    'ui' => 1,
                ),
                /*
                array (
                    'key' => 'field_club_infos_caption',
                    'label' => __('Caption', 'aofds'), // 'Légende',
                    'name' => 'club_infos_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_club_infos_logo',
                    'label' => __('Logo', 'aofds'),
                    'name' => 'club_infos_logo',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array (
                    'key' => 'field_club_infos_logo_caption',
                    'label' => __('Caption', 'aofds'), // 'Légende',
                    'name' => 'club_infos_logo_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                */
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'club',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR INFRASTRUCTURE                                              */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_infrastructure',
            'title' => __('Additional information', 'aofds'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_infrastructure_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'infrastructure_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aofds_layout_section(),
                        aofds_layout_subsection(),
                        aofds_layout_content(),
                        aofds_layout_images(),
                        aofds_layout_features_table(),
                        aofds_layout_prices(),
                        aofds_layout_contact(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'infrastructure',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR MEETING                                                     */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_meeting',
            'title' => __('Additional information', 'aofds'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_meeting_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'meeting_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aofds_layout_section(),
                        aofds_layout_subsection(),
                        aofds_layout_content(),
                        aofds_layout_images(),
                        aofds_layout_features_table(),
                        aofds_layout_prices(),
                        aofds_layout_contact(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'meeting',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR PRICE                                                       */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_price',
            'title' => __('Prices list','aofds'), // 'Liste des tarifs',
            'fields' => array (
                array (
                    'key' => 'field_price_repeater',
                    'label' => '', //__('Prices list', 'aofds'), // 'Liste des tarifs',
                    'name' => 'price_repeater',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '1',
                    'max' => '',
                    'layout' => 'block',
                    'button_label' => __('Add a new price', 'aofds'), // 'Ajouter un nouveau tarif',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_price_publish_start_date',
                            'label' => __('Publish start date','aofds'), // 'Début de publication',
                            'name' => 'price_publish_start_date',
                            'type' => 'date_picker',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'display_format' => 'd/m/Y',
                            'return_format' => 'Ymd',
                            'first_day' => 1,
                        ),
                        array (
                            'key' => 'field_price_publish_end_date',
                            'label' => __('Publish end date','aofds'), // 'Fin de publication',
                            'name' => 'price_publish_end_date',
                            'type' => 'date_picker',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'display_format' => 'd/m/Y',
                            'return_format' => 'Ymd',
                            'first_day' => 1,
                        ),
                        array (
                            'key' => 'field_price_title',
                            'label' => __('Price title', 'aofds'), // 'Titre',
                            'name' => 'price_title',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),  
                        array (
                            'key' => 'field_price_flexible',
                            'label' => _x('Price', 'singular','aofds'), // 'Tarif',
                            'name' => 'price_flexible',
                            'type' => 'flexible_content',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                            'min' => '',
                            'max' => '',
                            'layouts' => array (
                                aofds_layout_section(),
                                aofds_layout_subsection(),
                                aofds_layout_content(),
                                aofds_layout_images(),
                                array (
                                    'key' => 'field_prices_table',
                                    'name' => 'prices_table',
                                    'label' => __('Prices table', 'aofds'), // 'Tableau de tarifs',
                                    'display' => 'block',
                                    'sub_fields' => array (
                                        array (
                                            'key' => 'field_prices_table_repeater',
                                            'label' => __('Prices', 'aofds'), // 'Tarifs',
                                            'name' => 'prices_table_repeater',
                                            'type' => 'repeater',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => '',
                                                'id' => '',
                                            ),
                                            'min' => '',
                                            'max' => '',
                                            'layout' => 'table',
                                            'button_label' => __('Add a price', 'aofds'), // 'Ajouter un tarif',
                                            'sub_fields' => array (
                                                array (
                                                    'key' => 'field_prices_table_repeater_description',
                                                    'label' => __('Description', 'aofds'), // 'Description',
                                                    'name' => 'prices_table_repeater_description',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => 60,
                                                        'class' => '',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                                array (
                                                    'key' => 'field_prices_table_repeater_price1',
                                                    'label' => __('Price 1', 'aofds'),
                                                    'name' => 'prices_table_repeater_price1',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => 20,
                                                        'class' => '',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                                array (
                                                    'key' => 'field_prices_table_repeater_price2',
                                                    'label' => __('Price 2', 'aofds'),
                                                    'name' => 'prices_table_repeater_price2',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => '',
                                                        'class' => '',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                            ),
                                        ),
                                        array (
                                            'key' => 'field_prices_teable_description_label',
                                            'label' => __('Description label', 'aofds'), // 'Titre',
                                            'name' => 'prices_teable_description_label',
                                            'type' => 'text',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => '',
                                                'id' => '',
                                            ),
                                            'default_value' => '',
                                            'placeholder' => __('Description', 'aofds'),
                                            'prepend' => '',
                                            'append' => '',
                                            'maxlength' => '',
                                            'readonly' => 0,
                                            'disabled' => 0,
                                        ),  
                                    ),
                                    'min' => '',
                                    'max' => '',
                                ),
								aofds_layout_equipment_table(),
                                aofds_layout_tab(),
                                aofds_layout_tabs_end(),
                            ),
                        ),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'price',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

                             
        acf_add_local_field_group(array (
            'key' => 'acf_price_captions',
            'title' => __('Price captions', 'aofds'), // 'Légendes des tarifs',
            'fields' => array (
                array (
                    'key' => 'field_price1_label',
                    'label' => __('Price 1 label', 'aofds'),
                    'name' => 'price1_label',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price1_label' ),
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_price1_caption',
                    'label' => __('Price 1 caption', 'aofds'),
                    'name' => 'price1_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price1_caption' ), // Jours ouvrables avant 17h
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_price2_label',
                    'label' => __('Price 2 label', 'aofds'),
                    'name' => 'price2_label',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price2_label' ),
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_price2_caption',
                    'label' => __('Price 2 caption', 'aofds'),
                    'name' => 'price2_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price2_caption' ), // Toutes les heures d'ouvertures
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'price',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR TEAMS                                                       */
		/*********************************************************************/

        /**
		 * Team
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		 register_field_group(array (
            'id' => 'acf_team',
            'title' => __('Team composition', 'aofds'), // Composition de l'équipe
            'fields' => array (
                array (
                    'key' => 'field_team_contacts_nbcolumns',
                    'label' => __('Number of columns to display', 'aofds'), // Type d'information
                    'name' => 'team_contacts_nbcolumns',
                    'type' => 'select',
                    'column_width' => '',
                    'choices' => array (
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '6' => '6',
                    ),
                    'default_value' => '1',
                    'allow_null' => 0,
                    'multiple' => 0,
                ),
                array (
                    'key' => 'field_team_contacts_list',
                    'label' => __('Contacts list', 'aofds'), // Liste des membres de l'équipe
                    'name' => 'team_contacts_list',
                    'type' => 'repeater',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_team_contacts_list_existing_contact',
                            'label' => __('Contact already recorded', 'aofds'), // Contact déjà encodé
                            'name' => 'team_contacts_list_existing_contact',
                            'type' => 'true_false',
                            'column_width' => '',
                            'message' => '',
                            'default_value' => 0,
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact',
                            'label' => __('Contact', 'aofds'), // Contact
                            'name' => 'team_contacts_list_contact',
                            'type' => 'post_object',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'column_width' => '',
                            'post_type' => array (
                                0 => 'contact',
                            ),
							'taxonomy' => array (
								0 => 'all',
							),
                            'allow_null' => 0,
                            'multiple' => 0,
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_name',
                            'label' => __('Name to display', 'aofds'), // Nom à afficher
                            'name' => 'team_contacts_list_contact_name',
                            'type' => 'text',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_title',
                            'label' => __('Title', 'aofds'), // Titre
                            'name' => 'team_contacts_list_contact_title',
                            'type' => 'text',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_address',
                            'label' => __('Address', 'aofds'), // Adresse
                            'name' => 'team_contacts_list_contact_address',
                            'type' => 'textarea',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => '',
                            'formatting' => 'br',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_infos',
                            'label' => __('Contact infos', 'aofds'), // Informations du contact
                            'name' => 'team_contacts_list_contact_infos',
                            'type' => 'repeater',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_team_contacts_list_contact_infos_type',
                                    'label' => __('Information type', 'aofds'), // Type d'information
                                    'name' => 'contact_infos_type',
                                    'type' => 'select',
                                    'column_width' => '',
                                    'choices' => array (
                                        'phone' => __('Phone', 'aofds'), // Téléphone
                                        'fax' => __('Fax', 'aofds'), // Fax
                                        'mobile' => __('Mobile', 'aofds'), // GSM
                                        'email' => __('Email', 'aofds'), // E-mail
                                        'webpage' => __('Webpage', 'aofds'), // Page web
                                    ),
                                    'default_value' => 'phone',
                                    'allow_null' => 0,
                                    'multiple' => 0,
                                ),
                                array (
                                    'key' => 'field_team_contacts_list_contact_infos_value',
                                    'label' => __('Value', 'aofds'), // valeur
                                    'name' => 'contact_infos_value',
                                    'type' => 'text',
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'html',
                                    'maxlength' => '',
                                ),
                            ),
                            'row_min' => '',
                            'row_limit' => '',
                            'layout' => 'table',
                            'button_label' => __('Add an information', 'aofds'), // Ajouter une information
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_motto',
                            'label' => __('Motto', 'aofds'), // Devise
                            'name' => 'team_contacts_list_contact_motto',
                            'type' => 'textarea',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => '',
                            'formatting' => 'br',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_image',
                            'label' => __('Picture', 'aofds'), // Photo
                            'name' => 'team_contacts_list_contact_image',
                            'type' => 'image',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'column_width' => '',
                            'save_format' => 'object',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_text',
                            'label' => __('Free text','aofds'), // Texte libre
                            'name' => 'team_contacts_list_contact_text',
                            'type' => 'wysiwyg',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'toolbar' => 'full',
                            'media_upload' => 'yes',
                        ),
                    ),
                    'row_min' => '',
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => __('Add a contact', 'aofds'), // Ajouter un membre
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'team',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));

        
        /*********************************************************************/
        /*   FOR CONTACT                                                     */
		/*********************************************************************/

        /**
		 * Contact
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		 register_field_group(array (
			'id' => 'acf_contact',
			'title' => __('Contact', 'aofds'), // Contact
			'fields' => array (
                array (
                    'key' => 'field_contact_image',
                    'label' => __('Picture', 'aofds'), // 'Photo',
                    'name' => 'contact_image',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
				array (
					'key' => 'field_contact_name',
					'label' => __('Name to display', 'aofds'), // Nom à afficher
					'name' => 'contact_name',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
                array (
                    'key' => 'field_contact_flexible',
                    'label' => '', //__('Additional information', 'aofds'), // 'Information supplémentaire',
                    'name' => 'contact_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aofds'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        //aofds_layout_section(),
                        aofds_layout_address(),
                        aofds_layout_phone(),
                        aofds_layout_mobile(),
                        aofds_layout_email(),
                        aofds_layout_web(),
                        aofds_layout_motto(),
                        //aofds_layout_description(),
                    ),
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'contact',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => array (
                    0 => 'the_content',
				),
            'active' => 1,
            'description' => '',
		));
		
		

                             
        /*********************************************************************/
        /*   FOR SPORT AND INFRASTRUCTURE                                    */
		/*********************************************************************/

		/**
		 * Linked sports
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_sports',
			'title' => __('Linked sports','aofds'), // Disciplines liées
			'fields' => array (
				array (
					'key' => 'field_linked_sports_title',
					'label' => __('Title','aofds'), // Title
					'name' => 'linked_sports_title',
					'type' => 'text',
					'default_value' => __('Linked sports','aofds'), // Disciplines liées
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_sports_number',
					'label' => __('Number to display','aofds'), // Nombre à afficher
					'name' => 'linked_sports_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_sports_list',
					'label' => __('Sports list','aofds'), // Liste des sports liés
					'name' => 'linked_sports_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_sports_list_sport',
							'label' => __('Sport','aofds'), // Discipline
							'name' => 'linked_sports_list_sport',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'sport',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add a sport','aofds'), // Ajouter une discipline
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 2,
		));


        /**
		 * Linked infrastructure
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_infrastructures',
			'title' => __('Linked infrastructures','aofds'), // Infrastructures liée
			'fields' => array (
				array (
					'key' => 'field_linked_infrastructures_title',
					'label' => __('Title','aofds'), // Titre
					'name' => 'linked_infrastructures_title',
					'type' => 'text',
					'default_value' => __('Linked infrastructures','aofds'), // Infrastructures liée
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_infrastructures_number',
					'label' => __('Number to display','aofds'), // Nombre à afficher
					'name' => 'linked_infrastructures_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_infrastructures_list',
					'label' => __('List','aofds'), // Liste
					'name' => 'linked_infrastructures_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_infrastructures_list_infrastructure',
							'label' => __('Infrastructure','aofds'), // Infrastructure
							'name' => 'linked_infrastructures_list_infrastructure',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'infrastructure',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add an infrastructure','aofds'), // Ajouter une infrastructure
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'club',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 3,
		));


                             
        /*********************************************************************/
        /*   FOR SPORT AND INFRASTRUCTURE                                    */
		/*********************************************************************/

		/**
		 * Linked sports
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_sports',
			'title' => __('Linked sports','aofds'), // Disciplines liées
			'fields' => array (
				array (
					'key' => 'field_linked_sports_title',
					'label' => __('Title','aofds'), // Title
					'name' => 'linked_sports_title',
					'type' => 'text',
					'default_value' => __('Linked sports','aofds'), // Disciplines liées
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_sports_number',
					'label' => __('Number to display','aofds'), // Nombre à afficher
					'name' => 'linked_sports_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_sports_list',
					'label' => __('Sports list','aofds'), // Liste des sports liés
					'name' => 'linked_sports_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_sports_list_sport',
							'label' => __('Sport','aofds'), // Discipline
							'name' => 'linked_sports_list_sport',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'sport',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add a sport','aofds'), // Ajouter une discipline
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 2,
		));


        /*********************************************************************/
        /*   FOR SPORT                                                       */
		/*********************************************************************/

		/**
		 * Linked clubs
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_clubs',
			'title' => __('Linked clubs','aofds'), // Disciplines liées
			'fields' => array (
				array (
					'key' => 'field_linked_clubs_title',
					'label' => __('Title','aofds'), // Title
					'name' => 'linked_clubs_title',
					'type' => 'text',
					'default_value' => __('Linked clubs','aofds'), // Disciplines liées
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_clubs_number',
					'label' => __('Number to display','aofds'), // Nombre à afficher
					'name' => 'linked_clubs_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_clubs_list',
					'label' => __('Clubs list','aofds'), // Liste des clubs liés
					'name' => 'linked_clubs_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_clubs_list_sport',
							'label' => __('Club','aofds'), // Discipline
							'name' => 'linked_clubs_list_club',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'club',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add a club','aofds'), // Ajouter un club
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 2,
		));


		/**
		 * Linked clubs
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_sport_parent',
			'title' => __('Sport parent','aofds'), // Disciplines parent
			'fields' => array (
                array (
                    'key' => 'field_sport_parent',
                    'label' => '', //__('Prices', 'aofds'), // 'Contact',
                    'name' => 'sport_parent',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array (
                        0 => 'sport',
                    ),
                    'taxonomy' => array (
                    ),
                    'allow_null' => 1,
                    'multiple' => 0,
                    'return_format' => 'id',
                    'ui' => 1,
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 1,
		));


        /*********************************************************************/
        /*   FOR NEWS, SPORT, INFRASTRUCTURE AND PAGE                        */
		/*********************************************************************/
        
        /*
		 * Header and thumbnails images 
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_images',
			'title' => __('Images and attributes','aofds'), // Images et attributs
			'fields' => array (
				array (
					'key' => 'field_images_header',
					'label' => __('Header image','aofds'), // Image d'en-tête
					'name' => 'images_header',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail',
					'label' => __('Thumbnail','aofds'), // Image miniature
					'name' => 'images_thumbnail',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail_name',
					'label' => __('Short name for thumbnail','aofds'), // Nom abrégé pour la miniature
					'name' => 'images_thumbnail_name',
					'type' => 'text',
                    'instructions' => __('If empty, the title will be used', 'aofds'), // Si vide, le titre sera utilisé
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_images_icon',
					'label' => __('Icon','aofds'), // Icône
					'name' => 'images_icon',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_direct_contact_label',
					'label' => __('Direct contact label','aofds'), // Icône
					'name' => 'direct_contact_label',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => __('Phone number', 'aofds'), // Téléphone
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_direct_contact_number',
					'label' => __('Direct contact number','aofds'), // Icône
					'name' => 'direct_contact_number',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'news',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 3,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'club',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'meeting',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'team',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));

		register_field_group(array (
			'id' => 'acf_images_infra',
			'title' => __('Images and attributes','aofds'), // Images et attributs
			'fields' => array (
				array (
					'key' => 'field_images_header',
					'label' => __('Header image','aofds'), // Image d'en-tête
					'name' => 'images_header',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_map',
					'label' => __('Map','aofds'), // Image miniature
					'name' => 'images_map',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail',
					'label' => __('Thumbnail','aofds'), // Image miniature
					'name' => 'images_thumbnail',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail_name',
					'label' => __('Short name for thumbnail','aofds'), // Nom abrégé pour la miniature
					'name' => 'images_thumbnail_name',
					'type' => 'text',
                    'instructions' => __('If empty, the title will be used', 'aofds'), // Si vide, le titre sera utilisé
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_images_icon',
					'label' => __('Icon','aofds'), // Icône
					'name' => 'images_icon',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_direct_contact_label',
					'label' => __('Direct contact label','aofds'), // Icône
					'name' => 'direct_contact_label',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => __('Phone number', 'aofds'), // Téléphone
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_direct_contact_number',
					'label' => __('Direct contact number','aofds'), // Icône
					'name' => 'direct_contact_number',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
        
        /**
		 * Share buttons
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_social_share',
			'title' => __('Social network', 'aofds'), // Réseaux sociaux
			'fields' => array (
				array (
					'key' => 'field_social_share',
					'label' => __('Social share', 'aofds'), // Partage sur les réseaux sociaux
					'name' => 'social_share',
					'type' => 'true_false',
					'message' => __('Display share buttons', 'aofds'), // Afficher les boutons de partage
					'default_value' => 1,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'news',
						'order_no' => 0,
						'group_no' => 2,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 3,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'club',
						'order_no' => 0,
						'group_no' => 4,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'team',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 4,
		));
        
	}
}
add_action('init', 'aofds_custom_fields_load_acf');





function aofds_layout_section() {
    
    $layout = array (
        'key' => 'field_section',
        'name' => 'section',
        'label' => __('Section title (Title 2)', 'aofds'), // 'Section',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_section_title',
                'label' => '', //__('Section title', 'aofds'), // 'Titre de la section',
                'name' => 'section_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_subsection() {

     $layout = array (
        'key' => 'field_subsection',
        'name' => 'subsection',
        'label' => __('Subsection title (Title 3)', 'aofds'), // 'Sous-section',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_subsection_title',
                'label' => '', //__('Subsection title', 'aofds'), // 'Titre de la sous-section',
                'name' => 'subsection_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_content() {
    
    $layout = array (
        'key' => 'field_content',
        'name' => 'content',
        'label' => __('Content', 'aofds'), // 'Texte',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_content_text',
                'label' => '', //__('Content text', 'aofds'), // 'Texte du contenu',
                'name' => 'content_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_images() {

    $layout = array (
        'key' => 'field_images',
        'name' => 'images',
        'label' => __('Pictures', 'aofds'), // 'Photos',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_images_repeater',
                'label' => '', //__('Pictures', 'aofds'), // 'Photos',
                'name' => 'images_repeater',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'min' => '',
                'max' => '',
                'layout' => 'table',
                'button_label' => __('Add a picture', 'aofds'), // 'Ajouter une photo',
                'sub_fields' => array (
                    array (
                        'key' => 'field_images_repeater_image',
                        'label' => __('Picture', 'aofds'), // 'Photo',
                        'name' => 'images_repeater_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array (
                        'key' => 'field_images_repeater_caption',
                        'label' => __('Caption', 'aofds'), // 'Légende',
                        'name' => 'images_repeater_caption',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => 3,
                        'new_lines' => 'wpautop',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
            ),
            array (
                'key' => 'field_images_options',
                'label' => __('Display options', 'aofds'), // 'Options d'affichage',
                'name' => 'images_options',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array (
                    'carousel' => __('Diaporama', 'aofds'), // 'Diaporama',
                    'gallery' => __('Gallery', 'aofds'), // 'Galerie',
                ),
                'default_value' => array (
                    'carousel' => 'carousel',
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
                'disabled' => 0,
                'readonly' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_prices() {
    
    $layout = array (
        'key' => 'field_prices',
        'name' => 'prices',
        'label' => __('Link prices', 'aofds'), // 'Tarifs',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_prices_object',
                'label' => '', //__('Prices', 'aofds'), // 'Contact',
                'name' => 'prices_object',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'price',
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_equipment_table() {

    $layout = array (
        'key' => 'field_equipment_table',
        'name' => 'equipment_table',
        'label' => __('Equipment table', 'aofds'), // 'Location de matériel',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_equipment_table_repeater',
                'label' => '', //__('Equipment rentals table', 'aofds'), // 'Location de matériel',
                'name' => 'equipment_table_repeater',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_equipment_table_repeater_element',
                        'label' => __('Equipment', 'aofds'), // 'Matériel',
                        'name' => 'equipment_table_repeater_element',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '70',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_equipment_table_repeater_price',
                        'label' => __('Price', 'aofds'), //'Prix',
                        'name' => 'equipment_table_repeater_price',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '30',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
                'row_min' => 1,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => __('Add an equipment', 'aofds'), // Ajouter un matériel
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_features_table() {

    $layout = array (
        'key' => 'field_features_table',
        'name' => 'features_table',
        'label' => __('Features table', 'aofds'), // 'Caractéristique',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_features_table_repeater',
                'label' => '', 
                'name' => 'features_table_repeater',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_features_table_repeater_element',
                        'label' => __('Feature', 'aofds'), // 'Caractéristique',
                        'name' => 'features_table_repeater_element',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '70',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_features_table_repeater_value',
                        'label' => __('Value', 'aofds'), //'Valeur',
                        'name' => 'features_table_repeater_value',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '30',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
                'row_min' => 1,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => __('Add a feature', 'aofds'), // Ajouter une caractéristique
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_schedule_table() {

    $layout = array (
        'key' => 'field_schedule_table',
        'name' => 'schedule_table',
        'label' => __('Schedule table', 'aofds'), // 'Horaires',
        'display' => 'block',
        'sub_fields' => array (
            array(
                'key' => 'field_schedule_table_repeater',
                'label' => '', //__('Schedule','aofds'), // 'Horaires',
                'name' => 'schedule_table_repeater',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'schedule_table_repeater_day',
                        'label' => __('Day','aofds'), // Jour
                        'name' => 'schedule_table_repeater_day',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_time',
                        'label' => __('Time','aofds'), // Horaire
                        'name' => 'schedule_table_repeater_time',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_infrastructure',
                        'label' => __('Infrastructure','aofds'), // Salle
                        'name' => 'schedule_table_repeater_infrastructure',
                        'type' => 'post_object',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'post_type' => array (
                            0 => 'infrastructure',
                        ),
                        'taxonomy' => array (
                        ),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'return_format' => 'object',
                        'ui' => 1,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_type',
                        'label' => __('Type','aofds'), // Type
                        'name' => 'schedule_table_repeater_type',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_level',
                        'label' => __('Level','aofds'), // Niveau
                        'name' => 'schedule_table_repeater_level',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '30',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_age',
                        'label' => __('Age','aofds'), // Age
                        'name' => 'schedule_table_repeater_age',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
                'row_min' => 0,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => __('Add a period','aofds'), // Ajouter une période
            ),
        ),
    );

    return $layout;
}


function aofds_layout_address() {

     $layout = array (
        'key' => 'field_address',
        'name' => 'address',
        'label' => __('Address', 'aofds'), // 'Adresse',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_address_text',
                'label' => '', //__('Address', 'aofds'), // 'Address',
                'name' => 'address_text',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 3,
                'new_lines' => 'wpautop',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_phone() {

     $layout = array (
        'key' => 'field_phone',
        'name' => 'phone',
        'label' => __('Phone', 'aofds'), // 'Téléphone',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_phone_text',
                'label' => '', //__('Phone', 'aofds'), // 'Téléphone',
                'name' => 'phone_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_mobile() {

     $layout = array (
        'key' => 'field_mobile',
        'name' => 'mobile',
        'label' => __('Mobile', 'aofds'), // 'Portable',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_mobile_text',
                'label' => '', //__('Mobile', 'aofds'), // 'Portable',
                'name' => 'mobile_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_email() {

     $layout = array (
        'key' => 'field_email',
        'name' => 'email',
        'label' => __('Email', 'aofds'), // 'Email',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_email_text',
                'label' => '', //__('Email', 'aofds'), // 'email',
                'name' => 'email_text',
                'type' => 'email',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_web() {

     $layout = array (
        'key' => 'field_web',
        'name' => 'web',
        'label' => __('Web page', 'aofds'), // 'Page web',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_web_text',
                'label' => '', //__('Web page', 'aofds'), // 'Page web',
                'name' => 'web_text',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_facebook() {

     $layout = array (
        'key' => 'field_facebook',
        'name' => 'facebook',
        'label' => __('Facebook page', 'aofds'), // 'Page Facebook',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_facebook_text',
                'label' => '', //__('Facebook page', 'aofds'), // 'Page Facebook',
                'name' => 'facebook_text',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_motto() {

     $layout = array (
        'key' => 'field_motto',
        'name' => 'motto',
        'label' => __('Motto', 'aofds'), // 'Devise',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_motto_text',
                'label' => '', 
                'name' => 'motto_text',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 3,
                'new_lines' => 'wpautop',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_description() {

     $layout = array (
        'key' => 'field_description',
        'name' => 'description',
        'label' => __('Description', 'aofds'), // 'Description',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_description_text',
                'label' => '', //__('Description', 'aofds'), // 'Description',
                'name' => 'description_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aofds_layout_contact() {
    
    $layout = array (
        'key' => 'field_contact',
        'name' => 'contact',
        'label' => __('Link a contact', 'aofds'), // 'Contact',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_contact_object',
                'label' => '', //__('Contact', 'aofds'), // 'Contact',
                'name' => 'contact_object',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'contact',
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aofds_layout_tab() {
    
    $layout = array (
        'key' => 'field_tab',
        'name' => 'tab',
        'label' => __('Tab', 'aofds'), // 'Contact',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_tab_title',
                'label' => __('Tab title', 'aofds'), 
                'name' => 'tab_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),  
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aofds_layout_tabs_end() {
    
    $layout = array (
        'key' => 'field_tabs_end',
        'name' => 'tabs_end',
        'label' => __('Tabs end', 'aofds'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'acf_tabs_end_message',
                'label' => '', //__('Tabs end', 'aofds'), 
                'name' => 'tabs_end_message',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => __('This element indicates the end of tabs contents', 'aofds'), 
                'esc_html' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



/* End of file aofds-custom-fields.php */
/* Location: ./wp-content/themes/aofds/inc/aofds-custom-fields.php */