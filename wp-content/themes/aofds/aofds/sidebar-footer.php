<?php
/** sidebar-bottom-page.php
 *
 * Displays the sidebar at the bottom of the page (full page width)
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Bottom Page -->
<?php if (is_active_sidebar( 'sidebar-bottom-page' )) : ?>
    <div id="sidebar-bottom-page">
	   <?php dynamic_sidebar( 'sidebar-bottom-page' ); ?>
    </div>
<?php endif; ?>
<!--/Sidebar Bottom Page -->

<?
/* End of file sidebar-bottom-page.php */
/* Location: ./wp-content/themes/aofds/sidebar-bottom-page.php */