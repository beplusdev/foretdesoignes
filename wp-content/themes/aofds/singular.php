<?php
/** singular.php
 *
 * The template for displaying all singular posts.
 *
 * @author	Alley Oop
 * @package	AO Foret de Soignes
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <article>

                    <?php if ($post_type == 'club') : ?>
                        <?php if ($image = get_field('images_thumbnail')) : ?>
                            <div class="entry-logo">
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title; ?>" title="<?php the_title; ?>" />
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <h1 class="entry-title">
                        <?php aofds_event_calendar(); ?>
                        <?php the_title(); ?>
                    </h1>   

                    <?php if ($post_type == 'news') : ?>
                        <div class="entry-meta">
                            <?php aofds_entry_meta_news(); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ( has_term( 'event', 'newscat', get_the_ID() ) ) : ?>
                    <div class="entry-event">
                        <?php echo aofds_display_layout_event(); ?>
                    </div>
                    <?php endif; ?>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>   

                    <?php
                    $flexible_field = get_post_type() . '_flexible';

                    // check if the flexible content field has rows of data
                    if ( have_rows( $flexible_field ) ):

                        // loop through the rows of data
                        while ( have_rows( $flexible_field ) ) : the_row();

                            echo aofds_display_layout( get_row_layout() );

                        endwhile;

                    else :

                        // no layouts found

                    endif;

                    ?>

                </article>

                <?php

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <!-- Social share -->
        <?php if (function_exists('ao_insert_social_share')) : ?>
            <?php $social_share = get_field('social_share'); ?>
            <?php if ($social_share) : ?>
                <?php $url= get_permalink(); ?>
                <?php ao_insert_social_share($url); ?>
            <?php endif; ?>	
        <?php endif; ?>
        <!--/Social share -->

        <?php
        //$post = $wp_query->post;
        //echo $post->ID;
        ?>
        
    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file singular.php */
/* Location: ./wp-content/themes/aofds/singular.php */