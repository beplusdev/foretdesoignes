<?php
/** archive.php
 *
 * The template for displaying archive page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
*/
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 meetings">

        <h1 class="archive-title">
            <?php 
            post_type_archive_title(); 
            if (is_tax()) :
                global $wp_query;
                $term = $wp_query->get_queried_object();
                echo $term->name;
            endif;
            ?>
        </h1>

        <?php get_sidebar( 'bottom-title' ); ?>
        
        <?php 
        if ( have_posts() ) : 
        
            ?>
        
            <div class="row">
                
            <?php
            $pre_terms = array();
            $first = true;
        
            // The loop 
            while ( have_posts() ) : the_post();

                $post_type = get_post_type();
                
                ?>
                <div class="meeting col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <article>
        
                    <a href="<?php the_permalink(); ?>"> 

                        <div class="image-wrapper">
                            <?php 
                            $image = get_field('images_thumbnail'); 
                            if ($image) : 
                                $title = get_the_title();
                                echo '<div class="image" style="background-image: url('.$image['sizes']['two-third'].');"></div>';		
                            elseif ($default = get_option('default_'.$post_type.'_image')) : 
                                echo '<div class="image" style="background-image: url('.$default.');"></div>';
                            endif;
                            ?>
                            <div class="filter"></div>
                        </div>

                        <div class="caption ">

                            <div class="title">
                                <?php //aoa3f_event_calendar(); ?>
                                <?php the_title(); ?>
                            </div>   

                            <div class="content">

                                <?php 
                                if (!$excerpt = get_the_excerpt()) :
                                    $excerpt = get_the_content();
                                endif;
                                $excerpt = wp_strip_all_tags($excerpt);
                                if ( mb_strlen( $excerpt ) > $charlength ) :
                                    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                                    $exwords = explode( ' ', $subex );
                                    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                                    if ( $excut < 0 ) :
                                        echo mb_substr( $subex, 0, $excut );
                                    else :
                                        echo $subex;
                                    endif;
                                    echo '...';
                                else :
                                    echo $excerpt;
                                endif;
                                ?>

                            </div>   

                            <button class="btn read-more">
                                <?php _e('Read more', 'aoa3f'); ?>
                            </button>                            

                        </a>
                        
                    </div>
                    
                </article>
                </div>
                    
                <?php
            endwhile;
            ?>
                
            </div>

            <?php
        
        // No posts to display
        else :

            echo '<p class="no-post">'.__('There is currently no meeting room.', 'aoa3f').'</p>';

        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <?php
//$post = $wp_query->post;

//echo $post->ID;
    ?>
        
    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file archive.php */
/* Location: ./wp-content/themes/aoa3f/archive.php */