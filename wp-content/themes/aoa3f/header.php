<?php
/** header.php
 *
 * Displays all of the <head> section and everything up till </header>
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php load_theme_textdomain( 'aoa3f', get_template_directory() . '/lang' ); ?>

<script> 
var $buoop = {vs:{i:8,f:15,o:12.1,s:5.1},c:2}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script> 


<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
        
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="format-detection" content="telephone=no">
		
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
        
        <?php aoa3f_social_meta_tags(); ?>
        
		<?php wp_head(); ?>

	</head>
	
	<body <?php body_class(); ?>>

        
        <div id="site-header" class="clearfix">

            <div class="inner">
                
                <?php //get_sidebar( 'top' ); ?>
                <div id="site-header-top">
                    <div class="container">
                        <div class="contact">
                            <div class="mail">
                                <span class="flaticon flaticon-interface"></span>
                                <span class="info"><a href="mailto:<?php echo get_option('contact_email'); ?>"><?php echo get_option('contact_email'); ?></a></span>
                            </div>
                            <div class="phone">
                                <span class="flaticon flaticon-phone"></span>
                                <span class="info"><a href="tel:<?php echo get_option('contact_number'); ?>"><?php echo get_option('contact_number'); ?></a></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="container">

                    <div class="row">

                        <nav class="navbar" role="navigation">
                            <div class="navbar-inner">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only"><?php _e('Toggle navigation', 'aoa3f'); ?></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php echo (function_exists(pll_home_url)) ? pll_home_url() :  get_site_url(); ?>">
                                        <img class="logo" src="<?php echo get_option( 'logo_url' ); ?>" alt="<?php echo get_bloginfo( 'name', 'display' ); ?>">
                                        <span class="title"><?php echo get_bloginfo( 'name', 'display' ); ?></span>
                                    </a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <?php
                                    wp_nav_menu( array(
                                        'container'			=>	'nav',
                                        'container_class'	=>	'subnav clearfix',
                                        'theme_location'	=>	'primary',
                                        'menu_class'		=>	'nav navbar-nav pull-right',
                                        'depth'				=>	3,
                                        'fallback_cb'		=>	false,
                                        'walker'			=>	new AOForetDeSoignes_Nav_Walker,
                                    ) ); 
                                    ?>
                                <ul class="lang"><?php pll_the_languages(array('display_names_as' => 'slug', 'hide_if_empty' => 0, 'hide_current' => '1'));?></ul>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
                
            </div>

        </div>

        <?php
        $post = $wp_query->post;
        $has_header_image = false;
        if (is_home()) :
        elseif (is_singular()) :
            if ($header_image = get_field('images_header', $post->ID)) :
                $has_header_image = true;
            endif;
        else :
            $has_header_image = true;
         endif; 
        ?>

        <div id="site" class="<?php echo ($has_header_image || is_front_page()) ? 'has-header-image' : 'no-header-image'; ?>">
        
            <div class="">
                            
                <div id="site-center" class="clearfix">

                    <?php /*
                    <div id="site-left-column">
                
                        <?php get_sidebar( 'left-content' ); ?>

                    </div>
                    */ ?>

                    <div id="site-main-column">

                        <div id="site-main-header">

                            <?php
                            $post = $wp_query->post;
                            if (is_home()) :
                                get_sidebar( 'header' );
                            elseif (is_singular()) :
                                if ($header_image = get_field('images_header', $post->ID)) :
                                    echo '<div class="header-image" style="background-image: url('.$header_image['url'].');"></div>';
                                endif;
                            else :
                                echo '<div class="header-image" style="background-image: url('.get_option('header_image').');"></div>';
                            endif; 
                            ?>

                            
                            
                            <?php get_sidebar( 'top' ); ?>
                            
                            <?php /*
                            <div class="container ovale">
                                
                            <?php 
                            if (is_singular()) :
                                $sport_id = get_field('club_infos_sport', $post->ID);
                                if ($sport_id) $parent_id = get_field('sport_parent', $sport_id);
                                else $parent_id = get_field('sport_parent', $post->ID);
                                if ($parent_id or $sport_id) : ?>
                                    <div class="sport">
                                        <?php if ($parent_id) : ?>
                                            <a href="<?php echo get_post_permalink($parent_id); ?>">
                                                <i class="flaticon flaticon-<?php echo get_field('images_icon', $parent_id); ?>"></i><?php echo get_the_title($parent_id); ?>
                                            </a>
                                            <?php if ($sport_id) : ?>
                                            <i class="fa fa-caret-right"></i>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php if ($sport_id) : ?>
                                            <a href="<?php echo get_post_permalink($sport_id); ?>">
                                                <i class="flaticon flaticon-<?php echo get_field('images_icon', $sport_id); ?>"></i><?php echo get_the_title($sport_id); ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <?php 
                                endif; 
                            endif; 
                            ?> 

                            <?php 
                            $phone_label = (is_singular() and isset($post) and get_field('direct_contact_label', $post->ID)) ? get_field('direct_contact_label', $post->ID) : get_option('direct_contact_label');
                            $phone_number = (is_singular() and isset($post) and get_field('direct_contact_number', $post->ID)) ? get_field('direct_contact_number', $post->ID) : get_option('direct_contact_number');
                            if (is_mobile()) $phone_number = '<a href="tel:'.$phone_number.'">'.$phone_number.'</a>';
                            ?>
                            
                            <div class="booking">
                                <div class="phone">
                                    <h3><?php echo $phone_label; ?></h3>
                                    <div>
                                        <i class="flaticon flaticon-phone21"></i><?php echo $phone_number; ?>
                                    </div>
                                </div>
                                <div class="address">
                                    <h3 style="margin-top: 10px;">Adresse</h3>
                                    <address><em>Chaussée de Wavre, 2057<br>1160 Bruxelles</em></address>
                                </div>
                            </div>

                            <div class="booking-mobile">
                                <div class="phone">
                                    <i class="flaticon flaticon-phone21"></i><?php echo $phone_number; ?>
                                </div>
                                <div class="address nowrap">
                                    <i class="fa fa-map-marker"></i><address><em>Chaussée de Wavre 2057, 1160 Bruxelles</em></address>
                                </div>
                            </div>

                            </div>
                            */ ?>
                            
                            <?php /*
                            <div id="booking-form">
                                <div class="container">
                                    <?php $url = (pll_current_language('slug') == 'en') ? get_option('group_url_en') : get_option('group_url_fr'); ?>
                                    <form class="form-inline row" action="<?php echo $url; ?>" method="post">
                                        <div class="form-group col-sm-3">
                                            <div class="input-group">
                                                <div class="input-group-addon"><?php _e('From', 'aoa3f'); ?></div>
                                                <input type="text" class="form-control datepicker" id="startdate" name="startdate" placeholder="">
                                                <div class="input-group-addon"><span class="flaticon flaticon-calendar"></span></div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <div class="input-group">
                                                <div class="input-group-addon"><?php _e('To', 'aoa3f'); ?></div>
                                                <input type="text" class="form-control datepicker" id="enddate" name="enddate" placeholder="">
                                                <div class="input-group-addon"><span class="flaticon flaticon-calendar"></span></div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <button type="submit" class="btn btn-default" id="room"><?php _e('Book room', 'aoa3f'); ?></button>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <button type="submit" class="btn btn-default" id="group"><?php _e('Group quotation', 'aoa3f'); ?></button>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                        jQuery(function() {
                                            jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ "<?php echo pll_current_language('slug'); ?>" ] );
                                            jQuery.datepicker.formatDate( "dd/mm/yyyy" );
                                            jQuery( "#booking-form #startdate" ).datepicker({
                                                minDate: new Date(),
                                                defaultDate: "+1w",
                                                changeMonth: false,
                                                numberOfMonths: 2,
                                                firstDay: 1,
                                                onClose: function( selectedDate ) {
                                                    var date = jQuery(this).datepicker('getDate');
                                                    if (date) {
                                                          date.setDate(date.getDate() + 1);
                                                    }
                                                    jQuery( "#booking-form #enddate" ).datepicker( "option", "minDate", date );
                                                    jQuery( "#booking-form #enddate" ).datepicker( "show" );
                                                }
                                            });
                                            jQuery( "#booking-form #enddate" ).datepicker({
                                                defaultDate: "+1w",
                                                changeMonth: false,
                                                numberOfMonths: 2,
                                                firstDay: 1,
                                            });
                                            jQuery( "#booking-form #room").click(function() {
                                                var s = jQuery( "#booking-form #startdate" ).val().split( "/" );
                                                var e = jQuery( "#booking-form #enddate" ).val().split( "/" );
                                                var startdate = s[2] + '-' + s[1] + '-' + s[0];
                                                var enddate = e[2] + '-' + e[1] + '-' + e[0];
                                                centerWindowBooking('https://booking.cubilis.eu/select_dates.aspx?logisid=3351&booktemplate=1&lang=fr&startdatum='+startdate+'&einddatum='+enddate,'465','819','no','no');
                                                return false;
                                            });
                                            jQuery( "#booking-form #group").click(function() {
                                                var s = jQuery( "#booking-form #startdate" ).val().split( "/" );
                                                var e = jQuery( "#booking-form #enddate" ).val().split( "/" );
                                                var startdate = s[2] + '-' + s[1] + '-' + s[0];
                                                var enddate = e[2] + '-' + e[1] + '-' + e[0];
                                                jQuery( "booking-form" ).submit();
                                            });
                                        });
                                    </script>
                                    
                                </div>
                            </div>
                            */ ?>
                            
                        </div>

                        <div id="site-main" class="container">


<?
/* End of file header.php */
/* Location: ./wp-content/themes/aoa3f/header.php */
