<?php
/** archive.php
 *
 * The template for displaying archive page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
*/
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <h1 class="archive-title">
            <?php 
            post_type_archive_title(); 
            if (is_tax()) :
                global $wp_query;
                $term = $wp_query->get_queried_object();
                echo $term->name;
            endif;
            ?>
        </h1>

        <?php
        if (is_tax('newscat', 'event')) : 
            $when = (isset($_GET['w']) and ($_GET['w'] == 'past')) ? 'past' : 'future'; 
            $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
            ?>
            <ul class="nav nav-tabs event-tabs">
                <li class="<?php echo ($when == 'future') ? 'active' : ''; ?>"><a href="<?php echo $url; ?>"><?php _e('Future', 'aoa3f'); ?></a></li>
                <li class="<?php echo ($when == 'past') ? 'active' : ''; ?>"><a href="<?php echo $url.'?w=past'; ?>"><?php _e('Past', 'aoa3f'); ?></a></li>				
            </ul>
            <?php
        endif;
        ?>
        
        <?php 
        if ( have_posts() ) : 
        
            ?>
            
            <?php

            // The loop 
            while ( have_posts() ) : the_post();

                $post_type = get_post_type();

                if (!get_field('sport_parent')) :
                ?>
        
                <article>
        
                    <a href="<?php the_permalink(); ?>">

                        <?php 
                        $image = get_field('images_thumbnail'); 
                        if ($image) : 
                            $title = get_the_title();
                            echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$title.'" title="'.$title.'" class="thumbnail" />';		
                        elseif ($default = get_option('default_'.$post_type.'_image')) : 
                            echo '<img src="'.$default.'" class="default thumbnail" />';
                        endif;
                        ?>

                    </a>
                    
                    <h1 class="entry-title">
                        <a href="<?php the_permalink(); ?>">
                            <?php aoa3f_event_calendar(); ?>
                            <?php the_title(); ?>
                        </a>
                    </h1>   
                    
                    <div class="entry-meta">

                        <?php if ($post_type == 'news') : ?>
                        
                            <?php aoa3f_entry_meta_news(); ?>
                        
                        <?php elseif ($post_type == 'club') : ?>
                        
                            <?php 
                            $categories = array();
                            $terms = get_the_terms( get_the_ID(), 'clubcat' );
                            if ( !empty( $terms ) ) :
                                foreach ( $terms as $term )
                                    $categories[] = '<a href="' .get_term_link($term->slug, 'clubcat') .'">'.$term->name.'</a> ';
                            endif;
                            $categories_list = implode(', ', $categories); //get_the_taxonomies(); get_the_category_list( __( ', ', 'aoa3f' ) );
                            if ( $categories_list ) :
                                echo '<span class="cat clubcat"><span class="fa fa-folder-open"></span>' . $categories_list . '</span>';
                            endif;
                            ?>
                        
                        <?php elseif ($post_type == 'infrastructure') : ?>
                        
                            <?php 
                            $categories = array();
                            $terms = get_the_terms( get_the_ID(), 'infrastructurecat' );
                            if ( !empty( $terms ) ) :
                                foreach ( $terms as $term )
                                    $categories[] = '<a href="' .get_term_link($term->slug, 'infrastructurecat') .'">'.$term->name.'</a> ';
                            endif;
                            $categories_list = implode(', ', $categories); //get_the_taxonomies(); get_the_category_list( __( ', ', 'aoa3f' ) );
                            if ( $categories_list ) :
                                echo '<span class="cat infrastructurecat"><span class="fa fa-folder-open"></span>' . $categories_list . '</span>';
                            endif;
                            ?>
                        
                        <?php elseif ($post_type == 'meeting') : ?>
                        
                            <?php 
                            $categories = array();
                            $terms = get_the_terms( get_the_ID(), 'meetingcat' );
                            if ( !empty( $terms ) ) :
                                foreach ( $terms as $term )
                                    $categories[] = '<a href="' .get_term_link($term->slug, 'meetingcat') .'">'.$term->name.'</a> ';
                            endif;
                            $categories_list = implode(', ', $categories); //get_the_taxonomies(); get_the_category_list( __( ', ', 'aoa3f' ) );
                            if ( $categories_list ) :
                                echo '<span class="cat meetingcat"><span class="fa fa-folder-open"></span>' . $categories_list . '</span>';
                            endif;
                            ?>
                        
                        <?php endif; ?>

                        <!-- Linked posts -->
                        <?php /*if ($linked_posts = get_field('linked_'.$posttype.'s_list')) : ?>

                            <div class="linked linked-<?php echo $posttype; ?>s <?php echo $layout; ?>">

                                <?php echo $before_title . $title . $after_title; ?>

                                <div class="row">
                                    <?php 
                                    $i = 0;
                                    $n = (get_field('linked_'.$posttype.'s_number')) ? min(count($linked_posts), get_field('linked_'.$posttype.'s_number')) : count($linked_posts); 
                                    ?>
                                    <?php while (($i < $n) and count($linked_posts)) : ?>

                                        <?php
                                        $random = array_rand( $linked_posts );
                                        $random_post = $linked_posts[ $random ];
                                        $linked_post = $random_post['linked_'.$posttype.'s_list_'.$posttype];
                                        unset($linked_posts[$random]);
                                        ?>

                                        <div class="col col-lg-<?php echo $grid; ?> col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-<?php echo $grid; ?> effect <?php if (($i % (12/$grid)) == 0) echo 'clearfix'; ?>">

                                            <?php $i = $i + 1; ?>

                                            <?php 
                                            if ($layout == 'titles-with-icons') :
                                                $icon = get_field('images_icon', $linked_post->ID);
                                                echo '<a href="'.get_post_permalink($linked_post->ID).'">';
                                                echo '<i class="flaticon flaticon-'.$icon.'"></i>';
                                                echo '</a>';
                                            endif; 
                                            ?>

                                            <?php 
                                            if ($layout == 'titles-with-logos') :
                                                $image = get_field('images_thumbnail', $linked_post->ID); 
                                                if ($image) : 
                                                    $title = get_the_title($linked_post->ID);
                                                    echo '<a href="'.get_post_permalink($linked_post->ID).'">';
                                                    echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$title.'" title="'.$title.'" />';
                                                    echo '</a>';
                                                elseif ($default = get_option('default_'.$posttype.'_image')) : 
                                                    echo '<img src="'.$default.'" class="default" />';
                                                endif;
                                            endif;
                                            ?>

                                            <?php 
                                            if ($layout == 'titles-with-maps') :
                                                $image = get_field('images_map', $linked_post->ID); 
                                                if ($image) : 
                                                    $title = get_the_title($linked_post->ID);
                                                    echo '<a href="'.get_post_permalink($linked_post->ID).'">';
                                                    echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$title.'" title="'.$title.'" />';
                                                    echo '</a>';
                                                elseif ($default = get_option('default_'.$posttype.'_image')) : 
                                                    echo '<img src="'.$default.'" class="default" />';
                                                endif;
                                            endif;
                                            ?>

                                            <?php 
                                            $sport_id = get_field('club_infos_sport', $linked_post->ID);
                                            $parent_id = get_field('sport_parent', $sport_id);
                                            if ($parent_id and ($parent_id == $post->ID)) : ?>
                                                <div class="sport">
                                                    <a href="<?php echo get_post_permalink($sport_id); ?>">
                                                        <?php echo get_the_title($sport_id); ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?> 

                                            <div class="title">
                                                <a href="<?php echo get_post_permalink($linked_post->ID); ?>">
                                                <?php 
                                                if ($title = get_field('images_thumbnail_name', $linked_post->ID)) :
                                                    echo $title;
                                                else :
                                                    $title = get_the_title($linked_post->ID);
                                                    echo $title;
                                                    //echo (strlen($title) > 15) ? substr($title, 0, 15).'...' : $title; 
                                                endif;
                                                ?>
                                                </a>
                                            </div>

                                        </div>

                                    <?php endwhile; ?>
                                </div>

                            </div>

                        <?php endif; */?>
                        <!--/Linked posts -->

                    </div>   

                    <div class="entry-excerpt">

                        <a href="<?php the_permalink(); ?>">

                        <?php the_excerpt(); ?>

                        </a>

                    </div>   

                    <div class="read-more">
                        <a href="<?php the_permalink(); ?>">    
                            <button class="btn">
                                <?php _e('Read more', 'aoa3f'); ?>
                            </button>                            
                        </a>
                    </div>
                    
                </article>
                    
                <?php
                endif;

            endwhile;
                        
        // No posts to display
        else :

            if (is_tax('newscat', 'news')) :
                echo '<p class="no-post">'.__('There is currently no news.', 'aoa3f').'</p>';
            elseif (is_tax('newscat', 'event')) :
                echo '<p class="no-post">'.__('There is currently no event.', 'aoa3f').'</p>';
            else :
                echo '<p class="no-post">'.__('There is currently no post.', 'aoa3f').'</p>';
            endif;

        endif; 
        ?>
    
        <nav class="navigation paging-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php __( 'Posts navigation', 'aoa3f' ); ?></h1>
            <div class="nav-links">

                <?php if ( get_next_posts_link() ) : ?>
                <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'aoa3f' ) ); ?></div>
                <?php endif; ?>

                <?php if ( get_previous_posts_link() ) : ?>
                <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'aoa3f' ) ); ?></div>
                <?php endif; ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->

        <?php get_sidebar( 'bottom-content' ); ?>
        
        <?php
//$post = $wp_query->post;

//echo $post->ID;
    ?>
        
    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file archive.php */
/* Location: ./wp-content/themes/aoa3f/archive.php */