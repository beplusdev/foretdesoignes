<?php
/** sidebar-right-content.php
 *
 * Displays the sidebar on the right side of the content
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Right Content -->
<?php if (is_active_sidebar( 'sidebar-right-content' )) : ?>
    <div id="sidebar-right-content" class="sidebar-content">
        <div class="row">
        <?php dynamic_sidebar( 'sidebar-right-content' ); ?>
        </div>
    </div>
<?php endif; ?>
<!--/Sidebar Right Content -->

<?
/* End of file sidebar-right-content.php */
/* Location: ./wp-content/themes/aoa3f/sidebar-right-content.php */