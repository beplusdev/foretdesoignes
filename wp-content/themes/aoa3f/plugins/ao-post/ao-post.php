<?php
/*
 * Plugin Name: AO Post
 * Author URI: http://www.alleyoop.be
 * Description: A widget to display a post.
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AO_POST_VERSION', '1.0');
define('AO_POST_AUTHOR', 'Alley Oop');
define('AO_POST_URL', get_template_directory_uri() . '/plugins/ao-post/');


class ao_post_widget extends WP_Widget{

	## Initialize
	
	function ao_post_widget(){
	
		// set text domain
		$dom = 'ao-post';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-ao-post',
			'description' => __('Widget to display a post', 'ao-post'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('ao-post', __('Feature post', 'ao-post'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('ao_post', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$post_id = $instance['post_id'];
        
        echo $before_widget; 
        if ($title) echo $before_title . $title . $after_title;
        if ($post_id) {
            echo '<div class="featured-post">';
            echo apply_filters('the_content', get_post_field('post_content', $post_id));

            $flexible_field = get_post_type($post_id) . '_flexible';
            
            //echo $flexible_field;

            // check if the flexible content field has rows of data
            if ( have_rows( $flexible_field, $post_id ) ):

                echo '<div class="flexible">';

                // loop through the rows of data
                while ( have_rows( $flexible_field, $post_id ) ) : the_row();

                    echo aoa3f_display_layout( get_row_layout() );

                endwhile;

                echo '</div>';

            endif;
            
            echo '</div>';
        }
		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('ao_post', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		$instance = $old_instance;
		$instance['title'] = stripslashes($new_instance['title']);
		$instance['post_id'] = stripslashes($new_instance['post_id']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['ao_post']) )
			delete_option('ao_post');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('ao_post', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$post_id = isset( $instance['post_id'] ) ? esc_attr( $instance['post_id'] ) : '';
		
		?>

		<p class="ao-post-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'ao-post' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p class="ao-post-id">
			<label for="<?php echo $this->get_field_id( 'post_id' ); ?>"><?php _e( 'Post ID:', 'ao-post' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_id' ); ?>" name="<?php echo $this->get_field_name( 'post_id' ); ?>" type="text" value="<?php echo $post_id; ?>" />
		</p>

		<?php
	}
}

function ao_post_init(){
	register_widget('ao_post_widget');
}
add_action('widgets_init', 'ao_post_init');


function ao_post_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'ao_post_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'ao_post_widget_scripts' );

function ao_post_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'ao_post_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'ao_post_widget_css' );

?>
