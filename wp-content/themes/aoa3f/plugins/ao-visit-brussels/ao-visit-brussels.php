<?php
/*
 * Plugin Name: AO Visit Brussels
 * Author URI: http://www.alleyoop.be
 * Description: A visit brussels widget.
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AO_LAST_NEWS_VERSION', '1.0');
define('AO_LAST_NEWS_AUTHOR', 'Alley Oop');
define('AO_LAST_NEWS_URL', get_template_directory_uri() . '/plugins/ao-visit-brussels/');


## Include the required scripts
function ao_visit_brussels_public_scripts(){
    wp_enqueue_script('jquery');
	wp_register_script('ao-visit-brussels-js', AO_LAST_NEWS_URL . 'js/ao-visit-brussels.js', array(), '1');
	wp_enqueue_script(array('jquery', 'ao-visit-brussels-js'));
}
add_action('wp_enqueue_scripts', 'ao_visit_brussels_public_scripts');


## Include the required styles
function ao_visit_brussels_public_styles(){
	wp_register_style('ao-visit-brussels-css', AO_LAST_NEWS_URL . 'css/ao-visit-brussels.css', array(), '2');
	wp_enqueue_style('ao-visit-brussels-css');
}
add_action('wp_enqueue_scripts', 'ao_visit_brussels_public_styles');



class ao_visit_brussels_widget extends WP_Widget{

	## Initialize
	
	function ao_visit_brussels_widget(){
	
		// set text domain
		$dom = 'ao-visit-brussels';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-ao-visit-brussels',
			'description' => __('Widget to display Visit Brussels events', 'ao-visit-brussels'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('ao-visit-brussels', __('Visit Brussels', 'ao-visit-brussels'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('ao_visit_brussels', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        
		$number = ( ! empty( $instance['number'] ) ) ? $instance['number'] : '-1';
        $order = isset( $instance['order'] ) ? $instance['order'] : 'date';
        $direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
        //$posttype = isset( $instance['posttype'] ) ? $instance['posttype'] : 'sport';
        $layout = isset( $instance['layout'] ) ? $instance['layout'] : 'titles-with-text-and-thumbnails';
		$grid = ( ! empty( $instance['grid'] ) ) ? $instance['grid'] : '12';
        $diapo = isset( $instance['diapo'] ) ? $instance['diapo'] : '0';
        $newscat = $instance['newscat'];
        $charlength = ( ! empty( $instance['charlength'] ) ) ? $instance['charlength'] : '100';
        $options = json_decode($instance['options']);
		//$show_all_title = $instance['show_all_title'];

        
        echo '==== IP ADDRESS : '.$_SERVER['SERVER_ADDR'].'========<br>';
        $url = 'http://api.agenda.be/events/updates';
        $content = file_get_contents($url);
        $data = json_decode($content);
        var_dump($content);
        
		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'news',
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'newscat',
						'field' 	=> 'id',
						'terms' 	=> $newscat,
					),
				),
				'orderby' 	=> $order, 
                'order'     => $direction,
			)
		));
		//echo $r->request; 
        
        echo $before_widget; 
        echo $before_title . $title . $after_title;
        
        $i = 0;
        $n = 0;
        
        $id = 'carousel-news-'.rand(0, 999999);
        ?>

        <!-- Last news -->
        <?php if ($r->have_posts()) : ?>

            <div class="visit-brussels visit-brussels-<?php echo $newscat; ?>s <?php echo $layout; ?>">

                <?php if ($diapo) : ?>

                    <div id="<?php echo $id; ?>" class="carousel slide" data-ride="carousel" data-interval="false">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <div class="item active row" id="carousel-item-1">

                            <?php while ( $r->have_posts() ) : $r->the_post(); ?>

                                <?php if (($i > 0) and ($i % (12/$grid)) == 0) : ?>
                                    <?php $n += 1; ?>
                                    </div>
                                    <div class="item row" id="carousel-item-<?php echo $n; ?>">
                                <?php endif; ?>

                                <div class="col col-<?php echo $grid; ?> col-lg-<?php echo $grid; ?> col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-<?php echo $grid; ?> effect <?php if (($i % (12/$grid)) == 0) echo 'clearfix'; ?>">

                                    <a href="<?php the_permalink(); ?>">

                                        <?php 
                                        if (in_array($layout, array('titles-with-thumbnails', 'titles-with-texts-and-thumbnails', 'titles-with-texts-and-dates-and-thumbnails'))) :
                                            $image = get_field('images_thumbnail', get_the_ID()); 
                                            if ($image) : 
                                                $title = get_the_title($linked_post->ID);
                                                //echo '<img src="'.$image['sizes']['one-third'].'" alt="'.$title.'" title="'.$title.'" />';		
                                                echo '<div class="image" style="background-image: url('.$image['sizes']['one-third'].');"></div>';
                                            elseif ($default = get_option('default_'.$posttype.'_image')) : 
                                                //echo '<img src="'.$default.'" class="default" />';
                                                echo '<div class="image" style="background-image: url('.$default.');"></div>';
                                            endif;
                                        endif;
                                        ?>

                                        <div class="title">
                                            <?php 
                                            if (in_array($layout, array('titles-with-dates', 'titles-with-texts-and-dates', 'titles-with-texts-and-dates-and-thumbnails'))) :
                                                echo '<span class="date">'.get_the_date('d/m').'</span> - ';
                                            endif;
                                            the_title(); 
                                            ?>
                                        </div>

                                        <?php 
                                        if (in_array($layout, array('titles-with-texts', 'titles-with-texts-and-thumbnails', 'titles-with-texts-and-dates-and-thumbnails'))) :
                                            echo '<div class="content">';
                                            if (!$excerpt = get_the_excerpt()) :
                                                $excerpt = get_the_content();
                                            endif;
                                            $excerpt = wp_strip_all_tags($excerpt);
                                            if ( mb_strlen( $excerpt ) > $charlength ) :
                                                $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                                                $exwords = explode( ' ', $subex );
                                                $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                                                if ( $excut < 0 ) :
                                                    echo mb_substr( $subex, 0, $excut );
                                                else :
                                                    echo $subex;
                                                endif;
                                                echo '[...]';
                                            else :
                                                echo $excerpt;
                                            endif;
                                            echo '</div>';
                                        endif;
                                        ?>

                                    </a>

                                </div>

                                <?php $i = $i + 1 ; ?>

                            <?php endwhile; ?>

                            </div>

                        </div>

                        <!-- Controls -->
                        <?php if ($n > 0) : ?>
                            <a class="left carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="prev">
                                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only"><?php _e('Previous', 'aoa3f'); ?></span>
                            </a>
                            <a class="right carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="next">
                                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only"><?php _e('Next', 'aoa3f'); ?></span>
                            </a>
                        <?php endif; ?>

                    </div>


                <?php else : ?>

                    <div class="row">
                        
                        <?php while ( $r->have_posts() ) : $r->the_post(); ?>

                            <div class="col col-<?php echo $grid; ?> col-lg-<?php echo $grid; ?> col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-<?php echo $grid; ?> effect <?php if (($i % (12/$grid)) == 0) echo 'clearfix'; ?>">

                                <a href="<?php the_permalink(); ?>">

                                    <?php 
                                    if (in_array($layout, array('titles-with-thumbnails', 'titles-with-texts-and-thumbnails', 'titles-with-texts-and-dates-and-thumbnails'))) :
                                        $image = get_field('images_thumbnail', get_the_ID()); 
                                        if ($image) : 
                                            $title = get_the_title($linked_post->ID);
                                            $size = ($grid == 12) ? 'thumbnail' : 'one-third';
                                            //echo '<img src="'.$image['sizes'][$size].'" alt="'.$title.'" title="'.$title.'" />';		
                                            echo '<div class="image" style="background-image: url('.$image['sizes'][$size].');"></div>';
                                        elseif ($default = get_option('default_'.$posttype.'_image')) : 
                                            //echo '<img src="'.$default.'" class="default" />';
                                            echo '<div class="image" style="background-image: url('.$default.');"></div>';
                                        endif;
                                    endif;
                                    ?>

                                    <div class="title">
                                        <?php 
                                        if (in_array($layout, array('titles-with-dates', 'titles-with-texts-and-dates', 'titles-with-texts-and-dates-and-thumbnails'))) :
                                            echo '<span class="date">'.get_the_date('d/m').'</span> - ';
                                        endif;
                                        the_title(); 
                                        ?>
                                    </div>

                                    <?php 
                                    if (in_array($layout, array('titles-with-texts', 'titles-with-texts-and-thumbnails', 'titles-with-texts-and-dates-and-thumbnails'))) :
                                        echo '<div class="content">';
                                        if (!$excerpt = get_the_excerpt()) :
                                            $excerpt = get_the_content();
                                        endif;
                                        $excerpt = wp_strip_all_tags($excerpt);
                                        if ( mb_strlen( $excerpt ) > $charlength ) :
                                            $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                                            $exwords = explode( ' ', $subex );
                                            $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                                            if ( $excut < 0 ) :
                                                echo mb_substr( $subex, 0, $excut );
                                            else :
                                                echo $subex;
                                            endif;
                                            echo '...';
                                        else :
                                            echo $excerpt;
                                        endif;
                                        echo '</div>';
                                    endif;
                                    ?>

                                </a>

                            </div>

                            <?php $i = $i + 1 ; ?>

                        <?php endwhile; ?>

                    </div>

                <?php endif; ?>
                                      
            </div>

        <?php endif; ?>
        <!--/Last news -->

        <?php

/*
		if ($r->have_posts()) :
        ?>
			<ul class="linked linked-<?php echo $posttype; ?>">
                
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li class="sport">
                        
                        <a href="<?php the_permalink(); ?>" class="title">

                            <?php 
                            if ($layout == 'titles-with-icons') :
                                $icon = get_field('images_icon');
                                echo '<i class="flaticon flaticon-'.$icon.'"></i>';
                            endif; 
                            ?>
                        
                            <?php 
                            if ($layout == 'titles-with-logos') :
                                if ($image = get_field('images_thumbnail')) :
                                    echo '<span class="id-photo" style="background-image:url('.$image['sizes']['medium'].');"></span>';
                                else : 
                                    echo '<span class="id-photo default"></span>'; 
                                endif;
                            endif; 
                            ?>
                        
                            <span><?php the_title(); ?></span>
                            
                        </a>

                    </li>
                <?php endwhile; ?>

            </ul>

        <?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no sport', 'ao-visit-brussels').'</p>';
		endif;
        */
        
		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('ao_visit_brussels', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		//error_log('sarickx >>> old instance linked: ' . json_encode($old_instance));;
		error_log('sarickx >>> new instance linked: ' . json_encode($new_instance));;
		error_log('sarickx >>> title : ' . $new_instance['title1'] );
		
		
		$instance = $old_instance;
		$instance['title']          = stripslashes($new_instance['title']);
		$instance['number']         = isset( $new_instance['number'] ) ? $new_instance['number'] : '-1';
		$instance['order']          = isset( $new_instance['order'] ) ? $new_instance['order'] : 'date';
		$instance['direction']      = isset( $new_instance['direction'] ) ? $new_instance['direction'] : 'asc';
		//$instance['posttype']       = isset( $new_instance['posttype'] ) ? $new_instance['posttype'] : 'sport';
		$instance['layout']         = isset( $new_instance['layout'] ) ? $new_instance['layout'] : 'titles-with-text-and-thumbnails';
		$instance['grid']           = isset( $new_instance['grid'] ) ? $new_instance['grid'] : '12';
		$instance['diapo']           = isset( $new_instance['diapo'] ) ? $new_instance['diapo'] : '0';
        $instance['newscat']        = $new_instance['newscat'];
        $instance['charlength']     = isset( $instance['charlength'] ) ? $new_instance['charlength'] : '100';
		//$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['ao_visit_brussels']) )
			delete_option('ao_visit_brussels');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('ao_upcoming_events', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? $instance['number'] : '-1';
		$order     = isset( $instance['order'] ) ? $instance['order'] : 'date';
		$direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
		//$posttype  = isset( $instance['posttype'] ) ? $instance['posttype'] : 'sport';
		$layout    = isset( $instance['layout'] ) ? $instance['layout'] : 'titles-with-text-and-thumbnails';
		$grid      = isset( $instance['grid'] ) ? $instance['grid'] : '12';
		$diapo      = isset( $instance['diapo'] ) ? $instance['diapo'] : '0';
		$newscat = isset( $instance['newscat'] ) ? $instance['newscat'] : '';
        $charlength = isset( $instance['charlength'] ) ? $instance['charlength'] : '100';
		//$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
		$options = json_decode($instance['options']);
		
		?>

		<p class="ao-visit-brussels-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'ao-visit-brussels' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
            <label for="<?php echo $this->get_field_id( 'newscat' ); ?>"><?php _e( 'Roles:', 'ao-visit-brussels' ); ?></label>
			<select multiple class="widefat newscat-selector" id="<?php echo 'newscat-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'newscat' ); ?>[]" >
                <?php
                $terms = get_terms( 'newscat', array('hide_empty' => 0) );
                foreach ($terms as $term) {
                    ?>
                    <option value="<?php echo $term->term_id; ?>" <?php if (in_array($term->term_id, $newscat)) echo 'selected="selected"'; ?> ><?php echo $term->name; ?></option>      
                    <?php 
                } ?>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'ao-visit-brussels' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order', 'ao-visit-brussels' ); ?></label>
			<select class="order-selector" id="<?php echo 'order-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" >
                <option value="rand" <?php if ($order == 'rand') echo 'selected="selected"'; ?>><?php _e('Random','ao-visit-brussels'); ?></option>
                <option value="title" <?php if ($order == 'title') echo 'selected="selected"'; ?>><?php _e('Title','ao-visit-brussels'); ?></option>
                <option value="date" <?php if ($order == 'date') echo 'selected="selected"'; ?>><?php _e('Date','ao-visit-brussels'); ?></option>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'direction' ); ?>"><?php _e( 'Direction', 'ao-visit-brussels' ); ?></label>
			<select class="direction-selector" id="<?php echo 'direction-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'direction' ); ?>" >
                <option value="asc" <?php if ($direction == 'asc') echo 'selected="selected"'; ?>><?php _e('Asc','ao-visit-brussels'); ?></option>
                <option value="desc" <?php if ($direction == 'desc') echo 'selected="selected"'; ?>><?php _e('Desc','ao-visit-brussels'); ?></option>
            </select>
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'posttype' ); ?>"><?php _e( 'Post type to be linked', 'ao-visit-brussels' ); ?></label>
			<select class="posttype-selector" id="<?php echo 'posttype-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'posttype' ); ?>" >
                <option value="sport" <?php if ($posttype == 'sport') echo 'selected="selected"'; ?>><?php _e('Sport','ao-visit-brussels'); ?></option>
                <option value="club" <?php if ($posttype == 'club') echo 'selected="selected"'; ?>><?php _e('Club','ao-visit-brussels'); ?></option>
                <option value="infrastructure" <?php if ($posttype == 'infrastructure') echo 'selected="selected"'; ?>><?php _e('Infrastructure','ao-visit-brussels'); ?></option>
                <option value="meeting" <?php if ($posttype == 'meeting') echo 'selected="selected"'; ?>><?php _e('Meeting','ao-visit-brussels'); ?></option>
            </select>
            
        </p>
        */ ?>

		<p>
            <label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Layout', 'ao-visit-brussels' ); ?></label>
			<select class="layout-selector" id="<?php echo 'layout-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>" >
                <option value="titles" <?php if ($layout == 'titles') echo 'selected="selected"'; ?>><?php _e('Titles','ao-visit-brussels'); ?></option>
                <option value="titles-with-texts" <?php if ($layout == 'titles-with-texts') echo 'selected="selected"'; ?>><?php _e('Titles - Texts','ao-visit-brussels'); ?></option>
                <option value="titles-with-thumbnails" <?php if ($layout == 'titles-with-thumbnails') echo 'selected="selected"'; ?>><?php _e('Thumbnails - Titles','ao-visit-brussels'); ?></option>
                <option value="titles-with-dates" <?php if ($layout == 'titles-with-dates') echo 'selected="selected"'; ?>><?php _e('Dates - Titles'); ?></option>
                <option value="titles-with-texts-and-thumbnails" <?php if ($layout == 'titles-with-texts-and-thumbnails') echo 'selected="selected"'; ?>><?php _e('Thumbnails - Titles - Texts','ao-visit-brussels'); ?></option>
                <option value="titles-with-texts-and-dates" <?php if ($layout == 'titles-with-texts-and-dates') echo 'selected="selected"'; ?>><?php _e('Dates - Titles','ao-visit-brussels'); ?></option>
                <option value="titles-with-dates-and-thumbnails" <?php if ($layout == 'titles-with-dates-and-thumbnails') echo 'selected="selected"'; ?>><?php _e('Thumbnails - Dates - Titles','ao-visit-brussels'); ?></option>
                <option value="titles-with-texts-and-dates-and-thumbnails" <?php if ($layout == 'titles-with-texts-and-dates-and-thumbnails') echo 'selected="selected"'; ?>><?php _e('Thumbnails - Dates - Titles - Texts','ao-visit-brussels'); ?></option>
            </select>
            
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'grid' ); ?>"><?php _e( 'Nombre de colonnes', 'ao-visit-brussels' ); ?></label>
			<select class="grid-selector" id="<?php echo 'grid-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'grid' ); ?>" >
                <option value="12" <?php if ($grid == '12') echo 'selected="selected"'; ?>><?php _e('1','ao-visit-brussels'); ?></option>
                <option value="6" <?php if ($grid == '6') echo 'selected="selected"'; ?>><?php _e('2','ao-visit-brussels'); ?></option>
                <option value="4" <?php if ($grid == '4') echo 'selected="selected"'; ?>><?php _e('3','ao-visit-brussels'); ?></option>
                <option value="3" <?php if ($grid == '3') echo 'selected="selected"'; ?>><?php _e('4','ao-visit-brussels'); ?></option>
                <option value="2" <?php if ($grid == '2') echo 'selected="selected"'; ?>><?php _e('6','ao-visit-brussels'); ?></option>
            </select>
            
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'diapo' ); ?>"><?php _e( 'View', 'ao-visit-brussels' ); ?></label>
			<select class="diapo-selector" id="<?php echo 'diapo-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'diapo' ); ?>" >
                <option value="0" <?php if ($diapo == '0') echo 'selected="selected"'; ?>><?php _e('Grid','ao-visit-brussels'); ?></option>
                <option value="1" <?php if ($diapo == '1') echo 'selected="selected"'; ?>><?php _e('Diaporama','ao-visit-brussels'); ?></option>
            </select>
            
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'charlength' ); ?>"><?php _e( 'Number of chars for the text:', 'ao-visit-brussels' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'charlength' ); ?>" name="<?php echo $this->get_field_name( 'charlength' ); ?>" type="text" value="<?php echo $charlength; ?>" size="3" />
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'ao-visit-brussels' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" />
        </p>
        */ ?>

		<?php
	}
}

function ao_visit_brussels_init(){
	register_widget('ao_visit_brussels_widget');
}
add_action('widgets_init', 'ao_visit_brussels_init');


function ao_visit_brussels_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'ao_visit_brussels_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'ao_visit_brussels_widget_scripts' );

function ao_visit_brussels_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'ao_visit_brussels_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'ao_visit_brussels_widget_css' );

?>
