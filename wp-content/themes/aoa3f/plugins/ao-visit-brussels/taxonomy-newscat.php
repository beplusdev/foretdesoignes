<?php
/** archive.php
 *
 * The template for displaying archive page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
*/
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <h1 class="archive-title">
            <?php 
            post_type_archive_title(); 
            if (is_tax()) :
                global $wp_query;
                $term = $wp_query->get_queried_object();
                echo $term->name;
            endif;
            ?>
        </h1>

        <?php
        if (is_tax('newscat', 'event')) : 
            $when = (isset($_GET['w']) and ($_GET['w'] == 'past')) ? 'past' : 'future'; 
            $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
            ?>
            <ul class="nav nav-tabs event-tabs">
                <li class="<?php echo ($when == 'future') ? 'active' : ''; ?>"><a href="<?php echo $url; ?>"><?php _e('Future', 'aoa3f'); ?></a></li>
                <li class="<?php echo ($when == 'past') ? 'active' : ''; ?>"><a href="<?php echo $url.'?w=past'; ?>"><?php _e('Past', 'aoa3f'); ?></a></li>				
            </ul>
            <?php
        endif;
        ?>
        
        <?php 
        if ( have_posts() ) : 
        
            ?>
            
            <div class="row">

                <?
                // The loop 
                while ( have_posts() ) : the_post();

                $post_type = get_post_type();
                    ?>

                    <div class="col col-6 col-lg-6 col-md-6 col-sm-6 col-xs-6 effect <?php if (($i % 2) == 0) echo 'clearfix'; ?>">

                        <a href="<?php the_permalink(); ?>">

                            <?php 
                            $image = get_field('images_thumbnail', get_the_ID()); 
                            if ($image) : 
                                $title = get_the_title($linked_post->ID);
                                $size = 'two-third';
                                echo '<div class="image" style="background-image: url('.$image['sizes'][$size].');"></div>';
                            elseif ($default = get_option('default_news_image')) : 
                                //echo '<img src="'.$default.'" class="default" />';
                                echo '<div class="image" style="background-image: url('.$default.');"></div>';
                            endif;
                            ?>

                            <div class="caption">

                                <div class="title">
                                    <?php 
                                    //echo '<span class="date">'.get_the_date('d/m').'</span> - ';
                                    the_title(); 
                                    ?>
                                </div>

                                <?php 
                                echo '<div class="content">';
                                /*
                                if (!$excerpt = get_the_excerpt()) :
                                    $excerpt = get_the_content();
                                endif;
                                $excerpt = wp_strip_all_tags($excerpt);
                                if ( mb_strlen( $excerpt ) > $charlength ) :
                                    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                                    $exwords = explode( ' ', $subex );
                                    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                                    if ( $excut < 0 ) :
                                        echo mb_substr( $subex, 0, $excut );
                                    else :
                                        echo $subex;
                                    endif;
                                    echo '...';
                                else :
                                    echo $excerpt;
                                endif;
                                */
                                the_excerpt();
                                echo '</div>';
                                ?>

                            </div>

                        </a>

                    </div>

                    <?php $i = $i + 1 ; ?>

                <?php endwhile; ?>

            </div>


<?php
/*
            // The loop 
            while ( have_posts() ) : the_post();

                $post_type = get_post_type();

                if (!get_field('sport_parent')) :
                ?>
        
                <article>
        
                    <a href="<?php the_permalink(); ?>">

                        <?php 
                        $image = get_field('images_thumbnail'); 
                        if ($image) : 
                            $title = get_the_title();
                            echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$title.'" title="'.$title.'" class="thumbnail" />';		
                        elseif ($default = get_option('default_'.$post_type.'_image')) : 
                            echo '<img src="'.$default.'" class="default thumbnail" />';
                        endif;
                        ?>

                    </a>
                    
                    <h1 class="entry-title">
                        <a href="<?php the_permalink(); ?>">
                            <?php aoa3f_event_calendar(); ?>
                            <?php the_title(); ?>
                        </a>
                    </h1>   
                    
                    <div class="entry-meta">

                        <?php if ($post_type == 'news') : ?>
                        
                            <?php aoa3f_entry_meta_news(); ?>
                        
                        <?php elseif ($post_type == 'club') : ?>
                        
                            <?php 
                            $categories = array();
                            $terms = get_the_terms( get_the_ID(), 'clubcat' );
                            if ( !empty( $terms ) ) :
                                foreach ( $terms as $term )
                                    $categories[] = '<a href="' .get_term_link($term->slug, 'clubcat') .'">'.$term->name.'</a> ';
                            endif;
                            $categories_list = implode(', ', $categories); //get_the_taxonomies(); get_the_category_list( __( ', ', 'aoa3f' ) );
                            if ( $categories_list ) :
                                echo '<span class="cat clubcat"><span class="fa fa-folder-open"></span>' . $categories_list . '</span>';
                            endif;
                            ?>
                        
                        <?php elseif ($post_type == 'infrastructure') : ?>
                        
                            <?php 
                            $categories = array();
                            $terms = get_the_terms( get_the_ID(), 'infrastructurecat' );
                            if ( !empty( $terms ) ) :
                                foreach ( $terms as $term )
                                    $categories[] = '<a href="' .get_term_link($term->slug, 'infrastructurecat') .'">'.$term->name.'</a> ';
                            endif;
                            $categories_list = implode(', ', $categories); //get_the_taxonomies(); get_the_category_list( __( ', ', 'aoa3f' ) );
                            if ( $categories_list ) :
                                echo '<span class="cat infrastructurecat"><span class="fa fa-folder-open"></span>' . $categories_list . '</span>';
                            endif;
                            ?>
                        
                        <?php elseif ($post_type == 'meeting') : ?>
                        
                            <?php 
                            $categories = array();
                            $terms = get_the_terms( get_the_ID(), 'meetingcat' );
                            if ( !empty( $terms ) ) :
                                foreach ( $terms as $term )
                                    $categories[] = '<a href="' .get_term_link($term->slug, 'meetingcat') .'">'.$term->name.'</a> ';
                            endif;
                            $categories_list = implode(', ', $categories); //get_the_taxonomies(); get_the_category_list( __( ', ', 'aoa3f' ) );
                            if ( $categories_list ) :
                                echo '<span class="cat meetingcat"><span class="fa fa-folder-open"></span>' . $categories_list . '</span>';
                            endif;
                            ?>
                        
                        <?php endif; ?>


                    </div>   

                    <div class="entry-excerpt">

                        <a href="<?php the_permalink(); ?>">

                        <?php the_excerpt(); ?>

                        </a>

                    </div>   

                    <div class="read-more">
                        <a href="<?php the_permalink(); ?>">    
                            <button class="btn">
                                <?php _e('Read more', 'aoa3f'); ?>
                            </button>                            
                        </a>
                    </div>
                    
                </article>
                    
                <?php
                endif;

            endwhile;
 */                       
        // No posts to display
        else :

            if (is_tax('newscat', 'news')) :
                echo '<p class="no-post">'.__('There is currently no news.', 'aoa3f').'</p>';
            elseif (is_tax('newscat', 'event')) :
                echo '<p class="no-post">'.__('There is currently no event.', 'aoa3f').'</p>';
            else :
                echo '<p class="no-post">'.__('There is currently no post.', 'aoa3f').'</p>';
            endif;

        endif; 
        ?>
    
        <nav class="navigation paging-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php __( 'Posts navigation', 'aoa3f' ); ?></h1>
            <div class="nav-links">

                <?php if ( get_next_posts_link() ) : ?>
                <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'aoa3f' ) ); ?></div>
                <?php endif; ?>

                <?php if ( get_previous_posts_link() ) : ?>
                <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'aoa3f' ) ); ?></div>
                <?php endif; ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->

        <?php get_sidebar( 'bottom-content' ); ?>
        
        <?php
//$post = $wp_query->post;

//echo $post->ID;
    ?>
        
    </div>
    
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file archive.php */
/* Location: ./wp-content/themes/aoa3f/archive.php */