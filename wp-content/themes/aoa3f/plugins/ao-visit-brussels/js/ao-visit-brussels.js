/*
Plugin Name: AO Visit Brussels
Author URI: http://www.alleyoop.be
Description: A visit brussels widget.
Author: Alley Oop
Version: 1.0
*/

jQuery(function(){
    jQuery('.visit-brussels .carousel').carousel({
      interval: 6000
    });
});
