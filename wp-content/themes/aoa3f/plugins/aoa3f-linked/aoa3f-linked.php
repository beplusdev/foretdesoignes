<?php
/*
 * Plugin Name: AO A3F - Linked 
 * Author URI: http://www.alleyoop.be
 * Description: Widget to display linked sport, club, infrastructure or meeting rooms to a single post (for AO A3F).
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AOA3F_LINKED_VERSION', '1.0');
define('AOA3F_LINKED_AUTHOR', 'Alley Oop');
define('AOA3F_LINKED_URL', get_template_directory_uri() . '/plugins/aoa3f-linked/');


## Include the required scripts
function aoa3f_linked_public_scripts(){
    wp_enqueue_script('jquery');
	wp_register_script('aoa3f-linked-js', AOA3F_LINKED_URL . 'js/aoa3f-linked.js', array(), '1');
	wp_enqueue_script(array('jquery', 'aoa3f-linked-js'));
}
add_action('wp_enqueue_scripts', 'aoa3f_linked_public_scripts');


## Include the required styles
function aoa3f_linked_public_styles(){
	wp_register_style('aoa3f-linked-css', AOA3F_LINKED_URL . 'css/aoa3f-linked.css', array(), '2');
	wp_enqueue_style('aoa3f-linked-css');
}
add_action('wp_enqueue_scripts', 'aoa3f_linked_public_styles');


## To sort all posts
function cmp($a, $b)
{
    return strcmp($a->post_title, $b->post_title);
}


class aoa3f_linked_widget extends WP_Widget{

	## Initialize
	
	function aoa3f_linked_widget(){
	
		// set text domain
		$dom = 'aoa3f-linked';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-aoa3f-linked',
			'description' => __('A display of linked', 'aoa3f-linked'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('aoa3f-linked', __('Linked', 'aoa3f-linked'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('aoa3f_linked', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        
		//$number = ( ! empty( $instance['number'] ) ) ? $instance['number'] : '-1';

        $order = isset( $instance['order'] ) ? $instance['order'] : 'rand';
        //$direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
        $posttype = isset( $instance['posttype'] ) ? $instance['posttype'] : 'sport';
        $layout = isset( $instance['layout'] ) ? $instance['layout'] : 'titles';
		$grid = ( ! empty( $instance['grid'] ) ) ? $instance['grid'] : '12';
        //$sportcat = $instance['sportcat'];
        $options = json_decode($instance['options']);
		//$show_all_title = $instance['show_all_title'];

        /*
		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => $posttype,
                /*
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'sportcat',
						'field' 	=> 'id',
						'terms' 	=> $sportcat,
					),
				),
                */
                /*
				'orderby' 	=> $order, 
                'order'     => $direction,
			)
		));
		//echo $r->request; 
        */
        
        // Only for single post
        if (!is_single()) {
            return true;
        }
        
        // Current post
        $post = $GLOBALS['post'];
        $post_id = $post->ID;
        
        // Title
        if (get_field('linked_'.$posttype.'s_title')) :
            $title = get_field('linked_'.$posttype.'s_title');
        elseif (!$title) :
            switch ($posttype) :
            case 'sport' :
                $title = __('Other sports', 'aoa3f-linked');
                break;
            case 'club' :
                $title = __('Other clubs', 'aoa3f-linked');
                break;
            case 'infrastructure' :
                $title = __('Other infrastructures', 'aoa3f-linked');
                break;
            case 'meeting' :
                $title = __('Other meeting rooms', 'aoa3f-linked');
                break;
            endswitch;
        endif;
        
        
        echo $before_widget; 
        ?>

        <!-- Linked posts -->
        <?php if ($linked_posts = get_field('linked_'.$posttype.'s_list')) : ?>

            <div class="linked linked-<?php echo $posttype; ?>s <?php echo $layout; ?>">

                <?php echo $before_title . $title . $after_title; ?>

                <div class="row">
                    <?php 
                    $i = 0;
                    $n = (get_field('linked_'.$posttype.'s_number')) ? min(count($linked_posts), get_field('linked_'.$posttype.'s_number')) : count($linked_posts); 
                    if ($order == 'title') :
                        foreach ($linked_posts as $p) :
                            $posts[] = $p['linked_'.$posttype.'s_list_'.$posttype];
                        endforeach;
                        usort($posts, "cmp");
                    endif;
                    ?>
                    <?php while (($i < $n) and count($linked_posts)) : ?>
                    
                        <?php
                        switch ($order) :
                        case 'rand' :
                            $random = array_rand( $linked_posts );
                            $random_post = $linked_posts[ $random ];
                            $linked_post = $random_post['linked_'.$posttype.'s_list_'.$posttype];
                            unset($linked_posts[$random]);
                            break;
                        case 'title' :                            
                            $linked_post = $posts[$i];
                            break;
                        default :
                            $post = $linked_posts[$i];
                            $linked_post = $post['linked_'.$posttype.'s_list_'.$posttype];
                            break;
                        endswitch;                        
                        ?>
                    
                        <div class="col col-lg-<?php echo $grid; ?> col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-<?php echo ($grid == '12') ? $grid : '6'; ?> effect <?php if (($i % (12/$grid)) == 0) echo 'clearfix'; ?>">
                            
                            <?php $i = $i + 1; ?>
                    
                            <?php 
                            if ($layout == 'titles-with-icons') :
                                $icon = get_field('images_icon', $linked_post->ID);
                                echo '<a href="'.get_post_permalink($linked_post->ID).'">';
                                echo '<i class="flaticon flaticon-'.$icon.'"></i>';
                                echo '</a>';
                            endif; 
                            ?>

                            <?php 
                            $sport = '';
                            $sport_id = get_field('club_infos_sport', $linked_post->ID);
                            $parent_id = get_field('sport_parent', $sport_id);
                            if ($parent_id and ($parent_id == $post->ID)) :
                                $sport = '
                                <div class="sport">
                                    <a href="'.get_post_permalink($sport_id).'">'.get_the_title($sport_id).'</a>
                                </div>';
                            endif; 
                            ?> 

                            <?php 
                            if ($layout == 'titles-with-logos') :
                                $image = get_field('images_thumbnail', $linked_post->ID); 
                                echo '<div class="logo">';
                                echo '<a href="'.get_post_permalink($linked_post->ID).'">';
                                if ($image) : 
                                    $title = get_the_title($linked_post->ID);
                                    echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$title.'" title="'.$title.'" />';
                                elseif ($default = get_option('default_'.$posttype.'_image')) : 
                                    echo '<img src="'.$default.'" class="default" />';
                                endif;
                                echo '</a>';
                                echo $sport;
                                echo '</div>';
                            endif;
                            ?>

                            <?php 
                            if ($layout == 'titles-with-maps') :
                                $image = get_field('images_map', $linked_post->ID); 
                                echo '<div class="map">';
                                echo '<a href="'.get_post_permalink($linked_post->ID).'">';
                                if ($image) : 
                                    $title = get_the_title($linked_post->ID);
                                    echo '<img src="'.$image['sizes']['thumbnail'].'" alt="'.$title.'" title="'.$title.'" />';
                                elseif ($default = get_option('default_'.$posttype.'_image')) : 
                                    echo '<img src="'.$default.'" class="default" />';
                                endif;
                                echo '</div>';
                                echo $sport;
                                echo '</a>';
                            endif;
                            ?>

                            <div class="title">
                                <a href="<?php echo get_post_permalink($linked_post->ID); ?>">
                                <?php 
                                if ($title = get_field('images_thumbnail_name', $linked_post->ID)) :
                                    echo $title;
                                else :
                                    $title = get_the_title($linked_post->ID);
                                    echo $title;
                                    //echo (strlen($title) > 15) ? substr($title, 0, 15).'...' : $title; 
                                endif;
                                ?>
                                </a>
                            </div>
                            
                        </div>
                    
                    <?php endwhile; ?>
                </div>

            </div>

        <?php endif; ?>
        <!--/Linked posts -->

        <?php

/*
		if ($r->have_posts()) :
        ?>
			<ul class="linked linked-<?php echo $posttype; ?>">
                
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li class="sport">
                        
                        <a href="<?php the_permalink(); ?>" class="title">

                            <?php 
                            if ($layout == 'titles-with-icons') :
                                $icon = get_field('images_icon');
                                echo '<i class="flaticon flaticon-'.$icon.'"></i>';
                            endif; 
                            ?>
                        
                            <?php 
                            if ($layout == 'titles-with-logos') :
                                if ($image = get_field('images_thumbnail')) :
                                    echo '<span class="id-photo" style="background-image:url('.$image['sizes']['medium'].');"></span>';
                                else : 
                                    echo '<span class="id-photo default"></span>'; 
                                endif;
                            endif; 
                            ?>
                        
                            <span><?php the_title(); ?></span>
                            
                        </a>

                    </li>
                <?php endwhile; ?>

            </ul>

        <?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no sport', 'aoa3f-linked').'</p>';
		endif;
        */
        
		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('aoa3f_linked', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		//error_log('sarickx >>> old instance linked: ' . json_encode($old_instance));;
		error_log('sarickx >>> new instance linked: ' . json_encode($new_instance));;
		error_log('sarickx >>> title : ' . $new_instance['title1'] );
		
		
		$instance = $old_instance;
		$instance['title']          = stripslashes($new_instance['title']);
		//$instance['number']         = isset( $new_instance['number'] ) ? $new_instance['number'] : '-1';
		$instance['order']          = isset( $new_instance['order'] ) ? $new_instance['order'] : 'rand';
		//$instance['direction']      = isset( $new_instance['direction'] ) ? $new_instance['direction'] : 'asc';
		$instance['posttype']       = isset( $new_instance['posttype'] ) ? $new_instance['posttype'] : 'sport';
		$instance['layout']         = isset( $new_instance['layout'] ) ? $new_instance['layout'] : 'titles';
		$instance['grid']           = isset( $new_instance['grid'] ) ? $new_instance['grid'] : '12';
        //$instance['sportcat']      = $new_instance['sportcat'];
		$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['aoa3f_linked']) )
			delete_option('aoa3f_linked');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('ao_upcoming_events', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		//$number    = isset( $instance['number'] ) ? $instance['number'] : '-1';
		$order     = isset( $instance['order'] ) ? $instance['order'] : 'rand';
		//$direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
		$posttype  = isset( $instance['posttype'] ) ? $instance['posttype'] : 'sport';
		$layout    = isset( $instance['layout'] ) ? $instance['layout'] : 'titles';
		$grid      = isset( $instance['grid'] ) ? $instance['grid'] : '12';
		//$sportcat = isset( $instance['sportcat'] ) ? $instance['sportcat'] : '';
		//$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
		$options = json_decode($instance['options']);
		
		?>

		<p class="aoa3f-linked-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'aoa3f-linked' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'sportcat' ); ?>"><?php _e( 'Roles:', 'aoa3f-linked' ); ?></label>
			<select multiple class="widefat sportcat-selector" id="<?php echo 'sportcat-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'sportcat' ); ?>[]" >
                <?php
                $terms = get_terms( 'sportcat', array('hide_empty' => 0) );
                foreach ($terms as $term) {
                    ?>
                    <option value="<?php echo $term->term_id; ?>" <?php if (in_array($term->term_id, $sportcat)) echo 'selected="selected"'; ?> ><?php echo $term->name; ?></option>      
                    <?php 
                } ?>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'aoa3f-linked' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
        */ ?>

		<p>
            <label for="<?php echo $this->get_field_id( 'posttype' ); ?>"><?php _e( 'Post type to be linked', 'aoa3f-linked' ); ?></label>
			<select class="posttype-selector" id="<?php echo 'posttype-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'posttype' ); ?>" >
                <option value="sport" <?php if ($posttype == 'sport') echo 'selected="selected"'; ?>><?php _e('Sport','aoa3f-linked'); ?></option>
                <option value="club" <?php if ($posttype == 'club') echo 'selected="selected"'; ?>><?php _e('Club','aoa3f-linked'); ?></option>
                <option value="infrastructure" <?php if ($posttype == 'infrastructure') echo 'selected="selected"'; ?>><?php _e('Infrastructure','aoa3f-linked'); ?></option>
                <option value="meeting" <?php if ($posttype == 'meeting') echo 'selected="selected"'; ?>><?php _e('Meeting','aoa3f-linked'); ?></option>
            </select>
            
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order', 'aoa3f-linked' ); ?></label>
			<select class="order-selector" id="<?php echo 'order-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" >
                <option value="rand" <?php if ($order == 'rand') echo 'selected="selected"'; ?>><?php _e('Random','aoa3f-linked'); ?></option>
                <option value="title" <?php if ($order == 'title') echo 'selected="selected"'; ?>><?php _e('Title','aoa3f-linked'); ?></option>
                <option value="free" <?php if ($order == 'free') echo 'selected="selected"'; ?>><?php _e('Free (as listed)','aoa3f-linked'); ?></option>
            </select>
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'direction' ); ?>"><?php _e( 'Direction', 'aoa3f-linked' ); ?></label>
			<select class="direction-selector" id="<?php echo 'direction-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'direction' ); ?>" >
                <option value="asc" <?php if ($direction == 'asc') echo 'selected="selected"'; ?>><?php _e('Asc','aoa3f-linked'); ?></option>
                <option value="desc" <?php if ($direction == 'desc') echo 'selected="selected"'; ?>><?php _e('Desc','aoa3f-linked'); ?></option>
            </select>
        </p>
        */ ?>

        <p>
            <label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Layout', 'aoa3f-linked' ); ?></label>
			<select class="layout-selector" id="<?php echo 'layout-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>" >
                <option value="titles" <?php if ($layout == 'titles') echo 'selected="selected"'; ?>><?php _e('List of titles','aoa3f-linked'); ?></option>
                <option value="titles-with-icons" <?php if ($layout == 'titles-with-icons') echo 'selected="selected"'; ?>><?php _e('List of titles with icons','aoa3f-linked'); ?></option>
                <option value="titles-with-logos" <?php if ($layout == 'titles-with-logos') echo 'selected="selected"'; ?>><?php _e('List of titles with logos','aoa3f-linked'); ?></option>
                <option value="titles-with-maps" <?php if ($layout == 'titles-with-maps') echo 'selected="selected"'; ?>><?php _e('List of titles with maps','aoa3f-linked'); ?></option>
            </select>
            
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'grid' ); ?>"><?php _e( 'Nombre de colonnes', 'aoa3f-linked' ); ?></label>
			<select class="grid-selector" id="<?php echo 'grid-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'grid' ); ?>" >
                <option value="12" <?php if ($grid == '12') echo 'selected="selected"'; ?>><?php _e('1','aoa3f-linked'); ?></option>
                <option value="6" <?php if ($grid == '6') echo 'selected="selected"'; ?>><?php _e('2','aoa3f-linked'); ?></option>
                <option value="4" <?php if ($grid == '4') echo 'selected="selected"'; ?>><?php _e('3','aoa3f-linked'); ?></option>
                <option value="3" <?php if ($grid == '3') echo 'selected="selected"'; ?>><?php _e('4','aoa3f-linked'); ?></option>
                <option value="2" <?php if ($grid == '2') echo 'selected="selected"'; ?>><?php _e('6','aoa3f-linked'); ?></option>
            </select>
            
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'aoa3f-linked' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" />
        </p>
        */ ?>

		<?php
	}
}

function aoa3f_linked_init(){
	register_widget('aoa3f_linked_widget');
}
add_action('widgets_init', 'aoa3f_linked_init');


function aoa3f_linked_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'aoa3f_linked_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'aoa3f_linked_widget_scripts' );

function aoa3f_linked_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'aoa3f_linked_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'aoa3f_linked_widget_css' );

?>
