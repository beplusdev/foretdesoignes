/*
Plugin Name: AO Last News
Author URI: http://www.alleyoop.be
Description: A last news widget.
Author: Alley Oop
Version: 1.0
*/

jQuery(function(){
    jQuery('.last-news .carousel').carousel({
      interval: 6000
    });
});
