<?php
/*
 * Plugin Name: AO A3F - Sports 
 * Author URI: http://www.alleyoop.be
 * Description: Sports widget for AO A3F.
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AOA3F_SPORTS_VERSION', '1.0');
define('AOA3F_SPORTS_AUTHOR', 'Alley Oop');
define('AOA3F_SPORTS_URL', get_template_directory_uri() . '/plugins/aoa3f-sports/');


## Include the required scripts
function aoa3f_sports_public_scripts(){
    wp_enqueue_script('jquery');
	wp_register_script('aoa3f-sports-js', AOA3F_SPORTS_URL . 'js/aoa3f-sports.js', array(), '1');
	wp_enqueue_script(array('jquery', 'aoa3f-sports-js'));
}
add_action('wp_enqueue_scripts', 'aoa3f_sports_public_scripts');


## Include the required styles
function aoa3f_sports_public_styles(){
	wp_register_style('aoa3f-sports-css', AOA3F_SPORTS_URL . 'css/aoa3f-sports.css', array(), '2');
	wp_enqueue_style('aoa3f-sports-css');
}
add_action('wp_enqueue_scripts', 'aoa3f_sports_public_styles');



class aoa3f_sports_widget extends WP_Widget{

	## Initialize
	
	function aoa3f_sports_widget(){
	
		// set text domain
		$dom = 'aoa3f-sports';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-aoa3f-sports',
			'description' => __('A display of sports', 'aoa3f-sports'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('aoa3f-sports', __('Sports', 'aoa3f-sports'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('aoa3f_sports', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        
		$number = ( ! empty( $instance['number'] ) ) ? $instance['number'] : '-1';

        $order = isset( $instance['order'] ) ? $instance['order'] : 'rand';
        $direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
        $layout = isset( $instance['layout'] ) ? $instance['layout'] : 'titles';
        //$sportcat = $instance['sportcat'];
        $options = json_decode($instance['options']);
		//$show_all_title = $instance['show_all_title'];

		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'sport',
                /*
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'sportcat',
						'field' 	=> 'id',
						'terms' 	=> $sportcat,
					),
				),
                */
				'orderby' 	=> $order, 
                'order'     => $direction,
			)
		));
		//echo $r->request; 
        
        // Current post
        $post = $GLOBALS['post'];
        $post_id = $post->ID;        
        $sport_id = get_field('club_infos_sport', $post_id);
        if ($sport_id) $parent_id = get_field('sport_parent', $sport_id);
        else $parent_id = get_field('sport_parent', $post_id);

        echo $before_widget; 
		if ( $title ) echo $before_title . $title . $after_title; 

		if ($r->have_posts()) :
        ?>
			<ul class="sports">
                
                <li class="all <?php if (is_post_type_archive('sport')) echo 'active'; ?>">
                    <a href="<?php echo get_post_type_archive_link('sport'); ?>" class="title">
                        <span><?php _e('All sports', 'aoa3f-sports'); ?></span>
                    </a>
                </li>    
                
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                
                    <?php if (!get_field('sport_parent')) : ?>
                
                        <li class="sport <?php if (is_singular() and ((get_the_ID() == $post_id) or (get_the_ID() == $parent_id) or (get_the_ID() == $sport_id))) echo 'active'; ?> ">

                            <a href="<?php the_permalink(); ?>" class="title">

                                <?php 
                                if ($layout == 'titles-with-icons') :
                                    $icon = get_field('images_icon');
                                    echo '<i class="flaticon flaticon-'.$icon.'"></i>';
                                endif; 
                                ?>

                                <?php 
                                if ($layout == 'titles-with-logos') :
                                    if ($image = get_field('images_thumbnail')) :
                                        echo '<span class="id-photo" style="background-image:url('.$image['sizes']['medium'].');"></span>';
                                    else : 
                                        echo '<span class="id-photo default"></span>'; 
                                    endif;
                                endif; 
                                ?>

                                <span><?php the_title(); ?></span>

                            </a>

                        </li>

                    <?php endif; ?>
                
                <?php endwhile; ?>

                <?php /* if ($show_all_title) : ?>
                <div class="buttons">
                    <a class="btn btn-success" href="<?php echo get_post_type_archive_link( 'sport' ); ?>"><?php echo $show_all_title; ?></a>
                </div>
                <?php endif; */?>

            </ul>

        <?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no sport', 'aoa3f-sports').'</p>';
		endif;

		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('aoa3f_sports', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		//error_log('sarickx >>> old instance sports: ' . json_encode($old_instance));;
		error_log('sarickx >>> new instance sports: ' . json_encode($new_instance));;
		error_log('sarickx >>> title : ' . $new_instance['title1'] );
		
		
		$instance = $old_instance;
		$instance['title']          = stripslashes($new_instance['title']);
		$instance['number']         = isset( $new_instance['number'] ) ? $new_instance['number'] : '-1';
		$instance['order']          = isset( $new_instance['order'] ) ? $new_instance['order'] : 'rand';
		$instance['direction']      = isset( $new_instance['direction'] ) ? $new_instance['direction'] : 'asc';
		$instance['layout']         = isset( $new_instance['layout'] ) ? $new_instance['layout'] : 'titles';
        //$instance['sportcat']      = $new_instance['sportcat'];
		$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['aoa3f_sports']) )
			delete_option('aoa3f_sports');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('ao_upcoming_events', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? $instance['number'] : '-1';
		$order     = isset( $instance['order'] ) ? $instance['order'] : 'rand';
		$direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
		$layout    = isset( $instance['layout'] ) ? $instance['layout'] : 'titles';
		//$sportcat = isset( $instance['sportcat'] ) ? $instance['sportcat'] : '';
		//$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
		$options = json_decode($instance['options']);
		
		?>

		<p class="aoa3f-sports-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'aoa3f-sports' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'sportcat' ); ?>"><?php _e( 'Roles:', 'aoa3f-sports' ); ?></label>
			<select multiple class="widefat sportcat-selector" id="<?php echo 'sportcat-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'sportcat' ); ?>[]" >
                <?php
                $terms = get_terms( 'sportcat', array('hide_empty' => 0) );
                foreach ($terms as $term) {
                    ?>
                    <option value="<?php echo $term->term_id; ?>" <?php if (in_array($term->term_id, $sportcat)) echo 'selected="selected"'; ?> ><?php echo $term->name; ?></option>      
                    <?php 
                } ?>
            </select>
        </p>
        */ ?>

		<p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'aoa3f-sports' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order', 'aoa3f-sports' ); ?></label>
			<select class="order-selector" id="<?php echo 'order-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" >
                <option value="rand" <?php if ($order == 'rand') echo 'selected="selected"'; ?>><?php _e('Random','aoa3f-sports'); ?></option>
                <option value="title" <?php if ($order == 'title') echo 'selected="selected"'; ?>><?php _e('Title','aoa3f-sports'); ?></option>
                <option value="date" <?php if ($order == 'date') echo 'selected="selected"'; ?>><?php _e('Date','aoa3f-sports'); ?></option>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'direction' ); ?>"><?php _e( 'Direction', 'aoa3f-sports' ); ?></label>
			<select class="direction-selector" id="<?php echo 'direction-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'direction' ); ?>" >
                <option value="asc" <?php if ($direction == 'asc') echo 'selected="selected"'; ?>><?php _e('Asc','aoa3f-sports'); ?></option>
                <option value="desc" <?php if ($direction == 'desc') echo 'selected="selected"'; ?>><?php _e('Desc','aoa3f-sports'); ?></option>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Layout', 'aoa3f-sports' ); ?></label>
			<select class="layout-selector" id="<?php echo 'layout-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>" >
                <option value="titles" <?php if ($layout == 'titles') echo 'selected="selected"'; ?>><?php _e('List of titles','aoa3f-sports'); ?></option>
                <option value="titles-with-icons" <?php if ($layout == 'titles-with-icons') echo 'selected="selected"'; ?>><?php _e('List of titles with icons','aoa3f-sports'); ?></option>
                <option value="titles-with-logos" <?php if ($layout == 'titles-with-logos') echo 'selected="selected"'; ?>><?php _e('List of titles with logos','aoa3f-sports'); ?></option>
            </select>
            
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'aoa3f-sports' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" />
        </p>
        */ ?>

		<?php
	}
}

function aoa3f_sports_init(){
	register_widget('aoa3f_sports_widget');
}
add_action('widgets_init', 'aoa3f_sports_init');


function aoa3f_sports_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'aoa3f_sports_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'aoa3f_sports_widget_scripts' );

function aoa3f_sports_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'aoa3f_sports_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'aoa3f_sports_widget_css' );

?>
