/*
Plugin Name: AO Rooms
Author URI: http://www.alleyoop.be
Description: A rooms widget.
Author: Alley Oop
Version: 1.0
*/

jQuery(function(){
    jQuery('.rooms .carousel').carousel({
      interval: 6000
    });
});
