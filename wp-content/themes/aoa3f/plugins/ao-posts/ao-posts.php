<?php
/*
 * Plugin Name: AO Posts
 * Author URI: http://www.alleyoop.be
 * Description: A posts widget.
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AO_POSTS_VERSION', '1.0');
define('AO_POSTS_AUTHOR', 'Alley Oop');
define('AO_POSTS_URL', get_template_directory_uri() . '/plugins/ao-posts/');


## Include the required scripts
function ao_posts_public_scripts(){
    wp_enqueue_script('jquery');
	wp_register_script('ao-posts-js', AO_POSTS_URL . 'js/ao-posts.js', array(), '1');
	wp_enqueue_script(array('jquery', 'ao-posts-js'));
}
add_action('wp_enqueue_scripts', 'ao_posts_public_scripts');


## Include the required styles
function ao_posts_public_styles(){
	wp_register_style('ao-posts-css', AO_POSTS_URL . 'css/ao-posts.css', array(), '2');
	wp_enqueue_style('ao-posts-css');
}
add_action('wp_enqueue_scripts', 'ao_posts_public_styles');



class ao_posts_widget extends WP_Widget{

	## Initialize
	
	function ao_posts_widget(){
	
		// set text domain
		$dom = 'ao-posts';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-ao-posts',
			'description' => __('Widget to display featured posts', 'ao-posts'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('ao-posts', __('Fetaured posts', 'ao-posts'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('ao_posts', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        
		$number = ( ! empty( $instance['number'] ) ) ? $instance['number'] : '-1';
        $order = isset( $instance['order'] ) ? $instance['order'] : 'date';
        $direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
        //$posttype = isset( $instance['posttype'] ) ? $instance['posttype'] : 'sport';
        $layout = isset( $instance['layout'] ) ? $instance['layout'] : 'titles-with-text-and-thumbnails';
		$grid = ( ! empty( $instance['grid'] ) ) ? $instance['grid'] : '12';
        //$diapo = isset( $instance['diapo'] ) ? $instance['diapo'] : '0';
        $category = $instance['category'];
        $imageheight = ( ! empty( $instance['imageheight'] ) ) ? $instance['imageheight'] : '100';
        $imagewidth = ( ! empty( $instance['imagewidth'] ) ) ? $instance['imagewidth'] : '60';
        $options = json_decode($instance['options']);
		//$show_all_title = $instance['show_all_title'];

		$r = new WP_Query( apply_filters( 'widget_posts_args', 
			array ( 
				'post_status' => 'publish', 
				'posts_per_page' => $number, 
				'post_type' => 'post',
				'tax_query'	=> array(
					array(
						'taxonomy' 	=> 'category',
						'field' 	=> 'id',
						'terms' 	=> $category,
					),
				),
				'orderby' 	=> $order, 
                'order'     => $direction,
			)
		));
		//echo $r->request; 
        
        echo $before_widget; 
        if ($title) echo $before_title . $title . $after_title;
        
        $i = 0;
        ?>

        <!-- Posts -->
        <?php if ($r->have_posts()) : ?>

            <div class="featured-posts posts-<?php echo $category; ?>s <?php echo $layout; ?>">

                <div class="row">

                    <?php while ( $r->have_posts() ) : $r->the_post(); ?>

                        <div class="col col-<?php echo $grid; ?> col-lg-<?php echo $grid; ?> col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-<?php echo $grid; ?> effect <?php if (($i % (12/$grid)) == 0) echo 'clearfix'; ?>">

                            <div class="post image-<?php if ($layout == 'alternate') { echo ($i % 2) ? 'right' : 'left'; } else { echo $layout; } ?>" <?php if (($layout == 'bg') or ($layout == 'fixed')) echo 'style="height : '.$imageheight.'px"'; ?>>

                                <?php 
                                $image = get_field('images_header', get_the_ID()); 
                                if ($image) : 
                                    $title = get_the_title($linked_post->ID);
                                    $size = ($grid == 12) ? 'large' : 'medium';
                                    //echo '<div class="image-wrapper" style="height : '.$imageheight.'">';
                                    echo '<div class="image" style="background-image: url('. ((($layout == 'bg') or ($layout == 'fixed')) ? $image['url'] : $image['sizes'][$size]) .'); height : '.$imageheight.'px; '.((($layout != 'bg') and ($layout != 'fixed')) ? 'width: '.$imagewidth.'%;' : '').'"></div>';
                                    if (($layout == 'bg') or ($layout == 'fixed')) : 
                                        echo '<div class="filter" style="height : '.$imageheight.'px"></div>';
                                    endif;
                                    //echo '</div>';
                                endif;
                                ?>

                                <div class="caption <?php if (($layout == 'bg') or ($layout == 'fixed')) echo 'container'; ?>"  <?php if (($layout != 'bg') and ($layout != 'fixed')) echo 'style="height : '.$imageheight.'px"'; ?>>

                                    <div class="title">
                                        <?php the_title(); ?>
                                    </div>

                                    <div class="content">
                                        <?php the_content(); ?>
                                        
                                        <?php
                                        $flexible_field = 'post_flexible';
                                        // check if the flexible content field has rows of data
                                        if ( have_rows( $flexible_field ) ):
                                            // loop through the rows of data
                                            while ( have_rows( $flexible_field ) ) : the_row();
                                                echo aoa3f_display_layout( get_row_layout() );
                                            endwhile;
                                        endif;
                                        ?>
                                        
                                    </div>

                                </div>

                            </div>

                        </div>

                        <?php $i = $i + 1 ; ?>

                    <?php endwhile; ?>

                </div>

            </div>

        <?php endif; ?>
        <!--/Posts -->

        <?php

/*
		if ($r->have_posts()) :
        ?>
			<ul class="linked linked-<?php echo $posttype; ?>">
                
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li class="sport">
                        
                        <a href="<?php the_permalink(); ?>" class="title">

                            <?php 
                            if ($layout == 'titles-with-icons') :
                                $icon = get_field('images_icon');
                                echo '<i class="flaticon flaticon-'.$icon.'"></i>';
                            endif; 
                            ?>
                        
                            <?php 
                            if ($layout == 'titles-with-logos') :
                                if ($image = get_field('images_thumbnail')) :
                                    echo '<span class="id-photo" style="background-image:url('.$image['sizes']['medium'].');"></span>';
                                else : 
                                    echo '<span class="id-photo default"></span>'; 
                                endif;
                            endif; 
                            ?>
                        
                            <span><?php the_title(); ?></span>
                            
                        </a>

                    </li>
                <?php endwhile; ?>

            </ul>

        <?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		else : 
			echo '<p class="no-event">'.__('There is no sport', 'ao-posts').'</p>';
		endif;
        */
        
		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('ao_posts', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		//error_log('sarickx >>> old instance linked: ' . json_encode($old_instance));;
		error_log('sarickx >>> new instance linked: ' . json_encode($new_instance));;
		error_log('sarickx >>> title : ' . $new_instance['title1'] );
		
		
		$instance = $old_instance;
		$instance['title']          = stripslashes($new_instance['title']);
		$instance['number']         = isset( $new_instance['number'] ) ? $new_instance['number'] : '-1';
		$instance['order']          = isset( $new_instance['order'] ) ? $new_instance['order'] : 'date';
		$instance['direction']      = isset( $new_instance['direction'] ) ? $new_instance['direction'] : 'asc';
		//$instance['posttype']       = isset( $new_instance['posttype'] ) ? $new_instance['posttype'] : 'sport';
		$instance['layout']         = isset( $new_instance['layout'] ) ? $new_instance['layout'] : 'titles-with-text-and-thumbnails';
		$instance['grid']           = isset( $new_instance['grid'] ) ? $new_instance['grid'] : '12';
		//$instance['diapo']           = isset( $new_instance['diapo'] ) ? $new_instance['diapo'] : '0';
        $instance['category']        = $new_instance['category'];
        $instance['imageheight']     = isset( $instance['imageheight'] ) ? $new_instance['imageheight'] : '100';
        $instance['imagewidth']     = isset( $instance['imagewidth'] ) ? $new_instance['imagewidth'] : '60';
		//$instance['show_all_title'] = strip_tags($new_instance['show_all_title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['ao_posts']) )
			delete_option('ao_posts');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('ao_upcoming_events', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? $instance['number'] : '-1';
		$order     = isset( $instance['order'] ) ? $instance['order'] : 'date';
		$direction = isset( $instance['direction'] ) ? $instance['direction'] : 'asc';
		//$posttype  = isset( $instance['posttype'] ) ? $instance['posttype'] : 'sport';
		$layout    = isset( $instance['layout'] ) ? $instance['layout'] : 'titles-with-text-and-thumbnails';
		$grid      = isset( $instance['grid'] ) ? $instance['grid'] : '12';
		//$diapo      = isset( $instance['diapo'] ) ? $instance['diapo'] : '0';
		$category = isset( $instance['category'] ) ? $instance['category'] : '';
        $imageheight = isset( $instance['imageheight'] ) ? $instance['imageheight'] : '100';
        $imagewidth = isset( $instance['imagewidth'] ) ? $instance['imagewidth'] : '60';
		//$show_all_title = isset( $instance['show_all_title'] ) ? esc_attr( $instance['show_all_title'] ) : '';
		$options = json_decode($instance['options']);
		
		?>

		<p class="ao-posts-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'ao-posts' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Roles:', 'ao-posts' ); ?></label>
			<select multiple class="widefat category-selector" id="<?php echo 'category-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'category' ); ?>[]" >
                <?php
                $terms = get_terms( 'category', array('hide_empty' => 0) );
                foreach ($terms as $term) {
                    ?>
                    <option value="<?php echo $term->term_id; ?>" <?php if (in_array($term->term_id, $category)) echo 'selected="selected"'; ?> ><?php echo $term->name; ?></option>      
                    <?php 
                } ?>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'ao-posts' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order', 'ao-posts' ); ?></label>
			<select class="order-selector" id="<?php echo 'order-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" >
                <option value="rand" <?php if ($order == 'rand') echo 'selected="selected"'; ?>><?php _e('Random','ao-posts'); ?></option>
                <option value="title" <?php if ($order == 'title') echo 'selected="selected"'; ?>><?php _e('Title','ao-posts'); ?></option>
                <option value="date" <?php if ($order == 'date') echo 'selected="selected"'; ?>><?php _e('Date','ao-posts'); ?></option>
            </select>
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'direction' ); ?>"><?php _e( 'Direction', 'ao-posts' ); ?></label>
			<select class="direction-selector" id="<?php echo 'direction-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'direction' ); ?>" >
                <option value="asc" <?php if ($direction == 'asc') echo 'selected="selected"'; ?>><?php _e('Asc','ao-posts'); ?></option>
                <option value="desc" <?php if ($direction == 'desc') echo 'selected="selected"'; ?>><?php _e('Desc','ao-posts'); ?></option>
            </select>
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'posttype' ); ?>"><?php _e( 'Post type to be linked', 'ao-posts' ); ?></label>
			<select class="posttype-selector" id="<?php echo 'posttype-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'posttype' ); ?>" >
                <option value="sport" <?php if ($posttype == 'sport') echo 'selected="selected"'; ?>><?php _e('Sport','ao-posts'); ?></option>
                <option value="club" <?php if ($posttype == 'club') echo 'selected="selected"'; ?>><?php _e('Club','ao-posts'); ?></option>
                <option value="infrastructure" <?php if ($posttype == 'infrastructure') echo 'selected="selected"'; ?>><?php _e('Infrastructure','ao-posts'); ?></option>
                <option value="meeting" <?php if ($posttype == 'meeting') echo 'selected="selected"'; ?>><?php _e('Meeting','ao-posts'); ?></option>
            </select>
            
        </p>
        */ ?>

		<p>
            <label for="<?php echo $this->get_field_id( 'grid' ); ?>"><?php _e( 'Nombre de colonnes', 'ao-posts' ); ?></label>
			<select class="grid-selector" id="<?php echo 'grid-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'grid' ); ?>" >
                <option value="12" <?php if ($grid == '12') echo 'selected="selected"'; ?>><?php _e('1','ao-posts'); ?></option>
                <option value="6" <?php if ($grid == '6') echo 'selected="selected"'; ?>><?php _e('2','ao-posts'); ?></option>
                <option value="4" <?php if ($grid == '4') echo 'selected="selected"'; ?>><?php _e('3','ao-posts'); ?></option>
                <option value="3" <?php if ($grid == '3') echo 'selected="selected"'; ?>><?php _e('4','ao-posts'); ?></option>
                <option value="2" <?php if ($grid == '2') echo 'selected="selected"'; ?>><?php _e('6','ao-posts'); ?></option>
            </select>
            
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'diapo' ); ?>"><?php _e( 'View', 'ao-posts' ); ?></label>
			<select class="diapo-selector" id="<?php echo 'diapo-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'diapo' ); ?>" >
                <option value="0" <?php if ($diapo == '0') echo 'selected="selected"'; ?>><?php _e('Grid','ao-posts'); ?></option>
                <option value="1" <?php if ($diapo == '1') echo 'selected="selected"'; ?>><?php _e('Diaporama','ao-posts'); ?></option>
            </select>
            
        </p>
        */ ?>

		<p>
            <label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Header image position', 'ao-posts' ); ?></label>
			<select class="layout-selector" id="<?php echo 'layout-'.$this->number; ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>" >
                <option value="bg" <?php if ($layout == 'bg') echo 'selected="selected"'; ?>><?php _e('Background','ao-posts'); ?></option>
                <option value="fixed" <?php if ($layout == 'fixed') echo 'selected="selected"'; ?>><?php _e('Fixed background','ao-posts'); ?></option>
                <option value="left" <?php if ($layout == 'left') echo 'selected="selected"'; ?>><?php _e('Left','ao-posts'); ?></option>
                <option value="right" <?php if ($layout == 'right') echo 'selected="selected"'; ?>><?php _e('Right','ao-posts'); ?></option>
                <option value="alternate" <?php if ($layout == 'alternate') echo 'selected="selected"'; ?>><?php _e('Alternate','ao-posts'); ?></option>
            </select>
            
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'imageheight' ); ?>"><?php _e( 'Header image height (px)', 'ao-posts' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'imageheight' ); ?>" name="<?php echo $this->get_field_name( 'imageheight' ); ?>" type="text" value="<?php echo $imageheight; ?>" size="6" />
        </p>

		<p>
            <label for="<?php echo $this->get_field_id( 'imagewidth' ); ?>"><?php _e( 'Header image width (%)', 'ao-posts' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'imagewidth' ); ?>" name="<?php echo $this->get_field_name( 'imagewidth' ); ?>" type="text" value="<?php echo $imagewidth; ?>" size="6" />
        </p>

        <?php /*
		<p>
            <label for="<?php echo $this->get_field_id( 'show_all_title' ); ?>"><?php _e( 'Show all title:', 'ao-posts' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'show_all_title' ); ?>" name="<?php echo $this->get_field_name( 'show_all_title' ); ?>" type="text" value="<?php echo $show_all_title; ?>" />
        </p>
        */ ?>

		<?php
	}
}

function ao_posts_init(){
	register_widget('ao_posts_widget');
}
add_action('widgets_init', 'ao_posts_init');


function ao_posts_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'ao_posts_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'ao_posts_widget_scripts' );

function ao_posts_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'ao_posts_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'ao_posts_widget_css' );

?>
