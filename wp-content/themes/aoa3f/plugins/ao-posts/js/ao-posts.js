/*
Plugin Name: AO Posts
Author URI: http://www.alleyoop.be
Description: A posts widget.
Author: Alley Oop
Version: 1.0
*/

jQuery(function(){
    jQuery('.posts .carousel').carousel({
      interval: 6000
    });
});
