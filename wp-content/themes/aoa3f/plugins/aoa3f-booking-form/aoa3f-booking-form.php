<?php
/*
 * Plugin Name: AOA3F Booking Form
 * Author URI: http://www.alleyoop.be
 * Description: A widget to display the booking form.
 * Author: Alley Oop
 * Version: 1.0
 */


## Constants definition
define('AOA3F_BOOKING_FORM_VERSION', '1.0');
define('AOA3F_BOOKING_FORM_AUTHOR', 'Alley Oop');
define('AOA3F_BOOKING_FORM_URL', get_template_directory_uri() . '/plugins/aoa3f-booking-form/');


class aoa3f_booking_form_widget extends WP_Widget{

	## Initialize
	
	function aoa3f_booking_form_widget(){
	
		// set text domain
		$dom = 'aoa3f-booking-form';
		$mofile = trailingslashit(dirname(__File__)) . 'languages/' . $dom . '-' . get_locale() . '.mo';
		load_textdomain( $dom, $mofile );
		
		$widget_ops = array(
			'classname' => 'widget-aoa3f-booking-form',
			'description' => __('Widget to display the booking form', 'aoa3f-booking-form'),
		);
		
		$control_ops = array('width' => 250, 'height' => 500);
		parent::WP_Widget('aoa3f-booking-form', __('Booking form', 'aoa3f-booking-form'), $widget_ops, $control_ops);
	}
	
	## Display the Widget
	
	function widget($args, $instance){
        
		$cache = wp_cache_get('aoa3f_booking_form', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = $instance['title'];
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        
        echo $before_widget; 
        if ($title) echo $before_title . $title . $after_title;
        
        $lang = 'fr';
        if (function_exists(pll_current_language)) $lang = pll_current_language('slug');
        ?>
    
        <div id="booking-form">
            <div class="container">
                <?php $url = ($lang == 'en') ? get_option('group_url_en') : get_option('group_url_fr'); ?>
                <form class="form-inline row" action="<?php echo $url; ?>" method="post">
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <div class="input-group-addon"><?php _e('From', 'aoa3f'); ?></div>
                            <input type="text" class="form-control datepicker" id="startdate" name="startdate" placeholder="">
                            <div class="input-group-addon cal"><span class="flaticon flaticon-calendar"></span></div>
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <div class="input-group">
                            <div class="input-group-addon"><?php _e('To', 'aoa3f'); ?></div>
                            <input type="text" class="form-control datepicker" id="enddate" name="enddate" placeholder="">
                            <div class="input-group-addon cal"><span class="flaticon flaticon-calendar"></span></div>
                        </div>
                    </div>
                    <div class="form-group col-sm-3">
                        <button type="submit" class="btn btn-default" id="room"><?php _e('Book room', 'aoa3f-booking-form'); ?></button>
                    </div>
                    <div class="form-group col-sm-3">
                        <button type="submit" class="btn btn-default" id="group"><?php _e('Group quotation', 'aoa3f-booking-form'); ?></button>
                    </div>
                </form>
                <script type="text/javascript">
                    jQuery(function() {
                        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ "<?php echo $lang; ?>" ] );
                        jQuery.datepicker.formatDate( "dd/mm/yyyy" );
                        jQuery( "#booking-form #startdate" ).datepicker({
                            minDate: new Date(),
                            defaultDate: "+1w",
                            changeMonth: false,
                            numberOfMonths: 2,
                            firstDay: 1,
                            onClose: function( selectedDate ) {
                                var date = jQuery(this).datepicker('getDate');
                                if (date) {
                                      date.setDate(date.getDate() + 1);
                                }
                                jQuery( "#booking-form #enddate" ).datepicker( "option", "minDate", date );
                                jQuery( "#booking-form #enddate" ).datepicker( "show" );
                            }
                        });
                        jQuery( "#booking-form #enddate" ).datepicker({
                            defaultDate: "+1w",
                            changeMonth: false,
                            numberOfMonths: 2,
                            firstDay: 1,
                            /*
                            onClose: function( selectedDate ) {
                                var date = jQuery(this).datepicker('getDate');
                                if (date) {
                                      date.setDate(date.getDate() - 1);
                                }
                                jQuery( "#booking-form #startdate" ).datepicker( "option", "maxDate", date );
                            }
                            */
                        });
                        /*
                        jQuery( "#booking-form #go").click(function() {
                            var s = jQuery( "#booking-form #startdate" ).val().split( "/" );
                            var e = jQuery( "#booking-form #enddate" ).val().split( "/" );
                            var startdate = s[2] + '-' + s[1] + '-' + s[0];
                            var enddate = e[2] + '-' + e[1] + '-' + e[0];
                            if ( jQuery( "#booking-form #type" ).prop( "checked" ) == true ) {
                                jQuery( "booking-form" ).submit();
                            }
                            else {
                                centerWindowBooking('https://booking.cubilis.eu/select_dates.aspx?logisid=3351&booktemplate=1&lang=fr&startdatum='+startdate+'&einddatum='+enddate,'465','819','no','no');
                                return false;
                            }
                        });
                        */
                        jQuery( "#booking-form #room").click(function() {
                            var s = jQuery( "#booking-form #startdate" ).val().split( "/" );
                            var e = jQuery( "#booking-form #enddate" ).val().split( "/" );
                            /*var startdate = s[2] + '-' + s[1] + '-' + s[0];
                            var enddate = e[2] + '-' + e[1] + '-' + e[0];
                            centerWindowBooking('https://booking.cubilis.eu/select_dates.aspx?logisid=3351&booktemplate=1&taal=<?php echo $lang; ?>&startdatum='+startdate+'&einddatum='+enddate,'465','819','no','no');*/
							var startdate = s[0] + '-' + s[1] + '-' + s[2];
                            var enddate = e[0] + '-' + e[1] + '-' + e[2];
							centerWindowBooking('https://reservations.cubilis.eu/auberge-des-3-fontaines-brussels/Rooms/Select?Arrival='+startdate+'&Departure='+enddate+'&Language=<?php echo $lang; ?>','780','819','no','no');
                            return false;
                        });
                        jQuery( "#booking-form #group").click(function() {
                            var s = jQuery( "#booking-form #startdate" ).val().split( "/" );
                            var e = jQuery( "#booking-form #enddate" ).val().split( "/" );
                            var startdate = s[2] + '-' + s[1] + '-' + s[0];
                            var enddate = e[2] + '-' + e[1] + '-' + e[0];
                            jQuery( "booking-form" ).submit();
                        });
                    });
                </script>

                <?php /*
                <script type="text/javascript">

                    jQuery(document).ready(function() {
                        jQuery('#booking-form .datepicker').datepicker({
                            format: "dd/mm/yyyy",
                            language: "fr",
                            orientation: "bottom auto",
                            startDate: "0d",
                            autoclose: true,
                            todayHighlight: true
                        });
                    });

                    jQuery(document).ready(function() {
                        jQuery('#<?php echo $datepicker_range_fieldId; ?>').on('changeDate', function(selected){
                            var startDate = new Date(selected.date.valueOf());
                            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
                            jQuery('#<?php echo $id; ?>').datepicker('setStartDate', startDate);
                            if ((jQuery('#<?php echo $datepicker_range_fieldId; ?>').val() != '') && (jQuery('#<?php echo $id; ?>').val() != '')) {
                                var fromArray = jQuery('#<?php echo $datepicker_range_fieldId; ?>').val().split('/');
                                var from = new Date(fromArray[2], parseInt(fromArray[1], 10) - 1, fromArray[0]);
                                var toArray = jQuery('#<?php echo $id; ?>').val().split('/');
                                var to = new Date(toArray[2], parseInt(toArray[1], 10) - 1, toArray[0]);
                                if(from >= to) {
                                    jQuery('#<?php echo $id; ?>').val('');
                                }
                            }
                        });
                    });
                </script>
                */ ?>
            </div>
        </div>

        <?php
        
		echo $after_widget; 

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('aoa3f_booking_form', $cache, 'widget');

	}
	
	## Save settings
	
	function update($new_instance, $old_instance){
	
		$instance = $old_instance;
		$instance['title']          = stripslashes($new_instance['title']);
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['aoa3f_booking_form']) )
			delete_option('aoa3f_booking_form');

		return $instance;
	}
	
	function flush_widget_cache() {
		wp_cache_delete('aoa3f_booking_form', 'widget');
	}

	## Widget form
	
	function form($instance){
	
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		
		?>

		<p class="aoa3f-booking-form-title">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'aoa3f-booking-form' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<?php
	}
}

function aoa3f_booking_form_init(){
	register_widget('aoa3f_booking_form_widget');
}
add_action('widgets_init', 'aoa3f_booking_form_init');


function aoa3f_booking_form_widget_scripts(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--Customizer Javascript--> 
	<script type="text/javascript">
	</script>

	<?php
	endif;
	
}
add_action('admin_footer', 'aoa3f_booking_form_widget_scripts');
add_action( 'customize_controls_print_footer_scripts', 'aoa3f_booking_form_widget_scripts' );

function aoa3f_booking_form_widget_css(){

	if(in_array($GLOBALS['pagenow'], array('widgets.php', 'customize.php'))) :
	?>
	
	<!--/Customizer Javascript--> 
	<style type="text/css">
	</style>
	
	<?php
	endif;
}
add_action('admin_head', 'aoa3f_booking_form_widget_css');
add_action( 'customize_controls_print_footer_scripts', 'aoa3f_booking_form_widget_css' );

?>
