<?php
/** 404.php
 *
 * The template for displaying 404 pages (Not Found).
 *
 * @author      Alley Oop
 * @package     AO Auberge des 3 fontaines
 * @since       1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <h1><?php _e( 'Ups!', 'aoa3f'); ?></h1>
        <h2><?php _e('It looks like the page you\'re looking for doesn\'t exist.', 'aoa3f' ); ?></h2>
        <p>
            <a href="<?php echo get_site_url(); ?>"><?php _e('Click here to go back to the home page', 'aoa3f'); ?></a>
        <p>
        <p class="code-error"><?php _e( '(Error code: 404)', 'aoa3f' ); ?></p>

    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>


<?
/* End of file 404.php */
/* Location: ./wp-content/themes/aoa3f/404.php */