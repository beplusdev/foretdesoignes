<?php
/** single.php
 *
 * The template for displaying all single posts.
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <?php if (is_active_sidebar( 'right-content' )) : ?>
    <div id="site-content" class="home col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <?php else : ?>
    <div id="site-content" class="home col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php endif; ?>

        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>   
        
                <?php if ( has_term( 'event', 'newscat', get_the_ID() ) ) : ?>
                <div class="entry-event">
                    <?php aoa3f_display_layout_event(); ?>
                </div>
                <?php endif; ?>
        
                <?php
                $flexible_field = get_post_type() . '_flexible';

                // check if the flexible content field has rows of data
                if ( have_rows( $flexible_field ) ):

                    // loop through the rows of data
                    while ( have_rows( $flexible_field ) ) : the_row();

                        echo aoa3f_display_layout( get_row_layout() );

                    endwhile;

                else :

                    // no layouts found

                endif;

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
    </div>
    
    <?php if (is_active_sidebar( 'right-content' )) : ?>
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    <?php endif; ?>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file single.php */
/* Location: ./wp-content/themes/aoa3f/sinle.php */