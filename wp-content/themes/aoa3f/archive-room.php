<?php
/** archive.php
 *
 * The template for displaying archive page
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
*/
?>

<?php get_header(); ?>

<div class="row">

    <div id="site-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rooms">

        <?php /*
        <h1 class="archive-title">
            <?php 
            post_type_archive_title(); 
            if (is_tax()) :
                global $wp_query;
                $term = $wp_query->get_queried_object();
                echo $term->name;
            endif;
            ?>
        </h1>
        */ ?>
        
        <?php get_sidebar( 'bottom-title' ); ?>

        <?php 
        if ( have_posts() ) : 
        
            $pre_terms = array();
            $first = true;
        
            // The loop 
            while ( have_posts() ) : the_post();

                $post_type = get_post_type();
                
                $terms = wp_get_post_terms($post->ID, 'roomcat', array("fields" => "names"));
                if ( !empty( $terms ) and ( $terms != $pre_terms ) ) :
                    if (!$first) echo '</div>';
                    else $first = false;
                    echo '<div class="row"><h1 class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' . implode(', ', $terms) . '</h1>';
                    $pre_terms = $terms;
                endif;
                ?>
                <div class="room col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <article>
        
                    <a href="<?php the_permalink(); ?>"> 

                        <div class="image-wrapper">
                            <?php 
                            $image = get_field('images_thumbnail'); 
                            if ($image) : 
                                $title = get_the_title();
                                echo '<div class="image" style="background-image: url('.$image['sizes']['two-third'].');"></div>';		
                            elseif ($default = get_option('default_'.$post_type.'_image')) : 
                                echo '<div class="image" style="background-image: url('.$default.');"></div>';
                            endif;
                            ?>
                            <div class="filter"></div>
                        </div>

                        <div class="caption ">

                            <div class="title">
                                <?php //aoa3f_event_calendar(); ?>
                                <?php the_title(); ?>
                            </div>   

                            <div class="content">

                                <?php 
                                if (!$excerpt = get_the_excerpt()) :
                                    $excerpt = get_the_content();
                                endif;
                                $excerpt = wp_strip_all_tags($excerpt);
                                if ( mb_strlen( $excerpt ) > $charlength ) :
                                    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                                    $exwords = explode( ' ', $subex );
                                    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                                    if ( $excut < 0 ) :
                                        echo mb_substr( $subex, 0, $excut );
                                    else :
                                        echo $subex;
                                    endif;
                                    echo '...';
                                else :
                                    echo $excerpt;
                                endif;
                                ?>

                                <?php 
                                if ($price_from = get_field('room_price_from')) :
                                    echo '<div class="price-from"><span class="text">' . __('Price from', 'aoa3f') . '</span><span class="price">' . $price_from . '</span></div>';
                                endif; 
                                ?>

                            </div>   

                            <button class="btn read-more">
                                <?php _e('Read more', 'aoa3f'); ?>
                            </button>                            

                            <?php if ($button_book_link = get_field('room_button_link')) : ?>
                                <a href="javascript:void(0)" class="btn book-now" onclick="centerWindowBooking('<?php echo $button_book_link; ?>','465','819','no','no');" class="imgLnk"><?php echo ($button_book_now = get_field('room_button')) ? $button_book_now : __('Book now !', 'aoa3f'); ?></a>
                            <?php endif; ?>

                        </a>
                        
                    </div>
                    
                </article>
                </div>
                    
                <?php
            endwhile;
            ?>
                
            </div>

            <?php
        
        // No posts to display
        else :

            echo '<p class="no-post">'.__('There is currently no room.', 'aoa3f').'</p>';

        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <?php
//$post = $wp_query->post;

//echo $post->ID;
    ?>
        
    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file archive.php */
/* Location: ./wp-content/themes/aoa3f/archive.php */