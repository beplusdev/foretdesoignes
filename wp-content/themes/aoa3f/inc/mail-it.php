<?php
/** mail-it.php
 *
 * Script to send an email from the contact form
 *
 * @author	Alley Oop
 * @package	AO Clubs
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php
$send_from = aoa3f_decrypt_email(cleanupentries($_POST["form1"]));
$send_to = aoa3f_decrypt_email(cleanupentries($_POST["form2"]));
$test = cleanupentries($_POST["form3"]); // should be empty
$send_subject = cleanupentries($_POST["send_subject"]);
$send_body = cleanupentries($_POST["send_body"]);
//$from_ip = $_SERVER['REMOTE_ADDR'];
//$from_browser = $_SERVER['HTTP_USER_AGENT'];

function cleanupentries($entry) {
    $entry = trim($entry);
    $entry = stripslashes($entry);
    $entry = str_replace('|ao|', '<br>', htmlspecialchars($entry));

    return $entry;
}

function aoa3f_decrypt_email($number) {

    $chars = array();
    $strlen = strlen( $number ) / 7;
    for( $i = 0; $i < $strlen; $i++ ) {
        $n = substr( $number, $i*7, 7 );
        $chars[] = chr( 999 - ( $n / 2014 ) );
        // $char contains the current character, so do your processing here
    }
    return implode('', $chars);
}

$headers = "From: " . $send_from . "\r\n" .
    "Reply-To: " . $send_from . "\r\n" .
    "X-Mailer: PHP/" . phpversion() . "\r\n" .
    "MIME-Version: 1.0\r\n" .
    "Content-Type: text/html; charset=ISO-8859-1\r\n";

if (!$send_from) {
    echo "no sender email";
    exit;
}else if (!$send_to){
    echo "no recipient email";
    exit;
}else if (!$send_subject){
    echo "no subject";
    exit;
}else if (!$send_body){
    echo "no message";
    exit;
}else if ($test) {
    // test should be empty. If it is not, that means it is spamming. 
    // We don't send the email but act like if it was sent. 
    echo "ok";
    exit;    
}else {
    //echo $send_from . '<br><br>' . $send_to . '<br><br>' . $send_subject . '<br><br>' . $send_body . '<br><br>' . $headers . '<br><br>';
    //if (filter_var($send_from, FILTER_VALIDATE_EMAIL)) {
        mail($send_to, $send_subject, $send_body, $headers);
        echo "ok";
        exit;
    //}else{
    //    echo "mail invalide";
    //    exit;
    //}
}
?>