<?php
/** aoa3f-custom-posttypes.php
 *
 * AO Custom post types for the theme
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aoa3f', get_template_directory() . '/lang' );

/**
 * Generate specific post types and taxonomies.
 *
 * @link http://codex.wordpress.org/Post_Types
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @uses register_post_type()
 * @uses wp_insert_term()
 *
 * @since 1.0.0
 *
 * @return void
 */  
function aoa3f_custom_posttypes_and_taxonomies() {

	$post_types = array_map('trim', explode(",", get_option( 'post_types' )));
	
	
	## Post type : News
	
	if (in_array('news', $post_types)) :

		$labels = array(
			'name' 					=> _x('News', 'plural','aoa3f'),
			'singular_name' 		=> _x('News', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a news','aoa3f'),
			'edit_item' 			=> __('Modify a news','aoa3f'),
			'new_item' 				=> __('New news','aoa3f'),
			'view_item' 			=> __('View news','aoa3f'),
			'search_items' 			=> __('Find a news','aoa3f'),
			'not_found' 			=> __('No news found','aoa3f'),
			'not_found_in_trash' 	=> __('No news found in the bin','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'news' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor', 'excerpt'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'news'),
		) );

		// Insert taxonomy "newscat" for "news"
		$newscat_id = register_taxonomy( 'newscat', array('news'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('News categories','aoa3f'), 
			'singular_label' 	=> __('News category','aoa3f'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
	
        // Insert term "homepage"
        if ($term = get_term_by('slug', 'homepage', 'newscat')) {
            $homepage = $term;
        }
        else {	
            $homepage = wp_insert_term( __('Homepage','aoa3f'), 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'homepage',
            ) );
        }

        // Insert term "lastneaws"
        if ($term = get_term_by('slug', 'lastnews', 'newscat')) {
            $lastnews = $term;
        }
        else {	
            $lastnews = wp_insert_term( 'News', 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'lastnews',
            ) );
        }

        // Insert term "event"
        if ($term = get_term_by('slug', 'event', 'newscat')) {
            $event = $term;
        }
        else {	
            $event = wp_insert_term( __('Events','aoa3f'), 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'event',
            ) );
        }

        // Insert term "press"
        if ($term = get_term_by('slug', 'press', 'newscat')) {
            $press = $term;
        }
        else {	
            $press = wp_insert_term( __('Press','aoa3f'), 'newscat', $args = array(
                'description'	=> '',
                'slug' 			=> 'press',
            ) );
        }

    
	endif;
	
	
	## Post type : Sport
	
	if (in_array('sport', $post_types)) :

		$labels = array(
			'name' 					=> _x('Sports', 'plural','aoa3f'),
			'singular_name' 		=> _x('Sport', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a sport','aoa3f'),
			'edit_item' 			=> __('Modify a sport','aoa3f'),
			'new_item' 				=> __('New sport','aoa3f'),
			'view_item' 			=> __('View sport','aoa3f'),
			'search_items' 			=> __('Find a sport','aoa3f'),
			'not_found' 			=> __('No sport found','aoa3f'),
			'not_found_in_trash' 	=> __('No sport found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'sport' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor', 'excerpt'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'sport'),
		) );
        
        /*
		$newscat_id = register_taxonomy( 'sportcat', array('sport'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Sport categories','aoa3f'), 
			'singular_label' 	=> __('Sport category','aoa3f'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
        */
	
	endif;
	
	
	## Post type : Club
	
	if (in_array('club', $post_types)) :

		$labels = array(
			'name' 					=> _x('Clubs', 'plural','aoa3f'),
			'singular_name' 		=> _x('Club', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a club','aoa3f'),
			'edit_item' 			=> __('Modify a club','aoa3f'),
			'new_item' 				=> __('New club','aoa3f'),
			'view_item' 			=> __('View club','aoa3f'),
			'search_items' 			=> __('Find a club','aoa3f'),
			'not_found' 			=> __('No club found','aoa3f'),
			'not_found_in_trash' 	=> __('No club found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'club' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'menu_position' 		=> null,
			'supports' 				=> array('title', 'editor', 'excerpt'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'club'),
		) );
        
		// Insert taxonomy "newscat" for "news"
		$clubcat_id = register_taxonomy( 'clubcat', array('club'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Club categories','aoa3f'), 
			'singular_label' 	=> __('Club category','aoa3f'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
		
	endif;
	
	
	## Post type : Infrastructure

	if (in_array('infrastructure', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Infrastructures', 'plural','aoa3f'),
			'singular_name' 		=> _x('Infrastructure', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add an infrastructure','aoa3f'),
			'edit_item' 			=> __('Modify an infrastructure','aoa3f'),
			'new_item' 				=> __('New infrastructure','aoa3f'),
			'view_item' 			=> __('View infrastructure','aoa3f'),
			'search_items' 			=> __('Find an infrastructure','aoa3f'),
			'not_found' 			=> __('No infrastructure found','aoa3f'),
			'not_found_in_trash' 	=> __('No infrastructure found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'infrastructure' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor', 'excerpt'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'infrastructure'),
		) );

 		$infrastructurecat_id = register_taxonomy( 'infrastructurecat', array('infrastructure'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Infrastructure categories','aoa3f'), 
			'singular_label' 	=> __('Infrastructure category','aoa3f'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
 
	endif;


	## Post type : Room

	if (in_array('room', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Rooms', 'plural','aoa3f'),
			'singular_name' 		=> _x('Room', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a room','aoa3f'),
			'edit_item' 			=> __('Modify a room','aoa3f'),
			'new_item' 				=> __('New room','aoa3f'),
			'view_item' 			=> __('View room','aoa3f'),
			'search_items' 			=> __('Find a room','aoa3f'),
			'not_found' 			=> __('No room found','aoa3f'),
			'not_found_in_trash' 	=> __('No room found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'room' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor', 'excerpt'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'room'),
		) );

 		$roomcat_id = register_taxonomy( 'roomcat', array('room'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Room categories','aoa3f'), 
			'singular_label' 	=> __('Room category','aoa3f'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );
 
	endif;


	## Post type : meeting

	if (in_array('meeting', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Meeting rooms', 'plural','aoa3f'),
			'singular_name' 		=> _x('Meeting room', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a meeting room','aoa3f'),
			'edit_item' 			=> __('Modify a meeting room','aoa3f'),
			'new_item' 				=> __('New meeting room','aoa3f'),
			'view_item' 			=> __('View meeting room','aoa3f'),
			'search_items' 			=> __('Find a meeting room','aoa3f'),
			'not_found' 			=> __('No meeting room found','aoa3f'),
			'not_found_in_trash' 	=> __('No meeting room found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'meeting' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> true,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor', 'excerpt'), //,'thumbnail', 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'meeting'),
		) );

		$meetingcat_id = register_taxonomy( 'meetingcat', array('meeting'), array(
			'hierarchical' 		=> true, 
			'label' 			=> __('Meeting room categories','aoa3f'), 
			'singular_label' 	=> __('Meeting room category','aoa3f'),
			'show_ui' 			=> true,
			'query_var' 		=> true,
			'_builtin' 			=> false,
			'paged'				=> true,
			'rewrite' 			=> true,
		) );

	endif;


	## Post type : Price
	
	if (in_array('price', $post_types)) :

		$labels = array(
			'name' 					=> _x('Prices', 'plural','aoa3f'),
			'singular_name' 		=> _x('Price', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a price','aoa3f'),
			'edit_item' 			=> __('Modify a price','aoa3f'),
			'new_item' 				=> __('New price','aoa3f'),
			'view_item' 			=> __('View price','aoa3f'),
			'search_items' 			=> __('Find a price','aoa3f'),
			'not_found' 			=> __('No price found','aoa3f'),
			'not_found_in_trash' 	=> __('No price found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'price' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'menu_position' 		=> null,
			'supports' 				=> array('title'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'price'),
		) );
        
	endif;
	
	
	## Post type : Team
	
	if (in_array('team', $post_types)) :

		$labels = array(
			'name' 					=> _x('Teams', 'plural','aoa3f'),
			'singular_name' 		=> _x('Team', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a team','aoa3f'),
			'edit_item' 			=> __('Modify a team','aoa3f'),
			'new_item' 				=> __('New team','aoa3f'),
			'view_item' 			=> __('View team','aoa3f'),
			'search_items' 			=> __('Find a team','aoa3f'),
			'not_found' 			=> __('No team found','aoa3f'),
			'not_found_in_trash' 	=> __('No team found in the bin','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'team' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor'), //, 'comments'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'team'),
		) );
    
    endif;


    ## Post type : Contact

	if (in_array('contact', $post_types)) :
	
		$labels = array(
			'name' 					=> _x('Contacts', 'plural','aoa3f'),
			'singular_name' 		=> _x('Contact', 'singular','aoa3f'),
			'add_new' 				=> __('Add', 'aoa3f'),
			'add_new_item' 			=> __('Add a contact','aoa3f'),
			'edit_item' 			=> __('Modify a contact','aoa3f'),
			'new_item' 				=> __('New contact','aoa3f'),
			'view_item' 			=> __('View contact','aoa3f'),
			'search_items' 			=> __('Find a contact','aoa3f'),
			'not_found' 			=> __('No contact found','aoa3f'),
			'not_found_in_trash' 	=> __('No contact found in the trash','aoa3f'),
			'parent_item_colon' 	=> ''
		); 

		register_post_type( 'contact' ,  array(
			'labels' 				=> $labels,
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'rewrite' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'menu_position' 		=> null,
			'supports' 				=> array('title','editor','thumbnail'),
			'has_archive' 			=> true,
			'rewrite'				=> array('slug' => 'contact'),
		) );

	endif;
	


    flush_rewrite_rules();
}
add_action('init', 'aoa3f_custom_posttypes_and_taxonomies');



/* Function to display the menu */
/*
add_action('admin_init', 'debug_admin_menu');
function debug_admin_menu() {
    echo '<pre>'.print_r($GLOBALS['menu'], true).'</pre>';
}
*/


/**
 * Custom admin menu 
 *
 * @link http://code.tutsplus.com/articles/customizing-the-wordpress-admin-custom-admin-menus--wp-33200
 *
 * @uses wp_get_current_user()
 # @uses WP_Users::has_cap()
 * @uses remove_menu_page()
 * @uses remove_submenu_page()
 * @uses get_page_by_path()
 * @uses add_menu_page()
 * @uses add_submenu_page()
 *
 * @since 1.0.0
 *
 * @return void
 */
function aoa3f_custom_menu_items() {

	global $menu;
    global $submenu;

    // Remove not authorized entries
    $user = wp_get_current_user();
    if ( ! $user->has_cap( 'manage_network' ) ) {
        //remove_menu_page( 'edit.php' );
        //remove_menu_page( 'edit.php?post_type=page' );
        remove_menu_page( 'edit.php?post_type=acf' );
        //remove_menu_page( 'nav-menus.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'options-general.php' );
        //remove_menu_page( 'edit-comments.php' );
		//remove_menu_page( 'edit.php?post_type=cycloneslider' );
		remove_menu_page( 'themes.php' );
        //remove_submenu_page( 'themes.php', 'nav-menus.php' );
		//remove_submenu_page( 'index.php', 'my-sites.php' );
		remove_submenu_page( 'edit.php?post_type=cycloneslider', 'cycloneslider-settings' );
        remove_submenu_page( 'wp-polls/polls-manager.php', 'wp-polls/polls-templates.php' );
        remove_submenu_page( 'wp-polls/polls-manager.php', 'wp-polls/polls-options.php' );
		remove_submenu_page( 'edit.php?post_type=lshowcase', 'lshowcase_settings' );
		remove_submenu_page( 'edit.php?post_type=lshowcase', 'lshowcase_shortcode' );
		
        // Add widget and menus separatley 
		add_menu_page( __('Menus'), __('Menus'), 'edit_pages', 'nav-menus.php', '', '' ); 
		add_menu_page( __('Widgets'), __('Widgets'), 'edit_pages', 'widgets.php', '', '' ); 
    }
}
add_action( 'admin_menu', 'aoa3f_custom_menu_items', 999 );


// Remove tags support from posts
function myprefix_unregister_tags() {
    unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'myprefix_unregister_tags');

/**
 * Change admin menu labels 
 *
 * @link http://wordpress.stackexchange.com/questions/9211/changing-admin-menu-labels
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 *
 * @return void
 */
function aoa3f_custom_post_menu_label() {

	global $menu;
    global $submenu;
    
    //error_log('$menu:'.json_encode($menu));
    //error_log('$submenu:'.json_encode($submenu));
    
    // Posts
    $menu[5][0] = __('Featured posts', 'aoa3f');
    //$submenu['edit.php'][5][0] = __('All news', 'aoa3f');
    //$submenu['edit.php'][10][0] = __('Add news', 'aoa3f');
    //$submenu['edit.php'][15][0] = 'Status'; // Change name for categories
    //$submenu['edit.php'][16][0] = 'Labels'; // Change name for tags
    
    // Pages
    $menu[20][0] = __('Other pages', 'aoa3f');
	//$menu[60][0] = __('Menus', 'aoa3f');
	//$menu[61][0] = __('Widgets', 'aoa3f');
	echo '';
}
add_action( 'admin_menu', 'aoa3f_custom_post_menu_label' );


/**
 * Custom admin menu order
 *
 * @link http://code.tutsplus.com/articles/customizing-the-wordpress-admin-custom-admin-menus--wp-33200
 *
 * @uses get_page_by_path()
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 *
 * @return array -> Menu order
 */
function aoa3f_custom_menu_order( $menu_order ) {
	
	$order[] = 'index.php';
    $order[] = "separator1";
	$order[] = 'edit.php?post_type=cycloneslider';
	
	$post_types = array_map('trim', explode(",", get_option( 'post_types' )));
	foreach ($post_types as $post_type) :
		$order[] = 'edit.php?post_type=' . $post_type;
	endforeach;
	
	$order[] = "edit.php?post_type=page";
	$order[] = "edit.php";
    $order[] = "separator2";
	$order[] = "upload.php";
	//$order[] = "edit-comments.php";
    $order[] = "wp-polls/polls-manager.php";
	$order[] = "widgets.php";
	$order[] = "nav-menus.php";
	$order[] = "edit.php?post_type=lshowcase";
    $order[] = "separator-last";
    $order[] = "users.php";
	
	return $order;
}
add_filter( 'custom_menu_order', '__return_true' );
add_filter( 'menu_order', 'aoa3f_custom_menu_order' );


/**
 * Custom admin menu icons (with FontAwesome) by inserting some styles in 
 * the <head> section of the administration panel
 *
 * @link http://clarknikdelpowell.com/blog/3-ways-to-use-icon-fonts-in-your-wordpress-theme-admin/
 *
 * @uses get_page_by_path()
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 *
 * @return void
 */
function aoa3f_custom_menu_icons() {

	?>
	<style type="text/css">
		#adminmenu #menu-posts-cycloneslider .menu-icon-post div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f03e';
		}
		#adminmenu #menu-posts-news .menu-icon-news div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1ea';
		}
		#adminmenu #menu-posts-sport .menu-icon-sport div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1e3'; /*'\f091';*/
		}
		#adminmenu #menu-posts-club .menu-icon-club div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f015'; /*'\f091';*/
		}
		#adminmenu #menu-posts-infrastructure .menu-icon-infrastructure div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f19c';
		}
		#adminmenu #menu-posts-room .menu-icon-room div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f19c';
		}
		#adminmenu #menu-posts-meeting .menu-icon-meeting div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f086';
		}
		#adminmenu #menu-posts-price .menu-icon-price div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f153'; /*'\f091';*/
		}
		#adminmenu #menu-posts-team .menu-icon-team div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f0c0';
		}
		#adminmenu #menu-posts-contact .menu-icon-contact div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f095';
		}
		#adminmenu li.wp-menu-separator {
    		height: 8px;
    	}
		#adminmenu #toplevel_page_widgets div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f1b3'; /*'\f12e';*/
    		margin-top: 1px;
		}
		#adminmenu #toplevel_page_nav-menus div.wp-menu-image:before {
			font-family: Fontawesome !important;
			font-size: 16px !important;
			content: '\f0c9';
    		margin-top: 1px;
		}
     </style>
	<?php
}
add_action('admin_head', 'aoa3f_custom_menu_icons');


/**
 * Add FontAwesome stylesheet 
 *
 * @link http://fortawesome.github.io/Font-Awesome/
 *
 * @uses wp_enqueue_style()
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 *
 * @return void
 */
function aoa3f_fontawesome_dashboard() {
   wp_enqueue_style('fontawesome', 'http:////netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css', '', '4.2.0', 'all'); 
}
add_action('admin_init', 'aoa3f_fontawesome_dashboard');

/**
 * Management of users and profiles/roles 
 */

/**
 * Remove all unnecessary roles, to only keep administrator and editor.
 *
 * @uses WP_Roles::remove_role()
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 */
 function aoa3f_remove_roles() {
	$wp_roles = new WP_Roles();
	$wp_roles->remove_role( 'contributor' );
	$wp_roles->remove_role( 'author' );
	$wp_roles->remove_role( 'subscriber' );
	$wp_roles->remove_role( 'mstw_tr_admin' );
}
add_action( 'init', 'aoa3f_remove_roles' );



/*
function ao_comments_open( $open, $post_id ) {

	$post = get_post( $post_id );

	if ( in_array($post->post_type, array('news', 'sport', 'infrastructure')) ) {
		$open = true;
    }
    else {
		$open = false;
    }
    
	return $open;
}
add_filter( 'comments_open', 'ao_comments_open', 10, 2 );
*/




/**
 * Custom columns displayed for 'Price' posts
 *
 * @uses add_filter()
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 *
 * @param array actualites_columns -> ???? to check
 * @return array -> The updated columns
 */
function aoa3f_add_new_price_columns($price_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';     
    $new_columns['title'] = __('Price', 'aoa3f');
    $new_columns['sport'] = __('Sport','aoa3f');
    $new_columns['publish_start_date'] = __('Publish start date','aoa3f');     
    $new_columns['publish_end_date'] = __('Publish end date','aoa3f');     
    $new_columns['author'] = __('Author','aoa3f');     
    $new_columns['date'] = _x('Date', 'column name','aoa3f');
	if (is_super_admin()) 
        $new_columns['id'] = __('ID');
 
    return $new_columns;
}

function aoa3f_custom_price() 
{
	add_filter('manage_edit-price_columns', 'aoa3f_add_new_price_columns');
}
add_action( 'admin_init', 'aoa3f_custom_price' );

function aoa3f_manage_price_columns($column_name, $id) {
    global $wpdb, $post;
    switch ($column_name) {
    case 'id':
        echo $id;
		break; 
    case 'sport':
        echo get_the_title(get_field('price_sport'));
		break;
    case 'publish_start_date':
		echo get_field('price_publish_start_date');  
		break;
    case 'publish_end_date':
		echo get_field('price_publish_end_date');  
		break;
    default:
        break;
    } // end switch
}
add_action('manage_price_posts_custom_column', 'aoa3f_manage_price_columns', 10, 2);




/**
 * Add 'Sport' filter for prices list
 *
 * @uses add_filter()
 *
 * @since AO Custom Auberge des 3 fontaines 1.0.0
 */
/* sarickx--- DOESN'T WORK !!

function aoa3f_restrict_price_by_sport() {
    global $typenow;
    if ($typenow == 'price') {
        $selected = isset($_GET['sport']) ? $_GET['sport'] : '';
        $arg = array(
            'show_option_none' => __("Show for all sports"),
            'orderby' => 'title',
            'hide_empty' => false,
            'suppress_filters' => true,
            'post_type' => 'sport',
            'selected' => $selected
        );
        wp_dropdown_pages($arg);
    };
}
add_action('restrict_manage_posts', 'aoa3f_restrict_price_by_sport');

function aoa3f_filter_price_on_sport($query) {
    global $pagenow;
    $post_type = 'price'; 
    $q_vars = &$query->query_vars;
    if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type) {
    $selected = isset($_GET['sport']) ? $_GET['sport'] : '';
        $q_vars['meta_query'] = array( array(
            'key'       => 'price_sport',
            'value'     => $selected,
            'compare'   => '='
        ));
    }
}
add_filter('parse_query', 'aoa3f_filter_price_on_sport');

---sarickx */


/* End of file aoa3f-custom-posttypes.php */
/* Location: ./wp-content/themes/aoa3f/inc/aoa3f-custom-posttypes.php */