<?php
/** aoa3f-custom-options.php
 *
 * AO Custom options for the theme
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aoa3f', get_template_directory() . '/lang' );

function aoa3f_custom_options_create_menu() {

	// Create new sublevel menu
	add_submenu_page( 'options-general.php', __('AO Options', 'aoa3f'), __('AO Options', 'aoa3f'), 'manage_options', 'aoa3f-custom-options', 'aoa3f_custom_options_page');

	// Call register options function
	add_action( 'admin_init', 'aoa3f_custom_options_register' );
}
add_action( 'admin_menu', 'aoa3f_custom_options_create_menu' );


function aoa3f_custom_options_register() {
	
	//register our settings
	register_setting( 'aoa3f-custom-options-group', 'bg_image_url' );
	register_setting( 'aoa3f-custom-options-group', 'logo_url' );
	register_setting( 'aoa3f-custom-options-group', 'small_logo_url' );
	register_setting( 'aoa3f-custom-options-group', 'social_share_logo_url' );
	register_setting( 'aoa3f-custom-options-group', 'main_bgcolor' ); //#B8D532
	register_setting( 'aoa3f-custom-options-group', 'second_bgcolor' ); //#58595B
	register_setting( 'aoa3f-custom-options-group', 'main_textcolor' );
	register_setting( 'aoa3f-custom-options-group', 'second_textcolor' );
	register_setting( 'aoa3f-custom-options-group', 'header_image' );
	register_setting( 'aoa3f-custom-options-group', 'news_image' );
	register_setting( 'aoa3f-custom-options-group', 'post_types' );
	register_setting( 'aoa3f-custom-options-group', 'contact_email' );
	register_setting( 'aoa3f-custom-options-group', 'contact_number' );
	register_setting( 'aoa3f-custom-options-group', 'footer_contacts' );
	register_setting( 'aoa3f-custom-options-group', 'address_lat' );
	register_setting( 'aoa3f-custom-options-group', 'address_lng' );
	register_setting( 'aoa3f-custom-options-group', 'map_icon' );
	register_setting( 'aoa3f-custom-options-group', 'group_url_fr' );
	register_setting( 'aoa3f-custom-options-group', 'group_url_en' );
	register_setting( 'aoa3f-custom-options-group', 'free_css' );
}


function aoa3f_custom_options_page() {
?>
<div class="wrap">
<h2><?php _e('AO Options', 'aoa3f'); ?></h2>

<form method="post" action="options.php" class="aoa3f-custom-options">
    <?php settings_fields( 'aoa3f-custom-options-group' ); ?>
    <?php do_settings_sections( 'aoa3f-custom-options-group' ); ?>
    <table class="form-table">

        <tr valign="top">
        <th scope="row"><?php _e('Background image (url)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="bg_image_url" value="<?php echo esc_attr( get_option('bg_image_url') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Logo (url)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="logo_url" value="<?php echo esc_attr( get_option('logo_url') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Small Logo (url)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="small_logo_url" value="<?php echo esc_attr( get_option('small_logo_url') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Social share default Logo (url)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="social_share_logo_url" value="<?php echo esc_attr( get_option('social_share_logo_url') ); ?>" /></td>
        </tr>
		
        <tr valign="top">
        <th scope="row"><?php _e('Main BG Color (hexa)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="main_bgcolor" value="<?php echo esc_attr( get_option('main_bgcolor') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Second BG Color (hexa)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="second_bgcolor" value="<?php echo esc_attr( get_option('second_bgcolor') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Main Text Color (hexa)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="main_textcolor" value="<?php echo esc_attr( get_option('main_textcolor') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php _e('Second Text Color (hexa)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="second_textcolor" value="<?php echo esc_attr( get_option('second_textcolor') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default header image', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="header_image" value="<?php echo esc_attr( get_option('header_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Default news image', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="news_image" value="<?php echo esc_attr( get_option('news_image') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Post types (coma separated)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="post_types" value="<?php echo esc_attr( get_option('post_types') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Contact email', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="contact_email" value="<?php echo esc_attr( get_option('contact_email') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Contact number', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="contact_number" value="<?php echo esc_attr( get_option('contact_number') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Contacts id for the footer (coma separated)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="footer_contacts" value="<?php echo esc_attr( get_option('footer_contacts') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Latitude de l\'adresse', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="address_lat" value="<?php echo esc_attr( get_option('address_lat') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Longitude de l\'adresse', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="address_lng" value="<?php echo esc_attr( get_option('address_lng') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Map icon', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="map_icon" value="<?php echo esc_attr( get_option('map_icon') ); ?>" /></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Page de réservation groupe FR (url)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="group_url_fr" value="<?php echo esc_attr( get_option('group_url_fr') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Page de réservation groupe EN (url)', 'aoa3f'); ?></th>
        <td><input type="text" size="70" name="group_url_en" value="<?php echo esc_attr( get_option('group_url_en') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row"><?php _e('Free CSS', 'aoa3f'); ?></th>
            <td><textarea type="text" size="70" name="free_css" rows="20" cols="60"><?php echo esc_attr( get_option('free_css') ); ?></textarea></td>
        </tr>

    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php 
}


function aoa3f_custom_options_css() {

    ?>
	<!-- AO Theme Options CSS --> 
	<style type="text/css">
        body {
            <?php if ($bg = get_option( 'bg_image_url' )) : ?>
                background-image: url(<?php echo $bg; ?>);
                background-repeat: no-repeat;
                background-size: cover;
            background-attachment: fixed;
            <?php endif; ?>
        }
	#map .overlay{
		background-image: url(<?php echo get_option( 'map_icon' ); ?>);
	}
    <?php
    /*
    .aologo {
		background: url("<?php echo get_option( 'logo_url' ); ?>") no-repeat;
	}
	.aomainbgcolor, #navbar .nav > li:hover, #nabar .dropdown-menu > li:hover > a {
		background: <?php echo get_option( 'main_bgcolor' ); ?> !important;
	}
	.aomaintextcolor, #navbar .nav > li:hover > a, #navbar .dropdown-menu > li:hover > a {
		color: <?php echo get_option( 'main_textcolor' ); ?> !important;
	}
	.aosecondbgcolor {
		background: <?php echo get_option( 'second_bgcolor' ); ?>;
	}
	.aosecondtextcolor {
		color: <?php echo get_option( 'second_textcolor' ); ?>;
	}
        
    a, a:hover {
        color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    #navbar .nav .dropdown-menu > li:hover > a {
    background: <?php echo get_option( 'main_bgcolor' ); ?> !important; 
    }
    .homepage .news .entry-meta .edit-link i.fa {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .not-homepage .entry-meta .edit-link i.fa {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    #content .event-pills > li.active > a, 
    #content .event-pills > li.active > a:hover, 
    #content .event-pills > li.active > a:focus {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .panel-heading {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?> !important;
    }
    #footer h3, #footer h4 {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .event-date {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') -45px -85px no-repeat; 
    }
    .sport .bookings .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') bottom right no-repeat;  
    }
    .sport #sport_subscriptions .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') bottom right no-repeat;  
    }
    .sport .club .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') bottom right no-repeat;  
    }
    .infrastructure .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') bottom right no-repeat;  
    }
    .sidebar-content .widget-ao-upcoming-events li a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget-ao-upcoming-events .event-date {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') -45px -85px no-repeat;
    }
    .sidebar-content .widget-ao-upcoming-events .btn-success {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > a:before {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > ul li a:hover {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_nav_menu ul.menu > li > ul li a:before {
    color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .panel-heading {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .wp-polls-ans .btn-success {
    background-color: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .sidebar-content .widget_polls-widget .wp-polls .pollbar {
    background: <?php echo get_option( 'main_bgcolor' ); ?>;
    }
    .infrastructure .contacts .btn-success {
    background: <?php echo get_option( 'main_bgcolor' ); ?> url('<?php echo get_template_directory_uri(); ?>/images/a3f-bg5.png') bottom right no-repeat;  
    }
    .gallery img {
    }
        
    @media (max-width: 992px) {

        .aomainbgcolor, #navbar .nav > li:hover, #nabar .dropdown-menu > li:hover > a,
        .aomaintextcolor, #navbar .nav > li:hover > a, #navbar .dropdown-menu > li:hover > a,
        #navbar .nav .dropdown-menu > li:hover > a {
        background-color: transparent !important;
        color: #333 !important;
        }
    }

    <?php echo get_option( 'free_css' ); ?>

    */
    ?>
        
	</style> 
	<!-- /AO Theme Options CSS -->
	<?php
}
add_action( 'wp_head' , 'aoa3f_custom_options_css' );



function aoa3f_custom_options_scripts() {
	?>
		<script type="text/javascript">
		var map;
		jQuery(document).ready(function(){"use strict";
		  map = new GMaps({
			scrollwheel: false,
			el: '#map',
			lat: <?php echo get_option( 'address_lat' ); ?>,
			lng: <?php echo get_option( 'address_lng' ); ?>,
			'zoom':13,
			//icon: '<?php echo get_option( 'map_icon' ); ?>'
			});
		  map.drawOverlay({
			lat: map.getCenter().lat(),
			lng: map.getCenter().lng(),
			layer: 'overlayLayer',
			content: '<div class="overlay"></div>',
			verticalAlign: 'bottom',
			horizontalAlign: 'center',
			//icon: '<?php echo get_option( 'map_icon' ); ?>'
		  });
		});
    </script>
    <?php
}
add_action( 'wp_head' , 'aoa3f_custom_options_scripts' );


/* End of file aoa3f-custom-options.php */
/* Location: ./wp-content/themes/aoa3f/inc/aoa3f-custom-options.php */