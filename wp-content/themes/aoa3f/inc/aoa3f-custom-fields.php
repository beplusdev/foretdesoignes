<?php
/** aoa3f-custom-fields.php
 *
 * AO Custom fields added with ACF
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aoa3f', get_template_directory() . '/lang' );


$icons = array (
    'add' => __('add', 'aoa3f'),
    'add-circular-button' => __('add-circular-button', 'aoa3f'),
    'arrow' => __('arrow', 'aoa3f'),
    'arrows' => __('arrows', 'aoa3f'),
    'arrows-1' => __('arrows-1', 'aoa3f'),
    'arrows-2' => __('arrows-2', 'aoa3f'),
    'bath' => __('bath', 'aoa3f'),
    'black' => __('black', 'aoa3f'),
    'business' => __('business', 'aoa3f'),
    'business-1' => __('business-1', 'aoa3f'),
    'calendar' => __('calendar', 'aoa3f'),
    'chain' => __('chain', 'aoa3f'),
    'check' => __('check', 'aoa3f'),
    'circle' => __('circle', 'aoa3f'),
    'circle-1' => __('circle-1', 'aoa3f'),
    'coins' => __('coins', 'aoa3f'),
    'computer' => __('computer', 'aoa3f'),
    'days' => __('days', 'aoa3f'),
    'drink' => __('drink', 'aoa3f'),
    'food' => __('food', 'aoa3f'),
    'food-1' => __('food-1', 'aoa3f'),
    'food-2' => __('food-2', 'aoa3f'),
    'food-3' => __('food-3', 'aoa3f'),
    'interface' => __('interface', 'aoa3f'),
    'interface-1' => __('interface-1', 'aoa3f'),
    'interface-2' => __('interface-2', 'aoa3f'),
    'logo' => __('logo', 'aoa3f'),
    'map-location' => __('map-location', 'aoa3f'),
    'mark' => __('mark', 'aoa3f'),
    'medical' => __('medical', 'aoa3f'),
    'minus-circular-button' => __('minus-circular-button', 'aoa3f'),
    'money' => __('money', 'aoa3f'),
    'money-1' => __('money-1', 'aoa3f'),
    'music' => __('music', 'aoa3f'),
    'nature' => __('nature', 'aoa3f'),
    'nature-1' => __('nature-1', 'aoa3f'),
    'nature-2' => __('nature-2', 'aoa3f'),
    'note' => __('note', 'aoa3f'),
    'people' => __('people', 'aoa3f'),
    'people-1' => __('people-1', 'aoa3f'),
    'people-2' => __('people-2', 'aoa3f'),
    'people-3' => __('people-3', 'aoa3f'),
    'people-4' => __('people-4', 'aoa3f'),
    'people-5' => __('people-5', 'aoa3f'),
    'people-6' => __('people-6', 'aoa3f'),
    'people-7' => __('people-7', 'aoa3f'),
    'people-8' => __('people-8', 'aoa3f'),
    'people-9' => __('people-9', 'aoa3f'),
    'person' => __('person', 'aoa3f'),
    'phone' => __('phone', 'aoa3f'),
    'rest' => __('rest', 'aoa3f'),
    'restaurant' => __('resaturant', 'aoa3f'),
    'rose' => __('rose', 'aoa3f'),
    'shape' => __('shape', 'aoa3f'),
    'shapes' => __('shapes', 'aoa3f'),
    'sign' => __('sign', 'aoa3f'),
    'signs' => __('signs', 'aoa3f'),
    'social' => __('social', 'aoa3f'),
    'social-1' => __('social-1', 'aoa3f'),
    'social-2' => __('social-2', 'aoa3f'),
    'social-3' => __('social-3', 'aoa3f'),
    'social-4' => __('social-4', 'aoa3f'),
    'sport' => __('sport', 'aoa3f'),
    'sports' => __('sports', 'aoa3f'),
    'student' => __('student', 'aoa3f'),
    'symbol' => __('symbol', 'aoa3f'),
    'tag' => __('tag', 'aoa3f'),
    'technology' => __('technology', 'aoa3f'),
    'technology-1' => __('technology-1', 'aoa3f'),
    'technology-2' => __('technology-2', 'aoa3f'),
    'technology-3' => __('technology-3', 'aoa3f'),
    'technology-4' => __('technology-4', 'aoa3f'),
    'technology-5' => __('technology-5', 'aoa3f'),
    'technology-6' => __('technology-6', 'aoa3f'),
    'technology-7' => __('technology-7', 'aoa3f'),
    'technology-8' => __('technology-8', 'aoa3f'),
    'technology-9' => __('technology-9', 'aoa3f'),
    'technology-10' => __('technology-10', 'aoa3f'),
    'technology-11' => __('technology-11', 'aoa3f'),
    'technology-12' => __('technology-12', 'aoa3f'),
    'three' => __('three', 'aoa3f'),
    'two' => __('two', 'aoa3f'),
    'utensil' => __('utensil', 'aoa3f'),
    'web' => __('web', 'aoa3f'),
    'web-1' => __('web-1', 'aoa3f'),
    'web-2' => __('web-2', 'aoa3f'),
    'web-3' => __('web-3', 'aoa3f'), 
    'woman' => __('woman', 'aoa3f'),
);


/**
 * Generate and manage specific fields (with ACF).
 *
 * @since 1.0.0
 *
 * @return void
 */ 
function aoa3f_custom_fields_load_acf()
{
    
    global $icons;
    
	if(function_exists("register_field_group"))
	{

        
        /*********************************************************************/
        /*   FOR NEWS                                                        */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_news',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_news_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'news_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'news',
                    ),
                ),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR PAGE && FEATURED POSTS                                      */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_page',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_page_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'page_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_feature(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_form(),
                        aoa3f_layout_button(),
                        aoa3f_layout_faq(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
            ),
            'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        acf_add_local_field_group(array (
            'key' => 'acf_post',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_post_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'post_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_feature(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_form(),
                        aoa3f_layout_button(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
            ),
            'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
            ),
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR EVENT                                                       */
		/*********************************************************************/
        
        //$term_event = get_term_by('slug', 'event', 'newscat');
		register_field_group(array (
			'id' => 'acf_event',
			'title' => __('Activity details','aoa3f'), // Détails sur l'activité
			'fields' => array (
				array (
					'key' => 'field_event_start',
					'label' => __('Start date and time','aoa3f'), // Date et heure de début
					'name' => 'event_start',
					'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'show_date' => 'true',
                    'date_format' => 'dd/mm/yy',
                    'time_format' => 'hh:mm',
                    'show_week_number' => 'false',
                    'picker' => 'select',
                    'save_as_timestamp' => 'true',
                    'get_as_timestamp' => 'true',
				),
				array (
					'key' => 'field_event_end',
					'label' => __('End date and time','aoa3f'), // Date et heure de fin
					'name' => 'event_end',
					'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'show_date' => 'true',
                    'date_format' => 'dd/mm/yy',
                    'time_format' => 'hh:mm',
                    'show_week_number' => 'false',
                    'picker' => 'select',
                    'save_as_timestamp' => 'true',
                    'get_as_timestamp' => 'true',
				),
                array (
                    'key' => 'field_event_location_flexible',
                    'label' => __('Location', 'aoa3f'), // 'Lieu',
                    'name' => 'event_location_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), //'Ajouter une élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        array (
                            'key' => 'field_event_location_infrastructure',
                            'name' => 'event_location_infrastructure',
                            'label' => __('Infrastructure', 'aoa3f'), // 'Infrastructure',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_event_location_infrastructure_repeater',
                                    'label' => '', 
                                    'name' => 'event_location_infrastructure_repeater',
                                    'type' => 'repeater',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'min' => '1',
                                    'max' => '',
                                    'layout' => 'block',
                                    'button_label' => __('Add an infrastructure', 'aoa3f'), // 'Ajouter une infrastructure',
                                    'sub_fields' => array (
                                        array (
                                            'key' => 'event_location_infrastructure_repeater_object',
                                            'label' => '', // 'Infrastructure',
                                            'name' => 'event_location_infrastructure_repeater_object',
                                            'type' => 'post_object',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => '',
                                                'id' => '',
                                            ),
                                            'post_type' => array (
                                                0 => 'infrastructure',
                                            ),
                                            'taxonomy' => array (
                                            ),
                                            'allow_null' => 0,
                                            'multiple' => 0,
                                            'return_format' => 'object',
                                            'ui' => 1,
                                        ),
                                    ),
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                        array (
                            'key' => 'field_event_location_address',
                            'name' => 'event_location_address',
                            'label' => __('Address', 'aoa3f'), // 'Adresse',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_event_location_address_text',
                                    'label' => '', //__('Address', 'aoa3f'), // 'Adresse', 
                                    'name' => 'event_location_address_text',
                                    'type' => 'textarea',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'maxlength' => '',
                                    'rows' => 3,
                                    'new_lines' => 'wpautop',
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                        array (
                            'key' => 'field_event_location_map',
                            'name' => 'event_location_map',
                            'label' => __('Map', 'aoa3f'), // 'Infrastructure',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_event_location_map_google',
                                    'label' => '', // 'Google Map',
                                    'name' => 'event_location_map_google',
                                    'type' => 'google_map',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'center_lat' => '50.809391',
                                    'center_lng' => '4.443800',
                                    'zoom' => '',
                                    'height' => '',
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                    ),
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type', //'taxonomy'
						'operator' => '==',
						'value' => 'news', //$term_event->term_id,
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'left',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));   

        
        /*********************************************************************/
        /*   FOR SPORT                                                       */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_sport',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                /*
                array (
                    'key' => 'acf_sport_prices',
                    'label' => __('Prices', 'aoa3f'), // 'Tarifs',
                    'name' => '',
                    'type' => 'message',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => __('Prices will be displayed here', 'aoa3f'), // 'Les tarifs apparaitront ici..',
                    'esc_html' => 0,
                ),
                */
                array (
                    'key' => 'field_sport_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'sport_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_contact(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'sport',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR CLUBS                                              */
		/*********************************************************************/
        
        acf_add_local_field_group(array (
            'key' => 'acf_club',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_club_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'club_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_schedule_table(),
                        array (
                            'key' => 'field_club_trainer',
                            'name' => 'club_trainer',
                            'label' => __('Trainer', 'aoa3f'), // 'Entraineur',
                            'display' => 'block',
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_club_trainer_image',
                                    'label' => __('Picture', 'aoa3f'), // 'Photo',
                                    'name' => 'club_trainer_image',
                                    'type' => 'image',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'array',
                                    'preview_size' => 'thumbnail',
                                    'library' => 'all',
                                    'min_width' => '',
                                    'min_height' => '',
                                    'min_size' => '',
                                    'max_width' => '',
                                    'max_height' => '',
                                    'max_size' => '',
                                    'mime_types' => '',
                                ),
                                array (
                                    'key' => 'field_club_trainer_name',
                                    'label' => __('Name to display', 'aoa3f'), // Nom à afficher
                                    'name' => 'club_trainer_name',
                                    'type' => 'text',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'html',
                                    'maxlength' => '',
                                ),
                                array (
                                    'key' => 'field_club_trainer_flexible',
                                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                                    'name' => 'club_trainer_flexible',
                                    'type' => 'flexible_content',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                                    'min' => '',
                                    'max' => '',
                                    'layouts' => array (
                                        //aoa3f_layout_section(),
                                        aoa3f_layout_address(),
                                        aoa3f_layout_phone(),
                                        aoa3f_layout_mobile(),
                                        aoa3f_layout_email(),
                                        aoa3f_layout_web(),
                                        aoa3f_layout_facebook(),
                                        aoa3f_layout_description(),
                                    ),
                                ),
                            ),
                            'min' => '',
                            'max' => '',
                        ),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'club',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
        
        
        acf_add_local_field_group(array (
            'key' => 'acf_club_infos',
            'title' => __('Club infos', 'aoa3f'), // 'Infos club', 
            'fields' => array (
                array (
                    'key' => 'field_club_infos_sport',
                    'label' => __('Sport', 'aoa3f'), // 'Discipline',
                    'name' => 'club_infos_sport',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array (
                        0 => 'sport',
                    ),
                    'taxonomy' => array (
                    ),
                    'allow_null' => 0,
                    'multiple' => 0,
                    'return_format' => 'id',
                    'ui' => 1,
                ),
                /*
                array (
                    'key' => 'field_club_infos_caption',
                    'label' => __('Caption', 'aoa3f'), // 'Légende',
                    'name' => 'club_infos_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_club_infos_logo',
                    'label' => __('Logo', 'aoa3f'),
                    'name' => 'club_infos_logo',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array (
                    'key' => 'field_club_infos_logo_caption',
                    'label' => __('Caption', 'aoa3f'), // 'Légende',
                    'name' => 'club_infos_logo_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                */
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'club',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR INFRASTRUCTURE                                              */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_infrastructure',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_infrastructure_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'infrastructure_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_features_table(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_contact(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'infrastructure',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR ROOM                                                        */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_room',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_room_images_repeater',
                    'label' => __('Pictures', 'aoa3f'), // 'Photos',
                    'name' => 'room_images_repeater',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '',
                    'max' => '',
                    'layout' => 'table',
                    'button_label' => __('Add a picture', 'aoa3f'), // 'Ajouter une photo',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_room_images_repeater_image',
                            'label' => __('Picture', 'aoa3f'), // 'Photo',
                            'name' => 'room_images_repeater_image',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array (
                            'key' => 'field_room_images_repeater_caption',
                            'label' => __('Caption', 'aoa3f'), // 'Légende',
                            'name' => 'room_images_repeater_caption',
                            'type' => 'textarea',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => 3,
                            'new_lines' => 'wpautop',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                    ),
                ),
                array (
                    'key' => 'field_room_flexible',
                    'label' => __('First part', 'aoa3f'),
                    'name' => 'room_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_features_table(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_contact(),
                        //aoa3f_layout_booking_button(),
                        aoa3f_layout_feature(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
                array (
                    'key' => 'field_room_flexible_bg',
                    'label' => __('Second part (with background)', 'aoa3f'),
                    'name' => 'room_flexible_bg',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_features_table(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_contact(),
                        //aoa3f_layout_booking_button(),
                        aoa3f_layout_feature(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'room',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*
		 * Header and thumbnails images 
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_room_information',
			'title' => __('Room information','aoa3f'),
			'fields' => array (
				array (
					'key' => 'field_room_price_from',
					'label' => __('Price from','aoa3f'), 
					'name' => 'room_price_from',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
                array (
                    'key' => 'field_room_features_repeater',
                    'label' => __('Features', 'aoa3f'), // 'Tarifs',
                    'name' => 'room_features_repeater',
                    'type' => 'repeater',
                    'instructions' => '<a href="' . get_template_directory_uri() . '/vendor/flaticon/font/flaticon.html" target="_blank">' . __('View samples', 'aoa3f') . '</a>',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '',
                    'max' => '',
                    'layout' => 'block',
                    'button_label' => __('Add a feature', 'aoa3f'), // 'Ajouter une caractéristique',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_room_feature_icon',
                            'label' => __('Icone', 'aoa3f'), 
                            'name' => 'room_feature_icon',
                            'type' => 'select',
                            'column_width' => '',
                            'instructions' => '',
                            'choices' => $icons,
                            'default_value' => '',
                            'allow_null' => 0,
                            'multiple' => 0,
                        ),
                        array (
                            'key' => 'field_room_feature_text',
                            'label' => __('Text', 'aoa3f'),
                            'name' => 'room_feature_text',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '', 
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                    ),
                ),
                array (
                    'key' => 'field_room_number_guests',
                    'label' => __('Number of guests', 'aoa3f'), 
                    'name' => 'room_number_guests',
                    'type' => 'select',
                    'column_width' => '',
                    'instructions' => '',
                    'choices' => array (
                        '' => __('N/A', 'aoa3f'),
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                        '8' => '8',
                    ),
                    'default_value' => '',
                    'allow_null' => 0,
                    'multiple' => 0,
                ),
				array (
					'key' => 'field_room_button',
					'label' => __('"Book now" button','aoa3f'), 
					'name' => 'room_button',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => __('Book now', 'aoa3f'),
					'placeholder' => '', 
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_room_button_link',
					'label' => __('"Book now" link','aoa3f'), 
					'name' => 'room_button_link',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => 'https://booking.cubilis.eu/select_dates.aspx?logisid=3351&booktemplate=1&lang=fr',
					'placeholder' => '', 
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'room',
						'order_no' => 1, 
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));

        
        
        /*********************************************************************/
        /*   FOR MEETING                                                     */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_meeting',
            'title' => __('Additional information', 'aoa3f'), // 'Information supplémentaire',
            'fields' => array (
                array (
                    'key' => 'field_meeting_images_repeater',
                    'label' => __('Pictures', 'aoa3f'), // 'Photos',
                    'name' => 'meeting_images_repeater',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '',
                    'max' => '',
                    'layout' => 'table',
                    'button_label' => __('Add a picture', 'aoa3f'), // 'Ajouter une photo',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_meeting_images_repeater_image',
                            'label' => __('Picture', 'aoa3f'), // 'Photo',
                            'name' => 'meeting_images_repeater_image',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array (
                            'key' => 'field_meeting_images_repeater_caption',
                            'label' => __('Caption', 'aoa3f'), // 'Légende',
                            'name' => 'meeting_images_repeater_caption',
                            'type' => 'textarea',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => 3,
                            'new_lines' => 'wpautop',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                    ),
                ),
                array (
                    'key' => 'field_meeting_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'meeting_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_features_table(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_contact(),
                        aoa3f_layout_feature(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
                array (
                    'key' => 'field_meeting_flexible_bg',
                    'label' => __('Second part (with background)', 'aoa3f'),
                    'name' => 'meeting_flexible_bg',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        aoa3f_layout_mainsection(),
                        aoa3f_layout_section(),
                        aoa3f_layout_subsection(),
                        aoa3f_layout_content(),
                        aoa3f_layout_images(),
                        aoa3f_layout_features_table(),
                        aoa3f_layout_prices(),
                        aoa3f_layout_contact(),
                        aoa3f_layout_feature(),
                        aoa3f_layout_2columns(),
                        aoa3f_layout_3columns(),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'meeting',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
       /*
		 * Header and thumbnails images 
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_meeting_information',
			'title' => __('Meeting room information','aoa3f'),
			'fields' => array (
				array (
					'key' => 'field_meeting_price_from',
					'label' => __('Price from','aoa3f'), 
					'name' => 'meeting_price_from',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
                array (
                    'key' => 'field_meeting_features_repeater',
                    'label' => __('Features', 'aoa3f'), // 'Tarifs',
                    'name' => 'meeting_features_repeater',
                    'type' => 'repeater',
                    'instructions' => '<a href="' . get_template_directory_uri() . '/vendor/flaticon/font/flaticon.html" target="_blank">' . __('View samples', 'aoa3f') . '</a>',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '',
                    'max' => '',
                    'layout' => 'block',
                    'button_label' => __('Add a feature', 'aoa3f'), // 'Ajouter une caractéristique',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_meeting_feature_icon',
                            'label' => __('Icone', 'aoa3f'), 
                            'name' => 'meeting_feature_icon',
                            'type' => 'select',
                            'column_width' => '',
                            'instructions' => '',
                            'choices' => $icons,
                            'default_value' => '',
                            'allow_null' => 0,
                            'multiple' => 0,
                        ),
                        array (
                            'key' => 'field_meeting_feature_text',
                            'label' => __('Text', 'aoa3f'),
                            'name' => 'meeting_feature_text',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '', 
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                    ),
                ),
                array (
                    'key' => 'field_meeting_number_guests',
                    'label' => __('Number of guests', 'aoa3f'), 
                    'name' => 'meeting_number_guests',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '', 
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
                ),
                array (
                    'key' => 'field_meeting_surface',
                    'label' => __('Surface', 'aoa3f'), 
                    'name' => 'meeting_surface',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '', 
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
                ),
				array (
					'key' => 'field_meeting_button',
					'label' => __('"Call to action" button','aoa3f'), 
					'name' => 'meeting_button',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => __('Book now', 'aoa3f'),
					'placeholder' => '', 
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_meeting_button_link',
					'label' => __('"Call to action" link','aoa3f'), 
					'name' => 'meeting_button_link',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '', 
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'meeting',
						'order_no' => 1, 
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
         
        
        
        /*********************************************************************/
        /*   FOR PRICE                                                       */
		/*********************************************************************/

        acf_add_local_field_group(array (
            'key' => 'acf_price',
            'title' => __('Prices list','aoa3f'), // 'Liste des tarifs',
            'fields' => array (
                array (
                    'key' => 'field_price_repeater',
                    'label' => '', //__('Prices list', 'aoa3f'), // 'Liste des tarifs',
                    'name' => 'price_repeater',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '1',
                    'max' => '',
                    'layout' => 'block',
                    'button_label' => __('Add a new price', 'aoa3f'), // 'Ajouter un nouveau tarif',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_price_publish_start_date',
                            'label' => __('Publish start date','aoa3f'), // 'Début de publication',
                            'name' => 'price_publish_start_date',
                            'type' => 'date_picker',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'display_format' => 'd/m/Y',
                            'return_format' => 'Ymd',
                            'first_day' => 1,
                        ),
                        array (
                            'key' => 'field_price_publish_end_date',
                            'label' => __('Publish end date','aoa3f'), // 'Fin de publication',
                            'name' => 'price_publish_end_date',
                            'type' => 'date_picker',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'display_format' => 'd/m/Y',
                            'return_format' => 'Ymd',
                            'first_day' => 1,
                        ),
                        array (
                            'key' => 'field_price_title',
                            'label' => __('Price title', 'aoa3f'), // 'Titre',
                            'name' => 'price_title',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),  
                        array (
                            'key' => 'field_price_flexible',
                            'label' => _x('Price', 'singular','aoa3f'), // 'Tarif',
                            'name' => 'price_flexible',
                            'type' => 'flexible_content',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                            'min' => '',
                            'max' => '',
                            'layouts' => array (
                                aoa3f_layout_mainsection(),
                                aoa3f_layout_section(),
                                aoa3f_layout_subsection(),
                                aoa3f_layout_content(),
                                aoa3f_layout_images(),
                                array (
                                    'key' => 'field_prices_table',
                                    'name' => 'prices_table',
                                    'label' => __('Prices table', 'aoa3f'), // 'Tableau de tarifs',
                                    'display' => 'block',
                                    'sub_fields' => array (
                                        array (
                                            'key' => 'field_prices_table_repeater',
                                            'label' => __('Prices', 'aoa3f'), // 'Tarifs',
                                            'name' => 'prices_table_repeater',
                                            'type' => 'repeater',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => '',
                                                'id' => '',
                                            ),
                                            'min' => '',
                                            'max' => '',
                                            'layout' => 'table',
                                            'button_label' => __('Add a price', 'aoa3f'), // 'Ajouter un tarif',
                                            'sub_fields' => array (
                                                array (
                                                    'key' => 'field_prices_table_repeater_description',
                                                    'label' => __('Description', 'aoa3f'), // 'Description',
                                                    'name' => 'prices_table_repeater_description',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => 60,
                                                        'class' => '',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                                array (
                                                    'key' => 'field_prices_table_repeater_price1',
                                                    'label' => __('Price 1', 'aoa3f'),
                                                    'name' => 'prices_table_repeater_price1',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => 20,
                                                        'class' => '',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                                array (
                                                    'key' => 'field_prices_table_repeater_price2',
                                                    'label' => __('Price 2', 'aoa3f'),
                                                    'name' => 'prices_table_repeater_price2',
                                                    'type' => 'text',
                                                    'instructions' => '',
                                                    'required' => 0,
                                                    'conditional_logic' => 0,
                                                    'wrapper' => array (
                                                        'width' => '',
                                                        'class' => '',
                                                        'id' => '',
                                                    ),
                                                    'default_value' => '',
                                                    'placeholder' => '',
                                                    'prepend' => '',
                                                    'append' => '',
                                                    'maxlength' => '',
                                                    'readonly' => 0,
                                                    'disabled' => 0,
                                                ),
                                            ),
                                        ),
                                        array (
                                            'key' => 'field_prices_teable_description_label',
                                            'label' => __('Description label', 'aoa3f'), // 'Titre',
                                            'name' => 'prices_teable_description_label',
                                            'type' => 'text',
                                            'instructions' => '',
                                            'required' => 0,
                                            'conditional_logic' => 0,
                                            'wrapper' => array (
                                                'width' => '',
                                                'class' => '',
                                                'id' => '',
                                            ),
                                            'default_value' => '',
                                            'placeholder' => __('Description', 'aoa3f'),
                                            'prepend' => '',
                                            'append' => '',
                                            'maxlength' => '',
                                            'readonly' => 0,
                                            'disabled' => 0,
                                        ),  
                                    ),
                                    'min' => '',
                                    'max' => '',
                                ),
								aoa3f_layout_equipment_table(),
                                aoa3f_layout_tab(),
                                aoa3f_layout_tabs_end(),
                            ),
                        ),
                    ),
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'price',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

                             
        acf_add_local_field_group(array (
            'key' => 'acf_price_captions',
            'title' => __('Price captions', 'aoa3f'), // 'Légendes des tarifs',
            'fields' => array (
                array (
                    'key' => 'field_price1_label',
                    'label' => __('Price 1 label', 'aoa3f'),
                    'name' => 'price1_label',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price1_label' ),
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_price1_caption',
                    'label' => __('Price 1 caption', 'aoa3f'),
                    'name' => 'price1_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price1_caption' ), // Jours ouvrables avant 17h
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_price2_label',
                    'label' => __('Price 2 label', 'aoa3f'),
                    'name' => 'price2_label',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price2_label' ),
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_price2_caption',
                    'label' => __('Price 2 caption', 'aoa3f'),
                    'name' => 'price2_caption',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => get_option( 'price2_caption' ), // Toutes les heures d'ouvertures
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'price',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));

        
        /*********************************************************************/
        /*   FOR TEAMS                                                       */
		/*********************************************************************/

        /**
		 * Team
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		 register_field_group(array (
            'id' => 'acf_team',
            'title' => __('Team composition', 'aoa3f'), // Composition de l'équipe
            'fields' => array (
                array (
                    'key' => 'field_team_contacts_nbcolumns',
                    'label' => __('Number of columns to display', 'aoa3f'), // Type d'information
                    'name' => 'team_contacts_nbcolumns',
                    'type' => 'select',
                    'column_width' => '',
                    'choices' => array (
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '6' => '6',
                    ),
                    'default_value' => '1',
                    'allow_null' => 0,
                    'multiple' => 0,
                ),
                array (
                    'key' => 'field_team_contacts_list',
                    'label' => __('Contacts list', 'aoa3f'), // Liste des membres de l'équipe
                    'name' => 'team_contacts_list',
                    'type' => 'repeater',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_team_contacts_list_existing_contact',
                            'label' => __('Contact already recorded', 'aoa3f'), // Contact déjà encodé
                            'name' => 'team_contacts_list_existing_contact',
                            'type' => 'true_false',
                            'column_width' => '',
                            'message' => '',
                            'default_value' => 0,
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact',
                            'label' => __('Contact', 'aoa3f'), // Contact
                            'name' => 'team_contacts_list_contact',
                            'type' => 'post_object',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '==',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'column_width' => '',
                            'post_type' => array (
                                0 => 'contact',
                            ),
							'taxonomy' => array (
								0 => 'all',
							),
                            'allow_null' => 0,
                            'multiple' => 0,
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_name',
                            'label' => __('Name to display', 'aoa3f'), // Nom à afficher
                            'name' => 'team_contacts_list_contact_name',
                            'type' => 'text',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_title',
                            'label' => __('Title', 'aoa3f'), // Titre
                            'name' => 'team_contacts_list_contact_title',
                            'type' => 'text',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_address',
                            'label' => __('Address', 'aoa3f'), // Adresse
                            'name' => 'team_contacts_list_contact_address',
                            'type' => 'textarea',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => '',
                            'formatting' => 'br',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_infos',
                            'label' => __('Contact infos', 'aoa3f'), // Informations du contact
                            'name' => 'team_contacts_list_contact_infos',
                            'type' => 'repeater',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_team_contacts_list_contact_infos_type',
                                    'label' => __('Information type', 'aoa3f'), // Type d'information
                                    'name' => 'contact_infos_type',
                                    'type' => 'select',
                                    'column_width' => '',
                                    'choices' => array (
                                        'phone' => __('Phone', 'aoa3f'), // Téléphone
                                        'fax' => __('Fax', 'aoa3f'), // Fax
                                        'mobile' => __('Mobile', 'aoa3f'), // GSM
                                        'email' => __('Email', 'aoa3f'), // E-mail
                                        'webpage' => __('Webpage', 'aoa3f'), // Page web
                                    ),
                                    'default_value' => 'phone',
                                    'allow_null' => 0,
                                    'multiple' => 0,
                                ),
                                array (
                                    'key' => 'field_team_contacts_list_contact_infos_value',
                                    'label' => __('Value', 'aoa3f'), // valeur
                                    'name' => 'contact_infos_value',
                                    'type' => 'text',
                                    'column_width' => '',
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'formatting' => 'html',
                                    'maxlength' => '',
                                ),
                            ),
                            'row_min' => '',
                            'row_limit' => '',
                            'layout' => 'table',
                            'button_label' => __('Add an information', 'aoa3f'), // Ajouter une information
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_motto',
                            'label' => __('Motto', 'aoa3f'), // Devise
                            'name' => 'team_contacts_list_contact_motto',
                            'type' => 'textarea',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'column_width' => '',
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => '',
                            'formatting' => 'br',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_image',
                            'label' => __('Picture', 'aoa3f'), // Photo
                            'name' => 'team_contacts_list_contact_image',
                            'type' => 'image',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'column_width' => '',
                            'save_format' => 'object',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                        ),
                        array (
                            'key' => 'field_team_contacts_list_contact_text',
                            'label' => __('Free text','aoa3f'), // Texte libre
                            'name' => 'team_contacts_list_contact_text',
                            'type' => 'wysiwyg',
                            'conditional_logic' => array (
                                'status' => 1,
                                'rules' => array (
                                    array (
                                        'field' => 'field_team_contacts_list_existing_contact',
                                        'operator' => '!=',
                                        'value' => '1',
                                    ),
                                ),
                                'allorany' => 'all',
                            ),
                            'default_value' => '',
                            'toolbar' => 'full',
                            'media_upload' => 'yes',
                        ),
                    ),
                    'row_min' => '',
                    'row_limit' => '',
                    'layout' => 'row',
                    'button_label' => __('Add a contact', 'aoa3f'), // Ajouter un membre
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'team',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));

        
        /*********************************************************************/
        /*   FOR CONTACT                                                     */
		/*********************************************************************/

        /**
		 * Contact
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		 register_field_group(array (
			'id' => 'acf_contact',
			'title' => __('Contact', 'aoa3f'), // Contact
			'fields' => array (
                array (
                    'key' => 'field_contact_image',
                    'label' => __('Picture', 'aoa3f'), // 'Photo',
                    'name' => 'contact_image',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'array',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
				array (
					'key' => 'field_contact_name',
					'label' => __('Name to display', 'aoa3f'), // Nom à afficher
					'name' => 'contact_name',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
                array (
                    'key' => 'field_contact_flexible',
                    'label' => '', //__('Additional information', 'aoa3f'), // 'Information supplémentaire',
                    'name' => 'contact_flexible',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                    'min' => '',
                    'max' => '',
                    'layouts' => array (
                        //aoa3f_layout_section(),
                        aoa3f_layout_address(),
                        aoa3f_layout_phone(),
                        aoa3f_layout_mobile(),
                        aoa3f_layout_email(),
                        aoa3f_layout_web(),
                        aoa3f_layout_motto(),
                        //aoa3f_layout_description(),
                    ),
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'contact',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => array (
                    0 => 'the_content',
				),
            'active' => 1,
            'description' => '',
		));
		
		

                             
        /*********************************************************************/
        /*   FOR SPORT AND INFRASTRUCTURE                                    */
		/*********************************************************************/

		/**
		 * Linked sports
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_sports',
			'title' => __('Linked sports','aoa3f'), // Disciplines liées
			'fields' => array (
				array (
					'key' => 'field_linked_sports_title',
					'label' => __('Title','aoa3f'), // Title
					'name' => 'linked_sports_title',
					'type' => 'text',
					'default_value' => __('Linked sports','aoa3f'), // Disciplines liées
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_sports_number',
					'label' => __('Number to display','aoa3f'), // Nombre à afficher
					'name' => 'linked_sports_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_sports_list',
					'label' => __('Sports list','aoa3f'), // Liste des sports liés
					'name' => 'linked_sports_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_sports_list_sport',
							'label' => __('Sport','aoa3f'), // Discipline
							'name' => 'linked_sports_list_sport',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'sport',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add a sport','aoa3f'), // Ajouter une discipline
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 2,
		));


        /**
		 * Linked infrastructure
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_infrastructures',
			'title' => __('Linked infrastructures','aoa3f'), // Infrastructures liée
			'fields' => array (
				array (
					'key' => 'field_linked_infrastructures_title',
					'label' => __('Title','aoa3f'), // Titre
					'name' => 'linked_infrastructures_title',
					'type' => 'text',
					'default_value' => __('Linked infrastructures','aoa3f'), // Infrastructures liée
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_infrastructures_number',
					'label' => __('Number to display','aoa3f'), // Nombre à afficher
					'name' => 'linked_infrastructures_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_infrastructures_list',
					'label' => __('List','aoa3f'), // Liste
					'name' => 'linked_infrastructures_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_infrastructures_list_infrastructure',
							'label' => __('Infrastructure','aoa3f'), // Infrastructure
							'name' => 'linked_infrastructures_list_infrastructure',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'infrastructure',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add an infrastructure','aoa3f'), // Ajouter une infrastructure
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'club',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 3,
		));


                             
        /*********************************************************************/
        /*   FOR SPORT AND INFRASTRUCTURE                                    */
		/*********************************************************************/

		/**
		 * Linked sports
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_sports',
			'title' => __('Linked sports','aoa3f'), // Disciplines liées
			'fields' => array (
				array (
					'key' => 'field_linked_sports_title',
					'label' => __('Title','aoa3f'), // Title
					'name' => 'linked_sports_title',
					'type' => 'text',
					'default_value' => __('Linked sports','aoa3f'), // Disciplines liées
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_sports_number',
					'label' => __('Number to display','aoa3f'), // Nombre à afficher
					'name' => 'linked_sports_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_sports_list',
					'label' => __('Sports list','aoa3f'), // Liste des sports liés
					'name' => 'linked_sports_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_sports_list_sport',
							'label' => __('Sport','aoa3f'), // Discipline
							'name' => 'linked_sports_list_sport',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'sport',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add a sport','aoa3f'), // Ajouter une discipline
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 2,
		));


        /*********************************************************************/
        /*   FOR SPORT                                                       */
		/*********************************************************************/

		/**
		 * Linked clubs
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_linked_clubs',
			'title' => __('Linked clubs','aoa3f'), // Disciplines liées
			'fields' => array (
				array (
					'key' => 'field_linked_clubs_title',
					'label' => __('Title','aoa3f'), // Title
					'name' => 'linked_clubs_title',
					'type' => 'text',
					'default_value' => __('Linked clubs','aoa3f'), // Disciplines liées
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_linked_clubs_number',
					'label' => __('Number to display','aoa3f'), // Nombre à afficher
					'name' => 'linked_clubs_number',
					'type' => 'select',
					'choices' => array (
						0 => 'Tous',
						1 => 1,
						2 => 2,
                        3 => 3,
						4 => 4,
						5 => 5,
						6 => 6,
						7 => 7,
						8 => 8,
						9 => 9,
					),
					'default_value' => 0,
					'allow_null' => 0,
					'multiple' => 0,
				),
				array (
					'key' => 'field_linked_clubs_list',
					'label' => __('Clubs list','aoa3f'), // Liste des clubs liés
					'name' => 'linked_clubs_list',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_linked_clubs_list_sport',
							'label' => __('Club','aoa3f'), // Discipline
							'name' => 'linked_clubs_list_club',
							'type' => 'post_object',
							'column_width' => '',
							'post_type' => array (
								0 => 'club',
							),
							'taxonomy' => array (
								0 => 'all',
							),
							'allow_null' => 1,
							'multiple' => 0,
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => __('Add a club','aoa3f'), // Ajouter un club
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 2,
		));


		/**
		 * Linked clubs
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_sport_parent',
			'title' => __('Sport parent','aoa3f'), // Disciplines parent
			'fields' => array (
                array (
                    'key' => 'field_sport_parent',
                    'label' => '', //__('Prices', 'aoa3f'), // 'Contact',
                    'name' => 'sport_parent',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'post_type' => array (
                        0 => 'sport',
                    ),
                    'taxonomy' => array (
                    ),
                    'allow_null' => 1,
                    'multiple' => 0,
                    'return_format' => 'id',
                    'ui' => 1,
                ),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 1,
		));


        /*********************************************************************/
        /*   FOR NEWS, SPORT, INFRASTRUCTURE, ROOM, PAGE AND FEATURED POST   */
		/*********************************************************************/
        
        /*
		 * Header and thumbnails images 
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_images',
			'title' => __('Images and attributes','aoa3f'), // Images et attributs
			'fields' => array (
				array (
					'key' => 'field_images_header',
					'label' => __('Header image','aoa3f'), // Image d'en-tête
					'name' => 'images_header',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail',
					'label' => __('Thumbnail','aoa3f'), // Image miniature
					'name' => 'images_thumbnail',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail_name',
					'label' => __('Short name for thumbnail','aoa3f'), // Nom abrégé pour la miniature
					'name' => 'images_thumbnail_name',
					'type' => 'text',
                    'instructions' => __('If empty, the title will be used', 'aoa3f'), // Si vide, le titre sera utilisé
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'news',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 3,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'club',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'meeting',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'team',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'room',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));

		register_field_group(array (
			'id' => 'acf_images_infra',
			'title' => __('Images and attributes','aoa3f'), // Images et attributs
			'fields' => array (
				array (
					'key' => 'field_images_header',
					'label' => __('Header image','aoa3f'), // Image d'en-tête
					'name' => 'images_header',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_map',
					'label' => __('Map','aoa3f'), // Image miniature
					'name' => 'images_map',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail',
					'label' => __('Thumbnail','aoa3f'), // Image miniature
					'name' => 'images_thumbnail',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_images_thumbnail_name',
					'label' => __('Short name for thumbnail','aoa3f'), // Nom abrégé pour la miniature
					'name' => 'images_thumbnail_name',
					'type' => 'text',
                    'instructions' => __('If empty, the title will be used', 'aoa3f'), // Si vide, le titre sera utilisé
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_images_icon',
					'label' => __('Icon','aoa3f'), // Icône
					'name' => 'images_icon',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_direct_contact_label',
					'label' => __('Direct contact label','aoa3f'), // Icône
					'name' => 'direct_contact_label',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => __('Phone number', 'aoa3f'), // Téléphone
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_direct_contact_number',
					'label' => __('Direct contact number','aoa3f'), // Icône
					'name' => 'direct_contact_number',
					'type' => 'text',
                    'instructions' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
        
        /**
		 * Share buttons
		 *
		 * @since 	1.0.0
		 * @return 	void
		 */
		register_field_group(array (
			'id' => 'acf_social_share',
			'title' => __('Social network', 'aoa3f'), // Réseaux sociaux
			'fields' => array (
				array (
					'key' => 'field_social_share',
					'label' => __('Social share', 'aoa3f'), // Partage sur les réseaux sociaux
					'name' => 'social_share',
					'type' => 'true_false',
					'message' => __('Display share buttons', 'aoa3f'), // Afficher les boutons de partage
					'default_value' => 1,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'news',
						'order_no' => 0,
						'group_no' => 2,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sport',
						'order_no' => 0,
						'group_no' => 3,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'club',
						'order_no' => 0,
						'group_no' => 4,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'infrastructure',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'team',
						'order_no' => 0,
						'group_no' => 5,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 4,
		));
        
	}
}
add_action('init', 'aoa3f_custom_fields_load_acf');





function aoa3f_layout_mainsection() {
    
    $layout = array (
        'key' => 'field_mainsection',
        'name' => 'mainsection',
        'label' => __('Main section title (Title 1)', 'aoa3f'), // 'Section principale',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_mainsection_title',
                'label' => '', //__('Section title', 'aoa3f'), // 'Titre de la section',
                'name' => 'mainsection_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_section() {
    
    $layout = array (
        'key' => 'field_section',
        'name' => 'section',
        'label' => __('Section title (Title 2)', 'aoa3f'), // 'Section',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_section_title',
                'label' => '', //__('Section title', 'aoa3f'), // 'Titre de la section',
                'name' => 'section_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_subsection() {

     $layout = array (
        'key' => 'field_subsection',
        'name' => 'subsection',
        'label' => __('Subsection title (Title 3)', 'aoa3f'), // 'Sous-section',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_subsection_title',
                'label' => '', //__('Subsection title', 'aoa3f'), // 'Titre de la sous-section',
                'name' => 'subsection_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_content() {
    
    $layout = array (
        'key' => 'field_content',
        'name' => 'content',
        'label' => __('Content', 'aoa3f'), // 'Texte',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_content_text',
                'label' => '', //__('Content text', 'aoa3f'), // 'Texte du contenu',
                'name' => 'content_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_images() {

    $layout = array (
        'key' => 'field_images',
        'name' => 'images',
        'label' => __('Pictures', 'aoa3f'), // 'Photos',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_images_repeater',
                'label' => '', //__('Pictures', 'aoa3f'), // 'Photos',
                'name' => 'images_repeater',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'min' => '',
                'max' => '',
                'layout' => 'table',
                'button_label' => __('Add a picture', 'aoa3f'), // 'Ajouter une photo',
                'sub_fields' => array (
                    array (
                        'key' => 'field_images_repeater_image',
                        'label' => __('Picture', 'aoa3f'), // 'Photo',
                        'name' => 'images_repeater_image',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'array',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array (
                        'key' => 'field_images_repeater_caption',
                        'label' => __('Caption', 'aoa3f'), // 'Légende',
                        'name' => 'images_repeater_caption',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'rows' => 3,
                        'new_lines' => 'wpautop',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
            ),
            array (
                'key' => 'field_images_options',
                'label' => __('Display options', 'aoa3f'), // 'Options d'affichage',
                'name' => 'images_options',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array (
                    'carousel' => __('Diaporama', 'aoa3f'), // 'Diaporama',
                    'gallery' => __('Gallery', 'aoa3f'), // 'Galerie',
                ),
                'default_value' => array (
                    'carousel' => 'carousel',
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
                'disabled' => 0,
                'readonly' => 0,
            ),
            array (
                'key' => 'field_images_columns',
                'label' => __('Columns', 'aoa3f'), 
                'name' => 'images_columns',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    ''  => __('Auto', 'aoa3f'),
                    '1' => __('1', 'aoa3f'),
                    '2' => __('2', 'aoa3f'),
                    '3' => __('3', 'aoa3f'),
                    '4' => __('4', 'aoa3f'),
                    '6' => __('6', 'aoa3f'),
                    '12' => __('12', 'aoa3f'),
                ),
                'default_value' => '',
                'allow_null' => 0,
                'multiple' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_prices() {
    
    $layout = array (
        'key' => 'field_prices',
        'name' => 'prices',
        'label' => __('Link prices', 'aoa3f'), // 'Tarifs',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_prices_object',
                'label' => '', //__('Prices', 'aoa3f'), // 'Contact',
                'name' => 'prices_object',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'price',
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_equipment_table() {

    $layout = array (
        'key' => 'field_equipment_table',
        'name' => 'equipment_table',
        'label' => __('Equipment table', 'aoa3f'), // 'Location de matériel',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_equipment_table_repeater',
                'label' => '', //__('Equipment rentals table', 'aoa3f'), // 'Location de matériel',
                'name' => 'equipment_table_repeater',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_equipment_table_repeater_element',
                        'label' => __('Equipment', 'aoa3f'), // 'Matériel',
                        'name' => 'equipment_table_repeater_element',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '70',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_equipment_table_repeater_price',
                        'label' => __('Price', 'aoa3f'), //'Prix',
                        'name' => 'equipment_table_repeater_price',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '30',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
                'row_min' => 1,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => __('Add an equipment', 'aoa3f'), // Ajouter un matériel
            ),
            array (
                'key' => 'field_equipment_table_element_label',
                'label' => __('Equipment column label', 'aoa3f'), // 'Titre',
                'name' => 'equipment_table_element_label',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => __('Equipment', 'aoa3f'),
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),  
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_features_table() {

    $layout = array (
        'key' => 'field_features_table',
        'name' => 'features_table',
        'label' => __('Features table', 'aoa3f'), // 'Caractéristique',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_features_table_repeater',
                'label' => '', 
                'name' => 'features_table_repeater',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_features_table_repeater_element',
                        'label' => __('Feature', 'aoa3f'), // 'Caractéristique',
                        'name' => 'features_table_repeater_element',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '70',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_features_table_repeater_value',
                        'label' => __('Value', 'aoa3f'), //'Valeur',
                        'name' => 'features_table_repeater_value',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '30',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
                'row_min' => 1,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => __('Add a feature', 'aoa3f'), // Ajouter une caractéristique
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_schedule_table() {

    $layout = array (
        'key' => 'field_schedule_table',
        'name' => 'schedule_table',
        'label' => __('Schedule table', 'aoa3f'), // 'Horaires',
        'display' => 'block',
        'sub_fields' => array (
            array(
                'key' => 'field_schedule_table_repeater',
                'label' => '', //__('Schedule','aoa3f'), // 'Horaires',
                'name' => 'schedule_table_repeater',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'schedule_table_repeater_day',
                        'label' => __('Day','aoa3f'), // Jour
                        'name' => 'schedule_table_repeater_day',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_time',
                        'label' => __('Time','aoa3f'), // Horaire
                        'name' => 'schedule_table_repeater_time',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_infrastructure',
                        'label' => __('Infrastructure','aoa3f'), // Salle
                        'name' => 'schedule_table_repeater_infrastructure',
                        'type' => 'post_object',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'post_type' => array (
                            0 => 'infrastructure',
                        ),
                        'taxonomy' => array (
                        ),
                        'allow_null' => 0,
                        'multiple' => 0,
                        'return_format' => 'object',
                        'ui' => 1,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_type',
                        'label' => __('Type','aoa3f'), // Type
                        'name' => 'schedule_table_repeater_type',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_level',
                        'label' => __('Level','aoa3f'), // Niveau
                        'name' => 'schedule_table_repeater_level',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '30',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                    array (
                        'key' => 'field_schedule_table_repeater_age',
                        'label' => __('Age','aoa3f'), // Age
                        'name' => 'schedule_table_repeater_age',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                        'readonly' => 0,
                        'disabled' => 0,
                    ),
                ),
                'row_min' => 0,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => __('Add a period','aoa3f'), // Ajouter une période
            ),
        ),
    );

    return $layout;
}


function aoa3f_layout_address() {

     $layout = array (
        'key' => 'field_address',
        'name' => 'address',
        'label' => __('Address', 'aoa3f'), // 'Adresse',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_address_text',
                'label' => '', //__('Address', 'aoa3f'), // 'Address',
                'name' => 'address_text',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 3,
                'new_lines' => 'wpautop',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_phone() {

     $layout = array (
        'key' => 'field_phone',
        'name' => 'phone',
        'label' => __('Phone', 'aoa3f'), // 'Téléphone',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_phone_text',
                'label' => '', //__('Phone', 'aoa3f'), // 'Téléphone',
                'name' => 'phone_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_mobile() {

     $layout = array (
        'key' => 'field_mobile',
        'name' => 'mobile',
        'label' => __('Mobile', 'aoa3f'), // 'Portable',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_mobile_text',
                'label' => '', //__('Mobile', 'aoa3f'), // 'Portable',
                'name' => 'mobile_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_email() {

     $layout = array (
        'key' => 'field_email',
        'name' => 'email',
        'label' => __('Email', 'aoa3f'), // 'Email',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_email_text',
                'label' => '', //__('Email', 'aoa3f'), // 'email',
                'name' => 'email_text',
                'type' => 'email',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_web() {

     $layout = array (
        'key' => 'field_web',
        'name' => 'web',
        'label' => __('Web page', 'aoa3f'), // 'Page web',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_web_text',
                'label' => '', //__('Web page', 'aoa3f'), // 'Page web',
                'name' => 'web_text',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_facebook() {

     $layout = array (
        'key' => 'field_facebook',
        'name' => 'facebook',
        'label' => __('Facebook page', 'aoa3f'), // 'Page Facebook',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_facebook_text',
                'label' => '', //__('Facebook page', 'aoa3f'), // 'Page Facebook',
                'name' => 'facebook_text',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_motto() {

     $layout = array (
        'key' => 'field_motto',
        'name' => 'motto',
        'label' => __('Motto', 'aoa3f'), // 'Devise',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_motto_text',
                'label' => '', 
                'name' => 'motto_text',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 3,
                'new_lines' => 'wpautop',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_description() {

     $layout = array (
        'key' => 'field_description',
        'name' => 'description',
        'label' => __('Description', 'aoa3f'), // 'Description',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_description_text',
                'label' => '', //__('Description', 'aoa3f'), // 'Description',
                'name' => 'description_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_contact() {
    
    $layout = array (
        'key' => 'field_contact',
        'name' => 'contact',
        'label' => __('Link a contact', 'aoa3f'), // 'Contact',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_contact_object',
                'label' => '', //__('Contact', 'aoa3f'), // 'Contact',
                'name' => 'contact_object',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array (
                    0 => 'contact',
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_tab() {
    
    $layout = array (
        'key' => 'field_tab',
        'name' => 'tab',
        'label' => __('Tab', 'aoa3f'), // 'Contact',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_tab_title',
                'label' => __('Tab title', 'aoa3f'), 
                'name' => 'tab_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),  
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_tabs_end() {
    
    $layout = array (
        'key' => 'field_tabs_end',
        'name' => 'tabs_end',
        'label' => __('Tabs end', 'aoa3f'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'acf_tabs_end_message',
                'label' => '', //__('Tabs end', 'aoa3f'), 
                'name' => 'tabs_end_message',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => __('This element indicates the end of tabs contents', 'aoa3f'), 
                'esc_html' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}


function aoa3f_layout_form() {
    
    $layout = array (
        'key' => 'field_form',
        'name' => 'form',
        'label' => __('Form', 'aoa3f'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_form_transition',
                'label' => __('Form transition effect', 'aoa3f'), 
                'name' => 'form_transition',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    ''          => __('No transaction', 'aoa3f'),
                    'slide'     => __('Slide', 'aoa3f'),
                    'fade'      => __('Fade', 'aoa3f'),
                ),
                'default_value' => 'text',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_horizontal',
                'label' => __('Horizontal form', 'aoa3f'), 
                'name' => 'form_horizontal',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    '0'     => __('No', 'aoa3f'),
                    '2'     => __('Yes - | 2 | 10 |', 'aoa3f'),
                    '3'     => __('Yes - | 3 | 9 |', 'aoa3f'),
                    '4'     => __('Yes - | 4 | 8 |', 'aoa3f'),
                    '5'     => __('Yes - | 5 | 7 |', 'aoa3f'),
                    '6'     => __('Yes - | 6 | 6 |', 'aoa3f'),
                ),
                'default_value' => 'text',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_with_pager',
                'label' => __('With pager', 'aoa3f'), 
                'name' => 'form_with_pager',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    '0'     => __('No', 'aoa3f'),
                    '1'     => __('Yes', 'aoa3f'),
                ),
                'default_value' => 'text',
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_transition',
                            'operator' => '!=',
                            'value' => '',
                        ),
                    ),
                ),
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_fields',
                'label' => __('Form fields', 'aoa3f'), 
                'name' => 'form_fields',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add a field', 'aoa3f'), 
                'min' => '',
                'max' => '',
                'layouts' => array (
                    //aoa3f_layout_form_field(),
                    aoa3f_layout_form_section(),
                    //aoa3f_layout_form_subsection(),
                    //aoa3f_layout_form_description(),
                ),
            ),
            array (
                'key' => 'field_form_id',
                'label' => __('Form id', 'aoa3f'), 
                'name' => 'form_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_submit_button_title',
                'label' => __('Submit button', 'aoa3f'), 
                'name' => 'form_submit_button_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_transition',
                            'operator' => '==',
                            'value' => '',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => __('Submit', 'aoa3f'),
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_sender',
                'label' => __('Email sender', 'aoa3f'), 
                'name' => 'form_sender',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_recipient',
                'label' => __('Email recipient ', 'aoa3f'), 
                'name' => 'form_recipient',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_subject',
                'label' => __('Email subject ', 'aoa3f'), 
                'name' => 'form_subject',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_success',
                'label' => __('Success message', 'aoa3f'), 
                'name' => 'form_success',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_css',
                'label' => __('Free CSS', 'aoa3f'),  
                'name' => 'form_css',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 15,
                'new_lines' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_javascript',
                'label' => __('Free javascript', 'aoa3f'),  
                'name' => 'form_javascript',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 15,
                'new_lines' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_form_field() {

     $layout = array (
        'key' => 'field_form_field',
        'name' => 'form_field',
        'label' => __('Form field', 'aoa3f'),
        'display' => 'row',
        'sub_fields' => array (
            array (
                'key' => 'field_form_field_type',
                'label' => __('Field type', 'aoa3f'), 
                'name' => 'form_field_type',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    'text'          => __('Text', 'aoa3f'),
                    'email'         => __('Email', 'aoa3f'),
                    'number'        => __('Number', 'aoa3f'),
                    'select'        => __('Select box', 'aoa3f'),
                    'datepicker'    => __('Datepicker', 'aoa3f'),
                    'range'         => __('Datepicker range', 'aoa3f'),
                    'textarea'      => __('Textarea', 'aoa3f'),
                ),
                'default_value' => 'text',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_field_title',
                'label' => __('Field title', 'aoa3f'), 
                'name' => 'form_field_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_select_values',
                'label' => __('Values', 'aoa3f'),  
                'name' => 'form_field_select_values',
                'type' => 'textarea',
                'instructions' => 'Séparez les valeurs par un ";"',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_field_type',
                            'operator' => '==',
                            'value' => 'select',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => 6,
                'new_lines' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_default_value',
                'label' => __('Default value', 'aoa3f'), 
                'name' => 'form_field_default_value',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_default_value2',
                'label' => __('Default value (second field)', 'aoa3f'), 
                'name' => 'form_field_default_value2',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_field_type',
                            'operator' => '==',
                            'value' => 'range',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_post_field_id',
                'label' => __('Post field id', 'aoa3f'), 
                'name' => 'form_field_post_field_id',
                'type' => 'text',
                'instructions' => __('This is the ID of the correspondant field from a calling form', 'aoa3f'),
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_post_field_id2',
                'label' => __('Post field id (second field)', 'aoa3f'), 
                'name' => 'form_field_post_field_id2',
                'type' => 'text',
                'instructions' => __('If the page is called from another form. This is the ID of a correspondant field from the calling form', 'aoa3f'),
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_field_type',
                            'operator' => '==',
                            'value' => 'range',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_id',
                'label' => __('Field id', 'aoa3f'), 
                'name' => 'form_field_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_mandatory',
                'label' => __('Mandatory ?', 'aoa3f'), 
                'name' => 'form_field_mandatory',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    '1' => __('Yes', 'aoa3f'),
                    '0' => __('No', 'aoa3f'),
                ),
                'default_value' => '1',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_field_datepicker_range_field_id',
                'label' => __('Range with field id', 'aoa3f'), 
                'name' => 'form_field_datepicker_range_field_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_field_type',
                            'operator' => '==',
                            'value' => 'datepicker',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_condition_operator',
                'label' => __('Display condition'), 
                'name' => 'form_field_condition_operator',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    '-'     => __('No condition', 'aoa3f'),
                    '=='     => __('Condition : field = value', 'aoa3f'),
                    '!='    => __('Condition : field != value', 'aoa3f'),
                    '>'     => __('Condition : field > value', 'aoa3f'),
                    '>='    => __('Condition : field >= value', 'aoa3f'),
                    '<'     => __('Condition : field <= value', 'aoa3f'),
                    '<='    => __('Condition : field <= value', 'aoa3f'),
                ),
                'default_value' => 'text',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_field_condition_field_id',
                'label' => __('Condition : field id', 'aoa3f'), 
                'name' => 'form_field_condition_field_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_field_condition_operator',
                            'operator' => '!=',
                            'value' => '-',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_field_condition_value',
                'label' => __('Condition : value', 'aoa3f'), 
                'name' => 'form_field_condition_value',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_field_condition_operator',
                            'operator' => '!=',
                            'value' => '-',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_form_section() {
    
    $layout = array (
        'key' => 'field_form_section',
        'name' => 'form_section',
        'label' => __('Form section', 'aoa3f'), // 'Section',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_form_section_title',
                'label' => __('Section title', 'aoa3f'), // 'Titre de la section',
                'name' => 'form_section_title',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_section_fields',
                'label' => __('Form fields', 'aoa3f'), 
                'name' => 'form_section_fields',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add a field', 'aoa3f'), 
                'min' => '',
                'max' => '',
                'layouts' => array (
                    aoa3f_layout_form_field(),
                    aoa3f_layout_form_description(),
                ),
            ),
            array (
                'key' => 'field_form_section_id',
                'label' => __('Section id', 'aoa3f'), 
                'name' => 'form_section_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_section_button_previous',
                'label' => __('Previous section button', 'aoa3f'), 
                'name' => 'form_section_button_previous',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_transition',
                            'operator' => '!=',
                            'value' => '',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => __('Previous', 'aoa3f'),
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_section_button_next',
                'label' => __('Next section button', 'aoa3f'), 
                'name' => 'form_section_button_next',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array (
                    array (
                        array (
                            'field' => 'field_form_transition',
                            'operator' => '!=',
                            'value' => '',
                        ),
                    ),
                ),
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => __('Next', 'aoa3f'),
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_form_description() {
    
    $layout = array (
        'key' => 'field_form_description',
        'name' => 'form_description',
        'label' => __('Form description title', 'aoa3f'), // 'Section',
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_form_description_text',
                'label' => __('Description', 'aoa3f'), // 'Description',
                'name' => 'form_description_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_description_id',
                'label' => __('Field id', 'aoa3f'), 
                'name' => 'form_description_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_description_condition_field_id',
                'label' => __('Condition : field id', 'aoa3f'), 
                'name' => 'form_description_condition_field_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_form_description_condition_operator',
                'label' => __('Condition : operator', 'aoa3f'), 
                'name' => 'form_description_condition_operator',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    'equals'            => __('=', 'aoa3f'),
                    'greater'           => __('>', 'aoa3f'),
                    'greater_equals'    => __('>=', 'aoa3f'),
                    'lower'             => __('<', 'aoa3f'),
                    'lower_equals'      => __('<=', 'aoa3f'),
                ),
                'default_value' => 'text',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_form_description_condition_value',
                'label' => __('Condition : value', 'aoa3f'), 
                'name' => 'form_description_condition_value',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}




function aoa3f_layout_button() {

     $layout = array (
        'key' => 'field_button',
        'name' => 'button',
        'label' => __('Button', 'aoa3f'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_button_label',
                'label' => __('Button label', 'aoa3f'), 
                'name' => 'button_label',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => __('See', 'aoa3f'),
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_button_url',
                'label' => __('Button link', 'aoa3f'), 
                'name' => 'button_url',
                'type' => 'url',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_2columns() {
    
    $layout = array (
        'key' => 'field_2columns',
        'name' => '2columns',
        'label' => __('2 Colonnes', 'aoa3f'), 
        'display' => 'table',
        'sub_fields' => array (
            array (
                'key' => 'field_form_2columns_layout',
                'label' => __('Layout', 'aoa3f'), 
                'name' => 'form_2columns_layout',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    '1'     => __('| 1 | 11 |', 'aoa3f'),
                    '2'     => __('| 2 | 10 |', 'aoa3f'),
                    '3'     => __('| 3 | 9 |', 'aoa3f'),
                    '4'     => __('| 4 | 8 |', 'aoa3f'),
                    '5'     => __('| 5 | 7 |', 'aoa3f'),
                    '6'     => __('| 6 | 6 |', 'aoa3f'),
                    '7'     => __('| 7 | 5 |', 'aoa3f'),
                    '8'     => __('| 8 | 4 |', 'aoa3f'),
                    '9'     => __('| 9 | 3 |', 'aoa3f'),
                    '10'    => __('| 10 | 2 |', 'aoa3f'),
                    '11'    => __('| 11 | 1 |', 'aoa3f'),
                ),
                'default_value' => '6',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_2columns_1',
                'label' => __('Column 1', 'aoa3f'), 
                'name' => '2columns_1',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '45%',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add an element', 'aoa3f'), 
                'min' => '',
                'max' => '',
                'layouts' => array (
                    aoa3f_layout_section(),
                    aoa3f_layout_subsection(),
                    aoa3f_layout_content(),
                    aoa3f_layout_images(),
                    aoa3f_layout_feature(),
                    aoa3f_layout_features_table(),
                    aoa3f_layout_prices(),
                    aoa3f_layout_faq(),
                    //aoa3f_layout_booking_button(),
                ),
            ),
            array (
                'key' => 'field_2columns_2',
                'label' => __('Column 2', 'aoa3f'),
                'name' => '2columns_2',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '45%',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add an element', 'aoa3f'), 
                'min' => '',
                'max' => '',
                'layouts' => array (
                    aoa3f_layout_section(),
                    aoa3f_layout_subsection(),
                    aoa3f_layout_content(),
                    aoa3f_layout_images(),
                    aoa3f_layout_feature(),
                    aoa3f_layout_features_table(),
                    aoa3f_layout_faq(),
                    //aoa3f_layout_booking_button(),
                ),
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_3columns() {
    
    $layout = array (
        'key' => 'field_3columns',
        'name' => '3columns',
        'label' => __('3 Colonnes', 'aoa3f'), 
        'display' => 'block
        ',
        'sub_fields' => array (
            array (
                'key' => 'field_form_3columns_layout',
                'label' => __('Layout', 'aoa3f'), 
                'name' => 'form_3columns_layout',
                'type' => 'select',
                'column_width' => '',
                'choices' => array (
                    '4-4-4'     => __('| 4 | 4 | 4 |', 'aoa3f'),
                    '3-3-6'     => __('| 3 | 3 | 6 |', 'aoa3f'),
                    '3-6-3'     => __('| 3 | 6 | 3 |', 'aoa3f'),
                    '6-3-3'     => __('| 6 | 3 | 3 |', 'aoa3f'),
                    '2-2-8'     => __('| 2 | 2 | 8 |', 'aoa3f'),
                    '2-8-2'     => __('| 2 | 8 | 2 |', 'aoa3f'),
                    '8-2-2'     => __('| 8 | 2 | 2 |', 'aoa3f'),
                ),
                'default_value' => '4-4-4',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_3columns_1',
                'label' => __('Column 1', 'aoa3f'), 
                'name' => '3columns_1',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                'min' => '',
                'max' => '',
                'layouts' => array (
                    aoa3f_layout_section(),
                    aoa3f_layout_subsection(),
                    aoa3f_layout_content(),
                    aoa3f_layout_images(),
                    aoa3f_layout_feature(),
                    aoa3f_layout_features_table(),
                    aoa3f_layout_prices(),
                    aoa3f_layout_faq(),
                    //aoa3f_layout_booking_button(),
                ),
            ),
            array (
                'key' => 'field_3columns_2',
                'label' => __('Column 2', 'aoa3f'), 
                'name' => '3columns_2',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                'min' => '',
                'max' => '',
                'layouts' => array (
                    aoa3f_layout_section(),
                    aoa3f_layout_subsection(),
                    aoa3f_layout_content(),
                    aoa3f_layout_images(),
                    aoa3f_layout_feature(),
                    aoa3f_layout_features_table(),
                    aoa3f_layout_prices(),
                    aoa3f_layout_faq(),
                    //aoa3f_layout_booking_button(),
                ),
            ),
            array (
                'key' => 'field_3columns_3',
                'label' => __('Column 3', 'aoa3f'), 
                'name' => '3columns_3',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'button_label' => __('Add an element', 'aoa3f'), // 'Ajouter un élément',
                'min' => '',
                'max' => '',
                'layouts' => array (
                    aoa3f_layout_section(),
                    aoa3f_layout_subsection(),
                    aoa3f_layout_content(),
                    aoa3f_layout_images(),
                    aoa3f_layout_feature(),
                    aoa3f_layout_features_table(),
                    aoa3f_layout_prices(),
                    aoa3f_layout_faq(),
                    //aoa3f_layout_booking_button(),
                ),
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}




function aoa3f_layout_feature() {
    
    global $icons;
    
    $layout = array (
        'key' => 'field_feature',
        'name' => 'feature',
        'label' => __('Feature', 'aoa3f'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_feature_icon',
                'label' => __('Icone', 'aoa3f'), 
                'name' => 'feature_icon',
                'type' => 'select',
                'column_width' => '',
                'instructions' => '<a href="' . get_template_directory_uri() . '/vendor/flaticon/font/flaticon.html" target="_blank">' . __('View samples', 'aoa3f') . '</a>',
                'choices' => $icons,
                'default_value' => '',
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_feature_text',
                'label' => __('Text', 'aoa3f'),
                'name' => 'feature_text',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '3',
                'new_lines' => 'br',
                'readonly' => 0,
                'disabled' => 0,
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_booking_button() {

     $layout = array (
        'key' => 'field_booking_button',
        'name' => 'booking_button',
        'label' => __('Booking button (only for rooms !!)', 'aoa3f'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_booking_button_text',
                'label' => '', 
                'name' => 'booking_button_text',
                'type' => 'message',
                'instructions' => __('A button will be displayed here with the label and the link specified in parameters on the right (only for rooms !!)', 'aoa3f'),
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'esc_html' => '',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}



function aoa3f_layout_faq() {
    
    $layout = array (
        'key' => 'field_faq',
        'name' => 'faq',
        'label' => __('Question & answer', 'aoa3f'), 
        'display' => 'block',
        'sub_fields' => array (
            array (
                'key' => 'field_faq_question',
                'label' => __('Question', 'aoa3f'), 
                'name' => 'faq_question',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '3',
                'new_lines' => 'br',
                'readonly' => 0,
                'disabled' => 0,
            ),
            array (
                'key' => 'field_faq_answer',
                'label' => __('Answer', 'aoa3f'),
                'name' => 'faq_answer',
                'type' => 'wysiwyg',
                'conditional_logic' => 0,
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
            ),
        ),
        'min' => '',
        'max' => '',
    );
    
    return $layout;
}





/* End of file aoa3f-custom-fields.php */
/* Location: ./wp-content/themes/aoa3f/inc/aoa3f-custom-fields.php */