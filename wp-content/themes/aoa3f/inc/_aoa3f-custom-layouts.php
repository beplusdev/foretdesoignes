<?php
/** aoa3f-custom-layouts.php
 *
 * AO Custom layouts added with ACF
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */

defined('ABSPATH') or die("No script kiddies please!"); 

load_theme_textdomain( 'aoa3f', get_template_directory() . '/lang' );



function aoa3f_display_layout($layout) {
    
    $html = '';
    
    switch ( $layout ) :

        case 'mainsection' :
            $html .= aoa3f_display_mainsection();
            break;

        case 'section' :
            $html .= aoa3f_display_section();
            break;

        case 'subsection' :
            $html .= aoa3f_display_subsection();
            break;

        case 'content' :
            $html .= aoa3f_display_content();
            break;

        case 'images' :
            $html .= aoa3f_display_images();
            break;

        case 'prices' :
            $html .= aoa3f_display_prices();
            break;

        case 'equipment_table' :
            $html .= aoa3f_display_equipment_table();
            break;

        case 'features_table' :
            $html .= aoa3f_display_features_table();
            break;

        case 'schedule_table' :
            $html .= aoa3f_display_schedule_table();
            break;

        case 'club_trainer' :
            $html .= aoa3f_display_club_trainer();
            break;

        case 'contact' :
            $html .= aoa3f_display_contact();
            break;

        case 'address' :
            $html .= aoa3f_display_address();
            break;

        case 'phone' :
            $html .= aoa3f_display_phone();
            break;

        case 'mobile' :
            $html .= aoa3f_display_mobile();
            break;

        case 'email' :
            $html .= aoa3f_display_email();
            break;

        case 'web' :
            $html .= aoa3f_display_web();
            break;

        case 'facebook' :
            $html .= aoa3f_display_facebook();
            break;

        case 'description' :
            $html .= aoa3f_display_description();
            break;

        case 'form' :
            $html .= aoa3f_display_form();
            break;

        case 'button' :
            $html .= aoa3f_display_button();
            break;

        case '2columns' :
            $html .= aoa3f_display_2columns();
            break;

        case '3columns' :
            $html .= aoa3f_display_3columns();
            break;

        case 'feature' :
            $html .= aoa3f_display_feature();
            break;

        case 'booking_button' :
            $html .= aoa3f_display_booking_button();
            break;

        case 'faq' :
            $html .= aoa3f_display_faq();
            break;

    endswitch;
    
    return $html;
}



/***************************************************************************************
 	MAINSECTION
 ***************************************************************************************/


function aoa3f_display_mainsection() { 

    return '<h1 class="mainsection">'.get_sub_field('mainsection_title').'</h1>';
}



/***************************************************************************************
 	SECTION
 ***************************************************************************************/


function aoa3f_display_section() { 

    return '<h2 class="section">'.get_sub_field('section_title').'</h2>';
}



/***************************************************************************************
 	SUBSECTION
 ***************************************************************************************/


function aoa3f_display_subsection() { 

    return '<h3 class="subsection">'.get_sub_field('subsection_title').'</h3>';
}



/***************************************************************************************
 	CONTENT
 ***************************************************************************************/


function aoa3f_display_content() { 

    return '<div class="content">'.get_sub_field('content_text').'</div>';
}



/***************************************************************************************
 	IMAGES
 ***************************************************************************************/


function aoa3f_display_images() { 
    
    $html = '';
    
    // check if the nested repeater field has rows of data
    if ( have_rows( 'images_repeater' ) ) :
    
        $images = array();
        $caption = array();

        // loop through the rows of data
        while ( have_rows('images_repeater') ) : the_row();

            $images[] = get_sub_field('images_repeater_image');
            $captions[] = get_sub_field('images_repeater_caption');

        endwhile;
    
        //var_dump($images);
        //var_dump($captions);
    
        switch ( get_sub_field('images_options') ) :

            case 'gallery' :
                $html .= aoa3f_display_gallery($images, $captions, get_sub_field('images_columns'));
                break;

            case 'carousel' :
                $html .= aoa3f_display_carousel($images, $captions);
                break;

        endswitch;
    
    endif;
    
    return $html;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function aoa3f_display_carousel($medias, $descriptions) {

    $html = '';
    
    if ( $n = count( $medias ) ) :
    
        // We generate a random id to be able to have more than one carousel on the same page
        $id = 'carousel-'.rand(0, 999999);

        $html .= '<div id="'.$id.'" class="carousel slide" data-ride="carousel" data-interval="false">';
            
            if ($n > 1) : 
                $html .= '<ol class="carousel-indicators">';
                $first = true;
                foreach ($medias as $i => $media) :
                    $active = '';
                    if ($first) { 
                        $active = 'class="active"'; 
                        $first = false; 
                    }
                    $html .= '<li data-target="#'.$id.'" data-slide-to="'.$i.'" '.$active.'></li>';
                endforeach;              
                $html .= '</ol>';
            endif; 
            
            $html .= '<div class="carousel-inner" role="listbox">';
                $first = true; 
                foreach ($medias as $i => $media) :
                    $active = '';
                    if ($first) { 
                        $active = 'active'; 
                        $first = false; 
                    }
                    $html .= '<div class="item '.$active.'" id="carousel-item-'.$i.'">';
                        $html .= '<a href="'.$media['sizes']['large'].'" rel="lightbox[gallery-]" title="'.$descriptions[$i].'">';
                            $image_width = (is_mobile()) ? 'one-third' : 'two-third';
                            $html .= '<div class="image" style="background-image: url('.$media['sizes'][$image_width].');"></div>';
                            if ($descriptions[$i]) : 
                                $html .= '<div class="carousel-caption">'.$descriptions[$i].'</div>';
                            endif;
                        $html .= '</a>';
                    $html .= '</div>';
                endforeach;
                
            $html .= '</div>';
            
            if ($n > 1) :
                $html .= '
                <a class="left carousel-control" href="#'.$id.'" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">'.__('Previous', 'aoa3f').'</span>
                </a>
                <a class="right carousel-control" href="#'.$id.'" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">'.__('Next', 'aoa3f').'</span>
                </a>';
            endif;
    
        $html .= '</div>';

    endif;
    
    return $html;
}



/**
 * Prints HTML slider with images or videos in the header or the footer of an article
 *
 * @since 	1.0.0
 * @param string $position. Default 'header'.
 * @return string The HTML-formatted slider..
 */
function aoa3f_display_gallery($medias, $descriptions, $nbcolumns) {

    $html .= '<div class="row gallery">';

        $n = count($medias);
        foreach ($medias as $i => $media) : 
            if (!$nbcolumns) :
                if ($n >= 4) :
                    $html .= '<div class="col col-lg-3 col-md-3 col-sm-3 col-xs-4">';
                    $image_width = 'one-third';
                elseif ($n >= 3) :
                    $html .= '<div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">';
                    $image_width = 'one-third';
                elseif ($n >= 2) :
                    $html .= '<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">';
                    $image_width = (is_mobile()) ? 'one-third' : 'two-third';
                else :
                    echo '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
                    $image_width = (is_mobile()) ? 'one-third' : 'two-third';
                endif;
            else :
                switch ($nbcolumns) :
                case '1' :
                    $html .= '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">';
                    $image_width = (is_mobile()) ? 'one-third' : 'two-third';
                    break;
                case '2' :
                    $html .= '<div class="col col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                    $image_width = (is_mobile()) ? 'one-third' : 'two-third';
                    break;
                case '3' :
                    $html .= '<div class="col col-lg-4 col-md-4 col-sm-4 col-xs-12">';
                    $image_width = 'one-third';
                    break;
                case '4' :
                    $html .= '<div class="col col-lg-3 col-md-3 col-sm-3 col-xs-12">';
                    $image_width = 'one-third';
                    break;
                case '6' :
                    $html .= '<div class="col col-lg-2 col-md-2 col-sm-2 col-xs-4">';
                    $image_width = 'thumbnail';
                    break;
                case '12' :
                    $html .= '<div class="col col-lg-1 col-md-1 col-sm-2 col-xs-3">';
                    $image_width = 'thumbnail';
                    break;
                endswitch;
            endif;

            $html .= '
                <div class="item">
                    <a href="'.$media['sizes']['large'].'" rel="lightbox[gallery-medias]" title="'.$descriptions[$i].'">
                        <img src="'.$media['sizes'][$image_width].'" alt="'.$descriptions[$i].'">
                    </a>';
                    if ($descriptions[$i]) :
                        $html .= '<div class="description">'.$descriptions[$i].'</div>';
                    endif;
                $html .= '</div>';
            $html .= '</div>';
        endforeach;
    $html .= '</div>';

    return $html;
}



/***************************************************************************************
 	EVENT
 ***************************************************************************************/


function aoa3f_display_layout_event() {
    
    $html = '';
    
	if (get_field('event_start') || get_field('event_location')) : 

        $html .= '<div class="event-details">';

            if ($start = get_field('event_start')) : 
    
                $html .= '<p>';
    
                    if ($end = get_field('event_end')) :
                        $html .= '<i class="fa fa-calendar"></i> ' . __( 'From', 'aoa3f' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aoa3f' ) . date_i18n( ' G:i ' , $start ) . __( 'to', 'aoa3f' ) . date_i18n( ' l d/m ' , $end ) . __( 'at', 'aoa3f' ) . date_i18n( ' G:i' , $end );
                    else :
                        $html .= '<i class="fa fa-calendar"></i> ' . __( 'On', 'aoa3f' ) . date_i18n( ' l d/m ' , $start ) .  __( 'at', 'aoa3f' ) . date_i18n( ' G:i' , $start );
                    endif;	
                
                $html .= '</p>';
    
            endif; 
            
            if ( have_rows( 'event_location_flexible' ) ):
    
                while ( have_rows( 'event_location_flexible' ) ) : the_row();

                    switch ( get_row_layout() ) :
    
                        case 'event_location_infrastructure' :
                            $html .= aoa3f_display_event_location_infrastructure();
                            break;
    
                        case 'event_location_address' :
                            $html .= aoa3f_display_event_location_address();
                            break;
    
                        case 'event_location_map' :
                            $html .= aoa3f_display_event_location_map();
                            break;
    
                    endswitch;
    
                endwhile;
    
            endif;
            
        $html .= '</div>';

    endif;
    
    return $html;
}

  

function aoa3f_display_event_location_infrastructure() {
    
    $html = '';
    
    $locations = array();
    
    while (has_sub_field('event_location_infrastructure_repeater')) :
    
        $location = get_sub_field('event_location_infrastructure_repeater_object');
        $locations[] = '<a href="'.$location->guid.'">'.$location->post_title.'</a>';
   
    endwhile;
    
    $locations_list = implode(', ', $locations); 
    
    if ( $locations_list ) {
        
        //echo '<h3>'.__( 'Where ?', 'aoa3f' ).'</h3>';
        $html .= '<p><i class="fa fa-map-marker"></i> ' . $locations_list . '</p>';
        
    }    

    return $html;
}



function aoa3f_display_event_location_address() {
    
    $html = '';
    
    if ($location = get_sub_field('event_location_address_text')) :
    
        $html .= '<address><i class="fa fa-map-marker"></i> <a href="http://maps.google.com?q='.strip_tags($location).'" target="_blank">'.$location.'</a></address>';

    endif;
    
    return $html;
}



function aoa3f_display_event_location_map() {
    
    $html = '';
    
    if ( $location = get_sub_field('event_location_map_google') ) :
    
        $html .= '
        <p><i class="fa fa-map-marker"></i>'.$location['address'].'</p>
        <div class="map">
            <div id="gmap" class="gmap"></div> 
            <script type="text/javascript"> 
                jQuery(document).ready(function() {
                    var myLatlng = new google.maps.LatLng('.$location['coordinates'].');
                    var image = "'.get_template_directory_uri().'/images/basket-marker.png";
                    var myOptions = {
                        zoom: 14,
                        center: myLatlng,
                    };
                    var map = new google.maps.Map(document.getElementById("gmap"), myOptions);
                    var contentString = \'<div id="content">\'+
                          \'<div id="siteNotice">\'+
                          \'</div>\'+
                          \'<h4 id="firstHeading" class="firstHeading">'.get_the_title().'</h4>\'+
                          \'<div id="bodyContent">\'+
                          \'<p>'.$location['address'].'</p>\'+
                          \'</div>\'+
                          \'</div>\';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        icon: image,
                        map: map,
                        title: "'.get_the_title().'"
                    });
                    google.maps.event.addListener(marker, \'click\', function() {
                        infowindow.open(map,marker);
                    });
                });
            </script> 
        </div>';
    
    endif; 
    
    return $html;
}



/***************************************************************************************
 	PRICES
 ***************************************************************************************/


function aoa3f_display_prices() { 

    $html = '';
    
    $id = get_sub_field('prices_object');
    
    //setup_postdata(get_post($id));

    $price1_label = get_field( 'price1_label', $id ) ? get_field( 'price1_label', $id ) : get_option( 'price1_label' );
    $price1_caption = get_field( 'price1_caption', $id ) ? get_field( 'price1_caption', $id ) : get_option( 'price1_caption' );
    $price2_label = get_field( 'price2_label', $id ) ? get_field( 'price2_label', $id ) : get_option( 'price2_label' );
    $price2_caption = get_field( 'price2_caption', $id ) ? get_field( 'price2_caption', $id ) : get_option( 'price2_caption' );
    
    if( have_rows('price_repeater', $id) ):

        while ( have_rows('price_repeater', $id) ) : the_row();

            $start = get_sub_field('price_publish_start_date');
            $end = get_sub_field('price_publish_end_date');
            $today = date('Ymd');
            $firstTab = true;
            
            if ( ( !$start or ( $today >= $start ) ) and ( !$end or ( $today <= $end ) ) ) :

                $html .= '<h2>'.get_sub_field( 'price_title' ).'</h2>';
    
                if ( have_rows( 'price_flexible', $id ) ):
    
                    $temp = '';
                    $i = 0;
    
                    while ( have_rows( 'price_flexible', $id ) ) : the_row();

                        switch ( get_row_layout() ) :
    
                            case 'mainsection' :
                                $temp .= aoa3f_display_mainsection();
                                break;

                            case 'section' :
                                $temp .= aoa3f_display_section();
                                break;

                            case 'subsection' :
                                $temp .= aoa3f_display_subsection();
                                break;

                            case 'content' :
                                $temp .= aoa3f_display_content();
                                break;

                            case 'images' :
                                $temp .= aoa3f_display_images();
                                break;

                            case 'prices_table' :
                                $temp .= aoa3f_display_prices_table($price1_label, $price1_caption, $price2_label, $price2_caption);
                                break;

                            case 'equipment_table' :
                                $temp .= aoa3f_display_equipment_table();
                                break;
    
                            case 'tab' :
                                if ($firstTab) :
                                    $t = 'tabs-'.rand(0, 999999);
                                    $html .= $temp;
                                    $tabContent = array();
                                    $i = 0;
                                else :
                                    $tabContent[$i] = $temp;
                                    $i += 1;
                                endif;
                                $html .= aoa3f_display_tab($firstTab, $t.'-'.$i);
                                $firstTab = false;
                                $temp = '';
                                break;

                            case 'tabs_end' :
                                $tabContent[$i] = $temp;
                                $html .= aoa3f_display_tabs_end($tabContent, $t);
                                $firstTab = true;
                                $temp = '';
                                break;

                        endswitch;

                    endwhile;

                endif;
    
            endif;
    
        endwhile;

        if (!$firstTab) :
            $tabContent[$i] = $temp;
            $html .= aoa3f_display_tabs_end($tabContent, $t);
        endif;

        $html .= $temp;
    
    endif;
    
    return $html;
    //wp_reset_postdata();
}



function aoa3f_display_prices_table($label1, $caption1, $label2, $caption2) { 

    $html = '';
    
    if ( $nbrows = count( get_sub_field( 'prices_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        $html .= '
        <table class="table table-striped prices-table">
            <thead>
                <tr>
                    <th class="description">'.get_sub_field('prices_teable_description_label').'</th>
                    <th class="price price1" data-toggle="tooltip" data-placement="top" title="'.$caption1.'">'.$label1.'</th>
                    <th class="price price2" data-toggle="tooltip" data-placement="top" title="'.$caption2.'">'.$label2.'</th>
                </tr>
            </thead> ';
    
        if ($label1 and $caption1 and $label2 and $caption2) :
            $html .= '
            <tfoot>
                <tr>
                    <td colspan="3"><strong>'.$label1.'</strong>: '.$caption1.'<br><strong>'.$label2.'</strong>: '.$caption2.'</td>
                </tr>
            </tfoot>
            <tbody>';
        endif;
                
                while ( has_sub_field( 'prices_table_repeater' ) ) :

                    $html .= '
                    <tr>
                        <td class="description">'.get_sub_field('prices_table_repeater_description').'</td>
                        <td class="price price1">'.get_sub_field('prices_table_repeater_price1').'</td>
                        <td class="price price2">'.get_sub_field('prices_table_repeater_price2').'</td>
                    </tr>';

                endwhile;

            $html .= '
            </tbody>
        </table>';
    

    endif;
    
    return $html;
}



/***************************************************************************************
 	EQUIPMENT
 ***************************************************************************************/


function aoa3f_display_equipment() { 
}


function aoa3f_display_equipment_table($label1, $label2) { 
    
    $html = '';
    
    if ( $nbrows = count( get_sub_field( 'equipment_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        $html .= '
        <table class="table table-striped equipment-table">
            <thead>
                <tr>
                    <th class="equipment">'.((get_sub_field('equipment_table_element_label')) ? get_sub_field('equipment_table_element_label') : __('Equipment', 'aoa3f')).'</th>
                    <th class="price">'.__('Price', 'aoa3f').'</th>
                </tr>
            </thead>
            <tbody>';
                
                while ( has_sub_field( 'equipment_table_repeater' ) ) :

                    $html .= '
                    <tr>    
                        <td class="equipment">'.get_sub_field('equipment_table_repeater_element').'</td>
                        <td class="price">'.get_sub_field('equipment_table_repeater_price').'</td>
                    </tr>';

                endwhile;

            $html .= '
            </tbody>
        </table>';

    endif;
    
    return $html;
}



/***************************************************************************************
 	FEATURES
 ***************************************************************************************/


function aoa3f_display_features_table() { 
   
    $html = '';
    
    if ( $nbrows = count( get_sub_field( 'features_table_repeater' ) ) ) : 
    
        //if ($nbrows == 1) $grid = 12;
        //else $grid = 6;
    
        $html .= '<table class="table table-striped features-table"><tbody>';
                
        while ( has_sub_field( 'features_table_repeater' ) ) :

            $html .= '
                    <tr>    
                        <td class="feature">'.get_sub_field('features_table_repeater_element').'</td>
                        <td class="value">'.get_sub_field('features_table_repeater_value').'</td>
                    </tr>';

        endwhile;
    
        $html .= '</tbody></table>';

    endif;
    
    return $html;
}



/***************************************************************************************
 	SCHEDULE
 ***************************************************************************************/


function aoa3f_display_schedule_table() { 
    
    if ( $nbrows = count( get_sub_field( 'schedule_table_repeater' ) ) ) : 

    $html = '
        <div class="table-responsive effect schedule-table">
            <table class="table table--bordered table-striped">
                <thead>
                    <tr>
                        <th>'.__('Day', 'aoa3f').'</th>
                        <th>'.__('Time', 'aoa3f').'</th>
                        <th>'.__('Location', 'aoa3f').'</th>
                        <th>'.__('Type', 'aoa3f').'</th>
                        <th>'.__('Level', 'aoa3f').'</th>
                        <th>'.__('Age', 'aoa3f').'</th>
                    </tr>
                </thead>
                <tbody>';
    
                while (has_sub_field('schedule_table_repeater')) :
    
                    $html .= '
                    <tr>
                        <td class="day nowrap">'.get_sub_field('schedule_table_repeater_day').'</td>
                        <td class="time nowrap">'.get_sub_field('schedule_table_repeater_time').'</td>
                        <td class="court">';
                            if ($court = get_sub_field('schedule_table_repeater_infrastructure')) :
                                $html .= '<a href="'.$court->guid.'" title="'.__('See infrastructure', 'aoa3f').'">'.$court->post_title.'</a>';
                            endif;
                        $html .= '
                        </td>
                        <td class="type">'.get_sub_field('schedule_table_repeater_type').'</td>
                        <td class="level">'.get_sub_field('schedule_table_repeater_level').'</td>
                        <td class="age">'.get_sub_field('schedule_table_repeater_age').'</td>
                    </tr>';
                    
                endwhile;
                
                $html .= '
                </tbody>
            </table>
        </div>';

    endif;
    
    return $html;
}



/***************************************************************************************
 	TRAINER
 ***************************************************************************************/


function aoa3f_display_club_trainer() { 

    //$id = get_sub_field('contact_object');
    
    //setup_postdata(get_post($id));
    
    $html = '';

    $html .= '<div class="panel panel-default">';
        
        if ( $name = get_sub_field( 'club_trainer_name' ) ) :
        
            $html .= '<div class="panel-heading">'.$name.'</div>';
        
        endif;
        
        $html .= '<div class="panel-body">';
            
            if ( $image = get_sub_field( 'club_trainer_image' ) ) 
                $html .= '<div class="image"><img src="'.$image['sizes']['thumbnail'].'" /></div>';
    
    
            if ( have_rows( 'club_trainer_flexible' ) ):

                $html .= '<div class="details '.(($image) ? 'with-image' : '').'">';
                
                while ( have_rows( 'club_trainer_flexible' ) ) : the_row();

                    switch ( get_row_layout() ) :

                        case 'address' :
                            $html .= aoa3f_display_address();
                            break;

                        case 'phone' :
                            $html .= aoa3f_display_phone();
                            break;

                        case 'mobile' :
                            $html .= aoa3f_display_mobile();
                            break;

                        case 'email' :
                            $html .= aoa3f_display_email();
                            break;

                        case 'web' :
                            $html .= aoa3f_display_web();
                            break;

                        case 'motto' :
                            $html .= aoa3f_display_motto();
                            break;

                        case 'images' :
                            $html .= aoa3f_display_images();
                            break;

                        case 'facebook' :
                            $html .= aoa3f_display_facebook();
                            break;

                        case 'description' :
                            $html .= aoa3f_display_description();
                            break;

                    endswitch;

                endwhile;
    
                $html .= '</div>'; 

            endif;
    
        $html .= '</div>';
    
    $html .= '</div>';
 
    return $html;
    
    //wp_reset_postdata();
}



/***************************************************************************************
 	CONTACT
 ***************************************************************************************/


function aoa3f_display_contact() { 

    $id = get_sub_field('contact_object');
    
    //setup_postdata(get_post($id));

    $html = '';

    $html .= '<div class="panel panel-default">';
        
        if ( $name = get_field( 'contact_name', $id ) ) :
        
            $html .= '<div class="panel-heading">'.$name.'</div>';
        
        endif;
                
        $html .= '<div class="panel-body">';
            
            if ( $image = get_field( 'contact_image', $id ) ) 
                $html .= '<div class="image"><img src="'.$image['sizes']['thumbnail'].'" /></div>';
    
    
            if ( have_rows( 'contact_flexible', $id ) ):

                $html .= '<div class="details '.(($image) ? 'with-image' : '').'">';

                while ( have_rows( 'contact_flexible', $id ) ) : the_row();

                    switch ( get_row_layout() ) :

                        case 'address' :
                            $html .= aoa3f_display_address();
                            break;

                        case 'phone' :
                            $html .= aoa3f_display_phone();
                            break;

                        case 'mobile' :
                            $html .= aoa3f_display_mobile();
                            break;

                        case 'email' :
                            $html .= aoa3f_display_email();
                            break;

                        case 'web' :
                            $html .= aoa3f_display_web();
                            break;

                        case 'motto' :
                            $html .= aoa3f_display_motto();
                            break;

                        case 'images' :
                            $html .= aoa3f_display_images();
                            break;

                    endswitch;

                endwhile;
    
                $html .= '</div>';

            endif;
                          
        $html .= '</div>';
    
    $html .= '</div>';
 
    return $html;
    
    //wp_reset_postdata();
}



/***************************************************************************************
 	ADDRESS
 ***************************************************************************************/


function aoa3f_display_address() { 

    return '<address class="address">'.get_sub_field('address_text').'</address>';
}



/***************************************************************************************
 	PHONE
 ***************************************************************************************/


function aoa3f_display_phone() { 

    return '<div class="phone"><span class="fa fa-phone"></span><a href="tel:'.get_sub_field('phone_text').'">'.get_sub_field('phone_text').'</a></div>';
}



/***************************************************************************************
 	MOBILE
 ***************************************************************************************/


function aoa3f_display_mobile() { 

    return '<div class="mobile"><span class="fa fa-mobile"></span><a href="tel:'.get_sub_field('mobile_text').'">'.get_sub_field('mobile_text').'</a></div>';
}



/***************************************************************************************
 	EMAIL
 ***************************************************************************************/


function aoa3f_display_email() { 

    return '<div class="email"><span class="fa fa-envelope"></span><a href="mailto:' . antispambot( get_sub_field('email_text') ) . '">' . antispambot( get_sub_field('email_text') ) . '</a></div>';
}



/***************************************************************************************
 	WEB
 ***************************************************************************************/


function aoa3f_display_web() { 

    return '<div class="web"><span class="fa fa-globe"></span><a href="'.get_sub_field('web_text').'" target="_blank">'.get_sub_field('web_text').'</a></div>';
}



/***************************************************************************************
 	MOTTO
 ***************************************************************************************/


function aoa3f_display_motto() { 

    return '<div class="motto"><p>'.get_sub_field('motto_text').'</p></div>';
}



/***************************************************************************************
 	FACEBOOK
 ***************************************************************************************/


function aoa3f_display_facebook() { 

    return '<div class="facebook"><span class="fa fa-facebook"></span><a href="'.get_sub_field('facebook_text').'" target="_blank">'.get_sub_field('facebook_text').'</a></div>';
}



/***************************************************************************************
 	DESCRIPTION
 ***************************************************************************************/


function aoa3f_display_description() { 

    return '<div class="description"><p>'.get_sub_field('description_text').'</p></div>';
}



/***************************************************************************************
 	TAB
 ***************************************************************************************/


function aoa3f_display_tab($firstTab, $t) { 
    
    $html = '';
    if ($firstTab) :
        $html .= '<ul class="nav nav-tabs" role="tablist">';
        $html .= '<li role="presentation" class="active"><a href="#'.$t.'" aria-controls="'.$t.'" role="tab" data-toggle="tab">'.get_sub_field('tab_title').'</a></li>';
    else :
        $html .= '<li role="presentation" class=""><a href="#'.$t.'" aria-controls="'.$t.'" role="tab" data-toggle="tab">'.get_sub_field('tab_title').'</a></li>';
    endif;

    return $html;
}



/***************************************************************************************
 	TABS END
 ***************************************************************************************/


function aoa3f_display_tabs_end($tabContent, $t) { 
    
    $html = '</ul>';
    $html .= '<div class="tab-content">';
    $active = 'active';
    foreach ($tabContent as $i => $content) :
        $html .= '<div role="tabpanel" class="tab-pane '.$active.'" id="'.$t.'-'.$i.'">'.$content.'</div>';
        $active = '';
    endforeach;
    $html .= '</div>';

    return $html;
}




/***************************************************************************************
 	FORM
 ***************************************************************************************/


function aoa3f_display_form() { 
    
    //var_dump($_POST);
    
    $html = '';
    
    $horizontal_form = get_sub_field('form_horizontal');
    $with_pager = get_sub_field('form_with_pager');
    $transition = get_sub_field('form_transition') && !is_mobile();
    $form_id = get_sub_field('form_id');
    
    $html .= '<form action="' . get_template_directory_uri() . '/inc/mail-it.php" id="'.$form_id.'"  name="'.get_sub_field('form_id').'" class="form '.(($horizontal_form) ? 'form-horizontal' : '').' '.(($transition) ? 'with-transition' : '').'" style="position: relative; '.(($transition) ? 'overflow: hidden;' : '').'">';
    
    $count = count(get_sub_field('form_fields'));
    
    if ( have_rows( 'form_fields' ) ):
    
        $i = 0;
        $fields = '';
        $pager = '<div class="form-pager">';
    
        while ( have_rows( 'form_fields' ) ) : the_row();
    
            $i += 1;
            $first = ($i == 1);
            $last = ($i == $count);
    
            $pager .= '<div class="item item-'.$i.' '.(($first) ? 'active' : '').'"><span class="step" onclick="displaySection(\''.$i.'\', \''.$transition.'\')">'.$i.'</span><span class="title">'.get_sub_field('form_section_title').'</span><sapn class="line"></span></div>';
    
            switch ( get_row_layout() ) :

                /*
                case 'form_field' :
                    $fields .= aoa3f_display_form_field($i, $horizontal_form);
                    break;
                */
                case 'form_section' :
                    $fields .= aoa3f_display_form_section($i, $horizontal_form, $transition, $first, $last, $form_id);
                    break;
                /*
                case 'form_description' :
                    //$temp .= aoa3f_display_subsection($i);
                    break;
                */
    
            endswitch;
    
        endwhile;
    
        $pager .= '</div>';
    
    endif;
        
    if ($transition and $with_pager) $html .= $pager;
    
    $html .= $fields;
    
    if (!$transition) :
        if ($horizontal_form) :
            $html .= '<div class="col-sm-'.$horizontal_form.'"></div>';
            $html .= '<div class="col-sm-'.(12-$horizontal_form).'">';
            $html .= '<button class="btn btn-success" type="button" onclick="submitForm()">'.((get_sub_field('form_submit_button_title')) ? get_sub_field('form_submit_button_title') : __('Submit', 'aoa3f')).'</button>';
            $html .= '</div>';
        else :
            $html .= '<button class="btn btn-success" type="button" onclick="submitForm()">'.((get_sub_field('form_submit_button_title')) ? get_sub_field('form_submit_button_title') : __('Submit', 'aoa3f')).'</button>';
        endif;
    endif;
    
    $html .= '<input type="hidden" id="send-from" value="'.get_sub_field('form_sender').'">';
    $html .= '<input type="hidden" id="send-to" value="'.get_sub_field('form_recipient').'">';
    $html .= '<input type="hidden" id="send-subject" value="'.get_sub_field('form_subject').'">';
    $html .= '<input type="hidden" id="send-success" value="'.get_sub_field('form_success').'">';
    
    $html .= '</form>';
    
    $html .= '<div id="ajaxerror" class="alert alert-danger" role="alert" style="display: none;"></div>';
    $html .= '<div id="ajaxsuccess" class="alert alert-success" role="alert" style="display: none;"></div>';
    
    //if ( $css = get_sub_field('form_css') ) :
    //    $html .= '<script type="text/javascript">' . $js . '</script>';
    //endif;
    
    if ( $js = get_sub_field('form_javascript') ) :
        $html .= '<script type="text/javascript">' . $js . '</script>';
    endif;
    
    return $html;
    //wp_reset_postdata();
}



function aoa3f_form_css() { 

    $flexible_field = get_post_type() . '_flexible';

    // check if the flexible content field has rows of data
    if ( have_rows( $flexible_field ) ):

        // loop through the rows of data
        while ( have_rows( $flexible_field ) ) : the_row();

            if ( (get_row_layout() == 'form') and ($css = get_sub_field('form_css')) ) :
    
                echo '<style type="text/css">';
                echo $css;
                echo '</style>';
    
            endif;

        endwhile;

    endif;

}
add_action( 'wp_head', 'aoa3f_form_css' );



function aoa3f_display_form_section($i, $horizontal_form, $transition, $first, $last, $form_id) { 
    
    $html = '';
    
    $html .= '<div class="form-section panel panel-default" id="section-'.$i.'" style="'.(($transition) ? 'position: absolute; float: left; '.((!$first) ? 'display: none;' : '') : '').'">';
    
    if (get_sub_field('form_section_title')) $html .= '<div class="panel-heading"><h3 class="panel-title">' . __('Step', 'aoa3f') . ' ' . $i . ' - ' . get_sub_field('form_section_title').'</h3></div>';    

    if ( have_rows( 'form_section_fields' ) ):
    
        $html .= '<div class="panel-body">';
    
        while ( have_rows( 'form_section_fields' ) ) : the_row();
    
            switch ( get_row_layout() ) :

                case 'form_field' :
                    $html .= aoa3f_display_form_field($horizontal_form, $form_id);
                    break;

                case 'form_description' :
                    $html .= 'form description<br>';
                    break;

            endswitch;
    
        endwhile;
    
        if ($transition) :
            $html .= '<div class="form-group">';
            $html .= '<label class="control-label '.(($horizontal_form) ? 'col-sm-'.$horizontal_form : '').'" for="'.$id.'"></label>';
            if ($horizontal_form) $html .= '<div class="col-sm-'.(12-$horizontal_form).'">';
            if (!$first) :
                $prev = (get_sub_field('form_section_button_previous')) ? get_sub_field('form_section_button_previous') : __('Next', 'aoa3f');
                $html .= '<button class="btn btn-default" type="button" onclick="displayPreviousSection(\''.$i.'\', \''.$transition.'\')">'.$prev.'</button>';
            endif;
            if ($last) :
                $submit = (get_sub_field('form_section_button_next')) ? get_sub_field('form_section_button_next') : __('Submit form', 'aoa3f');
                $html .= '<button class="btn btn-success" type="button" onclick="submitForm()">'.$submit.'</button>';
            else :
                $next = (get_sub_field('form_section_button_next')) ? get_sub_field('form_section_button_next') : __('Next', 'aoa3f');
                $html .= '<button class="btn btn-primary" type="button" onclick="displayNextSection(\''.$i.'\', \''.$transition.'\')">'.$next.'</button>';
            endif;
            if ($horizontal_form) $html .= '</div>';
            $html .= '</div>';
        endif;
    
        $html .= '</div>';
    
    endif;
        
    $html .= '</div>';
    
    return $html;
}



function aoa3f_display_form_field($horizontal_form, $form_id) { 
    
    $html = '';
    
    $type = get_sub_field('form_field_type'); 
    $label = trim(get_sub_field('form_field_title'));
    $id = trim(get_sub_field('form_field_id'));
    $post_value = ($post_field_id = get_sub_field('form_field_post_field_id')) ? htmlspecialchars($_POST[$post_field_id]) : '';
    $default = ($post_value) ? $post_value : trim(get_sub_field('form_field_default_value'));
    $mandatory = get_sub_field('form_field_mandatory');

    $html .= '<div class="form-group form-group-visible '.(($mandatory) ? 'mandatory' : '').'" id="form-group-'.$id.'">';
    if ($label) $html .= '<label class="control-label '.(($horizontal_form) ? 'col-sm-'.$horizontal_form : '').'" for="'.$id.'">'.$label.'</label>';
    
    if ($horizontal_form) $html .= '<div class="col-sm-'.(12-$horizontal_form).'">';
    
    switch ( $type ) :

        case 'text' :
            $html .= '<input type="text" class="form-control form-text" id="'.$id.'" placeholder="" value="'.$default.'">';
            break;

        case 'email' :
            $html .= '<input type="email" class="form-control form-email" id="'.$id.'" placeholder="" value="'.$default.'">';
            break;

        case 'number' :
            $html .= '<input type="number" min="0" class="form-control form-number" id="'.$id.'" placeholder="" value="'.$default.'">';
            break;

        case 'select' :
            $html .= '<select class="form-control form-select" id="'.$id.'">';
            $options = array_map('trim', explode(';', get_sub_field('form_field_select_values')));
            foreach ($options as $option) :
                $sep = strpos($option, ':');
                if ($sep !== false) :
                    $value = trim(substr($option, 0, $sep));
                    $label = trim(substr($option, $sep+1));
                else :
                    $value = $option;
                    $label = $option;
                endif;
                if ($label) :
                    $selected = ($value == $default) ? 'selected="selected"' : '';
                    $html .= '<option value="'.$value.'" '.$selected.'>'.$label.'</option>';
                endif;
            endforeach;
            $html .= '</select>';
            break;

        case 'datepicker' :
            $html .= '<input type="text" class="form-control form-datepicker" id="'.$id.'" '.((is_mobile()) ? 'readonly="readonly"' : '') .'>';
            if ($datepicker_range_fieldId = get_sub_field('form_field_datepicker_range_field_id')) :
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#<?php echo $form_id; ?> #<?php echo $datepicker_range_fieldId; ?>').on('changeDate', function(selected){
                            var startDate = new Date(selected.date.valueOf());
                            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
                            jQuery('#<?php echo $form_id; ?> #<?php echo $id; ?>').datepicker('setStartDate', startDate);
                            if ((jQuery('#<?php echo $form_id; ?> #<?php echo $datepicker_range_fieldId; ?>').val() != '') && (jQuery('#<?php echo $form_id; ?> #<?php echo $id; ?>').val() != '')) {
                                var fromArray = jQuery('#<?php echo $form_id; ?> #<?php echo $datepicker_range_fieldId; ?>').val().split('/');
                                var from = new Date(fromArray[2], parseInt(fromArray[1], 10) - 1, fromArray[0]);
                                var toArray = jQuery('#<?php echo $form_id; ?> #<?php echo $id; ?>').val().split('/');
                                var to = new Date(toArray[2], parseInt(toArray[1], 10) - 1, toArray[0]);
                                if(from >= to) {
                                    jQuery('#<?php echo $id; ?>').val('');
                                }
                            }
                        });
                    });
                </script>
                <?php
            endif;
            break;

        case 'range' :
            $post_value2 = ($post_field_id2 = get_sub_field('form_field_post_field_id2')) ? htmlspecialchars($_POST[$post_field_id2]) : '';
            $default2 = ($post_value2) ? $post_value2 : trim(get_sub_field('form_field_default_value2'));
            $html .= '<div class="range form-inline" id="'.$id.'">';
            $html .= '<div class="input-group">';
            $html .= '<div class="input-group-addon">' . __('From', 'aoa3f') . '</div>';
            $html .= '<input type="text" class="form-control" id="'.$id.'-from" name="'.$id.'-from" placeholder="" value="'.$default.'" '.((is_mobile()) ? 'readonly="readonly"' : '') .'>';
            //$html .= '<div class="input-group-addon"><span class="flaticon flaticon-calendar"></span></div>';
            $html .= '</div>';
            $html .= '<div class="input-group">';
            $html .= '<div class="input-group-addon">' . __('To', 'aoa3f') . '</div>';
            $html .= '<input type="text" class="form-control" id="'.$id.'-to" name="'.$id.'-to" placeholder="" value="'.$default2.'" '.((is_mobile()) ? 'readonly="readonly"' : '') .'>';
            //$html .= '<div class="input-group-addon"><span class="flaticon flaticon-calendar"></span></div>';
            $html .= '</div>';
            $html .= '</div>';
            ?>
                <script type="text/javascript">
                    jQuery(function() {
                        jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ "<?php echo pll_current_language('slug'); ?>" ] );
                        jQuery.datepicker.formatDate( "dd/mm/yyyy" );
                        jQuery( "#<?php echo $form_id; ?> #<?php echo $id; ?>-from" ).datepicker({
                            minDate: new Date(),
                            defaultDate: "+1w",
                            changeMonth: true,
                            numberOfMonths: 2,
                            firstDay: 1,
                            onClose: function( selectedDate ) {
                                jQuery( "#<?php echo $form_id; ?> #<?php echo $id; ?>-to" ).datepicker( "option", "minDate", selectedDate );
                                jQuery( "#<?php echo $form_id; ?> #<?php echo $id; ?>-to" ).datepicker( "show" );
                            }
                        });
                        jQuery( "#<?php echo $form_id; ?> #<?php echo $id; ?>-to" ).datepicker({
                            defaultDate: "+1w",
                            changeMonth: true,
                            numberOfMonths: 2,
                            firstDay: 1,
                            onClose: function( selectedDate ) {
                                jQuery( "#<?php echo $form_id; ?> #<?php echo $id; ?>-from" ).datepicker( "option", "maxDate", selectedDate );
                            }
                        });
                    });
                </script>
            <?php
            break;

        case 'textarea' :
            $html .= '<textarea class="form-control form-textarea" id="'.$id.'" rows="3">'.$default.'</textarea>';
            break;

    endswitch;
    
    if ($horizontal_form) $html .= '</div>';

    $html .= '</div>';
                        
    $operator = get_sub_field('form_field_condition_operator');

    if ($operator and ($operator != '-')) :
        $fieldId = get_sub_field('form_field_condition_field_id');
        $value = get_sub_field('form_field_condition_value');
        if ($fieldId) :
        ?>
            <script type="text/javascript">
                jQuery(document).ready(function() {
                    display<?php echo ucfirst($id); ?>();
                    jQuery('#<?php echo $form_id; ?> #<?php echo $fieldId; ?>').change(function() {
                        //console.log(jQuery(this).val());
                        display<?php echo ucfirst($id); ?>();
                    });
                });
                                                                          
                function display<?php echo ucfirst($id); ?>() {
                    <?php if (($operator == '==') or ($operator == '!=')) : ?>
                        var str = '<?php echo $value; ?>';
                        var arr = [];
                        jQuery.each(str.split(';'), function() {
                            arr.push(jQuery.trim(this));
                        });
                        //console.log(str);
                        //console.log(arr);
                        //console.log(jQuery('#<?php echo $fieldId; ?>').val());
                        <?php if ($operator == '==')  : ?>
                            if (jQuery.inArray(jQuery('#<?php echo $fieldId; ?>').val(), arr) > -1) { 
                        <?php elseif ($operator == '!=') : ?>
                            if (jQuery.inArray(jQuery('#<?php echo $fieldId; ?>').val(), arr) == -1) { 
                                //console.log('inArray('+jQuery('#<?php echo $fieldId; ?>').val()+', '+arr+')'+((jQuery.inArray(jQuery('#<?php echo $fieldId; ?>').val(), arr)) == -1));
                        <?php endif; ?>
                    <?php else : ?>
                        if (jQuery('#<?php echo $fieldId; ?>').val() <?php echo $operator; ?> '<?php echo $value; ?>') {                        
                    <?php endif; ?>
                        jQuery('#<?php echo $form_id; ?> #form-group-<?php echo $id; ?>').removeClass('form-group-hidden').addClass('form-group-visible').show();
                    }
                    else {
                        jQuery('#<?php echo $form_id; ?> #form-group-<?php echo $id; ?>').removeClass('form-group-visible').addClass('form-group-hidden').hide();
                    }
                }
            </script>
        <?php
        endif;
    endif;
    
    return $html;    
}



function aoa3f_form_transition() { 
	?>
		<script type="text/javascript">
            
            jQuery(document).ready(function() {
                jQuery('input.form-datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    language: "fr",
                    orientation: "bottom auto",
                    startDate: "0d",
                    autoclose: true,
                    todayHighlight: true
                });
            });
            
            function displayNextSection(item, transition) {
                alert(item + ' ' + transition);
                if (checkFields(item)) {
                    var target = jQuery('#section-'+(parseInt(item)+1).toString()),
                        current = jQuery('#section-'+item);
                    switch(transition) {
                        case 'slide':
                            //jQuery('#section-'+current).hide("slide", { direction: "left" }, 1000);
                            //alert('#section-'+(parseInt(current)+1).toString());
                            //jQuery('#section-'+(parseInt(current)+1).toString()).show("slide", { direction: "right" }, 1000);
                            current.animate({
                                left: -(current.width() + parseInt(current.css('margin-right')))
                            }, 500);

                            target.show().css({
                                left: target.width() + parseInt(target.css('border-left-width')) + parseInt(target.css('border-right-width')) + parseInt(current.css('margin-right'))
                            }).animate({
                                left: 0
                            }, 500);
                            break;
                        case 'fade':
                            break;
                    }                
                    current.removeClass('active');
                    target.addClass('active');
                    jQuery('.form-pager .item').removeClass('active');
                    jQuery('.form-pager .item-'+item).addClass('done');
                    jQuery('.form-pager .item-'+(parseInt(item)+1).toString()).addClass('active');
                }
            }
            
            function displayPreviousSection(item, transition) {
                var target = jQuery('#section-'+(parseInt(item)-1).toString()),
                    current = jQuery('#section-'+item);
                switch(transition) {
                    case 'slide':
                        current.animate({
                            left: current.width() + parseInt(current.css('border-left-width')) + parseInt(current.css('border-right-width')) + parseInt(target.css('margin-right'))
                        }, 500);

                        target.show().animate({
                            left: 0
                        }, 500);
                        break;
                    case 'fade':
                        break;
                }
                current.removeClass('active');
                target.addClass('active');
                jQuery('.form-pager .item').removeClass('active');
                jQuery('.form-pager .item-'+(parseInt(item)-1).toString()).removeClass('done').addClass('active');
            }
            
            function displaySection(targetItem, transition) {
                var target = jQuery('#section-'+targetItem),
                    currentItem = jQuery('.form-pager .item.active span.step').html(),
                    current = jQuery('#section-'+currentItem);
                if (parseInt(targetItem) < parseInt(currentItem)) {
                    switch(transition) {
                        case 'slide':
                            current.animate({
                                left: current.width() + parseInt(current.css('border-left-width')) + parseInt(current.css('border-right-width')) + parseInt(target.css('margin-right'))
                            }, 500);
                            target.show().animate({
                                left: 0
                            }, 500);                            
                            break;
                        case 'fade':
                            break;
                    }
                    current.removeClass('active');
                    target.addClass('active');
                    jQuery('.form-pager .item').removeClass('active');
                    jQuery('.form-pager .item').each(function() {
                        if ( parseInt(jQuery(this).find('span.step').html()) >= targetItem ) {
                            jQuery(this).removeClass('done');
                        }
                    });
                    jQuery('.form-pager .item-'+targetItem).addClass('active');
                }
            }
            
            function submitForm(item) {
                if (checkFields(item)) {
                    var body = [];
                    jQuery('form .form-group-visible').each(function(){
                        body += jQuery(this).find('.control-label').html() + ' : ' + jQuery(this).find('.form-control').val() + '<br>';
                    });
                    jQuery.ajax({
                        type: "POST",
                        url: jQuery('form').attr('action'),
                        data: {
                            send_from: jQuery('form #send-from').val(),
                            send_to: jQuery('form #send-to').val(),
                            send_subject: jQuery('form #send-subject').val(),
                            send_body: body,
                        },
                        timeout: 6000,
                        error: function(request,error) {
                            jQuery('#ajaxerror').slideDown('slow');
                            if (error == "timeout") {
                                jQuery("#ajaxerror").html('<?php echo htmlspecialchars(__('Time out error', 'aoa3f'), ENT_QUOTES); ?>');
                            }
                            else {
                                jQuery("#ajaxerror").html('<?php echo htmlspecialchars(__('An error occurred: ', 'aoa3f'), ENT_QUOTES); ?>' + error + '');
                            }
                        },
                        success: function(response) {
                            jQuery('form').slideUp('slow');
                            jQuery('#ajaxsuccess').slideDown('slow');
                            jQuery("#ajaxsuccess").html(jQuery('form #send-success').val()); // + '<br><br>' + response); // + '<br><br>' + body);
                        }
                    });
                    //console.log('from: '+jQuery('form #send-from').val());
                    //console.log('to: '+jQuery('form #send-to').val());
                    //console.log('subject: '+jQuery('form #send-subject').val());
                    //console.log(body);
                }
            }
            
            function checkFields(item) {
                
                console.log('check fields');
                if (item) {
                    var toCheck = jQuery('#section-'+item);
                }
                else {
                    var toCheck = jQuery('form');
                }
                
                // remove alert messages
                toCheck.find('.alert').remove();
                var valid = true;
                
                // Mandatory fields
                toCheck.find('.form-group-visible.mandatory').each(function(){
                    jQuery(this).find('.form-control').each(function() {
                        if (jQuery(this).val().trim() == '') {
                            valid = false;
                            if (jQuery(this).parent().hasClass('input-group')) {
                                jQuery(this).parent().after('<div class="alert alert-danger" role="alert"><?php echo htmlspecialchars(__('This field is mandatory', 'aoa3f'), ENT_QUOTES); ?></div>');                                
                            }
                            else {
                                jQuery(this).after('<div class="alert alert-danger" role="alert"><?php echo htmlspecialchars(__('This field is mandatory', 'aoa3f'), ENT_QUOTES); ?></div>');
                            }
                        }
                    });
                });
                
                // Email
                toCheck.find('.form-group-visible .form-email').each(function(){
                    var value = jQuery(this).val();
                    if (value != '') {
                        if (!validateEmail(value)) {
                            valid = false;
                            jQuery(this).after('<div class="alert alert-danger" role="alert"><?php echo htmlspecialchars(__('This is not a valid email', 'aoa3f'), ENT_QUOTES); ?></div>');                        
                        }
                    }
                });
                
                // Number
                toCheck.find('.form-group-visible .form-number').each(function(){
                    var value = jQuery(this).val();
                    if (value != '') {
                        if (!validateNumber(value)) {
                            valid = false;
                            jQuery(this).after('<div class="alert alert-danger" role="alert"><?php echo htmlspecialchars(__('This is not a valid number', 'aoa3f'), ENT_QUOTES); ?></div>');                        
                        }
                    }
                });
                
                return valid;
            }
            
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                console.log(re.test(email));
                return re.test(email);
            }

            function validateNumber(number) {
                var re = /^\d+$/;
                console.log(re.test(number));
                return re.test(number);
            }
        
        </script>
	<?php
}
add_action('wp_footer', 'aoa3f_form_transition');



/***************************************************************************************
 	BUTTON
 ***************************************************************************************/


function aoa3f_display_button() { 

    return '<a href="'.get_sub_field('button_url').'" class="btn">'.get_sub_field('button_label').'</a>';
}



/***************************************************************************************
 	2 COLUMNS
 ***************************************************************************************/


function aoa3f_display_2columns() { 
    
    $layout = get_sub_field('form_2columns_layout');
    
    $html = '<div class="row"><div class="col-md-'.$layout.' col-sm-12">';
    
    if ( have_rows( '2columns_1' ) ):
    
        while ( have_rows( '2columns_1' ) ) : the_row();
    
            switch ( get_row_layout() ) :

                case 'section' :
                    $html .= aoa3f_display_section();
                    break;

                case 'subsection' :
                    $html .= aoa3f_display_subsection();
                    break;

                case 'content' :
                    $html .= aoa3f_display_content();
                    break;

                case 'images' :
                    $html .= aoa3f_display_images();
                    break;

                case 'feature' :
                    $html .= aoa3f_display_feature();
                    break;
    
                case 'prices' :
                    $html .= aoa3f_display_prices();
                    break;

                case 'equipment_table' :
                    $html .= aoa3f_display_equipment_table();
                    break;

                case 'features_table' :
                    $html .= aoa3f_display_features_table();
                    break;

                case 'booking_button' :
                    $html .= aoa3f_display_booking_button();
                    break;

                case 'faq' :
                    $html .= aoa3f_display_faq();
                    break;

            endswitch;
    
        endwhile;
    
    endif;
        
    $html .= '</div><div class="col-md-'.(12-$layout).' col-sm-12">';
    
    if ( have_rows( '2columns_2' ) ):
    
        while ( have_rows( '2columns_2' ) ) : the_row();
    
            switch ( get_row_layout() ) :

                case 'section' :
                    $html .= aoa3f_display_section();
                    break;

                case 'subsection' :
                    $html .= aoa3f_display_subsection();
                    break;

                case 'content' :
                    $html .= aoa3f_display_content();
                    break;

                case 'images' :
                    $html .= aoa3f_display_images();
                    break;

                case 'feature' :
                    $html .= aoa3f_display_feature();
                    break;
    
                case 'prices' :
                    $html .= aoa3f_display_prices();
                    break;

                case 'equipment_table' :
                    $html .= aoa3f_display_equipment_table();
                    break;

                case 'features_table' :
                    $html .= aoa3f_display_features_table();
                    break;

                case 'booking_button' :
                    $html .= aoa3f_display_booking_button();
                    break;

                case 'faq' :
                    $html .= aoa3f_display_faq();
                    break;

            endswitch;
    
        endwhile;
    
    endif;
        
    $html .= '</div></div>';
    
    return $html;
}



/***************************************************************************************
 	3 COLUMNS
 ***************************************************************************************/


function aoa3f_display_3columns() { 
    
    $layout = get_sub_field('form_3columns_layout');
    $grid = explode('-', $layout);

    $html = '<div class="row"><div class="col-md-'.$grid[0].' col-sm-12">';
    
    if ( have_rows( '3columns_1' ) ):
    
        while ( have_rows( '3columns_1' ) ) : the_row();
    
            switch ( get_row_layout() ) :

                case 'section' :
                    $html .= aoa3f_display_section();
                    break;

                case 'subsection' :
                    $html .= aoa3f_display_subsection();
                    break;

                case 'content' :
                    $html .= aoa3f_display_content();
                    break;

                case 'images' :
                    $html .= aoa3f_display_images();
                    break;

                case 'feature' :
                    $html .= aoa3f_display_feature();
                    break;
    
                case 'prices' :
                    $html .= aoa3f_display_prices();
                    break;

                case 'equipment_table' :
                    $html .= aoa3f_display_equipment_table();
                    break;

                case 'features_table' :
                    $html .= aoa3f_display_features_table();
                    break;

                case 'booking_button' :
                    $html .= aoa3f_display_booking_button();
                    break;

                case 'faq' :
                    $html .= aoa3f_display_faq();
                    break;

            endswitch;
    
        endwhile;
    
    endif;
        
    $html .= '</div><div class="col-md-'.$grid[1].' col-sm-12">';
    
    if ( have_rows( '3columns_2' ) ):
    
        while ( have_rows( '3columns_2' ) ) : the_row();
    
            switch ( get_row_layout() ) :

                case 'section' :
                    $html .= aoa3f_display_section();
                    break;

                case 'subsection' :
                    $html .= aoa3f_display_subsection();
                    break;

                case 'content' :
                    $html .= aoa3f_display_content();
                    break;

                case 'images' :
                    $html .= aoa3f_display_images();
                    break;

                case 'feature' :
                    $html .= aoa3f_display_feature();
                    break;
    
                case 'prices' :
                    $html .= aoa3f_display_prices();
                    break;

                case 'equipment_table' :
                    $html .= aoa3f_display_equipment_table();
                    break;

                case 'features_table' :
                    $html .= aoa3f_display_features_table();
                    break;

                case 'booking_button' :
                    $html .= aoa3f_display_booking_button();
                    break;

                case 'faq' :
                    $html .= aoa3f_display_faq();
                    break;

            endswitch;
    
        endwhile;
    
    endif;
        
    $html .= '</div><div class="col-md-'.$grid[2].' col-sm-12">';
    
    if ( have_rows( '3columns_3' ) ):
    
        while ( have_rows( '3columns_3' ) ) : the_row();
    
            switch ( get_row_layout() ) :

                case 'section' :
                    $html .= aoa3f_display_section();
                    break;

                case 'subsection' :
                    $html .= aoa3f_display_subsection();
                    break;

                case 'content' :
                    $html .= aoa3f_display_content();
                    break;

                case 'images' :
                    $html .= aoa3f_display_images();
                    break;

                case 'feature' :
                    $html .= aoa3f_display_feature();
                    break;
    
                case 'prices' :
                    $html .= aoa3f_display_prices();
                    break;

                case 'equipment_table' :
                    $html .= aoa3f_display_equipment_table();
                    break;

                case 'features_table' :
                    $html .= aoa3f_display_features_table();
                    break;

                case 'booking_button' :
                    $html .= aoa3f_display_booking_button();
                    break;

                case 'faq' :
                    $html .= aoa3f_display_faq();
                    break;

            endswitch;
    
        endwhile;
    
    endif;
        
    $html .= '</div></div>';
    
    return $html;
}



/***************************************************************************************
 	FEATURE
 ***************************************************************************************/


function aoa3f_display_feature() { 

    $html = '<div class="feature">';
    $html .= '<div class="icon"><span class="flaticon flaticon-'.get_sub_field('feature_icon').'"></span></div>';
    $html .= '<div class="text"><p>'.get_sub_field('feature_text').'</p></div>';
    $html .= '</div>';
    
    return $html;
}



/***************************************************************************************
 	BOOKING BUTTON
 ***************************************************************************************/


function aoa3f_display_booking_button() { 

    $button_book_link = get_field('button_book_link');
    return '<a href="javascript:void(0)" class="btn book-now" onclick="centerWindowBooking(\''.$button_book_link.'\',\'465\',\'819\',\'no\',\'no\');" class="imgLnk">'.(($button_book_now = get_field('button_book_now')) ? $button_book_now : __('Book now !', 'aoa3f')).'</a>';
}




/***************************************************************************************
 	FAQ
 ***************************************************************************************/


function aoa3f_display_faq() {
    
    $html = '';
        
    $question = get_sub_field('faq_question');
    $answer = get_sub_field('faq_answer');
    
    $n = rand(1, 999999);
    
    if ($question or $answer) :
    
        $html.= '<div class="faq panel panel-default">';
        $html.= '<div id="heading'.$n.'" class="panel-heading">';
        $html.= '<h4 class="panel-title"><a href="#collapse'.$n.'" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" class="collapsed">';
        $html.= $question;
        $html.= '<span class="flaticon flaticon-add plus"></span><span class="flaticon flaticon-minus-sign-in-a-circle minus"></span></a></h4>';
        $html.= '</div>';
        $html.= '<div id="collapse'.$n.'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">';
        $html.= '<div class="panel-body">'.$answer.'</div>';
        $html.= '</div>';
        $html.= '</div>';
    
    endif;
    
    return $html;
}


/* End of file aoa3f-custom-layouts.php */
/* Location: ./wp-content/themes/aoa3f/inc/aoa3f-custom-layouts.php */