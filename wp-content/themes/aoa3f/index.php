<?php
/** index.php
 *
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <h1 class="entry-title">
                    <?php aoa3f_event_calendar(); ?>
                    <?php the_title(); ?>
                </h1>   
        
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>   
        
                <?php

                // check if the flexible content field has rows of data
                if( have_rows('news_flexible') ):

                     // loop through the rows of data
                    while ( have_rows('news_flexible') ) : the_row();

                        switch (get_row_layout()) :

                            case 'section' :
                                echo '<h2>'.get_sub_field('section_title').'</h2>';
                                break;

                            case 'subsection' :
                                echo '<h3>'.get_sub_field('subsection_title').'</h3>';
                                break;

                            case 'content' :
                                echo '<div class="entry-content">'.get_sub_field('content_text').'</div>';
                                break;

                        endswitch;

                    endwhile;

                else :

                    // no layouts found

                endif;

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <?php
//$post = $wp_query->post;

//echo $post->ID;
    ?>
        
    </div>
    
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    
</div>
    
<?php get_footer(); ?>

<?
/* End of file index.php */
/* Location: ./wp-content/themes/aoa3f/index.php */
