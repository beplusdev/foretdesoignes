<?php
/** singular.php
 *
 * The template for displaying all singular posts.
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <?php if (is_active_sidebar( 'right-content' )) : ?>
    <div id="site-content" class="<?php if (is_front_page()) echo 'home'; ?> col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <?php else : ?>
    <div id="site-content" class="<?php if (is_front_page()) echo 'home'; ?> col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php endif; ?>
              
        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <article>

                    <?php if ($post_type == 'club') : ?>
                        <?php if ($image = get_field('images_thumbnail')) : ?>
                            <div class="entry-logo">
                                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php the_title; ?>" title="<?php the_title; ?>" />
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <h1 class="entry-title">
                        <?php aoa3f_event_calendar(); ?>
                        <?php the_title(); ?>
                    </h1>   

                    <?php if ($post_type == 'news') : ?>
                        <div class="entry-meta">
                            <?php aoa3f_entry_meta_news(); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (( has_term( 'event', 'newscat', get_the_ID() ) ) || (has_term( 'event-en', 'newscat', get_the_ID()))) : ?>
                    <div class="entry-event">
                        <?php echo aoa3f_display_layout_event(); ?>
                    </div>
                    <?php endif; ?>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>   

                    <?php
                    $flexible_field = get_post_type() . '_flexible';

                    // check if the flexible content field has rows of data
                    if ( have_rows( $flexible_field ) ):

                        echo '<div class="flexible">';
                    
                        // loop through the rows of data
                        while ( have_rows( $flexible_field ) ) : the_row();

                            echo aoa3f_display_layout( get_row_layout() );

                        endwhile;
                    
                        echo '</div>';
                    
                    endif;

                    $flexible_bg_field = get_post_type() . '_flexible_bg';

                    // check if the flexible content field has rows of data
                    if ( have_rows( $flexible_bg_field ) ):

                        echo '<div class="flexible-bg">';

                    // loop through the rows of data
                        while ( have_rows( $flexible_bg_field ) ) : the_row();

                            echo aoa3f_display_layout( get_row_layout() );

                        endwhile;
                    
                        echo '</div>';

                    endif;

                    ?>

                </article>

                <?php

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <!-- Social share -->
        <?php if (function_exists('ao_insert_social_share')) : ?>
            <?php $social_share = get_field('social_share'); ?>
            <?php if ($social_share) : ?>
                <?php $url= get_permalink(); ?>
                <?php ao_insert_social_share($url); ?>
            <?php endif; ?>	
        <?php endif; ?>
        <!--/Social share -->

        <?php
        //$post = $wp_query->post;
        //echo $post->ID;
        ?>
        
    </div>
    
    <?php if (is_active_sidebar( 'right-content' )) : ?>
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    <?php endif; ?>
              
</div>
    
<?php get_footer(); ?>

<?
/* End of file singular.php */
/* Location: ./wp-content/themes/aoa3f/singular.php */