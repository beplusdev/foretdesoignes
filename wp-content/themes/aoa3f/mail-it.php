<?php
/** mail-it.php
 *
 * Script to send an email from the contact form
 *
 * @author	Alley Oop
 * @package	AO Clubs
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php
$send_from = cleanupentries($_POST["form1"]);
$send_to = cleanupentries($_POST["form2"]);
$test = cleanupentries($_POST["form3"]);
$send_subject = cleanupentries($_POST["send_body"]);
$send_body = cleanupentries($_POST["send_body"]);
//$from_ip = $_SERVER['REMOTE_ADDR'];
//$from_browser = $_SERVER['HTTP_USER_AGENT'];

function cleanupentries($entry) {
    $entry = trim($entry);
    $entry = stripslashes($entry);
    $entry = htmlspecialchars($entry);

    return $entry;
}

$headers = "From: " . $send_from . "\r\n" .
    "Reply-To: " . $send_from . "\r\n" .
    "X-Mailer: PHP/" . phpversion() . "\r\n" .
    "MIME-Version: 1.0\r\n" .
    "Content-Type: text/html; charset=ISO-8859-1\r\n";

if (!$send_from) {
    echo "no sender email";
    exit;
}else if (!$send_to){
    echo "no recipient email";
    exit;
}else if (!$send_subject){
    echo "no subject";
    exit;
}else if (!$send_body){
    echo "no message";
    exit;
}else{
    if (filter_var($send_from, FILTER_VALIDATE_EMAIL)) {
        mail($send_to, $send_subject, $send_body, $headers);
        echo "ok";
        exit;
    }else{
        echo "mail invalide";
        exit;
    }
}
?>