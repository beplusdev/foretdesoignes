<?php
/** footer.php
 *
 * Displays the footer
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

                        </div>
                        <!--/#site-main -->

                    </div>
                    <!--/#site-main-column -->

                    <?php get_sidebar( 'bottom1' ); ?>

                    <?php get_sidebar( 'bottom2' ); ?>
 
                    <?php get_sidebar( 'bottom3' ); ?>

                </div>
                <!--/#site-center -->

                <div id="site-footer">

                    <?php /*
                    <div class="inner">
                        
                        <div class="footer-contacts">

                            <!-- Contacts -->
                            <?php 
                            $contacts = explode(',', get_option('footer_contacts'));
                            foreach ($contacts as $c) :
                                echo '<div class="contact">';
                                echo '<h2>'.get_field('contact_name', $c).'</h2>';
                                echo '<div>';

                                if ( have_rows( 'contact_flexible', $c ) ):

                                    echo '<div class="details '.(($image) ? 'with-image' : '').'">';

                                    while ( have_rows( 'contact_flexible', $c ) ) : the_row();

                                        switch ( get_row_layout() ) :

                                            case 'address' :
                                                echo aoa3f_display_address();
                                                break;

                                            case 'phone' :
                                                echo aoa3f_display_phone();
                                                break;

                                            case 'mobile' :
                                                echo aoa3f_display_mobile();
                                                break;

                                            case 'email' :
                                                echo aoa3f_display_email();
                                                break;

                                            case 'web' :
                                                echo aoa3f_display_web();
                                                break;

                                            case 'motto' :
                                                echo aoa3f_display_motto();
                                                break;

                                            case 'images' :
                                                echo aoa3f_display_images();
                                                break;

                                        endswitch;

                                    endwhile;

                                    echo '</div>';

                                endif;
                                echo '</div>';
                                echo '</div>';
                            endforeach;
                            ?>
                            <!--/Contacts -->

                        </div>

                    </div>
                    */ ?>
                    
                    <div id="map"></div>
                    <!--/#map -->

                    <div id="footer-menu">
                        <div class="container">
                            <?php wp_nav_menu( array('theme_location' => 'footer-menu') ); ?>
                        </div>
                    </div>
                    <!--/#footer menu -->

                    <?php get_sidebar( 'footer' ); ?>

                    <div id="copyright">
                        <p>Copyright &copy; <?php echo date('Y'); ?> Auberge des 3 fontaines  - <?php _e('All rights reserved', 'aoa3f'); ?>.</p>
                        <?php wp_nav_menu( array('container_class' => 'legal-menu', 'theme_location' => 'legal-menu') ); ?>
                        <div class="designed-by">
                            <a href="http://www.alleyoop.be" target="_blank">	
                                <img src="<?php echo get_template_directory_uri(); ?>/images/alleyoop.png" title="<?php _e('Designed by Alley Oop', 'aoa3f'); ?>" alt="<?php _e('Designed by Alley Oop', 'aoa3f'); ?>" />
                            </a>
                        </div>
                    </div>
                    <!--/#copyright -->	

                </div>
                <!--/#site-footer -->

            </div>
            <!--/.row -->
                         
        </div>
        <!--/#site -->

        <?php wp_footer(); ?>                

    </body>
    
</html>
           
<?php


/* End of file footer.php */
/* Location: ./wp-content/themes/aothemea3f/footer.php */