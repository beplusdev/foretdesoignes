<?php
/** sidebar-top.php
 *
 * Displays the sidebar on the top of the page
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Header -->
<?php if (is_active_sidebar( 'sidebar-top' )) : ?>
	<div id="sidebar-top">
        <?php dynamic_sidebar( 'sidebar-top' ); ?>
	</div>
<?php endif; ?>
<!--/Sidebar Header -->

<?
/* End of file sidebar-top.php */
/* Location: ./wp-content/themes/aoa3f/sidebar-top.php */