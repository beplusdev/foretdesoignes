<?php
/** single-room.php
 *
 * The template for displaying a room.
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<?php get_header(); ?>

<div class="row">

    <?php if (is_active_sidebar( 'right-content' )) : ?>
    <div id="site-content" class="<?php if (is_front_page()) echo 'home'; ?> col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <?php else : ?>
    <div id="site-content" class="<?php if (is_front_page()) echo 'home'; ?> col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php endif; ?>
              
        <?php 
        if ( have_posts() ) : 
        
            // The loop 
            while ( have_posts() ) : the_post();

                ?>
        
                <article>

                    <h1 class="entry-title">
                        <?php the_title(); ?>
                    </h1>   

                    <div class="row room-main">
                    
                        <?php if ( have_rows( 'room_images_repeater' ) ) : ?>
                        
                            <div class="col-md-8 col-sm-12">
                        
                                <?php $first = true; ?>
                                <?php $count = 0; ?>
                                <?php while ( have_rows('room_images_repeater') ) : the_row(); ?>
                                    <?php $image = get_sub_field('room_images_repeater_image'); ?>
                                    <?php $caption = get_sub_field('room_images_repeater_caption'); ?>
                                    <?php if ($image) : ?>

                                        <?php $count += 1 ; ?>
                                        <div class="room-image-main <?php echo ($first or is_mobile()) ? 'show' : ''; ?>" id="room-image-main-<?php echo $count; ?>" style="background-image: url('<?php echo $image['sizes']['large']; ?>');'">			
                                            <div class="filter">
                                                <div class="caption"><?php echo $caption; ?></div>
                                            </div>
                                        </div>

                                        <?php $first = false; ?>
                                    <?php endif; ?>
                                <?php endwhile; ?>

                                <?php if (!is_mobile()) : ?>
                                    <?php 
                                    switch ($count) :
                                        case '1' :
                                        case '2' :
                                        case '3' :
                                        case '4' :
                                            //$grid = 3;
                                            //break;
                                        case '5' :
                                        case '6' :
                                            $grid = 2;
                                            break;
                                        default :
                                            $grid = 1;
                                            break;
                                    endswitch;
                                    ?>

                                    <div class="row gallery">

                                        <?php $count = 0; ?>
                                        <?php while ( have_rows('room_images_repeater') ) : the_row(); ?>
                                            <?php $image = get_sub_field('room_images_repeater_image'); ?>
                                            <?php $caption = get_sub_field('room_images_repeater_caption'); ?>
                                            <?php if ($image) : ?>

                                                <?php $count += 1 ; ?>
                                                <div class="col col-md-<?php echo $grid; ?> col-sm-<?php echo $grid; ?> col-xs-<?php echo $grid; ?> effect">
                                                    <div class="item">
                                                        <img class="room-image-thumbnail" id="room-image-thumbnail-<?php echo $count; ?>" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $caption; ?>" title="<?php echo $caption; ?>" />			
                                                    </div>
                                                </div>

                                            <?php endif; ?> 
                                        <?php endwhile; ?>

                                    </div>
                                <?php endif; ?>

                            </div>
                                
                            <div class="col-md-4 col-sm-12">
                                
                        <?php elseif ($image = get_field('images_thumbnail')) : ?>
                        
                            <div class="col-md-8 col-sm-12">
                                <?php /* <img src="<?php echo $image['sizes']['large']; ?>" /> */ ?>
                                <div class="images_thumbnail" style="background-image: url(<?php echo $image['sizes']['large']; ?>)"></div>
                            </div>
                            <div class="col-md-4 col-sm-12">

                        <?php else : ?>

                            <div class="col-md-12 col-sm-12">
                        
                        <?php endif; ?>

                            
                            <?php 
                            if ($price_from = get_field('room_price_from')) :
                                echo '<div class="price-from"><span class="text">' . __('Price from', 'aoa3f') . '</span> <span class="price">' . $price_from . '</span></div>';
                            endif; 
                            ?>
                            
                            <?php if ($features = get_field('room_features_repeater')) : ?>
                                <?php while (has_sub_field('room_features_repeater')) : ?>
                                    <div class="feature">
                                        <div class="icon" data-toggle="tooltip" data-placement="bottom" title="<?php echo get_sub_field('room_feature_text'); ?>"><span class="flaticon flaticon-<?php echo get_sub_field('room_feature_icon'); ?>"></span></div>
                                        <div class="text"><p><?php echo get_sub_field('room_feature_text'); ?></p></div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>

                            <?php if ($number_guests = get_field('room_number_guests')) : ?>
                                <div class="nb-guests">
                                    <span class="icons"><?php echo str_repeat('<span class="flaticon flaticon-shape"></span>', $number_guests); ?></span>
                                    <span class="text"><?php echo __('Max.', 'aoa3f') . ' ' . $number_guests . ' ' . __('guests', 'aoa3f'); ?></span>
                                </div>
                            <?php endif; ?>
                            
                            <div class="content">
                                <?php the_content(); ?>
                            </div>
                            
                            <?php if ($button_book_now = get_field('room_button')) : ?>
                                <?php if ($button_book_link = get_field('room_button_link')) : ?>
                                    <a href="javascript:void(0)" class="btn book-now" onclick="centerWindowBooking('<?php echo $button_book_link; ?>','465','819','no','no');" class="imgLnk"><?php echo $button_book_now; ?></a>
                                <?php else: ?>
                                    <button class="btn book-now"><?php echo $button_book_now; ?></button>
                                <?php endif; ?>
                            <?php endif; ?>

                            
                        </div>
                    </div>
                    
                    <?php
                    $flexible_field = get_post_type() . '_flexible';

                    // check if the flexible content field has rows of data
                    if ( have_rows( $flexible_field ) ):

                        echo '<div class="flexible">';
                    
                        // loop through the rows of data
                        while ( have_rows( $flexible_field ) ) : the_row();

                            echo aoa3f_display_layout( get_row_layout() );

                        endwhile;
                    
                        echo '</div>';
                    
                    endif;

                    $flexible_bg_field = get_post_type() . '_flexible_bg';

                    // check if the flexible content field has rows of data
                    if ( have_rows( $flexible_bg_field ) ):

                        echo '<div class="flexible-bg">';

                    // loop through the rows of data
                        while ( have_rows( $flexible_bg_field ) ) : the_row();

                            echo aoa3f_display_layout( get_row_layout() );

                        endwhile;
                    
                        echo '</div>';

                    endif;

                    ?>

                </article>

                <?php

            endwhile;
                        
        endif; 
        ?>
    
        <?php get_sidebar( 'bottom-content' ); ?>
        
        <!-- Social share -->
        <?php if (function_exists('ao_insert_social_share')) : ?>
            <?php $social_share = get_field('social_share'); ?>
            <?php if ($social_share) : ?>
                <?php $url= get_permalink(); ?>
                <?php ao_insert_social_share($url); ?>
            <?php endif; ?>	
        <?php endif; ?>
        <!--/Social share -->

        <?php
        //$post = $wp_query->post;
        //echo $post->ID;
        ?>
        
    </div>
    
    <?php if (is_active_sidebar( 'right-content' )) : ?>
    <div id="site-right-content" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        
        <?php get_sidebar( 'right-content' ); ?>
        
    </div>
    <?php endif; ?>
              
</div>
    
<?php get_footer(); ?>

<?
/* End of file single-room.php */
/* Location: ./wp-content/themes/aoa3f/single-room.php */