<?php
/** sidebar-bottom2.php
 *
 * Displays the sidebar at the bottom of the page (full page width)
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Bottom Page -->
<?php if (is_active_sidebar( 'sidebar-bottom2' )) : ?>
    <div id="sidebar-bottom2" class="">
	   <?php dynamic_sidebar( 'sidebar-bottom2' ); ?>
    </div>
<?php endif; ?>
<!--/Sidebar Bottom Page -->

<?
/* End of file sidebar-bottom2.php */
/* Location: ./wp-content/themes/aoa3f/sidebar-bottom2.php */