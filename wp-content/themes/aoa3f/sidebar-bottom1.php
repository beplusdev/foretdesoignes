<?php
/** sidebar-bottom1.php
 *
 * Displays the sidebar at the bottom of the page
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Bottom Page -->
<?php if (is_active_sidebar( 'sidebar-bottom1' )) : ?>
    <div id="sidebar-bottom1">
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar( 'sidebar-bottom1' ); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--/Sidebar Bottom Page -->

<?
/* End of file sidebar-bottom1.php */
/* Location: ./wp-content/themes/aoa3f/sidebar-bottom1.php */