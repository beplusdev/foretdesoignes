<?php
/** sidebar-header.php
 *
 * Displays the sidebar on the header
 *
 * @author	Alley Oop
 * @package	AO Auberge des 3 fontaines
 * @since	1.0.0
 *
 * Copyright (C) 2014 Alley Oop <info@alleyoop.be>
 */
?>

<!-- Sidebar Header -->
<?php if (is_active_sidebar( 'sidebar-header' )) : ?>
	<div id="sidebar-header">
		<?php dynamic_sidebar( 'sidebar-header' ); ?>
	</div>
<?php endif; ?>
<!--/Sidebar Header -->

<?
/* End of file sidebar-header.php */
/* Location: ./wp-content/themes/aoa3f/sidebar-header.php */